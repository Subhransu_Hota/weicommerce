#!/bin/bash
echo "==============================================================="  
echo "|  Stopping Docker instance                                   |"
echo "==============================================================="

docker ps -a
 
echo "Type the year that you want to check (4 digits), followed by [ENTER]:"

read name
 
docker stop $name

echo "==============================================================="
echo "|  Removing all stopped instances                             |"
echo "==============================================================="

docker rm $(docker ps -a -q) 

echo "==============================================================="
echo "|  Building a new Docker instance                             |"
echo "==============================================================="

docker build -t rokittech/recommerce .

echo "==============================================================="
echo "|  Running Docker with a command from Dockerfile              |"
echo "==============================================================="

docker run -d -p 4567:4567 rokittech/recommerce 


echo "==============================================================="
echo "|  List of Running Docker instances                           |"
echo "==============================================================="

docker ps -a

 