package com.rokittech.ecommerce.web.controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.eclipse.jetty.util.IO;
import org.eclipse.jetty.util.MultiMap;
import org.eclipse.jetty.util.MultiPartInputStreamParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CopyObjectRequest;

import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import com.amazonaws.services.s3.model.S3ObjectSummary;


import com.hazelcast.core.HazelcastInstance;
import com.mongodb.util.JSON;
import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.props.GetEnvPropValues;
import com.rokittech.container.ecom.importing.defaults.ImportServiceInput;
import com.rokittech.container.ecom.importing.defaults.ImportServiceOutput;
import com.rokittech.container.ecom.importing.defaults.ImportServiceParams;
import com.rokittech.container.ecom.importing.defaults.ImportVocabulary;
import com.rokittech.container.ecom.importing.factory.ImportServiceFactory;
import com.rokittech.containers.api.core.Service;
import com.rokittech.containers.api.exceptions.HazelcastException;

import spark.Request;
import spark.Response;

public class ImportController {
	
	 private static final Logger slf4jLogger = LoggerFactory.getLogger(ImportController.class);
	 
	 private static String collectionName = "";
	 private final Request request;
	 private final Response response;
	 
	 private final Info info = new Info (); 
	    public ImportController(Request request, Response response) {
	        this.request = request;
	        this.response = response;
	        
	        info.setVersion(1);
	        info.setUserId("test1");
	        info.setAppId("wei");

	    }
	    
	    public String importingCSV(){
	    	
	    	slf4jLogger.info("Importing CSV file into database - ");
	    	
	    	String requestJson = request.body();
	    	collectionName = request.pathInfo();
	    	collectionName = collectionName.substring(1);
	    	collectionName = collectionName.substring(0, collectionName.indexOf("/"));
	    	String temp = collectionName.substring(0, 1).toUpperCase();
	    	collectionName = temp.concat(collectionName.substring(1));
	    	if (!PersistUtils.validateInput(requestJson)) {
	            slf4jLogger.error("invalid request!");
	            return "{\"ERROR\":\": invalid request!\"}";
	        }
	    
	    	 ImportServiceParams params = new ImportServiceParams();
	    	 ImportServiceInput input = new ImportServiceInput().fromJson(requestJson);
	         
	         params.getProperties().put(ImportVocabulary.COLLECTION.name(), collectionName);
	         params.getProperties().put(ImportVocabulary.COMMAND.name(), ImportVocabulary.IMPORT_UPSERT.name());
	         Service<ImportServiceInput, ImportServiceOutput, ImportServiceParams, DefaultPersist> service = new ImportServiceFactory("WEI_ECOMMERCE", 1).get();
	         
	         service.setParams(params);
	         service.setInput(input);
	         service.setOutput(new ImportServiceOutput());
	         
	         String output = service.serve().toJson();
	    	
	         slf4jLogger.info("CSV validation - ");
	         slf4jLogger.debug("Response " + output);
	         return output;
	    }
	    
	public String dumpRestore() {

		slf4jLogger.info("Importing CSV file into database - ");

		String requestJson = request.body();
		if (!PersistUtils.validateInput(requestJson)) {
			slf4jLogger.error("invalid request!");
			return "{\"ERROR\":\": invalid request!\"}";
		}

		String databaseName = "databasename";
		String mongodbRestoreLocation = "mongodbRestoreLocation";
		ImportServiceOutput output1 = new ImportServiceOutput();
		ImportServiceInput input = new ImportServiceInput().fromJson(requestJson);

		try {
			GetEnvPropValues prop = new GetEnvPropValues();
			mongodbRestoreLocation = prop.getPropValues(mongodbRestoreLocation);
			databaseName = prop.getPropValues(databaseName);

			String directory = input.getData().toString();
			directory = directory.substring(directory.indexOf("=") + 1, directory.length() - 1);

			String command = mongodbRestoreLocation + " --db " + databaseName + " " + directory;
			 Process proc  = Runtime.getRuntime().exec(command);
			 int exitVal = proc.waitFor();
			 if(exitVal == 0)
				 output1.setData("Mongodb dump restored succefully");
			 else
				 output1.setData("There was a problem with directory location that you have given");
				 
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		catch (Throwable e) {
			e.printStackTrace();
		}

		String output = output1.toJson();

		slf4jLogger.info("CSV validation - ");
		slf4jLogger.debug("Response " + output);
		return output;
	}
	
	
	public String uploadFiles() {
        final int BUFFER_SIZE = 4096;
        String requestJson = request.body();
        //request.raw().getAttribute("")
        String output = "success";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error(" invalid request..");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        //////start
        try {

            HttpServletRequest srequest = (HttpServletRequest) request.raw();
            if (srequest.getContentType() == null || !srequest.getContentType().startsWith("multipart/form-data")) {

                return null;
            }

            InputStream in = new BufferedInputStream(srequest.getInputStream());
            String content_type = srequest.getContentType();
            System.out.println("Content-type is :" +content_type);

            //Get current parameters so we can merge into them
            MultiMap params = new MultiMap();
            for (Map.Entry<String, String[]> entry : srequest.getParameterMap().entrySet()) {
                Object value = entry.getValue();
                if (value instanceof String[]) {
                    params.addValues(entry.getKey(), (String[]) value);
                } else {
                    params.add(entry.getKey(), value);
                }
            }
            File file1 =new File("test.csv");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+file1.getAbsolutePath());
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"+file1.getName());
            String location = "/opt/recsweb/";  // the directory location where files will be stored
            long maxFileSize = 100000000;  // the maximum size allowed for uploaded files
            long maxRequestSize = 100000000;  // the maximum size allowed for multipart/form-data requests
            int fileSizeThreshold = 1024;  // the size threshold after which files will be written to disk
            MultipartConfigElement multipartConfigElement = new MultipartConfigElement(location, maxFileSize, maxRequestSize, fileSizeThreshold);
            MultiPartInputStreamParser mpis = new MultiPartInputStreamParser(in, content_type, multipartConfigElement, new File(location));
            request.raw().setAttribute("org.eclipse.jetty.servlet.MultiPartFile.multiPartInputStream", mpis);
            
            Collection<Part> parts = mpis.getParts();
            if (parts != null) {
                Iterator<Part> itor = parts.iterator();
                while (itor.hasNext() && params.size() < Integer.getInteger("org.eclipse.jetty.server.Request.maxFormKeys", 1000)) {
                    Part p = itor.next();
                    MultiPartInputStreamParser.MultiPart mp = (MultiPartInputStreamParser.MultiPart) p;
                    if (mp.getFile() != null) {
                    	File file = mp.getFile();
                    	System.out.println(file.getName());
                    	
                    	Date date= new java.util.Date();
                		String str = new Timestamp(date.getTime()).toString();
                		str = str.substring(0, str.lastIndexOf("."));
                		str = str.replace(" ", "-");
                		str = str.replace(":", "");
                		String fileName =  "Organization"+"_"+str + ".csv";
                    	
                		System.out.println(fileName);
                    	
                    	
                    	
                        request.raw().setAttribute(mp.getName(), mp.getFile());
                        if (mp.getContentDispositionFilename() != null) {
                            params.add(mp.getName(), mp.getContentDispositionFilename());
                            if (mp.getContentType() != null) {
                                params.add(mp.getName() + ".org.eclipse.jetty.servlet.contentType", mp.getContentType());
                            }
                            
                            AWSCredentials credentials = new BasicAWSCredentials("AKIAI4LX6EPGIBQKIJCA", "eLwTJOHzHPeWOHD9XShWZPk0B5kg60yTz34vxzHM");
                    		AmazonS3 s3client = new AmazonS3Client(credentials);
                    		String fileName1 = "fileLocation"+"/"+fileName;
                    		s3client.putObject(new PutObjectRequest("weicommerce-dev", fileName1, file));
                    			
                            
                        }
                    } else {
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        IO.copy(p.getInputStream(), bytes);
                        params.add(p.getName(), bytes.toByteArray());
                        if (p.getContentType() != null) {
                            params.add(p.getName() + ".org.eclipse.jetty.servlet.contentType", p.getContentType());
                        }
                    }
                }
            }

        // handle request
            //chain.doFilter(new Wrapper(srequest,params),response);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(ImportController.class.getName()).log(Level.SEVERE, null, ex);

        } catch (ServletException ex) {
            java.util.logging.Logger.getLogger(ImportController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //deleteFiles(request);
        }
        
        String temp = getListOfFiles();
        System.out.println(temp);
		return temp;
	
	} 
	
	
	
	private String getListOfFiles() {

		slf4jLogger.info("List out all the files ");
		String collectionName = "";
		String output = "";
		String s3BucketName = "weicommerce-dev";
		try {

			collectionName = "Organization";
			
			AWSCredentials credentials = new BasicAWSCredentials("AKIAI4LX6EPGIBQKIJCA", "eLwTJOHzHPeWOHD9XShWZPk0B5kg60yTz34vxzHM");
			AmazonS3 s3client = new AmazonS3Client(credentials);
			ObjectListing objects = s3client.listObjects(s3BucketName);
			List<String> list = new ArrayList<String>();
	        do {
	            for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
	                String key = objectSummary.getKey();
	                if(key.contains(collectionName)){
	                	key = key.substring(key.lastIndexOf("/")+1);
	                	list.add(key);
	                }
	            }
	            objects = s3client.listNextBatchOfObjects(objects);
	        } while (objects.isTruncated());
	        
	        String[] files = list.toArray(new String[list.size()]);
			output = JSON.serialize(files);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}
	
	
	
	    private void initImportServiceEnv(ImportServiceParams params, DefaultPersist persist) {
	        if (!request.session().isNew()
	                && request.session().attribute(ImportVocabulary.MONGODB.name()) != null) {
	        } else {
	            params.initEnv("environment.properties");
	            request.session().attribute(ImportVocabulary.MONGODB.name(), params.getEnv());
	            try {
	                //        }
	                HazelcastInstance db = persist.initEnv("environment.properties");
	            } catch (HazelcastException ex) {

	            }
	            request.session().attribute(ImportVocabulary.HAZELCAST.name(), persist.getEnv());
	        }

	        params.setEnv(request.session().attribute(ImportVocabulary.MONGODB.name()));
	        persist.setEnv(request.session().attribute(ImportVocabulary.HAZELCAST.name()));
	    }
}
