/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceInput;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceOutput;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceParams;
import com.rokittech.container.ecom.catalog.defaults.CatalogVocabulary;
import com.rokittech.container.ecom.catalog.factory.CatalogServiceFactory;
import com.rokittech.container.ecom.commons.models.Catalog;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author alexmy
 */
public class CatalogController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(CatalogController.class);
    private final Request request;
    private final Response response;
    
    private static final String DB_CATALOG = "Catalog";
    private static final String DB_PRODUCT = "Product";
    private final Info info = new Info (); 
        
    public CatalogController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
    public String createCatalog() {
        slf4jLogger.info("Creating catalog - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        // This is real mandatory code to move in controller
        CatalogServiceParams params = new CatalogServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), DB_CATALOG);
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.INSERT.name());

        CatalogServiceInput input = new CatalogServiceInput().fromJson(requestJson);

        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = new CatalogServiceFactory(CatalogVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CatalogServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Creaetd catalog - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    public String deleteCatalog() {
        slf4jLogger.info("Deleting catalog - ");
        Catalog catalog = new Catalog ();
        catalog.setId(request.params(":id"));
        
        slf4jLogger.info("request param: " + catalog.toJson());
        CatalogServiceParams params = new CatalogServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), DB_CATALOG);
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.DELETE.name());
        
        CatalogServiceInput input = new CatalogServiceInput();
        input.setInfo(info);
        input.setData(catalog);

        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = 
                new CatalogServiceFactory(CatalogVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CatalogServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Deleted catalog - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    public String getCatalogById() {
        slf4jLogger.info("Get catalog - ");
        Catalog catalog = new Catalog ();
        catalog.setId(request.params(":id"));    
        slf4jLogger.info("request param: " + catalog.toJson());
         
        // This is real mandatory code to move in controller
        CatalogServiceParams params = new CatalogServiceParams();
        DefaultPersist persist = new DefaultPersist();

        CatalogServiceInput input = new CatalogServiceInput();
        input.setInfo(info);
        input.setData(catalog);
        
        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), DB_CATALOG);
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.RETRIEVE.name());

        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = new CatalogServiceFactory(CatalogVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CatalogServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("got catalog - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    public String updateCatalog() {
        slf4jLogger.info("Updating catalog - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        CatalogServiceParams params = new CatalogServiceParams();
        DefaultPersist persist = new DefaultPersist();

        CatalogServiceInput input = new CatalogServiceInput().fromJson(requestJson);

        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), DB_CATALOG);
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.UPDATE.name());

        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = new CatalogServiceFactory(CatalogVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CatalogServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("updated catalog - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    
    /**
     *
     * @param params
     * @param persist
     */
    private void initCatalogServiceEnv(CatalogServiceParams params, DefaultPersist persist) {
        persist.setEnv(request.session().attribute(CatalogVocabulary.HAZELCAST.name()));
    }

    public String listCatalog() {
        slf4jLogger.info("List catalog - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        CatalogServiceParams params = new CatalogServiceParams();
        DefaultPersist persist = new DefaultPersist();

        CatalogServiceInput input = new CatalogServiceInput();
        if ((null != requestJson) && !requestJson.isEmpty()){
            input = new CatalogServiceInput().fromJson(requestJson);
        }else{
            input.setInfo(info);
        }
        
        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), DB_CATALOG);
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.LIST.name());

        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = 
                new CatalogServiceFactory(CatalogVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CatalogServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("List catalog - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    
    public String listChildren() {
        slf4jLogger.info("List catalog - ");
        Catalog catalog = new Catalog ();
        catalog.setParentCatalogId(request.params(":id"));  
        
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        CatalogServiceParams params = new CatalogServiceParams();
        DefaultPersist persist = new DefaultPersist();

        CatalogServiceInput input = new CatalogServiceInput();
        input.setInfo(info);
        input.setData(catalog);

        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), DB_CATALOG);
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.LIST.name());

        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = 
                new CatalogServiceFactory(CatalogVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CatalogServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("List catalog - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
}
