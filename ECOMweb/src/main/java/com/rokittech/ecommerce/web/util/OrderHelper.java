/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.util;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Basket;
import com.rokittech.container.ecom.commons.models.Order;
import com.rokittech.container.ecom.commons.models.OrderStatusType;
import com.rokittech.container.ecom.commons.models.PaymentInformation;
import com.rokittech.container.ecom.order.defaults.OrderServiceInput;
import com.rokittech.container.ecom.order.defaults.OrderServiceOutput;
import com.rokittech.container.ecom.order.defaults.OrderServiceParams;
import com.rokittech.container.ecom.order.defaults.OrderVocabulary;
import com.rokittech.container.ecom.order.factory.OrderServiceFactory;
import com.rokittech.containers.api.core.Service;
import com.rokittech.ecommerce.web.controller.ProductController;
import com.rokittech.ecommerce.web.userobjects.OrderInput;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author asifaham
 */
public class OrderHelper {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(OrderHelper.class);
    private static final String DB_ORDER = "Order";
    
    private static int cout = 12345;
    
    public static String CreateOrder(OrderInput orderInput){
       
        // This is real mandatory code to move in controller
        OrderServiceParams params = new OrderServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(OrderVocabulary.COLLECTION.name(), DB_ORDER);
        params.getProperties().put(OrderVocabulary.COMMAND.name(), OrderVocabulary.INSERT.name());

        OrderServiceInput input = new OrderServiceInput();
        Basket basket = orderInput.getData();
        Order order = new Order();
        order.setBasketId(basket.getId());
        order.setCustomerId(basket.getUser().getId());
        order.setBillingAddress(basket.getBillingAddress());
        order.setGrossPrice(basket.getTotal());
        order.setNetPrice(basket.getSubTotal());
        order.setTax(basket.getTax());
        order.setLinetems(basket.getLineItems());
        order.setStatus(OrderStatusType.NEW);
        order.setOrderNumber(getAutoGenOrderNumber (order.getCustomerId(), orderInput.getInfo()));
        order.setCreatedDate(new Date());
        order.setPaymentInformation(new PaymentInformation(basket.getPaymentType(), basket.getPayment()));
        
        input.setInfo(orderInput.getInfo());
        input.setData(order);

        Service<OrderServiceInput, OrderServiceOutput, OrderServiceParams, DefaultPersist> service 
                = new OrderServiceFactory(OrderVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input); 
        service.setOutput(new OrderServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();
        
        return output; 
    }
    public static Order getOrder(String orderId){
       
        slf4jLogger.info("Find order - ");
        Order orderInput = new Order ();
        orderInput.setId(orderId);
        Info info=new Info();
        info.setAppId("weicommerce");
        info.setUserId("ttt");
        info.setVersion((int) 1.0);
        info.setServiceId("wei");
        
        slf4jLogger.info("request param: " + orderInput.toJson());
        
        // This is real mandatory code to move in controller
        OrderServiceParams params = new OrderServiceParams();
        DefaultPersist persist = new DefaultPersist();

        OrderServiceInput input = new OrderServiceInput();
        input.setInfo(info);
        input.setData(orderInput);
        
        params.getProperties().put(OrderVocabulary.COLLECTION.name(), DB_ORDER);
        params.getProperties().put(OrderVocabulary.COMMAND.name(), OrderVocabulary.RETRIEVE.name());

        Service<OrderServiceInput, OrderServiceOutput, OrderServiceParams, DefaultPersist> service = new OrderServiceFactory(OrderVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrderServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();
Order order = new Order().fromJson(output);
        slf4jLogger.info("got priduct - ");
        slf4jLogger.debug("Response " + output);
        
        
        
        return order; 
    }
    /**
     * 
     * @param info
     * @return 
     */
    private static String getAutoGenOrderNumber(String custId, Info info) {
        /*Counter counter = new Counter ();
        counter.setId("ordernumber");
        CountersServiceInput cinput = new CountersServiceInput();
        cinput.setInfo(info);
        cinput.setData(counter);
        CountersServiceOutput coutput = new CountersServiceOutput();
        // This is real mandatory code to move in controller
        CountersServiceParams cparams = new CountersServiceParams();
        DefaultPersist cpersist = new DefaultPersist();
        cparams.getProperties().put(CountersVocabulary.COLLECTION.name(), "counters");
        cparams.getProperties().put(CountersVocabulary.COMMAND.name(), CountersVocabulary.COUNTER.name());
        Service<CountersServiceInput, CountersServiceOutput, CountersServiceParams, DefaultPersist> cservice
        = new CountersServiceFactory(CountersVocabulary.WEI_ECOMMERCE.name(), 1).get();
        cservice.setParams(cparams);
        cservice.setPersist(cpersist);
        cservice.setInput(cinput); 
        cservice.setOutput(coutput);
        // Run service and get output data.
        //
        String cntr_output = cservice.serve().toJson();
        Counter cout = new Counter ().fromJson(cntr_output);*/
        cout++;
        
        return "RO" + LocalDate.now().format(DateTimeFormatter.ofPattern("ddMMyyyy")) + cout;
    }
}
