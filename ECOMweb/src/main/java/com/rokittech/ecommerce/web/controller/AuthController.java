/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.auth.defaults.AuthServiceInput;
import com.rokittech.container.ecom.auth.defaults.AuthServiceOutput;
import com.rokittech.container.ecom.auth.defaults.AuthServiceParams;
import com.rokittech.container.ecom.auth.defaults.AuthVocabulary;
import com.rokittech.container.ecom.auth.defaults.LoginStatus;
import com.rokittech.container.ecom.auth.factory.AuthServiceFactory;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.commons.models.UserCredentials;
import com.rokittech.container.ecom.user.defaults.UserServiceInput;
import com.rokittech.container.ecom.user.defaults.UserServiceOutput;
import com.rokittech.container.ecom.user.defaults.UserServiceParams;
import com.rokittech.container.ecom.user.defaults.UserVocabulary;
import com.rokittech.container.ecom.user.factory.UserServiceFactory;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 7th Oct 2015
 */
public class AuthController {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(PricingController.class);
    private final Request request;
    private final Response response;
    private final Info info = new Info();

    public AuthController(Request request, Response response) {
        this.request = request;
        this.response = response;

        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
    public String logout() {

        AuthServiceInput input = new AuthServiceInput();
        input.setInfo(info);
        AuthServiceOutput output = new AuthServiceOutput();
        output.setInfo(input.getInfo());
        LoginStatus status = new LoginStatus();
        status.setMsg("Logout Successfully");
        status.setStatus(false);
        output.setData(status.toJson());

        //TODO remove loggedin user info from session
        request.session().invalidate();

        //TODO replace  log
        slf4jLogger.info("Output Response .." + output);

        return output.toJson();
    }

    public String login() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error("Invalid request ..");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        slf4jLogger.info("Input request .." + requestJson);
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.AUTH.name());

        AuthServiceInput authInput = new AuthServiceInput().fromJson(requestJson);
        UserServiceInput input = new UserServiceInput();
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setUserName(authInput.getData().getUserName());
        userCredentials.setPassword(authInput.getData().getPassword());
        User user = new User();
        user.setUserCredentials(userCredentials);
        input.setInfo(info);
        input.setData(user);
        
        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        UserServiceOutput serviceOutput = service.serve();
        
        LoginStatus status = new LoginStatus();
        status.setUserName(authInput.getData().getUserName());
        if ("{ }".equals(serviceOutput.getData())) {
            status.setMsg("Login Failed: Incorrect username or password");
            status.setStatus(false);
            response.status(401);
        } else {

            //TODO temp code to be updated with session object of real container.
            status.setStatus(true);
            status.setMsg("Successfully logged in");
            request.session().attribute("UserName", authInput.getData().getUserName());
        }    
        //serviceOutput.setData(status.toJson());
        //output = serviceOutput.toJson();

        slf4jLogger.info ("Output Response .." + serviceOutput);

        return serviceOutput.toJson();
    }

    // UserCredentials CRUD
    //Insert UserCredentials
    public String createUserCredentials() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.info ("Invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        slf4jLogger.info ("Input request .." + requestJson);
        // This is real mandatory code to move in controller
        AuthServiceParams params = new AuthServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AuthVocabulary.COLLECTION.name(), "UserCredentials");
        params.getProperties().put(AuthVocabulary.COMMAND.name(), AuthVocabulary.INSERT.name());

        AuthServiceInput input = new AuthServiceInput().fromJson(requestJson);

        Service<AuthServiceInput, AuthServiceOutput, AuthServiceParams, DefaultPersist> service = new AuthServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AuthServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        slf4jLogger.info ("Output Response .." + output);

        return output;
    }

    //Update UserCredentials
    public String updateUserCredentials() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error ("Invalid request ..");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        slf4jLogger.info ("Input request .." + requestJson);
        // This is real mandatory code to move in controller
        AuthServiceParams params = new AuthServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AuthVocabulary.COLLECTION.name(), "UserCredentials");
        params.getProperties().put(AuthVocabulary.COMMAND.name(), AuthVocabulary.UPDATE.name());

        AuthServiceInput input = new AuthServiceInput().fromJson(requestJson);

        Service<AuthServiceInput, AuthServiceOutput, AuthServiceParams, DefaultPersist> service = new AuthServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AuthServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        slf4jLogger.info ("Output Response .." + output);

        return output;
    }

    //Delete UserCredentials
    public String deleteUserCredentials() {
        slf4jLogger.info("Deleting user - ");
        UserCredentials userCredentials = new UserCredentials();
        //userCredentials.setId(request.params(":id"));
        slf4jLogger.info("request param: " + userCredentials.toJson());

        // This is real mandatory code to move in controller
        AuthServiceParams params = new AuthServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AuthVocabulary.COLLECTION.name(), "UserCredentials");
        params.getProperties().put(AuthVocabulary.COMMAND.name(), AuthVocabulary.DELETE.name());

        AuthServiceInput input = new AuthServiceInput();
        input.setData(userCredentials);
        input.setInfo(info);

        Service<AuthServiceInput, AuthServiceOutput, AuthServiceParams, DefaultPersist> service = new AuthServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AuthServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info ("Output Response .." + output);

        return output;
    }
//Find Address

    public String findUserCredentials() {
        slf4jLogger.info("Finding user - ");
        UserCredentials userCredentials = new UserCredentials();
        //userCredentials.setId(request.params(":id"));
        slf4jLogger.info("request param: " + userCredentials.toJson());

        // This is real mandatory code to move in controller
        AuthServiceParams params = new AuthServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AuthVocabulary.COLLECTION.name(), "UserCredentials");
        params.getProperties().put(AuthVocabulary.COMMAND.name(), AuthVocabulary.RETRIEVE.name());

        AuthServiceInput input = new AuthServiceInput();
        input.setData(userCredentials);
        input.setInfo(info);

        Service<AuthServiceInput, AuthServiceOutput, AuthServiceParams, DefaultPersist> service = new AuthServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AuthServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info ("Output Response .." + output);

        return output;
    }

    /**
     *
     * @param params
     * @param persist
     */
    private void initAuthServiceEnv(AuthServiceParams params, DefaultPersist persist) {
        if (!request.session().isNew()
                && request.session().attribute(AuthVocabulary.MONGODB.name()) != null) {
        } else {
            params.initEnv("environment.properties");
            request.session().attribute(AuthVocabulary.MONGODB.name(), params.getEnv());
//            try {
//                //        }
//                HazelcastInstance db = persist.initEnv("environment.properties");
//            } catch (HazelcastException ex) {
//
//            }
//            request.session().attribute(AuthVocabulary.HAZELCAST.name(), persist.getEnv());
        }

        params.setEnv(request.session().attribute(AuthVocabulary.MONGODB.name()));
        persist.setEnv(request.session().attribute(AuthVocabulary.HAZELCAST.name()));
    }

}
