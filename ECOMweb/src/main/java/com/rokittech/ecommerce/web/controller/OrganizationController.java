/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Organization;
import com.rokittech.container.ecom.organization.defaults.OrganizationServiceInput;
import com.rokittech.container.ecom.organization.defaults.OrganizationServiceOutput;
import com.rokittech.container.ecom.organization.defaults.OrganizationServiceParams;
import com.rokittech.container.ecom.organization.defaults.OrganizationVocabulary;
import com.rokittech.container.ecom.organization.factory.OrganizationServiceFactory;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class OrganizationController {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(OrganizationController.class);
    private final Request request;
    private final Response response;
    private Info info = new Info();

    public OrganizationController(Request request, Response response) {
        this.request = request;
        this.response = response;

        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
    public String listOrganization() {
        slf4jLogger.info("List product - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        OrganizationServiceParams params = new OrganizationServiceParams();
        DefaultPersist persist = new DefaultPersist();

        OrganizationServiceInput input = new OrganizationServiceInput();
        if ((null != requestJson) && !requestJson.isEmpty()){
            input = new OrganizationServiceInput().fromJson(requestJson);
        }else{
            input.setInfo(info);
        }
        
        params.getProperties().put(OrganizationVocabulary.COLLECTION.name(), "Organization");
        params.getProperties().put(OrganizationVocabulary.COMMAND.name(), OrganizationVocabulary.LIST.name());

        Service<OrganizationServiceInput, OrganizationServiceOutput, OrganizationServiceParams, DefaultPersist> service = 
                new OrganizationServiceFactory(OrganizationVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrganizationServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("list Organization - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    public String createOrganization() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        OrganizationServiceParams params = new OrganizationServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(OrganizationVocabulary.COLLECTION.name(), "Organization");
        params.getProperties().put(OrganizationVocabulary.COMMAND.name(), OrganizationVocabulary.INSERT.name());

        OrganizationServiceInput input = new OrganizationServiceInput().fromJson(requestJson);

        Service<OrganizationServiceInput, OrganizationServiceOutput, OrganizationServiceParams, DefaultPersist> service = new OrganizationServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrganizationServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    public String deleteOrganization() {
        slf4jLogger.info("Deleting catalog - ");
        Organization organization = new Organization();
        organization.setId(request.params(":id"));
        slf4jLogger.info("request param: " + organization.toJson());

        String output = "";

        // This is real mandatory code to move in controller
        OrganizationServiceParams params = new OrganizationServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(OrganizationVocabulary.COLLECTION.name(), "Organization");
        params.getProperties().put(OrganizationVocabulary.COMMAND.name(), OrganizationVocabulary.DELETE.name());

        OrganizationServiceInput input = new OrganizationServiceInput();
        input.setData(organization);
        input.setInfo(info);

        Service<OrganizationServiceInput, OrganizationServiceOutput, OrganizationServiceParams, DefaultPersist> service = new OrganizationServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrganizationServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    public String getOrganization() {
        slf4jLogger.info("get organization - ");
        Organization organization = new Organization();
        organization.setId(request.params(":id"));
        slf4jLogger.info("request param: " + organization.toJson());

        String output = "";
        // This is real mandatory code to move in controller
        OrganizationServiceParams params = new OrganizationServiceParams();
        DefaultPersist persist = new DefaultPersist();

        OrganizationServiceInput input = new OrganizationServiceInput();
        input.setData(organization);
        input.setInfo(info);

        params.getProperties().put(OrganizationVocabulary.COLLECTION.name(), "Organization");
        params.getProperties().put(OrganizationVocabulary.COMMAND.name(), OrganizationVocabulary.RETRIEVE.name());

        Service<OrganizationServiceInput, OrganizationServiceOutput, OrganizationServiceParams, DefaultPersist> service = new OrganizationServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrganizationServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    public String updateOrganization() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        OrganizationServiceParams params = new OrganizationServiceParams();
        DefaultPersist persist = new DefaultPersist();

        OrganizationServiceInput input = new OrganizationServiceInput().fromJson(requestJson);

        params.getProperties().put(OrganizationVocabulary.COLLECTION.name(), "Organization");
        params.getProperties().put(OrganizationVocabulary.COMMAND.name(), OrganizationVocabulary.UPDATE.name());

        Service<OrganizationServiceInput, OrganizationServiceOutput, OrganizationServiceParams, DefaultPersist> service = new OrganizationServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrganizationServiceOutput());

        // Run service and get output data.
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    /**
     *
     * @param params
     * @param persist
     */
    private void initOrganizationServiceEnv(OrganizationServiceParams params, DefaultPersist persist) {
        if (!request.session().isNew()
                && request.session().attribute(OrganizationVocabulary.MONGODB.name()) != null) {
        } else {
            params.initEnv("environment.properties");
            request.session().attribute(OrganizationVocabulary.MONGODB.name(), params.getEnv());
//            try {
//                //        }
//                HazelcastInstance db = persist.initEnv("environment.properties");
//            } catch (HazelcastException ex) {
//
//            }
            request.session().attribute(OrganizationVocabulary.HAZELCAST.name(), persist.getEnv());
        }

        params.setEnv(request.session().attribute(OrganizationVocabulary.MONGODB.name()));
        persist.setEnv(request.session().attribute(OrganizationVocabulary.HAZELCAST.name()));
    }

}
