package com.rokittech.ecommerce.web;

import com.rokittech.ecommerce.web.controller.*;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import static spark.Spark.before;
import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.options;
import static spark.Spark.post;
import static spark.Spark.put;
import static spark.SparkBase.staticFileLocation;
import spark.template.freemarker.FreeMarkerEngine;

/**
 * Created by alexmy on 7/28/15.
 */
public class Main {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(Main.class);
    
    public static void main(String[] args) {
        slf4jLogger.info ("********Container Starting.....************");
        
        // Configure that static files directory.
        staticFileLocation("/pblic");
        
        options("/*", (request,response)->{
 
            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if(accessControlRequestMethod != null){
            response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });
 
        before((request,response)->{
            response.header("Access-Control-Allow-Origin", "*");
        });
        
        
        
        // Catalog CRUD
        post("/catalog/insert", "application/json", (req, res)
                -> new CatalogController(req, res).createCatalog());
        delete("/catalog/delete/:id", "application/json", (req, res)
                -> new CatalogController(req, res).deleteCatalog());
        get("/catalog/find/:id", "application/json", (req, res)
                -> new CatalogController(req, res).getCatalogById());
        put("/catalog/update", "application/json", (req, res)
                -> new CatalogController(req, res).updateCatalog());
        post("/catalog/list", "application/json", (req, res)
                -> new CatalogController(req, res).listCatalog());
        
        get("/catalog/getchildren/:id", "application/json", (req, res)
                -> new CatalogController(req, res).listChildren());
        
        // Product CRUD
        post("/product/insert", "application/json", (req, res)
                -> new ProductController(req, res).createProduct());
        delete("/product/delete/:id", "application/json", (req, res)
                -> new ProductController(req, res).deleteProduct());
        get("/product/find/:id", "application/json", (req, res)
                -> new ProductController(req, res).getProductById());
        put("/product/update", "application/json", (req, res)
                -> new ProductController(req, res).updateProduct());
        post("/product/list", "application/json", (req, res)
                -> new ProductController(req, res).listProduct());
        
        // Pricing CRUD
        post("/pricing/insert", "application/json", (req, res)
                -> new PricingController(req, res).createPricing());
        delete("/pricing/delete/:id", "application/json", (req, res)
                -> new PricingController(req, res).deletePricing());
        get("/pricing/find/:id", "application/json", (req, res)
                -> new PricingController(req, res).getPricingById());
        put("/pricing/update", "application/json", (req, res)
                -> new PricingController(req, res).updatePricing());
        post("/pricing/list", "application/json", (req, res)
                -> new PricingController(req, res).listPricing());

        // Organization CRUD
        post("/organization/insert", "application/json", (req, res)
                -> new OrganizationController(req, res).createOrganization());
        delete("/organization/delete/:id", "application/json", (req, res)
                -> new OrganizationController(req, res).deleteOrganization());
        get("/organization/find/:id", "application/json", (req, res)
                -> new OrganizationController(req, res).getOrganization());
        put("/organization/update", "application/json", (req, res)
                -> new OrganizationController(req, res).updateOrganization());
        post("/organization/list", "application/json", (req, res)
                -> new OrganizationController(req, res).listOrganization());
        
        // Customer CRUD
        post("/customer/insert", "application/json", (req, res)
                -> new CustomerController(req, res).createCustomer());
        put("/customer/update", "application/json", (req, res)
                -> new CustomerController(req, res).updateCustomer());
        delete("/customer/delete/:id", "application/json", (req, res)
                -> new CustomerController(req, res).deleteCustomer());
        get("/customer/find/:id", "application/json", (req, res)
                -> new CustomerController(req, res).findCustomer());
        post("/customer/list", "application/json", (req, res)
                -> new CustomerController(req, res).listCustomer());

        // User CRUD
        post("/user/insert", "application/json", (req, res)
                -> new UserController(req, res).createUser());
        put("/user/update", "application/json", (req, res)
                -> new UserController(req, res).updateUser());
        delete("/user/delete/:id", "application/json", (req, res)
                -> new UserController(req, res).deleteUser());
        get("/user/find/:id", "application/json", (req, res)
                -> new UserController(req, res).findUserWithRoles());
        post("/user/list", "application/json", (req, res)
                -> new UserController(req, res).listUser());
        
        // Role CRUD
        post("/role/insert", "application/json", (req, res)
                -> new RoleController(req, res).createRole());
        put("/role/update", "application/json", (req, res)
                -> new RoleController(req, res).updateRole());
        delete("/role/delete/:id", "application/json", (req, res)
                -> new RoleController(req, res).deleteRole());
        get("/role/find/:id", "application/json", (req, res)
                -> new RoleController(req, res).findRole());
        post("/permission/list", "application/json", (req, res)
                -> new RoleController(req, res).listPermission());
        
        // Seller CRUD
        post("/seller/insert", "application/json", (req, res)
                -> new SellerController(req, res).createSeller());
        put("/seller/update", "application/json", (req, res)
                -> new SellerController(req, res).updateSeller());//Address creadential
        delete("/seller/delete/:id", "application/json", (req, res)
                -> new SellerController(req, res).deleteSeller());
        get("/seller/find/:id", "application/json", (req, res)
                -> new SellerController(req, res).findSeller());

        // Address CRUD
        post("/address/insert", "application/json", (req, res)
                -> new AddressController(req, res).createAddress());
        put("/address/update", "application/json", (req, res)
                -> new AddressController(req, res).updateAddress());
        delete("/address/delete/:id", "application/json", (req, res)
                -> new AddressController(req, res).deleteAddress());
        get("/address/find/:id", "application/json", (req, res)
                -> new AddressController(req, res).findAddress());
        
        // Credentials CRUD
        post("/credentials/insert", "application/json", (req, res)
                -> new AuthController(req, res).createUserCredentials());
        put("/credentials/update", "application/json", (req, res)
                -> new AuthController(req, res).updateUserCredentials());
        delete("/credentials/delete/:id", "application/json", (req, res)
                -> new AuthController(req, res).deleteUserCredentials());
        get("/credentials/find/:id", "application/json", (req, res)
                -> new AuthController(req, res).findUserCredentials());
        
        // Preferences CRUD
        post("/preferences/insert", "application/json", (req, res)
                -> new PreferencesController(req, res).createPreferences());
        put("/preferences/update", "application/json", (req, res)
                -> new PreferencesController(req, res).updatePreferences());
        delete("/preferences/delete/:id", "application/json", (req, res)
                -> new PreferencesController(req, res).deletePreferences());
        get("/preferences/find/:id", "application/json", (req, res)
                -> new PreferencesController(req, res).findPreferences());

        //Login
        post("/credentials/login", "application/json", (req, res)
                -> new AuthController(req, res).login());
        get("/credentials/logout/:id", "application/json", (req, res)
                -> new AuthController(req, res).logout());
        
        
        //payment
        post("/payment/process", "application/json", (req, res)
                -> new PaymentController(req, res).process ());
        post("/payment/success", "application/json", (req, res)
                -> new PaymentController(req, res).success ());
        post("/payment/failure", "application/json", (req, res)
                -> new PaymentController(req, res).failure ());
        post("/payment/cancel", "application/json", (req, res)
                -> new PaymentController(req, res).cancel());
        //Search
        post("/search", "application/json", (req, res)
                -> new SearchController(req, res).search());
        //Basket
        post("/cart/insert", "application/json", (req, res)
                -> new BasketController(req, res).createBasket());
        put("/cart/update", "application/json", (req, res)
                -> new BasketController(req, res).updateBasket());
        get("/cart/find/:id", "application/json", (req, res)
                -> new BasketController(req, res).getBasket());
        delete("/cart/delete/:id", "application/json", (req, res)
                -> new BasketController(req, res).deleteBasket());
        put("/cart/addproduct", "application/json", (req, res)
                -> new BasketController(req, res).addProductToBasket());
        put("/cart/removeproduct", "application/json", (req, res)
                -> new BasketController(req, res).removeBasketLineItem());
        put("/cart/updateproduct", "application/json", (req, res)
                -> new BasketController(req, res).updateBasketLineItem());
        put("/cart/updateaddres", "application/json", (req, res)
                -> new BasketController(req, res).updateBasketAddres());
        put("/cart/setpaymenttype", "application/json", (req, res)
                -> new BasketController(req, res).updateBasket());
        put("/cart/setpayment", "application/json", (req, res)
                -> new BasketController(req, res).updateBasket());
        put("/cart/updateshippingmethod", "application/json", (req, res)
                -> new BasketController(req, res).updateBasketShippingMethod());
        
        // Inventory CRUD
        post("/inventory/insert", "application/json", (req, res)
                -> new InventoryController(req, res).createInventory());
        delete("/inventory/delete/:id", "application/json", (req, res)
                -> new InventoryController(req, res).deleteInventory());
        get("/inventory/find/:id", "application/json", (req, res)
                -> new InventoryController(req, res).getInventoryById());
        put("/inventory/update", "application/json", (req, res)
                -> new InventoryController(req, res).updateInventory());
        post("/inventory/check", "application/json", (req, res)
                -> new InventoryController(req, res).checkInventory());
        
        // AttributeGroup CRUD
        post("/attributegroup/insert", "application/json", (req, res)
                -> new AttributeGroupController(req, res).createAttributeGroup());
        put("/attributegroup/update", "application/json", (req, res)
                -> new AttributeGroupController(req, res).updateAttributeGroup());
        delete("/attributegroup/delete/:id", "application/json", (req, res)
                -> new AttributeGroupController(req, res).deleteAttributeGroup());
        get("/attributegroup/find/:id", "application/json", (req, res)
                -> new AttributeGroupController(req, res).findAttributeGroup());
        post("/attributegroup/list", "application/json", (req, res)
                -> new AttributeGroupController(req, res).listAttributeGroup());
        
        
        // Order CRUD
        put("/order/update", "application/json", (req, res)
                -> new OrderController(req, res).updateOrder());
        get("/order/find/:id", "application/json", (req, res)
                -> new OrderController(req, res).getOrderById());
        post("/order/list", "application/json", (req, res)
                -> new OrderController(req, res).listOrder());
        
        
        // import/export
        post("/organization/import", "application/json", (req, res)
                -> new ImportController(req, res).importingCSV());
        
        post("/organization/export", "application/json", (req, res)
                -> new ExportController(req, res).exportingCSV());
        
        post("/mongodb/restore", "application/json", (req, res)
                -> new ImportController(req, res).dumpRestore());
        
        post("/mongodb/dump", "application/json", (req, res)
                -> new ExportController(req, res).dumpCreation());
        get("export/organization/list", "application/json", (req, res)
                -> new ExportController(req, res).getListOfFiles());
        get("export/order/list", "application/json", (req, res)
                -> new ExportController(req, res).getListOfFiles());
        
        post("/order/export", "application/json", (req, res)
        		-> new ExportController(req, res).exportingCSV());
        
        post("/organization/upload", "application/json", (req, res)
        		-> new ImportController(req, res).uploadFiles());
        
        
        // Organization CRUD
        post("/shippingmethod/insert", "application/json", (req, res)
                -> new ShippingMethodController(req, res).createShippingMethod());
        delete("/shippingmethod/delete/:id", "application/json", (req, res)
                -> new ShippingMethodController(req, res).deleteShippingMethod());
        get("/shippingmethod/find/:id", "application/json", (req, res)
                -> new ShippingMethodController(req, res).getShippingMethod());
        put("/shippingmethod/update", "application/json", (req, res)
                -> new ShippingMethodController(req, res).updateShippingMethod());

        FreeMarkerEngine freeMarkerEngine = new FreeMarkerEngine();
        Configuration freeMarkerConfiguration = new Configuration();
        freeMarkerConfiguration.setTemplateLoader(new ClassTemplateLoader(Main.class, "/"));
        freeMarkerEngine.setConfiguration(freeMarkerConfiguration);         
        
        get("/payment", (request, response) -> {
                response.status(200);
                response.type("text/html");
                Map<String, Object> attributes = new HashMap<>();
                return freeMarkerEngine.render(new ModelAndView(attributes, "index.ftl"));
            
        });
        
        slf4jLogger.info ("*********Container Started successfully**********");
    }

}
