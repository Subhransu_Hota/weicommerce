/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Inventory;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceInput;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceOutput;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceParams;
import com.rokittech.container.ecom.inventory.defaults.InventoryVocabulary;
import com.rokittech.container.ecom.inventory.factory.InventoryServiceFactory;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author alexmy
 */
public class InventoryController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(InventoryController.class);
    private final Request request;
    private final Response response;

    private static final String DB_INVENTORY = "Inventory";
    private final Info info = new Info ();

    public InventoryController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
    public String createInventory() {
        slf4jLogger.info("Creating inventory - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        // This is real mandatory code to move in controller
        InventoryServiceParams params = new InventoryServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(InventoryVocabulary.COLLECTION.name(), DB_INVENTORY);
        params.getProperties().put(InventoryVocabulary.COMMAND.name(), InventoryVocabulary.INSERT.name());

        InventoryServiceInput input = new InventoryServiceInput();
        input.setData(new Inventory().fromJson(requestJson));
        input.setInfo(info);

        Service<InventoryServiceInput, InventoryServiceOutput, InventoryServiceParams, DefaultPersist> service = new InventoryServiceFactory(InventoryVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new InventoryServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Creaetd Inventory - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    public String deleteInventory() {
        slf4jLogger.info("Deleting Inventory - ");
        Inventory inventory = new Inventory ();
        inventory.setId(request.params(":id"));
        
        slf4jLogger.info("request param: " + inventory.toJson());
        InventoryServiceParams params = new InventoryServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(InventoryVocabulary.COLLECTION.name(), DB_INVENTORY);
        params.getProperties().put(InventoryVocabulary.COMMAND.name(), InventoryVocabulary.DELETE.name());
        
        InventoryServiceInput input = new InventoryServiceInput();
        input.setInfo(info);
        input.setData(inventory);

        Service<InventoryServiceInput, InventoryServiceOutput, InventoryServiceParams, DefaultPersist> service = 
                new InventoryServiceFactory(InventoryVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new InventoryServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Deleted Inventory - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    public String getInventoryById() {
        slf4jLogger.info("Get Inventory - ");
        Inventory Inventory = new Inventory ();
        Inventory.setId(request.params(":id"));    
        slf4jLogger.info("request param: " + Inventory.toJson());
         
        // This is real mandatory code to move in controller
        InventoryServiceParams params = new InventoryServiceParams();
        DefaultPersist persist = new DefaultPersist();

        InventoryServiceInput input = new InventoryServiceInput();
        input.setInfo(info);
        input.setData(Inventory);
        
        params.getProperties().put(InventoryVocabulary.COLLECTION.name(), DB_INVENTORY);
        params.getProperties().put(InventoryVocabulary.COMMAND.name(), InventoryVocabulary.RETRIEVE.name());

        Service<InventoryServiceInput, InventoryServiceOutput, InventoryServiceParams, DefaultPersist> service = new InventoryServiceFactory(InventoryVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new InventoryServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("got Inventory - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    public String updateInventory() {
        slf4jLogger.info("Updating Inventory - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        InventoryServiceParams params = new InventoryServiceParams();
        DefaultPersist persist = new DefaultPersist();

        InventoryServiceInput input = new InventoryServiceInput();
        input.setData(new Inventory().fromJson(requestJson));
        input.setInfo(info);

        params.getProperties().put(InventoryVocabulary.COLLECTION.name(), DB_INVENTORY);
        params.getProperties().put(InventoryVocabulary.COMMAND.name(), InventoryVocabulary.UPDATE.name());

        Service<InventoryServiceInput, InventoryServiceOutput, InventoryServiceParams, DefaultPersist> service = new InventoryServiceFactory(InventoryVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new InventoryServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("updated Inventory - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    
    /**
     *
     * @param params
     * @param persist
     */
    private void initInventoryServiceEnv(InventoryServiceParams params, DefaultPersist persist) {
        persist.setEnv(request.session().attribute(InventoryVocabulary.HAZELCAST.name()));
    }

    public String checkInventory() {
        slf4jLogger.info("Checking inventory - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        InventoryServiceParams params = new InventoryServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(InventoryVocabulary.COLLECTION.name(), DB_INVENTORY);
        params.getProperties().put(InventoryVocabulary.COMMAND.name(), InventoryVocabulary.FIND.name());

        InventoryServiceInput input = new InventoryServiceInput();
        input.setData(new Inventory().fromJson(requestJson));
        input.setInfo(info);

        Service<InventoryServiceInput, InventoryServiceOutput, InventoryServiceParams, DefaultPersist> service = new InventoryServiceFactory(InventoryVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new InventoryServiceOutput());

        service.serve();

        // todo: Such kind of operations can be moved into a separate layer where activity on response can be done
        Inventory response = new Inventory().fromJson(service.getOutput().getData());
        Boolean isInventoryAvailable = Boolean.FALSE;
        if(response.getQuantity() != null && response.getQuantity() > 0) {
            isInventoryAvailable = Boolean.TRUE;
        }
        service.getOutput().setData(isInventoryAvailable.toString());


        String output = service.getOutput().toJson();
        slf4jLogger.info("Checked Inventory - ");
        slf4jLogger.debug("Response " + output);

        return output;
    }
}
