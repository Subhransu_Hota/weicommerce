/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Role;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.permission.defaults.PermissionService;
import com.rokittech.container.ecom.permission.factory.PermissionServiceFactory;
import com.rokittech.container.ecom.role.defaults.RoleServiceInput;
import com.rokittech.container.ecom.role.defaults.RoleServiceOutput;
import com.rokittech.container.ecom.role.defaults.RoleServiceParams;
import com.rokittech.container.ecom.role.defaults.RoleVocabulary;
import com.rokittech.container.ecom.role.factory.RoleServiceFactory;
import com.rokittech.container.ecom.user.defaults.UserServiceInput;
import com.rokittech.container.ecom.user.defaults.UserServiceOutput;
import com.rokittech.container.ecom.user.defaults.UserServiceParams;
import com.rokittech.container.ecom.user.defaults.UserVocabulary;
import com.rokittech.container.ecom.user.factory.UserServiceFactory;
import com.rokittech.containers.api.core.Service;
import java.util.Arrays;
import java.util.HashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author asifahammed
 */
public class RoleController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(RoleController.class);
    private final Request request;
    private final Response response;
    private Info info = new Info ();
    
    public RoleController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }
    
    /**
     *
     * @return
     */
    // Role CRUD
    //Insert Role
    public String createRole() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request..");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        RoleServiceParams params = new RoleServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(RoleVocabulary.COLLECTION.name(), "Role");
        params.getProperties().put(RoleVocabulary.COMMAND.name(), RoleVocabulary.INSERT.name());

        RoleServiceInput input = new RoleServiceInput().fromJson(requestJson);

        Service<RoleServiceInput, RoleServiceOutput, RoleServiceParams, DefaultPersist> service = new RoleServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new RoleServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Update Role
    public String updateRole() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request..");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        RoleServiceParams params = new RoleServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(RoleVocabulary.COLLECTION.name(), "Role");
        params.getProperties().put(RoleVocabulary.COMMAND.name(), RoleVocabulary.UPDATE.name());

        RoleServiceInput input = new RoleServiceInput().fromJson(requestJson);

        Service<RoleServiceInput, RoleServiceOutput, RoleServiceParams, DefaultPersist> service = new RoleServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new RoleServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }
    
    /**
     * Checks if there exists some user with given role.
     * 
     * @params roleId
     * @return <code></code>
     */
    private boolean isUserExists(String roleId){
        slf4jLogger.info("Finding users with role - " + roleId);
        User user = new User ();
        user.setRoles(new HashSet<>(Arrays.asList(roleId)));
        
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.FIND.name());

        UserServiceInput input = new UserServiceInput();
        input.setData(user);
        input.setInfo(info);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        String output = service.serve().getData();
        
        if("{ }".equals(output))
            return false;
        else
            return true;
    }
    
    //Delete Role

    public String deleteRole() {
        slf4jLogger.info("Deleting role - ");
        Role role = new Role ();
        role.setId(request.params(":id"));
        slf4jLogger.info("request param: " + role.toJson());
        
        // This is real mandatory code to move in controller
        RoleServiceParams params = new RoleServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        // Check for if there is some user associated with given role
        if (isUserExists(role.getId())){
            // set forbidden code
            response.status(403);
            return "Operation not permitted. Role is associated with some user !!";        
        }

        params.getProperties().put(RoleVocabulary.COLLECTION.name(), "Role");
        params.getProperties().put(RoleVocabulary.COMMAND.name(), RoleVocabulary.DELETE.name());

        RoleServiceInput input = new RoleServiceInput();
        input.setData(role);
        input.setInfo(info);

        Service<RoleServiceInput, RoleServiceOutput, RoleServiceParams, DefaultPersist> service = new RoleServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new RoleServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Find Role

    public String findRole() {
        slf4jLogger.info("Finding role - ");
        Role role = new Role ();
        role.setId(request.params(":id"));
        slf4jLogger.info("request param: " + role.toJson());
        
        // This is real mandatory code to move in controller
        RoleServiceParams params = new RoleServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(RoleVocabulary.COLLECTION.name(), "Role");
        params.getProperties().put(RoleVocabulary.COMMAND.name(), RoleVocabulary.RETRIEVE.name());

        RoleServiceInput input = new RoleServiceInput();
        input.setData(role);
        input.setInfo(info);

        Service<RoleServiceInput, RoleServiceOutput, RoleServiceParams, DefaultPersist> service = new RoleServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new RoleServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }
    
    //List permissions
    public String listPermission(){
        slf4jLogger.info("List permission - ");
        
        PermissionService service = new PermissionServiceFactory("WEI_ECOMMERCE", 1).get();
        // Run service and get output data.
        String output = service.serve().toJson();

        
        slf4jLogger.info ("Output Response .." + output);

        return output;
    }

    /**
     *
     * @param params
     * @param persist
     */
    private void initRoleServiceEnv(RoleServiceParams params, DefaultPersist persist) {
        if (!request.session().isNew()
                && request.session().attribute(RoleVocabulary.MONGODB.name()) != null) {
        } else {
            params.initEnv("environment.properties");
            request.session().attribute(RoleVocabulary.MONGODB.name(), params.getEnv());
//            try {
//                //        }
//                HazelcastInstance db = persist.initEnv("environment.properties");
//            } catch (HazelcastException ex) {
//
//            }
//            request.session().attribute(RoleVocabulary.HAZELCAST.name(), persist.getEnv());
        }

        params.setEnv(request.session().attribute(RoleVocabulary.MONGODB.name()));
        persist.setEnv(request.session().attribute(RoleVocabulary.HAZELCAST.name()));
    }

}
