 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.userobjects;

import com.google.gson.Gson;
import com.rokittech.container.ecom.commons.models.Attribute;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class AssignmentAttribute implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    private String _id;
    private String assignmentId;
    private List<Attribute> assignments;
    
    public AssignmentAttribute() {
    }

    public AssignmentAttribute(String _id, String assignmentId, List<Attribute> assignments) {
        this._id = _id;
        this.assignmentId = assignmentId;
        this.assignments = assignments;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(String assignmentId) {
        this.assignmentId = assignmentId;
    }

    public List<Attribute> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<Attribute> assignments) {
        this.assignments = assignments;
    }


    @Override
    public String toJson() {
        return new Gson().toJson(this, AssignmentAttribute.class);
    }

    @Override
    public AssignmentAttribute fromJson(String json) {
        return new Gson().fromJson(json, AssignmentAttribute.class);
    }

    @Override
    public void init() {
        //TODO complete Address init
    }
}
