/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Seller;
import com.rokittech.container.ecom.seller.defaults.SellerServiceInput;
import com.rokittech.container.ecom.seller.defaults.SellerServiceOutput;
import com.rokittech.container.ecom.seller.defaults.SellerServiceParams;
import com.rokittech.container.ecom.seller.defaults.SellerVocabulary;
import com.rokittech.container.ecom.seller.factory.SellerServiceFactory;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author asifahammed
 */
public class SellerController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(SellerController.class);
    private final Request request;
    private final Response response;

    private Info info = new Info (); 
    
    public SellerController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
// Seller CRUD
    //Insert Seller
    public String createSeller() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request..");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        SellerServiceParams params = new SellerServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(SellerVocabulary.COLLECTION.name(), "Seller");
        params.getProperties().put(SellerVocabulary.COMMAND.name(), SellerVocabulary.INSERT.name());

        SellerServiceInput input = new SellerServiceInput().fromJson(requestJson);

        Service<SellerServiceInput, SellerServiceOutput, SellerServiceParams, DefaultPersist> service = new SellerServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new SellerServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Update Seller
    public String updateSeller() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request..");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        SellerServiceParams params = new SellerServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(SellerVocabulary.COLLECTION.name(), "Seller");
        params.getProperties().put(SellerVocabulary.COMMAND.name(), SellerVocabulary.UPDATE.name());

        SellerServiceInput input = new SellerServiceInput().fromJson(requestJson);

        Service<SellerServiceInput, SellerServiceOutput, SellerServiceParams, DefaultPersist> service = new SellerServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new SellerServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Delete Seller
    public String deleteSeller() {
        slf4jLogger.info("Deleting Seller - ");
        Seller seller = new Seller ();
        seller.setId(request.params(":id"));
        slf4jLogger.info("request param: " + seller.toJson());
         
        // This is real mandatory code to move in controller
        SellerServiceParams params = new SellerServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(SellerVocabulary.COLLECTION.name(), "Seller");
        params.getProperties().put(SellerVocabulary.COMMAND.name(), SellerVocabulary.DELETE.name());

        SellerServiceInput input = new SellerServiceInput();
        input.setInfo(info);
        input.setData(seller);

        Service<SellerServiceInput, SellerServiceOutput, SellerServiceParams, DefaultPersist> service = new SellerServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new SellerServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Find Seller
    public String findSeller() {
        slf4jLogger.info("Find Seller - ");
        Seller seller = new Seller ();
        seller.setId(request.params(":id"));
        slf4jLogger.info("request param: " + seller.toJson());
        
        String output = "";
        // This is real mandatory code to move in controller
        SellerServiceParams params = new SellerServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(SellerVocabulary.COLLECTION.name(), "Seller");
        params.getProperties().put(SellerVocabulary.COMMAND.name(), SellerVocabulary.RETRIEVE.name());

        SellerServiceInput input = new SellerServiceInput();
        input.setData(seller);
        input.setInfo(info);

        Service<SellerServiceInput, SellerServiceOutput, SellerServiceParams, DefaultPersist> service = new SellerServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new SellerServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    /**
     *
     * @param params
     * @param persist
     */
    private void initSellerServiceEnv(SellerServiceParams params, DefaultPersist persist) {
        if (!request.session().isNew()
                && request.session().attribute(SellerVocabulary.MONGODB.name()) != null) {
        } else {
            params.initEnv("environment.properties");
            request.session().attribute(SellerVocabulary.MONGODB.name(), params.getEnv());
//            try {
//                //        }
//                HazelcastInstance db = persist.initEnv("environment.properties");
//            } catch (HazelcastException ex) {
//
//            }
//            request.session().attribute(SellerVocabulary.HAZELCAST.name(), persist.getEnv());
        }

        params.setEnv(request.session().attribute(SellerVocabulary.MONGODB.name()));
        persist.setEnv(request.session().attribute(SellerVocabulary.HAZELCAST.name()));
    }
}
