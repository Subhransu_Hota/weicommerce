/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Product;
import com.rokittech.container.ecom.product.defaults.ProductServiceInput;
import com.rokittech.container.ecom.product.defaults.ProductServiceOutput;
import com.rokittech.container.ecom.product.defaults.ProductServiceParams;
import com.rokittech.container.ecom.product.defaults.ProductVocabulary;
import com.rokittech.container.ecom.product.factory.ProductServiceFactory;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author alexmy
 */
public class ProductController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(ProductController.class);
    private final Request request;
    private final Response response;
    
    private static final String DB_CATALOG = "Catalog";
    private static final String DB_PRODUCT = "Product";
    private final Info info = new Info (); 
        
    public ProductController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
    public String createProduct() {
        slf4jLogger.info("Creating product - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        // This is real mandatory code to move in controller
        ProductServiceParams params = new ProductServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(ProductVocabulary.COLLECTION.name(), DB_PRODUCT);
        params.getProperties().put(ProductVocabulary.COMMAND.name(), ProductVocabulary.INSERT.name());

        ProductServiceInput input = new ProductServiceInput().fromJson(requestJson);

        Service<ProductServiceInput, ProductServiceOutput, ProductServiceParams, DefaultPersist> service = new ProductServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ProductServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Creaetd product - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    public String deleteProduct() {
        slf4jLogger.info("Deleting product - ");
        Product product = new Product ();
        product.setId(request.params(":id"));
        
        slf4jLogger.info("request param: " + product.toJson());
        
        ProductServiceParams params = new ProductServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(ProductVocabulary.COLLECTION.name(), DB_PRODUCT);
        params.getProperties().put(ProductVocabulary.COMMAND.name(), ProductVocabulary.DELETE.name());
        
        ProductServiceInput input = new ProductServiceInput();
        input.setData(product);
        input.setInfo(info);

        Service<ProductServiceInput, ProductServiceOutput, ProductServiceParams, DefaultPersist> service = new ProductServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ProductServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Deleted product - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    public String getProductById() {
        slf4jLogger.info("Deleting product - ");
        Product product = new Product ();
        product.setId(request.params(":id"));
        
        slf4jLogger.info("request param: " + product.toJson());
        
        // This is real mandatory code to move in controller
        ProductServiceParams params = new ProductServiceParams();
        DefaultPersist persist = new DefaultPersist();

        ProductServiceInput input = new ProductServiceInput();
        input.setInfo(info);
        input.setData(product);
        
        params.getProperties().put(ProductVocabulary.COLLECTION.name(), DB_PRODUCT);
        params.getProperties().put(ProductVocabulary.COMMAND.name(), ProductVocabulary.RETRIEVE.name());

        Service<ProductServiceInput, ProductServiceOutput, ProductServiceParams, DefaultPersist> service = new ProductServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ProductServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("got priduct - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    public String updateProduct () {
        slf4jLogger.info("Updating product - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        ProductServiceParams params = new ProductServiceParams();
        DefaultPersist persist = new DefaultPersist();

        ProductServiceInput input = new ProductServiceInput().fromJson(requestJson);

        params.getProperties().put(ProductVocabulary.COLLECTION.name(), DB_PRODUCT);
        params.getProperties().put(ProductVocabulary.COMMAND.name(), ProductVocabulary.UPDATE.name());

        Service<ProductServiceInput, ProductServiceOutput, ProductServiceParams, DefaultPersist> service = new ProductServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ProductServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("updated product - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    
    /**
     *
     * @param params
     * @param persist
     */
    private void initProductServiceEnv(ProductServiceParams params, DefaultPersist persist) {
        persist.setEnv(request.session().attribute(ProductVocabulary.HAZELCAST.name()));
    }

    public String listProduct() {
        slf4jLogger.info("List product - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        ProductServiceParams params = new ProductServiceParams();
        DefaultPersist persist = new DefaultPersist();

        ProductServiceInput input = new ProductServiceInput();
        if ((null != requestJson) || !requestJson.isEmpty()){
            input = new ProductServiceInput().fromJson(requestJson);
        }else{
            input.setInfo(info);
        }
        
        params.getProperties().put(ProductVocabulary.COLLECTION.name(), DB_PRODUCT);
        params.getProperties().put(ProductVocabulary.COMMAND.name(), ProductVocabulary.LIST.name());

        Service<ProductServiceInput, ProductServiceOutput, ProductServiceParams, DefaultPersist> service = 
                new ProductServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ProductServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("list product - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
}
