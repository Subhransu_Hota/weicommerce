/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.hazelcast.core.HazelcastInstance;
import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Pricing;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceInput;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceOutput;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceParams;
import com.rokittech.container.ecom.pricing.defaults.PricingVocabulary;
import com.rokittech.container.ecom.pricing.factory.PricingServiceFactory;
import com.rokittech.containers.api.core.Service;
import com.rokittech.containers.api.exceptions.HazelcastException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author alexmy
 */
public class PricingController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PricingController.class);
   
    private static final String DB_PRICING = "Pricing";
    
    private final Request request;
    private final Response response;
    
    private final Info info = new Info (); 
    public PricingController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");

    }

    /**
     *
     * @return
     */
    public String createPricing() {
        slf4jLogger.info("Creating pricing - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        // This is real mandatory code to move in controller
        PricingServiceParams params = new PricingServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(PricingVocabulary.COLLECTION.name(), DB_PRICING);
        params.getProperties().put(PricingVocabulary.COMMAND.name(), PricingVocabulary.INSERT.name());

        PricingServiceInput input = new PricingServiceInput().fromJson(requestJson);

        Service<PricingServiceInput, PricingServiceOutput, PricingServiceParams, DefaultPersist> service = new PricingServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PricingServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Creaetd pricing - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    public String deletePricing () {
        slf4jLogger.info("Deleting catalog - ");
        Pricing pricing = new Pricing ();
        pricing.setId(request.params("id"));
        slf4jLogger.info("request param: " + pricing.toJson());
        
        PricingServiceParams params = new PricingServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(PricingVocabulary.COLLECTION.name(), DB_PRICING);
        params.getProperties().put(PricingVocabulary.COMMAND.name(), PricingVocabulary.DELETE.name());
        
        PricingServiceInput input = new PricingServiceInput();
        input.setData(pricing);
        input.setInfo(info);

        Service<PricingServiceInput, PricingServiceOutput, PricingServiceParams, DefaultPersist> service = new PricingServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PricingServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Deleted pricing - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    public String getPricingById() {
        slf4jLogger.info("get Pricing - ");
        Pricing pricing = new Pricing ();
        pricing.setId(request.params("id"));
        slf4jLogger.info("request param: " + pricing.toJson());
         
        // This is real mandatory code to move in controller
        PricingServiceParams params = new PricingServiceParams();
        DefaultPersist persist = new DefaultPersist();

        PricingServiceInput input = new PricingServiceInput();
        input.setData(pricing);
        input.setInfo(info);
        
        params.getProperties().put(PricingVocabulary.COLLECTION.name(), DB_PRICING);
        params.getProperties().put(PricingVocabulary.COMMAND.name(), PricingVocabulary.RETRIEVE.name());

        Service<PricingServiceInput, PricingServiceOutput, PricingServiceParams, DefaultPersist> service = new PricingServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PricingServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("got pricing - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    public String updatePricing () {
        slf4jLogger.info("Updating pricing - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        PricingServiceParams params = new PricingServiceParams();
        DefaultPersist persist = new DefaultPersist();

        PricingServiceInput input = new PricingServiceInput().fromJson(requestJson);

        params.getProperties().put(PricingVocabulary.COLLECTION.name(), DB_PRICING);
        params.getProperties().put(PricingVocabulary.COMMAND.name(), PricingVocabulary.UPDATE.name());

        Service<PricingServiceInput, PricingServiceOutput, PricingServiceParams, DefaultPersist> service = new PricingServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PricingServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("updated pricing - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    
    public String listPricing() {
        slf4jLogger.info("List pricing - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        PricingServiceParams params = new PricingServiceParams();
        DefaultPersist persist = new DefaultPersist();

        PricingServiceInput input = new PricingServiceInput();
        if ((null != requestJson) || !requestJson.isEmpty()){
            input = new PricingServiceInput().fromJson(requestJson);
        }else{
            input.setInfo(info);
        }
        
        params.getProperties().put(PricingVocabulary.COLLECTION.name(), DB_PRICING);
        params.getProperties().put(PricingVocabulary.COMMAND.name(), PricingVocabulary.LIST.name());

        Service<PricingServiceInput, PricingServiceOutput, PricingServiceParams, DefaultPersist> service = 
                new PricingServiceFactory(PricingVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PricingServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("list pricing - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    
    /**
     *
     * @param params
     * @param persist
     */
    private void initPricingServiceEnv(PricingServiceParams params, DefaultPersist persist) {
        if (!request.session().isNew()
                && request.session().attribute(PricingVocabulary.MONGODB.name()) != null) {
        } else {
            params.initEnv("environment.properties");
            request.session().attribute(PricingVocabulary.MONGODB.name(), params.getEnv());
            try {
                //        }
                HazelcastInstance db = persist.initEnv("environment.properties");
            } catch (HazelcastException ex) {

            }
            request.session().attribute(PricingVocabulary.HAZELCAST.name(), persist.getEnv());
        }

        params.setEnv(request.session().attribute(PricingVocabulary.MONGODB.name()));
        persist.setEnv(request.session().attribute(PricingVocabulary.HAZELCAST.name()));
    }
}
