/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.commons.models.UserType;
import com.rokittech.container.ecom.user.defaults.UserServiceInput;
import com.rokittech.container.ecom.user.defaults.UserServiceOutput;
import com.rokittech.container.ecom.user.defaults.UserServiceParams;
import com.rokittech.container.ecom.user.defaults.UserVocabulary;
import com.rokittech.container.ecom.user.factory.UserServiceFactory;
import com.rokittech.containers.api.core.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class CustomerController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(UserController.class);
    private final Request request;
    private final Response response;

    private Info info = new Info (); 
    
    public CustomerController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
// User CRUD
    //Insert Customer
    public String createCustomer() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request..");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.INSERT.name());

        UserServiceInput input = new UserServiceInput().fromJson(requestJson);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        input.getData().setUserType(UserType.Customer);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Update Customer
    public String updateCustomer() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request..");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.UPDATE.name());

        UserServiceInput input = new UserServiceInput().fromJson(requestJson);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        input.getData().setUserType(UserType.Customer);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Delete Customer
    public String deleteCustomer() {
        slf4jLogger.info("Deleting User - ");
        User user = new User ();
        user.setId(request.params(":id"));
        slf4jLogger.info("request param: " + user.toJson());
         
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.DELETE.name());

        UserServiceInput input = new UserServiceInput();
        input.setInfo(info);
        input.setData(user);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Find Customer
    public String findCustomer() {
        slf4jLogger.info("Find User - ");
        User user = new User ();
        user.setId(request.params(":id"));
        slf4jLogger.info("request param: " + user.toJson());
        
        String output = "";
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.RETRIEVE.name());

        UserServiceInput input = new UserServiceInput();
        input.setData(user);
        input.setInfo(info);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }
    
    //Find Customer
    public String listCustomer() {
        slf4jLogger.info("Find User - ");
        User user = new User ();
        slf4jLogger.info("request param: " + user.toJson());
        
        String output = "";
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.LIST.name());

        UserServiceInput input = new UserServiceInput();
        user.setUserType(UserType.Customer);
        input.setData(user);
        input.setInfo(info);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    /**
     *
     * @param params
     * @param persist
     */
    private void initUserServiceEnv(UserServiceParams params, DefaultPersist persist) {
        if (!request.session().isNew()
                && request.session().attribute(UserVocabulary.MONGODB.name()) != null) {
        } else {
            params.initEnv("environment.properties");
            request.session().attribute(UserVocabulary.MONGODB.name(), params.getEnv());
//            try {
//                //        }
//                HazelcastInstance db = persist.initEnv("environment.properties");
//            } catch (HazelcastException ex) {
//
//            }
//            request.session().attribute(UserVocabulary.HAZELCAST.name(), persist.getEnv());
        }

        params.setEnv(request.session().attribute(UserVocabulary.MONGODB.name()));
        persist.setEnv(request.session().attribute(UserVocabulary.HAZELCAST.name()));
    }

}
