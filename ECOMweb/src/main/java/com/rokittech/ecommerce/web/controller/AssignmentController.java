/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.attribute.defaults.AttributeServiceInput;
import com.rokittech.container.ecom.attribute.defaults.AttributeServiceOutput;
import com.rokittech.container.ecom.attribute.defaults.AttributeServiceParams;
import com.rokittech.container.ecom.attribute.defaults.AttributeVocabulary;
import com.rokittech.container.ecom.commons.models.ProductCatalogAssignment;
import com.rokittech.container.ecom.commons.models.ProductPriceAssignment;
import com.rokittech.container.ecom.product.defaults.ProductVocabulary;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceOutput;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceParams;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceInput;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentVocabulary;
import com.rokittech.container.ecom.productcatalogassignment.factory.ProductCatalogAssignmentServiceFactory;
import com.rokittech.container.ecom.productpriceassignment.defaults.ProductPriceAssignmentServiceInput;
import com.rokittech.container.ecom.productpriceassignment.defaults.ProductPriceAssignmentServiceOutput;
import com.rokittech.container.ecom.productpriceassignment.defaults.ProductPriceAssignmentServiceParams;
import com.rokittech.container.ecom.productpriceassignment.defaults.ProductPriceAssignmentVocabulary;
import com.rokittech.container.ecom.productpriceassignment.factory.ProductPriceAssignmentServiceFactory;
import com.rokittech.containers.api.core.Service;
import com.rokittech.ecommerce.web.userobjects.AssignmentAttribute;
import com.rokittech.ecommerce.web.userobjects.AssignmentAttributeInput;
import com.rokittech.ecommerce.web.userobjects.AssignmentString;
import com.rokittech.ecommerce.web.userobjects.AssignmentStringInput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author alexmy
 */
public class AssignmentController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(AssignmentController.class);
    private final Request request;
    private final Response response;
    
    private static final String DB_PRODUCT_CATALOG_ASSIGN = "ProductCatalogAssignment";
    private static final String DB_ATTRIBUTE = "Attribute";
    
    public AssignmentController(Request request, Response response) {
        this.request = request;
        this.response = response;
    }

    /**
     * 
     * @return 
     */
    public String assignProduct() {
        slf4jLogger.info("Assigning product - ");
        
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        AssignmentStringInput assignmentObjectInput = new AssignmentStringInput().fromJson(requestJson);
        AssignmentString assignmentObject = assignmentObjectInput.getData();
        List<String> output = new ArrayList<> ();
        assignmentObject.getAssignments().stream().map((assignmentId) -> {
            // This is real mandatory code to move in controller
            ProductCatalogAssignmentServiceParams params = new ProductCatalogAssignmentServiceParams();
            DefaultPersist persist = new DefaultPersist();
            params.getProperties().put(ProductCatalogAssignmentVocabulary.COLLECTION.name(), DB_PRODUCT_CATALOG_ASSIGN);
            params.getProperties().put(ProductCatalogAssignmentVocabulary.COMMAND.name(), ProductCatalogAssignmentVocabulary.INSERT.name());
            ProductCatalogAssignmentServiceInput input = new ProductCatalogAssignmentServiceInput();
            ProductCatalogAssignment pcAssignment = new ProductCatalogAssignment();
            pcAssignment.setProductId(assignmentObject.getId());
            pcAssignment.setCatalogId((String) assignmentId);
            input.setData(pcAssignment);
            input.setInfo(assignmentObjectInput.getInfo());
            Service<ProductCatalogAssignmentServiceInput, ProductCatalogAssignmentServiceOutput, ProductCatalogAssignmentServiceParams, DefaultPersist> service 
                    = new ProductCatalogAssignmentServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();
            service.setParams(params);
            service.setPersist(persist);
            service.setInput(input);
            return service;
        }).map((service) -> {
            service.setOutput(new ProductCatalogAssignmentServiceOutput());
            return service;
        }).forEach((service) -> {
            // Run service and get output data.
            //
            output.add(service.serve().toJson());
        });
       

        slf4jLogger.info("Creaetd product - ");
        slf4jLogger.debug("Response " + Arrays.toString(output.toArray()));
        
        return Arrays.toString(output.toArray());
    }

    public String unAssignProduct() {
        slf4jLogger.info("unassigning product - ");
        
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        AssignmentStringInput assignmentObjectInput = new AssignmentStringInput().fromJson(requestJson);
        AssignmentString assignmentObject = assignmentObjectInput.getData();
        List<String> output = new ArrayList<> ();
        assignmentObject.getAssignments().stream().map((assignmentId) -> {
            // This is real mandatory code to move in controller
            ProductCatalogAssignmentServiceParams params = new ProductCatalogAssignmentServiceParams();
            DefaultPersist persist = new DefaultPersist();
            params.getProperties().put(ProductCatalogAssignmentVocabulary.COLLECTION.name(), DB_PRODUCT_CATALOG_ASSIGN);
            params.getProperties().put(ProductCatalogAssignmentVocabulary.COMMAND.name(), ProductCatalogAssignmentVocabulary.DELETE.name());
            ProductCatalogAssignmentServiceInput input = new ProductCatalogAssignmentServiceInput();
            ProductCatalogAssignment pcAssignment = new ProductCatalogAssignment();
            pcAssignment.setProductId(assignmentObject.getId());
            pcAssignment.setCatalogId((String) assignmentId);
            input.setData(pcAssignment);
            input.setInfo(assignmentObjectInput.getInfo());
            Service<ProductCatalogAssignmentServiceInput, ProductCatalogAssignmentServiceOutput, 
                    ProductCatalogAssignmentServiceParams, DefaultPersist> service 
                    = new ProductCatalogAssignmentServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();
            service.setParams(params);
            service.setPersist(persist);
            service.setInput(input);
            return service;
        }).map((service) -> {
            service.setOutput(new ProductCatalogAssignmentServiceOutput());
            return service;
        }).forEach((service) -> {
            // Run service and get output data.
            //
            output.add(service.serve().toJson());
        });
       

        slf4jLogger.info("unassigned product - ");
        slf4jLogger.debug("Response " + Arrays.toString(output.toArray()));
        
        return Arrays.toString(output.toArray());
    }

    public String assignCatalog() {
        slf4jLogger.info("Assigning catalog - ");
        
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        AssignmentStringInput assignmentObjectInput = new AssignmentStringInput().fromJson(requestJson);
        AssignmentString assignmentObject = assignmentObjectInput.getData();
        List<String> output = new ArrayList<> ();
        assignmentObject.getAssignments().stream().map((assignmentId) -> {
            // This is real mandatory code to move in controller
            ProductCatalogAssignmentServiceParams params = new ProductCatalogAssignmentServiceParams();
            DefaultPersist persist = new DefaultPersist();
            params.getProperties().put(ProductCatalogAssignmentVocabulary.COLLECTION.name(), DB_PRODUCT_CATALOG_ASSIGN);
            params.getProperties().put(ProductCatalogAssignmentVocabulary.COMMAND.name(), ProductCatalogAssignmentVocabulary.INSERT.name());
            ProductCatalogAssignmentServiceInput input = new ProductCatalogAssignmentServiceInput();
            ProductCatalogAssignment pcAssignment = new ProductCatalogAssignment();
            pcAssignment.setCatalogId(assignmentObject.getId());
            pcAssignment.setProductId((String) assignmentId);
            input.setData(pcAssignment);
            input.setInfo(assignmentObjectInput.getInfo());
            Service<ProductCatalogAssignmentServiceInput, ProductCatalogAssignmentServiceOutput, ProductCatalogAssignmentServiceParams, DefaultPersist> service 
                    = new ProductCatalogAssignmentServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();
            service.setParams(params);
            service.setPersist(persist);
            service.setInput(input);
            return service;
        }).map((service) -> {
            service.setOutput(new ProductCatalogAssignmentServiceOutput());
            return service;
        }).forEach((service) -> {
            // Run service and get output data.
            //
            output.add(service.serve().toJson());
        });
       

        slf4jLogger.info("assigned catalog - ");
        slf4jLogger.debug("Response " + Arrays.toString(output.toArray()));
        
        return Arrays.toString(output.toArray());
    }

    public String unAssignCatalog() {
        slf4jLogger.info("Unassigning catalog - ");
        
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        AssignmentStringInput assignmentObjectInput = new AssignmentStringInput().fromJson(requestJson);
        AssignmentString assignmentObject = assignmentObjectInput.getData();
        List<String> output = new ArrayList<> ();
        assignmentObject.getAssignments().stream().map((assignmentId) -> {
            // This is real mandatory code to move in controller
            ProductCatalogAssignmentServiceParams params = new ProductCatalogAssignmentServiceParams();
            DefaultPersist persist = new DefaultPersist();
            params.getProperties().put(ProductCatalogAssignmentVocabulary.COLLECTION.name(), DB_PRODUCT_CATALOG_ASSIGN);
            params.getProperties().put(ProductCatalogAssignmentVocabulary.COMMAND.name(), ProductCatalogAssignmentVocabulary.DELETE.name());
            ProductCatalogAssignmentServiceInput input = new ProductCatalogAssignmentServiceInput();
            ProductCatalogAssignment pcAssignment = new ProductCatalogAssignment();
            pcAssignment.setCatalogId(assignmentObject.getId());
            pcAssignment.setProductId((String) assignmentId);
            input.setData(pcAssignment);
            input.setInfo(assignmentObjectInput.getInfo());
            Service<ProductCatalogAssignmentServiceInput, ProductCatalogAssignmentServiceOutput, ProductCatalogAssignmentServiceParams, DefaultPersist> service 
                    = new ProductCatalogAssignmentServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();
            service.setParams(params);
            service.setPersist(persist);
            service.setInput(input);
            return service;
        }).map((service) -> {
            service.setOutput(new ProductCatalogAssignmentServiceOutput());
            return service;
        }).forEach((service) -> {
            // Run service and get output data.
            //
            output.add(service.serve().toJson());
        });
       

        slf4jLogger.info("unassigned catalog - ");
        slf4jLogger.debug("Response " + Arrays.toString(output.toArray()));
        
        return Arrays.toString(output.toArray());
    }
    
    public String assignAttributes() {
        slf4jLogger.info("Assigning catalog - ");
        
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        AssignmentAttributeInput assignmentAttributeInput = new AssignmentAttributeInput().fromJson(requestJson);
        AssignmentAttribute assignmentAttribute = assignmentAttributeInput.getData();
        List<String> output = new ArrayList<> ();
        assignmentAttribute.getAssignments().stream().map((assignment) -> {
            // This is real mandatory code to move in controller
            AttributeServiceParams params = new AttributeServiceParams();
            DefaultPersist persist = new DefaultPersist();
            params.getProperties().put(AttributeVocabulary.COLLECTION.name(), DB_ATTRIBUTE);
            params.getProperties().put(AttributeVocabulary.COMMAND.name(), AttributeVocabulary.INSERT.name());
            AttributeServiceInput input = new AttributeServiceInput();
            assignment.setAttributeGroupId(assignmentAttributeInput.getData().getAssignmentId());
            input.setInfo(assignmentAttributeInput.getInfo());
            Service<AttributeServiceInput, AttributeServiceOutput, AttributeServiceParams, DefaultPersist> service 
                    = new ProductCatalogAssignmentServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();
            service.setParams(params);
            service.setPersist(persist);
            service.setInput(input);
            return service;
        }).map((service) -> {
            service.setOutput(new AttributeServiceOutput());
            return service;
        }).forEach((service) -> {
            // Run service and get output data.
            //
            output.add(service.serve().toJson());
        });
       

        slf4jLogger.info("assigned catalog - ");
        slf4jLogger.debug("Response " + Arrays.toString(output.toArray()));
        
        return Arrays.toString(output.toArray());
    }

    public String unAssignAttributes() {
        slf4jLogger.info("Assigning catalog - ");
        
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        AssignmentAttributeInput assignmentAttributeInput = new AssignmentAttributeInput().fromJson(requestJson);
        AssignmentAttribute assignmentAttribute = assignmentAttributeInput.getData();
        List<String> output = new ArrayList<> ();
        assignmentAttribute.getAssignments().stream().map((assignment) -> {
            // This is real mandatory code to move in controller
            AttributeServiceParams params = new AttributeServiceParams();
            DefaultPersist persist = new DefaultPersist();
            params.getProperties().put(AttributeVocabulary.COLLECTION.name(), DB_ATTRIBUTE);
            params.getProperties().put(AttributeVocabulary.COMMAND.name(), AttributeVocabulary.DELETE.name());
            AttributeServiceInput input = new AttributeServiceInput();
            assignment.setAttributeGroupId(assignmentAttributeInput.getData().getAssignmentId());
            input.setInfo(assignmentAttributeInput.getInfo());
            Service<AttributeServiceInput, AttributeServiceOutput, AttributeServiceParams, DefaultPersist> service 
                    = new ProductCatalogAssignmentServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();
            service.setParams(params);
            service.setPersist(persist);
            service.setInput(input);
            return service;
        }).map((service) -> {
            service.setOutput(new AttributeServiceOutput());
            return service;
        }).forEach((service) -> {
            // Run service and get output data.
            //
            output.add(service.serve().toJson());
        });
       

        slf4jLogger.info("assigned catalog - ");
        slf4jLogger.debug("Response " + Arrays.toString(output.toArray()));
        
        return Arrays.toString(output.toArray());
    }
    
    /**
     * 
     * @return 
     */
    public String assignPrice() {
        slf4jLogger.info("Assigning Price - ");
        
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        AssignmentStringInput assignmentObjectInput = new AssignmentStringInput().fromJson(requestJson);
        AssignmentString assignmentObject = assignmentObjectInput.getData();
        List<String> output = new ArrayList<> ();
        assignmentObject.getAssignments().stream().map((assignmentId) -> {
            // This is real mandatory code to move in controller
            ProductPriceAssignmentServiceParams params = new ProductPriceAssignmentServiceParams();
            DefaultPersist persist = new DefaultPersist();
            params.getProperties().put(ProductPriceAssignmentVocabulary.COLLECTION.name(), DB_PRODUCT_CATALOG_ASSIGN);
            params.getProperties().put(ProductPriceAssignmentVocabulary.COMMAND.name(), ProductPriceAssignmentVocabulary.INSERT.name());
            ProductPriceAssignmentServiceInput input = new ProductPriceAssignmentServiceInput();
            ProductPriceAssignment pcAssignment = new ProductPriceAssignment();
            pcAssignment.setPriceId(assignmentObject.getId());
            pcAssignment.setProductId((String) assignmentId);
            input.setData(pcAssignment);
            input.setInfo(assignmentObjectInput.getInfo());
            Service<ProductPriceAssignmentServiceInput, ProductPriceAssignmentServiceOutput, ProductPriceAssignmentServiceParams, DefaultPersist> service 
                    = new ProductPriceAssignmentServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();
            service.setParams(params);
            service.setPersist(persist);
            service.setInput(input);
            return service;
        }).map((service) -> {
            service.setOutput(new ProductPriceAssignmentServiceOutput());
            return service;
        }).forEach((service) -> {
            // Run service and get output data.
            //
            output.add(service.serve().toJson());
        });
       

        slf4jLogger.info("assigned Price - ");
        slf4jLogger.debug("Response " + Arrays.toString(output.toArray()));
        
        return Arrays.toString(output.toArray());
    }
    
    
    public String unAssignPrice() {
        slf4jLogger.info("Unassigning price - ");
        
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        AssignmentStringInput assignmentObjectInput = new AssignmentStringInput().fromJson(requestJson);
        AssignmentString assignmentObject = assignmentObjectInput.getData();
        List<String> output = new ArrayList<> ();
        assignmentObject.getAssignments().stream().map((assignmentId) -> {
            // This is real mandatory code to move in controller
            ProductPriceAssignmentServiceParams params = new ProductPriceAssignmentServiceParams();
            DefaultPersist persist = new DefaultPersist();
            params.getProperties().put(ProductPriceAssignmentVocabulary.COLLECTION.name(), DB_PRODUCT_CATALOG_ASSIGN);
            params.getProperties().put(ProductPriceAssignmentVocabulary.COMMAND.name(), ProductPriceAssignmentVocabulary.DELETE.name());
            ProductPriceAssignmentServiceInput input = new ProductPriceAssignmentServiceInput();
            ProductPriceAssignment pcAssignment = new ProductPriceAssignment();
            pcAssignment.setPriceId(assignmentObject.getId());
            pcAssignment.setProductId((String) assignmentId);
            input.setData(pcAssignment);
            input.setInfo(assignmentObjectInput.getInfo());
            Service<ProductPriceAssignmentServiceInput, ProductPriceAssignmentServiceOutput, ProductPriceAssignmentServiceParams, DefaultPersist> service 
                    = new ProductPriceAssignmentServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();
            service.setParams(params);
            service.setPersist(persist);
            service.setInput(input);
            return service;
        }).map((service) -> {
            service.setOutput(new ProductPriceAssignmentServiceOutput());
            return service;
        }).forEach((service) -> {
            // Run service and get output data.
            //
            output.add(service.serve().toJson());
        });
       

        slf4jLogger.info("unassigned price - ");
        slf4jLogger.debug("Response " + Arrays.toString(output.toArray()));
        
        return Arrays.toString(output.toArray());
    }
}
