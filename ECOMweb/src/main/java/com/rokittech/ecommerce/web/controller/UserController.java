/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.commons.models.UserType;
import com.rokittech.container.ecom.user.defaults.UserServiceInput;
import com.rokittech.container.ecom.user.defaults.UserServiceOutput;
import com.rokittech.container.ecom.user.defaults.UserServiceParams;
import com.rokittech.container.ecom.user.defaults.UserVocabulary;
import com.rokittech.container.ecom.user.factory.UserServiceFactory;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class UserController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(UserController.class);
    private final Request request;
    private final Response response;
    private Info info = new Info ();
    
    public UserController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
    // User CRUD
    //Insert User
    public String createUser() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.info (" Output Response .." + output);
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" Output Response .." + output);
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.INSERT.name());

        UserServiceInput input = new UserServiceInput().fromJson(requestJson);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        input.getData().setUserType(UserType.BackOfficeUser);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Update User
    public String updateUser() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request..");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" Output Response .." + output);
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.UPDATE.name());

        UserServiceInput input = new UserServiceInput().fromJson(requestJson);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        input.getData().setUserType(UserType.BackOfficeUser);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Delete User

    public String deleteUser() {
        slf4jLogger.info("Deleting user - ");
        User user = new User ();
        user.setId(request.params(":id"));
        slf4jLogger.info("request param: " + user.toJson());
        
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.DELETE.name());

        UserServiceInput input = new UserServiceInput();
        user.setUserType(UserType.BackOfficeUser);
        input.setData(user);
        input.setInfo(info);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Find User

    public String findUser() {
        slf4jLogger.info("Finding user - ");
        User user = new User ();
        user.setId(request.params(":id"));
        slf4jLogger.info("request param: " + user.toJson());
        
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.RETRIEVE.name());

        UserServiceInput input = new UserServiceInput();
        user.setUserType(UserType.Customer);
        input.setData(user);
        
        input.setInfo(info);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }
    
    //Find User with roles

    public String findUserWithRoles() {
        slf4jLogger.info("Finding user with roles  - ");
    
        User user = new User ();
        user.setId(request.params(":id"));
        slf4jLogger.info("request param: " + user.toJson());
        
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.RETRIEVE_WITH_ROLES.name());

        UserServiceInput input = new UserServiceInput();
        user.setUserType(UserType.Customer);
        input.setData(user);
        input.setInfo(info);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }
    
    public String listUser() {
        slf4jLogger.info("Finding user - ");
        User user = new User ();
        user.setId(request.params(":id"));
        slf4jLogger.info("request param: " + user.toJson());
        
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.LIST.name());

        UserServiceInput input = new UserServiceInput();
        user.setUserType(UserType.BackOfficeUser);
        input.setData(user);
        input.setInfo(info);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    /**
     *
     * @param params
     * @param persist
     */
    private void initUserServiceEnv(UserServiceParams params, DefaultPersist persist) {
        if (!request.session().isNew()
                && request.session().attribute(UserVocabulary.MONGODB.name()) != null) {
        } else {
            params.initEnv("environment.properties");
            request.session().attribute(UserVocabulary.MONGODB.name(), params.getEnv());
//            try {
//                //        }
//                HazelcastInstance db = persist.initEnv("environment.properties");
//            } catch (HazelcastException ex) {
//
//            }
//            request.session().attribute(UserVocabulary.HAZELCAST.name(), persist.getEnv());
        }

        params.setEnv(request.session().attribute(UserVocabulary.MONGODB.name()));
        persist.setEnv(request.session().attribute(UserVocabulary.HAZELCAST.name()));
    }

}
