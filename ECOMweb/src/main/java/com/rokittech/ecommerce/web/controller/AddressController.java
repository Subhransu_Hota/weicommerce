/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.address.defaults.AddressServiceInput;
import com.rokittech.container.ecom.address.defaults.AddressServiceOutput;
import com.rokittech.container.ecom.address.defaults.AddressServiceParams;
import com.rokittech.container.ecom.address.defaults.AddressVocabulary;
import com.rokittech.container.ecom.address.factory.AddressServiceFactory;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class AddressController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PricingController.class);
    private final Request request;
    private final Info info = new Info ();
    
    public AddressController(Request request, Response response) {
        this.request = request;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
    // Address CRUD
    //Insert Address
    public String createAddress() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error("Invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        slf4jLogger.info("Input request .." + requestJson);
        // This is real mandatory code to move in controller
        AddressServiceParams params = new AddressServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AddressVocabulary.COLLECTION.name(), "Address");
        params.getProperties().put(AddressVocabulary.COMMAND.name(), AddressVocabulary.INSERT.name());

        AddressServiceInput input = new AddressServiceInput().fromJson(requestJson);

        Service<AddressServiceInput, AddressServiceOutput, AddressServiceParams, DefaultPersist> service = new AddressServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AddressServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        slf4jLogger.info("Output Response .." + output);

        return output;
    }

    //Update Address
    public String updateAddress() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error("Invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        slf4jLogger.info("Input request ..." + requestJson);
        // This is real mandatory code to move in controller
        AddressServiceParams params = new AddressServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AddressVocabulary.COLLECTION.name(), "Address");
        params.getProperties().put(AddressVocabulary.COMMAND.name(), AddressVocabulary.UPDATE.name());

        AddressServiceInput input = new AddressServiceInput().fromJson(requestJson);

        Service<AddressServiceInput, AddressServiceOutput, AddressServiceParams, DefaultPersist> service = new AddressServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AddressServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        slf4jLogger.info("Output Response .." + output);

        return output;
    }

    //Delete Address

    public String deleteAddress() {
        slf4jLogger.info("Deleting user - ");
        Address user = new Address ();
        user.setAddressId(request.params(":id"));
        slf4jLogger.info("request param: " + user.toJson());
        
        // This is real mandatory code to move in controller
        AddressServiceParams params = new AddressServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AddressVocabulary.COLLECTION.name(), "Address");
        params.getProperties().put(AddressVocabulary.COMMAND.name(), AddressVocabulary.DELETE.name());

        AddressServiceInput input = new AddressServiceInput();
        input.setData(user);
        input.setInfo(info);

        Service<AddressServiceInput, AddressServiceOutput, AddressServiceParams, DefaultPersist> service = new AddressServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AddressServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        //TODO replace  log
        slf4jLogger.info("Output Response .." + output);

        return output;
    }

    //Find Address

    public String findAddress() {
        slf4jLogger.info("Finding user - ");
        Address user = new Address ();
        user.setAddressId(request.params(":id"));
        slf4jLogger.info("request param: " + user.toJson());
        
        // This is real mandatory code to move in controller
        AddressServiceParams params = new AddressServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AddressVocabulary.COLLECTION.name(), "Address");
        params.getProperties().put(AddressVocabulary.COMMAND.name(), AddressVocabulary.RETRIEVE.name());

        AddressServiceInput input = new AddressServiceInput();
        input.setData(user);
        input.setInfo(info);

        Service<AddressServiceInput, AddressServiceOutput, AddressServiceParams, DefaultPersist> service = new AddressServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AddressServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Output Response .." + output);

        return output;
    }

    /**
     *
     * @param params
     * @param persist
     */
    private void initAddressServiceEnv(AddressServiceParams params, DefaultPersist persist) {
        if (!request.session().isNew()
                && request.session().attribute(AddressVocabulary.MONGODB.name()) != null) {
        } else {
            params.initEnv("environment.properties");
            request.session().attribute(AddressVocabulary.MONGODB.name(), params.getEnv());
//            try {
//                //        }
//                HazelcastInstance db = persist.initEnv("environment.properties");
//            } catch (HazelcastException ex) {
//
//            }
//            request.session().attribute(AddressVocabulary.HAZELCAST.name(), persist.getEnv());
        }

        params.setEnv(request.session().attribute(AddressVocabulary.MONGODB.name()));
        persist.setEnv(request.session().attribute(AddressVocabulary.HAZELCAST.name()));
    }

}
