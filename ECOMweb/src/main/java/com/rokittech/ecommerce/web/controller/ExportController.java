package com.rokittech.ecommerce.web.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.hazelcast.core.HazelcastInstance;
import com.mongodb.util.JSON;
import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.props.GetEnvPropValues;
import com.rokittech.container.ecom.exporting.defaults.ExportServiceInput;
import com.rokittech.container.ecom.exporting.defaults.ExportServiceOutput;
import com.rokittech.container.ecom.exporting.defaults.ExportServiceParams;
import com.rokittech.container.ecom.exporting.defaults.ExportVocabulary;
import com.rokittech.container.ecom.exporting.factory.ExportServiceFactory;
import com.rokittech.container.ecom.importing.defaults.ImportServiceInput;
import com.rokittech.container.ecom.importing.defaults.ImportServiceOutput;
import com.rokittech.containers.api.core.Service;
import com.rokittech.containers.api.exceptions.HazelcastException;

import spark.Request;
import spark.Response;

public class ExportController {

	private static final Logger slf4jLogger = LoggerFactory.getLogger(ExportController.class);

	private static String collectionName = "";

	private final Request request;
	private final Response response;

	private final Info info = new Info();

	public ExportController(Request request, Response response) {
		this.request = request;
		this.response = response;

		info.setVersion(1);
		info.setUserId("test1");
		info.setAppId("wei");
	}

	public String exportingCSV() {

		slf4jLogger.info("Prcing CSV file validation - ");

		String requestJson = request.body();
		collectionName = request.pathInfo();
		collectionName = collectionName.substring(1);
		collectionName = collectionName.substring(0, collectionName.indexOf("/"));
		String temp = collectionName.substring(0, 1).toUpperCase();
		collectionName = temp.concat(collectionName.substring(1));
		if (!PersistUtils.validateInput(requestJson)) {
			slf4jLogger.error("invalid request!");
			return "{\"ERROR\":\": invalid request!\"}";
		}

		ExportServiceParams params = new ExportServiceParams();
		ExportServiceInput input = new ExportServiceInput().fromJson(requestJson);

		params.getProperties().put(ExportVocabulary.COLLECTION.name(), collectionName);
		params.getProperties().put(ExportVocabulary.COMMAND.name(), ExportVocabulary.EXPORT.name());
		Service<ExportServiceInput, ExportServiceOutput, ExportServiceParams, DefaultPersist> service = new ExportServiceFactory(
				"WEI_ECOMMERCE", 1).get();

		service.setParams(params);
		service.setInput(input);
		service.setOutput(new ExportServiceOutput());
		String output = service.serve().toJson();

		slf4jLogger.info("CSV validation - ");
		slf4jLogger.debug("Response " + output);
		return output;
	}

	public String dumpCreation() {

		slf4jLogger.info("Importing CSV file into database - ");

		String requestJson = request.body();
		if (!PersistUtils.validateInput(requestJson)) {
			slf4jLogger.error("invalid request!");
			return "{\"ERROR\":\": invalid request!\"}";
		}

		String databaseName = "databasename";
		String mongodbDumpLocation = "mongodbDumpLocation";

		ImportServiceInput input = new ImportServiceInput().fromJson(requestJson);
		ImportServiceOutput output1 = new ImportServiceOutput();

		try {
			GetEnvPropValues prop = new GetEnvPropValues();
			mongodbDumpLocation = prop.getPropValues(mongodbDumpLocation);
			databaseName = prop.getPropValues(databaseName);
			String directory = input.getData().toString();
			directory = directory.substring(directory.indexOf("=") + 1, directory.length() - 1);

			String command = mongodbDumpLocation + " --db " + databaseName + " -o " + directory;
			Process proc = Runtime.getRuntime().exec(command);
			int exitVal = proc.waitFor();
			if (exitVal == 0)
				output1.setData("Mongodb dump created succefully");
			else
				output1.setData("There was a problem with directory location that you have given");

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}

		String output = output1.toJson();

		slf4jLogger.info("CSV validation - ");
		slf4jLogger.debug("Response " + output);
		return output;
	}

	public String getListOfFiles() {

		slf4jLogger.info("List out all the files ");
		String collectionName = "";
		String output = "";
		String s3BucketName = "weicommerce-dev";
		try {

			collectionName = request.pathInfo();
			collectionName = collectionName.substring(1);
			collectionName = collectionName.replace("export", "");
			collectionName = collectionName.substring(1, collectionName.lastIndexOf(("/")));
			String temp1 = collectionName.substring(0, 1).toUpperCase();
			collectionName = temp1.concat(collectionName.substring(1));
			
			AWSCredentials credentials = new BasicAWSCredentials("AKIAI4LX6EPGIBQKIJCA", "eLwTJOHzHPeWOHD9XShWZPk0B5kg60yTz34vxzHM");
			AmazonS3 s3client = new AmazonS3Client(credentials);
			ObjectListing objects = s3client.listObjects(s3BucketName);
			List<String> list = new ArrayList<String>();
	        do {
	            for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
	                String key = objectSummary.getKey();
	                if(key.contains(collectionName)){
	                	key = key.substring(key.lastIndexOf("/")+1);
	                	list.add(key);
	                }
	            }
	            objects = s3client.listNextBatchOfObjects(objects);
	        } while (objects.isTruncated());
	        
	        String[] files = list.toArray(new String[list.size()]);
			output = JSON.serialize(files);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}
	
	
	
	private void initExportServiceEnv(ExportServiceParams params, DefaultPersist persist) {
		if (!request.session().isNew() && request.session().attribute(ExportVocabulary.MONGODB.name()) != null) {
		} else {
			params.initEnv("environment.properties");
			request.session().attribute(ExportVocabulary.MONGODB.name(), params.getEnv());
			try {
				// }
				HazelcastInstance db = persist.initEnv("environment.properties");
			} catch (HazelcastException ex) {

			}
			request.session().attribute(ExportVocabulary.HAZELCAST.name(), persist.getEnv());
		}

		params.setEnv(request.session().attribute(ExportVocabulary.MONGODB.name()));
		persist.setEnv(request.session().attribute(ExportVocabulary.HAZELCAST.name()));
	}

	
}
