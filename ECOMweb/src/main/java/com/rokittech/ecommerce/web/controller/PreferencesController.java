/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Preferences;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceInput;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceOutput;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceParams;
import com.rokittech.container.ecom.preferences.defaults.PreferencesVocabulary;
import com.rokittech.container.ecom.preferences.factory.PreferencesServiceFactory;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author asifahammed
 */
public class PreferencesController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PreferencesController.class);
    private final Request request;
    private final Response response;

    private Info info = new Info (); 
    
    public PreferencesController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
// Preferences CRUD
    //Insert Preferences
    public String createPreferences() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request " + requestJson);
        // This is real mandatory code to move in controller
        PreferencesServiceParams params = new PreferencesServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(PreferencesVocabulary.COLLECTION.name(), "Preferences");
        params.getProperties().put(PreferencesVocabulary.COMMAND.name(), PreferencesVocabulary.INSERT.name());

        PreferencesServiceInput input = new PreferencesServiceInput().fromJson(requestJson);

        Service<PreferencesServiceInput, PreferencesServiceOutput, PreferencesServiceParams, DefaultPersist> service = new PreferencesServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PreferencesServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response " + output);

        return output;
    }

    //Update Preferences
    public String updatePreferences() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request " + requestJson);
        // This is real mandatory code to move in controller
        PreferencesServiceParams params = new PreferencesServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(PreferencesVocabulary.COLLECTION.name(), "Preferences");
        params.getProperties().put(PreferencesVocabulary.COMMAND.name(), PreferencesVocabulary.UPDATE.name());

        PreferencesServiceInput input = new PreferencesServiceInput().fromJson(requestJson);

        Service<PreferencesServiceInput, PreferencesServiceOutput, PreferencesServiceParams, DefaultPersist> service = new PreferencesServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PreferencesServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response " + output);

        return output;
    }

    //Delete Preferences
    public String deletePreferences() {
        slf4jLogger.info("Deleting Preferences - ");
        Preferences preferences = new Preferences ();
        preferences.setId(request.params(":id"));
        slf4jLogger.info("request param: " + preferences.toJson());
         
        // This is real mandatory code to move in controller
        PreferencesServiceParams params = new PreferencesServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(PreferencesVocabulary.COLLECTION.name(), "Preferences");
        params.getProperties().put(PreferencesVocabulary.COMMAND.name(), PreferencesVocabulary.DELETE.name());

        PreferencesServiceInput input = new PreferencesServiceInput();
        input.setInfo(info);
        input.setData(preferences);

        Service<PreferencesServiceInput, PreferencesServiceOutput, PreferencesServiceParams, DefaultPersist> service = new PreferencesServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PreferencesServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response " + output);

        return output;
    }

    //Find Preferences
    public String findPreferences() {
        slf4jLogger.info("Find Preferences - ");
        Preferences preferences = new Preferences ();
        preferences.setId(request.params(":id"));
        slf4jLogger.info("request param: " + preferences.toJson());
        
        String output = "";
        // This is real mandatory code to move in controller
        PreferencesServiceParams params = new PreferencesServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(PreferencesVocabulary.COLLECTION.name(), "Preferences");
        params.getProperties().put(PreferencesVocabulary.COMMAND.name(), PreferencesVocabulary.RETRIEVE.name());

        PreferencesServiceInput input = new PreferencesServiceInput();
        input.setData(preferences);
        input.setInfo(info);

        Service<PreferencesServiceInput, PreferencesServiceOutput, PreferencesServiceParams, DefaultPersist> service = new PreferencesServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PreferencesServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response " + output);

        return output;
    }

    /**
     *
     * @param params
     * @param persist
     */
    private void initPreferencesServiceEnv(PreferencesServiceParams params, DefaultPersist persist) {
        if (!request.session().isNew()
                && request.session().attribute(PreferencesVocabulary.MONGODB.name()) != null) {
        } else {
            params.initEnv("environment.properties");
            request.session().attribute(PreferencesVocabulary.MONGODB.name(), params.getEnv());
//            try {
//                //        }
//                HazelcastInstance db = persist.initEnv("environment.properties");
//            } catch (HazelcastException ex) {
//
//            }
//            request.session().attribute(PreferencesVocabulary.HAZELCAST.name(), persist.getEnv());
        }

        params.setEnv(request.session().attribute(PreferencesVocabulary.MONGODB.name()));
        persist.setEnv(request.session().attribute(PreferencesVocabulary.HAZELCAST.name()));
    }
}
