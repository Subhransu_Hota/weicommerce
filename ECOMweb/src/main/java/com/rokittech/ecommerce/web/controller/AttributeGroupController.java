/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupServiceInput;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupServiceOutput;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupServiceParams;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupVocabulary;
import com.rokittech.container.ecom.attributegroup.factory.AttributeGroupServiceFactory;
import com.rokittech.container.ecom.commons.models.AttributeGroup;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author asifaham
 */
public class AttributeGroupController {
    
    private static final Logger slf4jLogger = LoggerFactory.getLogger(AttributeGroupController.class);
    private final Request request;
    private final Response response;
    private Info info = new Info ();
    
    public AttributeGroupController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }
    
    /**
     *
     * @return
     */
    // AttributeGroup CRUD
    //Insert AttributeGroup
    public String createAttributeGroup() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request..");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        AttributeGroupServiceParams params = new AttributeGroupServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AttributeGroupVocabulary.COLLECTION.name(), "AttributeGroup");
        params.getProperties().put(AttributeGroupVocabulary.COMMAND.name(), AttributeGroupVocabulary.INSERT.name());

        AttributeGroupServiceInput input = new AttributeGroupServiceInput().fromJson(requestJson);

        Service<AttributeGroupServiceInput, AttributeGroupServiceOutput, AttributeGroupServiceParams, DefaultPersist> service = new AttributeGroupServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AttributeGroupServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Update AttributeGroup
    public String updateAttributeGroup() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request..");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        AttributeGroupServiceParams params = new AttributeGroupServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AttributeGroupVocabulary.COLLECTION.name(), "AttributeGroup");
        params.getProperties().put(AttributeGroupVocabulary.COMMAND.name(), AttributeGroupVocabulary.UPDATE.name());

        AttributeGroupServiceInput input = new AttributeGroupServiceInput().fromJson(requestJson);

        Service<AttributeGroupServiceInput, AttributeGroupServiceOutput, AttributeGroupServiceParams, DefaultPersist> service = new AttributeGroupServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AttributeGroupServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }
    
    //Delete AttributeGroup

    public String deleteAttributeGroup() {
        slf4jLogger.info("Deleting attributeGroup - ");
        AttributeGroup attributeGroup = new AttributeGroup ();
        attributeGroup.setId(request.params(":id"));
        slf4jLogger.info("request param: " + attributeGroup.toJson());
        
        // This is real mandatory code to move in controller
        AttributeGroupServiceParams params = new AttributeGroupServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AttributeGroupVocabulary.COLLECTION.name(), "AttributeGroup");
        params.getProperties().put(AttributeGroupVocabulary.COMMAND.name(), AttributeGroupVocabulary.DELETE.name());

        AttributeGroupServiceInput input = new AttributeGroupServiceInput();
        input.setData(attributeGroup);
        input.setInfo(info);

        Service<AttributeGroupServiceInput, AttributeGroupServiceOutput, AttributeGroupServiceParams, DefaultPersist> service = new AttributeGroupServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AttributeGroupServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    //Find AttributeGroup

    public String findAttributeGroup() {
        slf4jLogger.info("Finding attributeGroup - ");
        AttributeGroup attributeGroup = new AttributeGroup ();
        attributeGroup.setId(request.params(":id"));
        slf4jLogger.info("request param: " + attributeGroup.toJson());
        
        // This is real mandatory code to move in controller
        AttributeGroupServiceParams params = new AttributeGroupServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AttributeGroupVocabulary.COLLECTION.name(), "AttributeGroup");
        params.getProperties().put(AttributeGroupVocabulary.COMMAND.name(), AttributeGroupVocabulary.RETRIEVE.name());

        AttributeGroupServiceInput input = new AttributeGroupServiceInput();
        input.setData(attributeGroup);
        input.setInfo(info);

        Service<AttributeGroupServiceInput, AttributeGroupServiceOutput, AttributeGroupServiceParams, DefaultPersist> service = new AttributeGroupServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AttributeGroupServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }
    
    public String listAttributeGroup() {
        slf4jLogger.info("List attributeGroup - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        AttributeGroupServiceParams params = new AttributeGroupServiceParams();
        DefaultPersist persist = new DefaultPersist();

        AttributeGroupServiceInput input = new AttributeGroupServiceInput();
        if ((null != requestJson) && !requestJson.isEmpty()){
            input = new AttributeGroupServiceInput().fromJson(requestJson);
        }else{
            input.setInfo(info);
        }
        
        params.getProperties().put(AttributeGroupVocabulary.COLLECTION.name(), "AttributeGroup");
        params.getProperties().put(AttributeGroupVocabulary.COMMAND.name(), AttributeGroupVocabulary.LIST.name());

        Service<AttributeGroupServiceInput, AttributeGroupServiceOutput, AttributeGroupServiceParams, DefaultPersist> service = 
                new AttributeGroupServiceFactory(AttributeGroupVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AttributeGroupServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("list attributeGroup - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    /**
     *
     * @param params
     * @param persist
     */
    private void initAttributeGroupServiceEnv(AttributeGroupServiceParams params, DefaultPersist persist) {
        if (!request.session().isNew()
                && request.session().attribute(AttributeGroupVocabulary.MONGODB.name()) != null) {
        } else {
            params.initEnv("environment.properties");
            request.session().attribute(AttributeGroupVocabulary.MONGODB.name(), params.getEnv());
//            try {
//                //        }
//                HazelcastInstance db = persist.initEnv("environment.properties");
//            } catch (HazelcastException ex) {
//
//            }
//            request.session().attribute(AttributeGroupVocabulary.HAZELCAST.name(), persist.getEnv());
        }

        params.setEnv(request.session().attribute(AttributeGroupVocabulary.MONGODB.name()));
        persist.setEnv(request.session().attribute(AttributeGroupVocabulary.HAZELCAST.name()));
    }

}
