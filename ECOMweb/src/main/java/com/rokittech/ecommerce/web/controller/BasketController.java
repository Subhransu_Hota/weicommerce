/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.basket.defaults.BasketAddressServiceInput;
import com.rokittech.container.ecom.basket.defaults.BasketProductServiceInput;
import com.rokittech.container.ecom.basket.defaults.BasketServiceInput;
import com.rokittech.container.ecom.basket.defaults.BasketServiceOutput;
import com.rokittech.container.ecom.basket.defaults.BasketServiceParams;
import com.rokittech.container.ecom.basket.defaults.BasketShippingMethodServiceInput;
import com.rokittech.container.ecom.basket.defaults.BasketVocabulary;
import com.rokittech.container.ecom.basket.factory.BasketServiceFactory;
import com.rokittech.container.ecom.basket.util.BasketHelper;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.container.ecom.commons.models.AddressType;
import com.rokittech.container.ecom.commons.models.Basket;
import com.rokittech.container.ecom.commons.models.BasketAddressRef;
import com.rokittech.container.ecom.commons.models.BasketProductRef;
import com.rokittech.container.ecom.commons.models.LineItem;
import com.rokittech.container.ecom.commons.models.Product;
import com.rokittech.container.ecom.commons.models.ShippingMethodRef;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.product.defaults.ProductServiceInput;
import com.rokittech.container.ecom.product.defaults.ProductServiceOutput;
import com.rokittech.container.ecom.product.defaults.ProductServiceParams;
import com.rokittech.container.ecom.product.defaults.ProductVocabulary;
import com.rokittech.container.ecom.product.factory.ProductServiceFactory;
import com.rokittech.container.ecom.user.defaults.UserServiceInput;
import com.rokittech.container.ecom.user.defaults.UserServiceOutput;
import com.rokittech.container.ecom.user.defaults.UserServiceParams;
import com.rokittech.container.ecom.user.defaults.UserVocabulary;
import com.rokittech.container.ecom.user.factory.UserServiceFactory;
import com.rokittech.containers.api.core.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class BasketController {
    
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PricingController.class);
    private final Request request;
    private final Response response;
    private Info info = new Info();
    private BasketHelper basketHelper = new BasketHelper();
    
    public BasketController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
    // Search CRUD
    //Insert Search
    public String updateBasketAddres() {
        
        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        BasketAddressServiceInput reqInput = new BasketAddressServiceInput().fromJson(requestJson);
        info = reqInput.getInfo();
        
        BasketAddressRef basketAddressRef = reqInput.getData();
        Basket basketInput;
        Product product;
        User user;
        String basketId = basketAddressRef.getBasketId();
        Address address = basketAddressRef.getAddress();
        String addressType = basketAddressRef.getAddressType();

        // check if request address id exists
        if (address == null) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        ////Get user for requested userId or ceate a Anonymous User
        if (basketId.isEmpty()) {
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
            
        }
        
        BasketServiceInput input = new BasketServiceInput();

        //basket = new Basket(lineItem, user);
        //basket.setUser(user);
        Basket basket = getExistingBasket(basketId);
        if (addressType.equalsIgnoreCase(AddressType.Billing.name())) {
            basket.setBillingAddress(address);
            //basket = basketHelper.updateBasketBillingAddress(basket, address);
        }
        if (addressType.equalsIgnoreCase(AddressType.Shipping.name())) {
            basket = basketHelper.updateBasketShippingAddress(basket, address);
        }
        
        input.setInfo(info);
        
        Basket calculatedBasket = basketHelper.calculateBasket(basket);
        input.setData(calculatedBasket);
        
        slf4jLogger.info(" input request " + requestJson);
        // This is real mandatory code to move in controller
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.UPDATE.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();
        
        slf4jLogger.info(" Output Response " + output);
        
        return output;
    }
    
    public String updateBasketLIQty() {
        
        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        BasketProductServiceInput reqInput = new BasketProductServiceInput().fromJson(requestJson);
        info = reqInput.getInfo();
        
        BasketProductRef basketProductRef = reqInput.getData();
        Basket basketInput;
        Product product;
        User user;
        String basketId = basketProductRef.getBasketId();
        String productId = basketProductRef.getProductId();
        String userId = basketProductRef.getUserId();
        double qty = basketProductRef.getQty();

        // check if request product id exists
        if (productId == null || productId.isEmpty()) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        // check if valid request product qty 
        if (qty <= 0) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        ////Get user for requested userId or ceate a Anonymous User
        if (basketId.isEmpty()) {
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
            
        }

        //Get product for requested productId
        //product = getProductById(productId);
        //LineItem lineItem = createProductLineItem(product, qty);
        BasketServiceInput input = new BasketServiceInput();;

        //basket = new Basket(lineItem, user);
        //basket.setUser(user);
        Basket basket = getExistingBasket(basketId);
        
        basket = basketHelper.updateBasketLineItemQty(basket, productId, qty);
        input.setInfo(info);
        
        Basket calculatedBasket = basketHelper.calculateBasket(basket);
        input.setData(calculatedBasket);
        
        slf4jLogger.info(" input request " + requestJson);
        // This is real mandatory code to move in controller
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.UPDATE.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();
        
        slf4jLogger.info(" Output Response " + output);
        
        return output;
    }
    
    public String updateBasketLineItem() {
        
        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        BasketProductServiceInput reqInput = new BasketProductServiceInput().fromJson(requestJson);
        info = reqInput.getInfo();
        
        BasketProductRef basketProductRef = reqInput.getData();
        Basket basketInput;
        Product product;
        User user;
        String basketId = basketProductRef.getBasketId();
        String productId = basketProductRef.getProductId();
        String userId = basketProductRef.getUserId();
        double qty = basketProductRef.getQty();

        // check if request product id exists
        if (productId == null || productId.isEmpty()) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        // check if valid request product qty 
        if (qty <= 0) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        ////Get user for requested userId or ceate a Anonymous User
        if (basketId.isEmpty()) {
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
            
        } else {
            user = getUserById(userId);
        }

        //Get product for requested productId
        //product = getProductById(productId);
        //LineItem lineItem = createProductLineItem(product, qty);
        BasketServiceInput input = new BasketServiceInput();;

        //basket = new Basket(lineItem, user);
        //basket.setUser(user);
        Basket basket = getExistingBasket(basketId);
        
        basket = basketHelper.removeBasketLineItem(basket, productId);
        input.setInfo(info);
        
        Basket calculatedBasket = basketHelper.calculateBasket(basket);
        input.setData(calculatedBasket);
        
        slf4jLogger.info(" input request " + requestJson);
        // This is real mandatory code to move in controller
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.UPDATE.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();
        
        slf4jLogger.info(" Output Response " + output);
        
        return output;
    }
    
    public String removeBasketLineItem() {
        
        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        BasketProductServiceInput reqInput = new BasketProductServiceInput().fromJson(requestJson);
        info = reqInput.getInfo();
        
        BasketProductRef basketProductRef = reqInput.getData();
        Basket basketInput;
        Product product;
        User user;
        String basketId = basketProductRef.getBasketId();
        String productId = basketProductRef.getProductId();
        String userId = basketProductRef.getUserId();
        double qty = basketProductRef.getQty();

        // check if request product id exists
        if (productId == null || productId.isEmpty()) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        // check if valid request product qty 
        if (qty <= 0) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        ////Get user for requested userId or ceate a Anonymous User
        if (basketId.isEmpty()) {
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
            
        } else {
            user = getUserById(userId);
        }

        //Get product for requested productId
        //product = getProductById(productId);
        //LineItem lineItem = createProductLineItem(product, qty);
        BasketServiceInput input = new BasketServiceInput();;

        //basket = new Basket(lineItem, user);
        //basket.setUser(user);
        Basket basket = getExistingBasket(basketId);
        
        basket = basketHelper.removeBasketLineItem(basket, productId);
        input.setInfo(info);
        
        Basket calculatedBasket = basketHelper.calculateBasket(basket);
        input.setData(calculatedBasket);
        
        slf4jLogger.info(" input request " + requestJson);
        // This is real mandatory code to move in controller
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.UPDATE.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();
        
        slf4jLogger.info(" Output Response " + output);
        
        return output;
    }
    
    public String addProductToBasket() {
        
        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        BasketProductServiceInput reqInput = new BasketProductServiceInput().fromJson(requestJson);
        info = reqInput.getInfo();
        
        BasketProductRef basketProductRef = reqInput.getData();
        Basket basketInput;
        Product product;
        User user;
        String basketId = basketProductRef.getBasketId();
        String productId = basketProductRef.getProductId();
        String userId = basketProductRef.getUserId();
        double qty = basketProductRef.getQty();

        // check if request product id exists
        if (productId == null || productId.isEmpty()) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        // check if valid request product qty 
        if (qty <= 0) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        ////Get user for requested userId or ceate a Anonymous User
        if (basketId.isEmpty()) {
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
            
        } else {
            user = getUserById(userId);
        }

        //Get product for requested productId
        product = getProductById(productId);
        
        BasketServiceInput input = new BasketServiceInput();
        LineItem lineItem = basketHelper.createProductLineItem(product, qty);

        //basket = new Basket(lineItem, user);
        //basket.setUser(user);
        Basket basket = getExistingBasket(basketId);
        List<LineItem> lineItems = basket.getLineItems();
        lineItems.add(lineItem);
        basket.setLineItems(lineItems);
        input.setInfo(info);
        
        Basket calculatedBasket = basketHelper.calculateBasket(basket);
        input.setData(calculatedBasket);
        
        slf4jLogger.info(" input request " + requestJson);
        // This is real mandatory code to move in controller
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.UPDATE.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();
        
        slf4jLogger.info(" Output Response " + output);
        
        return output;
    }
    
    public String updateBasket() {
        
        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        BasketServiceInput input = new BasketServiceInput().fromJson(requestJson);

        //Basket calculatedBasket = basketHelper.calculateBasket(input.getData());
        //input.setData(calculatedBasket);
        slf4jLogger.info(" input request " + requestJson);
        // This is real mandatory code to move in controller
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.UPDATE.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();
        
        slf4jLogger.info(" Output Response " + output);
        
        return output;
    }
    
    public String updateBasketPayment() {
        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        BasketServiceInput input = new BasketServiceInput().fromJson(requestJson);

        //Basket calculatedBasket = basketHelper.calculateBasket(input.getData());
        //input.setData(calculatedBasket);
        slf4jLogger.info(" input request " + requestJson);
        // This is real mandatory code to move in controller

        //basket = new Basket(lineItem, user);
        //basket.setUser(user);
        Basket basket = getExistingBasket(input.getData().getId());
        basket.setPayment(input.getData().getPayment());
        
        input.setData(basket);
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.UPDATE.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();
        
        slf4jLogger.info(" Output Response " + output);
        
        return output;
    }
    
    public String updateBasketShippingMethod() {
        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        BasketShippingMethodServiceInput input = new BasketShippingMethodServiceInput().fromJson(requestJson);

        //Basket calculatedBasket = basketHelper.calculateBasket(input.getData());
        //input.setData(calculatedBasket);
        slf4jLogger.info(" input request " + requestJson);
        // This is real mandatory code to move in controller
        
        ShippingMethodRef shippingMethod = input.getData();
        Basket basket = getExistingBasket(shippingMethod.getBasketId());
        for (LineItem lineItem : basket.getLineItems()) {
            if ((lineItem.getId() == null) && lineItem.getId().equals(lineItem.getId())) {
                lineItem.setShippingMethod(shippingMethod.getShippingMethod());
            }
        }
        
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        BasketServiceInput basketInput = new BasketServiceInput();
        basketInput.setData(basket);
        basketInput.setInfo(input.getInfo());
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.UPDATE.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(basketInput);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();
        
        slf4jLogger.info(" Output Response " + output);
        
        return output;
    }
    
    public String getBasket() {
        
        String requestJson = request.body();
        //String output = "";
        Basket basket = new Basket();
        
        basket.setId(request.params(":id"));
        
        BasketServiceInput input = new BasketServiceInput();
        input.setInfo(info);
        input.setData(basket);
        // This is real mandatory code to move in controller
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.RETRIEVE.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        BasketServiceOutput output = service.serve();
        Basket calculatedBasket = basketHelper.calculateBasket(new Basket().fromJson(output.getData()));
        output.setData(calculatedBasket.toJson());
        
        slf4jLogger.info(" Output Response " + output.toJson());
        
        return output.toJson();
    }
    
    private Basket getExistingBasket(String basketId) {
        
        String requestJson = request.body();
        Basket basket = new Basket();
        
        basket.setId(basketId);
        
        BasketServiceInput input = new BasketServiceInput();
        input.setInfo(info);
        input.setData(basket);
        // This is real mandatory code to move in controller
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.RETRIEVE.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().getData();
        Basket basketResp = new Basket().fromJson(output);
        
        slf4jLogger.info(" Output Response " + output);
        
        return basketResp;
    }
    
    public String deleteBasket() {
        
        String requestJson = request.body();
        String output = "";
        Basket basket = new Basket();
        
        basket.setId(request.params(":id"));
        
        BasketServiceInput input = new BasketServiceInput();
        input.setInfo(info);
        input.setData(basket);
        // This is real mandatory code to move in controller
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.DELETE.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();
        
        slf4jLogger.info(" Output Response " + output);
        
        return output;
    }
    
    public String createBasket() {
        
        String requestJson = request.body();
        //String output = "";
        if (!PersistUtils.validateInput(requestJson)) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        BasketProductServiceInput reqInput = new BasketProductServiceInput().fromJson(requestJson);
        info = reqInput.getInfo();
        
        BasketProductRef basketProductRef = reqInput.getData();
        Basket basket;
        Product product;
        User user;
        String productId = basketProductRef.getProductId();
        String userId = basketProductRef.getUserId();
        double qty = basketProductRef.getQty();

        // check if request product id exists
        if (productId == null || productId.isEmpty()) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        // check if valid request product qty 
        if (qty <= 0) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        ////Get user for requested userId or ceate a Anonymous User
        if (userId.isEmpty() || userId == null || userId.isEmpty()) {
            user = new User().fromJson(getAnonymousUser());
            
        } else {
            user = getUserById(userId);
        }

        //Get product for requested productId
        product = getProductById(productId);
        
        BasketServiceInput input = new BasketServiceInput();
        LineItem lineItem = basketHelper.createProductLineItem(product, qty);
        
        basket = new Basket(lineItem, user);
        basket.setUser(user);
        
        input.setInfo(info);
        input.setData(basket);
        
        slf4jLogger.info(" input request " + requestJson);
        // This is real mandatory code to move in controller
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.INSERT.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        BasketServiceOutput output = service.serve();
        Basket calculatedBasket = basketHelper.calculateBasket(new Basket().fromJson(output.getData()));
        output.setData(calculatedBasket.toJson());
        
        // To be changed later based on container.
        request.session().maxInactiveInterval(Integer.MAX_VALUE);
        request.session().attribute("Basket", calculatedBasket);
 
        
        slf4jLogger.info(" Output Response " + output.toJson());
        
        return output.toJson();
    }

    //Insert Search
    public String getAnonymousUser() {
        
        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {
            
            slf4jLogger.error(" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.info(" input request " + requestJson);
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.COLLECTIONID.name());
        
        UserServiceInput input = new UserServiceInput().fromJson(requestJson);
        
        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        service.serve();
        output = service.getOutput().getData();
        User user = new User().fromJson(output);
        
        slf4jLogger.info(" Output Response " + user.toJson());
        
        return user.toJson();
    }
    
    private Product getProductById(String productId) {
        slf4jLogger.info("Deleting product - ");
        Product product = new Product();
        product.setId(productId);
        
        slf4jLogger.info("request param: " + product.toJson());

        // This is real mandatory code to move in controller
        ProductServiceParams params = new ProductServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        ProductServiceInput input = new ProductServiceInput();
        input.setInfo(info);
        input.setData(product);
        
        params.getProperties().put(ProductVocabulary.COLLECTION.name(), "Product");
        params.getProperties().put(ProductVocabulary.COMMAND.name(), ProductVocabulary.RETRIEVE.name());
        
        Service<ProductServiceInput, ProductServiceOutput, ProductServiceParams, DefaultPersist> service = new ProductServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ProductServiceOutput());

        // Run service and get output data.
        //
        ProductServiceOutput output = service.serve();
        
        Product productResp = new Product().fromJson(output.getData());
        
        slf4jLogger.info("got priduct - ");
        slf4jLogger.debug("Response " + productResp.toJson());
        
        return productResp;
    }
    
    private User getUserById(String userId) {
        slf4jLogger.info("Finding user - ");
        User user = new User();
        user.setId(userId);
        slf4jLogger.info("request param: " + user.toJson());

        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.RETRIEVE.name());
        
        UserServiceInput input = new UserServiceInput();
        input.setData(user);
        input.setInfo(info);
        
        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        UserServiceOutput output = service.serve();
        User userResp = new User().fromJson(output.getData());
        userResp.setUserCredentials(null);
        
        return userResp;
    }

    /**
     *
     * @param params
     * @param persist
     */
    private void initBasketServiceEnv(BasketServiceParams params, DefaultPersist persist) {
        if (!request.session().isNew()
                && request.session().attribute(BasketVocabulary.MONGODB.name()) != null) {
        } else {
            params.initEnv("environment.properties");
            request.session().attribute(BasketVocabulary.MONGODB.name(), params.getEnv());
//            try {
//                //        }
//                HazelcastInstance db = persist.initEnv("environment.properties");
//            } catch (HazelcastException ex) {
//
//            }
//            request.session().attribute(BasketVocabulary.HAZELCAST.name(), persist.getEnv());
        }
        
        params.setEnv(request.session().attribute(BasketVocabulary.MONGODB.name()));
        persist.setEnv(request.session().attribute(BasketVocabulary.HAZELCAST.name()));
    }
    
}
