/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Order;
import com.rokittech.container.ecom.order.defaults.OrderServiceInput;
import com.rokittech.container.ecom.order.defaults.OrderServiceOutput;
import com.rokittech.container.ecom.order.defaults.OrderServiceParams;
import com.rokittech.container.ecom.order.defaults.OrderVocabulary;
import com.rokittech.container.ecom.order.factory.OrderServiceFactory;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

public class OrderController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(OrderController.class);
    private final Request request;
    private final Response response;
    private static final String DB_ORDER = "Order";
    private final Info info = new Info (); 
        
    public OrderController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

//    // Create Order
//    public String createOrder() {
//        
//        slf4jLogger.info("Creating order - ");
//        String requestJson = request.body();
//        if (!PersistUtils.validateInput(requestJson)) {
//            slf4jLogger.error("invalid request!");
//            return "{\"ERROR\":\": invalid request!\"}";
//        }
//        
//        slf4jLogger.debug("Request " + requestJson);
//        OrderInput orderInput = new OrderInput().fromJson(requestJson);
//        
//        String output = OrderHelper.CreateOrder(orderInput);
//
//        slf4jLogger.info("Created order - ");
//        slf4jLogger.debug("Response " + output);
//        
//        return output;
//    }

    public String getOrderById() {
        slf4jLogger.info("Find order - ");
        Order order = new Order ();
        order.setId(request.params(":id"));
        
        slf4jLogger.info("request param: " + order.toJson());
        
        // This is real mandatory code to move in controller
        OrderServiceParams params = new OrderServiceParams();
        DefaultPersist persist = new DefaultPersist();

        OrderServiceInput input = new OrderServiceInput();
        input.setInfo(info);
        input.setData(order);
        
        params.getProperties().put(OrderVocabulary.COLLECTION.name(), DB_ORDER);
        params.getProperties().put(OrderVocabulary.COMMAND.name(), OrderVocabulary.RETRIEVE.name());

        Service<OrderServiceInput, OrderServiceOutput, OrderServiceParams, DefaultPersist> service = new OrderServiceFactory(OrderVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrderServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("got priduct - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    
    public String updateOrder () {
        slf4jLogger.info("Updating order - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        OrderServiceParams params = new OrderServiceParams();
        DefaultPersist persist = new DefaultPersist();

        OrderServiceInput input = new OrderServiceInput().fromJson(requestJson);

        params.getProperties().put(OrderVocabulary.COLLECTION.name(), DB_ORDER);
        params.getProperties().put(OrderVocabulary.COMMAND.name(), OrderVocabulary.UPDATE.name());

        Service<OrderServiceInput, OrderServiceOutput, OrderServiceParams, DefaultPersist> service = new OrderServiceFactory(OrderVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrderServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("updated order - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    
    public String listOrder() {
        slf4jLogger.info("List order - ");
        String requestJson = request.body();
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        OrderServiceParams params = new OrderServiceParams();
        DefaultPersist persist = new DefaultPersist();

        OrderServiceInput input = new OrderServiceInput().fromJson(requestJson);

        params.getProperties().put(OrderVocabulary.COLLECTION.name(), DB_ORDER);
        params.getProperties().put(OrderVocabulary.COMMAND.name(), OrderVocabulary.LIST.name());

        Service<OrderServiceInput, OrderServiceOutput, OrderServiceParams, DefaultPersist> service = 
                new OrderServiceFactory(OrderVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrderServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("list order - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

}
