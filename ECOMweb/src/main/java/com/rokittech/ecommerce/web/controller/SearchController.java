/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.search.defaults.SearchServiceInput;
import com.rokittech.container.ecom.search.defaults.SearchServiceOutput;
import com.rokittech.container.ecom.search.defaults.SearchServiceParams;
import com.rokittech.container.ecom.search.defaults.SearchVocabulary;
import com.rokittech.container.ecom.search.factory.SearchServiceFactory;
import com.rokittech.containers.api.core.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class SearchController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PricingController.class);
    private final Request request;
    private final Response response;
    private Info info = new Info ();
    
    public SearchController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
    // Search CRUD
    //Insert Search
    public String search() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request..");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        SearchServiceParams params = new SearchServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(SearchVocabulary.COLLECTION.name(), "Product");
        params.getProperties().put(SearchVocabulary.COMMAND.name(), SearchVocabulary.QUERY.name());

        SearchServiceInput input = new SearchServiceInput().fromJson(requestJson);

        Service<SearchServiceInput, SearchServiceOutput, SearchServiceParams, DefaultPersist> service = new SearchServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new SearchServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }


    /**
     *
     * @param params
     * @param persist
     */
    private void initSearchServiceEnv(SearchServiceParams params, DefaultPersist persist) {
        if (!request.session().isNew()
                && request.session().attribute(SearchVocabulary.MONGODB.name()) != null) {
        } else {
            params.initEnv("environment.properties");
            request.session().attribute(SearchVocabulary.MONGODB.name(), params.getEnv());
//            try {
//                //        }
//                HazelcastInstance db = persist.initEnv("environment.properties");
//            } catch (HazelcastException ex) {
//
//            }
//            request.session().attribute(SearchVocabulary.HAZELCAST.name(), persist.getEnv());
        }

        params.setEnv(request.session().attribute(SearchVocabulary.MONGODB.name()));
        persist.setEnv(request.session().attribute(SearchVocabulary.HAZELCAST.name()));
    }

}
