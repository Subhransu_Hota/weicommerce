/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.basket.defaults.BasketServiceInput;
import com.rokittech.container.ecom.basket.defaults.BasketServiceOutput;
import com.rokittech.container.ecom.basket.defaults.BasketServiceParams;
import com.rokittech.container.ecom.basket.defaults.BasketVocabulary;
import com.rokittech.container.ecom.basket.factory.BasketServiceFactory;
import com.rokittech.container.ecom.basket.util.BasketHelper;
import com.rokittech.container.ecom.commons.models.Basket;
import com.rokittech.container.ecom.commons.models.Order;
import com.rokittech.container.ecom.commons.models.Payment;
import com.rokittech.container.ecom.order.defaults.OrderServiceOutput;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceInput;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceOutput;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceParams;
import com.rokittech.container.ecom.payment.factory.PaymentServiceFactory;
import com.rokittech.containers.api.core.Service;
import com.rokittech.ecommerce.web.userobjects.OrderInput;
import com.rokittech.ecommerce.web.util.HttpBodyParser;
import com.rokittech.ecommerce.web.util.OrderHelper;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class PaymentController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PaymentController.class);
    private final Request request;
    private final Response response;
    private Info info = new Info ();
    
   /*
    * in future, localhost should be replaced by node.js server host.
    */
    private String ORDER_REDIRECT_URL;
    
    public PaymentController(Request request, Response response) {
        this.request = request;
        this.response = response;
        
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
        
        initEnv();
    }
    
    public void initEnv() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("environment.properties");
            Properties properties = new Properties();
            properties.load(inputStream);
            this.ORDER_REDIRECT_URL = (String) properties.get("orderreviewurl");
        } catch (IOException ex) {
            slf4jLogger.error("ERROR: ", ex);
        }
    }

    /**
     *
     * @return
     */
    
    //Insert Address
    public String process () {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error(" invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        slf4jLogger.info (" input request **********\n" + requestJson);
        
        // This is real mandatory code to move in controller
        PaymentServiceParams params = new PaymentServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.initEnv("payment.properties");

        PaymentServiceInput input = new PaymentServiceInput().fromJson(requestJson);
        
        JsonElement jelement = new JsonParser().parse(requestJson);
        JsonObject  jobject = jelement.getAsJsonObject();
        String basketId = jobject.get("basketId").getAsString();
        //create order before payment
        input = CreateOrder(input, basketId);

        Service<PaymentServiceInput, PaymentServiceOutput, PaymentServiceParams, DefaultPersist> service 
                = new PaymentServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PaymentServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        slf4jLogger.info("Output Response **********\n" + output);

        return output;
    }
    
    private Basket getExistingBasket(String basketId) {
        
        Basket basket = new Basket();
        
        basket.setId(basketId);
        
        BasketServiceInput input = new BasketServiceInput();
        input.setInfo(info);
        input.setData(basket);
        // This is real mandatory code to move in controller
        BasketServiceParams params = new BasketServiceParams();
        DefaultPersist persist = new DefaultPersist();
        
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Basket");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.RETRIEVE.name());
        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("WEI_ECOMMERCE", 1).get();
        
        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new BasketServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().getData();
        Basket basketResp = new Basket().fromJson(output);
        
        slf4jLogger.info(" Output Response " + output);
        
        return basketResp;
    }
    
    private PaymentServiceInput CreateOrder(PaymentServiceInput input, String basketId){
        Basket basket = getExistingBasket(basketId);
        
        BasketHelper basketHelper = new BasketHelper();
        Basket calculatedBasket = basketHelper.calculateBasket(basket);
        
        //create order input
        OrderInput orderInput = new OrderInput();
        Payment payment = input.getData();
        calculatedBasket.setPayment(payment);
        calculatedBasket.setPaymentType(payment.getPaymentType());
        orderInput.setData(calculatedBasket);
        
        String output = OrderHelper.CreateOrder(orderInput);
        String data = new OrderServiceOutput().fromJson(output).getData();
        String orderId = new Order().fromJson(data).getId();
        // set orderId in payment
        if(input.getData().getUserDefinedFields() == null){
            input.getData().setUserDefinedFields(new HashMap<>());
        }
        input.getData().getUserDefinedFields().put("orderId", orderId);
        
        return input;
    }
    
    
    /**
     * 
     * @return 
     */
    public String success () {
        String requestJson = request.body();
        String output = "";
        
        slf4jLogger.info("Request data ==> " + requestJson);
        
        Map<String, Object> params = new HashMap<>();
        try {
             params = HttpBodyParser.asMap(requestJson);
        } catch (UnsupportedEncodingException ex) {
            slf4jLogger.error("Unsupported Encoding Exception while parsing: "+requestJson);
        }   
        
        String orderId = params.get("txnid").toString();
        
        //redirect to order page
        response.redirect(this.ORDER_REDIRECT_URL + orderId);
        
        return "redirecting ..";
    }
    
    /**
     * 
     * @return 
     */
    public String failure () {
        String requestJson = request.body();
        String output = "Payment Failed";
        
        slf4jLogger.info("Request data ==> " + requestJson);

        return output;
    }
    
    /**
     * 
     * @return 
     */
    public String cancel () {
        String requestJson = request.body();
        String output = "";
        
        slf4jLogger.info("Request data ==> " + requestJson);

        return output;
    }
}
