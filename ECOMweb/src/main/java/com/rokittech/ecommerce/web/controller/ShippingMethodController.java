/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.ShippingMethod;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodServiceInput;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodServiceOutput;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodServiceParams;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodVocabulary;
import com.rokittech.container.ecom.shippingmethod.factory.ShippingMethodServiceFactory;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class ShippingMethodController {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(ShippingMethodController.class);
    private final Request request;
    private final Response response;
    private Info info = new Info();

    public ShippingMethodController(Request request, Response response) {
        this.request = request;
        this.response = response;

        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    
    public String createShippingMethod() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        ShippingMethodServiceParams params = new ShippingMethodServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(ShippingMethodVocabulary.COLLECTION.name(), "ShippingMethod");
        params.getProperties().put(ShippingMethodVocabulary.COMMAND.name(), ShippingMethodVocabulary.INSERT.name());

        ShippingMethodServiceInput input = new ShippingMethodServiceInput().fromJson(requestJson);

        Service<ShippingMethodServiceInput, ShippingMethodServiceOutput, ShippingMethodServiceParams, DefaultPersist> service = new ShippingMethodServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ShippingMethodServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    public String deleteShippingMethod() {
        slf4jLogger.info("Deleting catalog - ");
        ShippingMethod organization = new ShippingMethod();
        organization.setId(request.params(":id"));
        slf4jLogger.info("request param: " + organization.toJson());

        String output = "";

        // This is real mandatory code to move in controller
        ShippingMethodServiceParams params = new ShippingMethodServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(ShippingMethodVocabulary.COLLECTION.name(), "ShippingMethod");
        params.getProperties().put(ShippingMethodVocabulary.COMMAND.name(), ShippingMethodVocabulary.DELETE.name());

        ShippingMethodServiceInput input = new ShippingMethodServiceInput();
        input.setData(organization);
        input.setInfo(info);

        Service<ShippingMethodServiceInput, ShippingMethodServiceOutput, ShippingMethodServiceParams, DefaultPersist> service = new ShippingMethodServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ShippingMethodServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    public String getShippingMethod() {
        slf4jLogger.info("get organization - ");
        ShippingMethod organization = new ShippingMethod();
        organization.setId(request.params(":id"));
        slf4jLogger.info("request param: " + organization.toJson());

        String output = "";
        // This is real mandatory code to move in controller
        ShippingMethodServiceParams params = new ShippingMethodServiceParams();
        DefaultPersist persist = new DefaultPersist();

        ShippingMethodServiceInput input = new ShippingMethodServiceInput();
        input.setData(organization);
        input.setInfo(info);

        params.getProperties().put(ShippingMethodVocabulary.COLLECTION.name(), "ShippingMethod");
        params.getProperties().put(ShippingMethodVocabulary.COMMAND.name(), ShippingMethodVocabulary.RETRIEVE.name());

        Service<ShippingMethodServiceInput, ShippingMethodServiceOutput, ShippingMethodServiceParams, DefaultPersist> service = new ShippingMethodServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ShippingMethodServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

    public String updateShippingMethod() {

        String requestJson = request.body();
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            slf4jLogger.error (" invalid request");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        
        slf4jLogger.info (" input request .." + requestJson);
        // This is real mandatory code to move in controller
        ShippingMethodServiceParams params = new ShippingMethodServiceParams();
        DefaultPersist persist = new DefaultPersist();

        ShippingMethodServiceInput input = new ShippingMethodServiceInput().fromJson(requestJson);

        params.getProperties().put(ShippingMethodVocabulary.COLLECTION.name(), "ShippingMethod");
        params.getProperties().put(ShippingMethodVocabulary.COMMAND.name(), ShippingMethodVocabulary.UPDATE.name());

        Service<ShippingMethodServiceInput, ShippingMethodServiceOutput, ShippingMethodServiceParams, DefaultPersist> service = new ShippingMethodServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ShippingMethodServiceOutput());

        // Run service and get output data.
        output = service.serve().toJson();

        
        slf4jLogger.info (" Output Response .." + output);

        return output;
    }

}
