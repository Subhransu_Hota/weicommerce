/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.core;

import spark.Request;
import spark.Response;

/**
 *
 * @author alexmy
 */
public class AbstractController {
    
    private final Request request;
    private final Response response;
    
    public AbstractController(Request request, Response response){
        this.request = request;
        this.response = response;
    }
    
}
