/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module("mainModule", [])
        .controller("mainController", function ($scope, $http)
        {
            $scope.payment = {"data":{}};
            
            $scope.payment.data.firstname = "test";
            $scope.payment.data.amount = "1000";
            $scope.payment.data.email = "sangameshh@gmail.com";
            $scope.payment.data.phone = "9845098450";
            $scope.payment.data.productInfo = "bag of chips";
            
            $scope.submitData = function (payment, resultVarName)
            {
                payment.data.paymentType = "PAYUMONEY";
                var info = {
                    version: "1",
                    userId: "test",
                    appId: "wei"
                };
                payment.info = info;

                $http.post("payment/process", payment)
                        .success(function (data, status, headers, config)
                        {
                            $scope[resultVarName] = angular.fromJson(data.data);
                        })
                        .error(function (data, status, headers, config)
                        {
                            $scope[resultVarName] = "SUBMIT ERROR";
                        });
            };
            
            $scope.submit = function (){
                var payuForm = document.forms.payuForm;
                payuForm.action = $scope.ajaxSubmitResult.host;
                payuForm.submit();
            }
        });