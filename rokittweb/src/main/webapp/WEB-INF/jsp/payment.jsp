<%-- 
    Document   : payment
    Created on : Nov 3, 2015, 11:40:03 AM
    Author     : sangamesh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>PayuMoney Form submit test Page</title>
        <meta charset="UTF-8"/>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/script.js"></script>

        <style>
            label
            {
                display: inline-block;
                width: 120px;
                vertical-align: middle;
            }

            input
            {
                display: inline-block;
                vertical-align: middle;
            }

            input.ng-invalid
            {
                border: solid red 2px;
            }

            textarea
            {
                width: 300px;
                height: 80px;
            }
        </style>
    </head>
    <body ng-app="mainModule">
        <div ng-controller="mainController">
            <h3>PayuMoney Form Submit Page (Test) </h3>

            <form name="paymentForm" novalidate>
                <label for="firstName">First name:</label>
                <input id="firstName" type="text" name="firstName" ng-model="payment.data.firstname" required /><br />
                <label for="amount">Amount:</label>
                <input id="amount" type="text" name="amount" ng-model="payment.data.amount" required /><br />
                <label for="email">Email:</label>
                <input id="email" type="text" name="email" ng-model="payment.data.email" required /><br />
                <label for="phone">Phone:</label>
                <input id="phone" type="text" name="phone" ng-model="payment.data.phone" required /><br />
                <label for="productinfo">Product Info:</label>
                <input id="productinfo" type="text" name="productinfo" ng-model="payment.data.productInfo" required /><br />
                <br />
                <button type="submit"
                        ng-click="submitData(payment, 'ajaxSubmitResult')"
                        ng-disabled="paymentForm.$invalid">Submit</button>
            </form>

            <strong><label for="submitDebugText">Following information will be sent to payumoney, click send!</label></strong><br />
            <h4>{{ajaxSubmitResult.formData| json}}</h4>
        
            
        <form method="post" name="payuForm" ng-submit="submit()" action>
            <input type="hidden" name="key" value="{{ajaxSubmitResult.formData.key}}">
            <input type="hidden" name="hash" value="{{ajaxSubmitResult.formData.hash}}"/>
            <input type="hidden" name="txnid" value="{{ajaxSubmitResult.formData.txnid}}" />
            <input type="hidden" name="service_provider" value="{{ajaxSubmitResult.formData.service_provider}}" />
            <input type="hidden" name="amount" value="{{ajaxSubmitResult.formData.amount}}" />
            <input type="hidden" name="firstname" value="{{ajaxSubmitResult.formData.firstname}}" />
            <input type="hidden" name="email" value="{{ajaxSubmitResult.formData.email}}" />
            <input type="hidden" name="productinfo" value="{{ajaxSubmitResult.formData.productinfo}}" />
            <input type="hidden" name="surl" value="{{ajaxSubmitResult.formData.surl}}" />
            <input type="hidden" name="furl" value="{{ajaxSubmitResult.formData.furl}}" />
            <input type="hidden" name="lastname" value="{{ajaxSubmitResult.formData.lastname}}" />
            <input type="hidden" name="address1" value="{{ajaxSubmitResult.formData.address1}}" />
            <input type="hidden" name="address2" value="{{ajaxSubmitResult.formData.address2}}" />
            <input type="hidden" name="city" value="{{ajaxSubmitResult.formData.city}}" />        
            <input type="hidden" name="state" value="{{ajaxSubmitResult.formData.state}}" />        
            <input type="hidden" name="country" value="{{ajaxSubmitResult.formData.country}}" />        
            <input type="hidden" name="zipcode" value="{{ajaxSubmitResult.formData.zipcode}}" />        
            <input type="hidden" name="udf1" value="{{ajaxSubmitResult.formData.udf1}}" />        
            <input type="hidden" name="udf2" value="{{ajaxSubmitResult.formData.udf2}}" />        
            <input type="hidden" name="udf3" value="{{ajaxSubmitResult.formData.udf3}}" />        
            <input type="hidden" name="udf4" value="{{ajaxSubmitResult.formData.udf4}}" />        
            <input type="hidden" name="udf5" value="{{ajaxSubmitResult.formData.udf5}}" /> 
            <input type="hidden" name="pg" value="{{ajaxSubmitResult.formData.pg}}" /> 
            <input type="submit" name="btmSubmit" value="Send!">
            </table>
    </form>
            </div>
</body>
</html>
