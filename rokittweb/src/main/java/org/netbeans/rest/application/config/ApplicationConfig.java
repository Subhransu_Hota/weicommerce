/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.rest.application.config;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author sangamesh
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.rokittech.ecommerce.web.controller.AddressController.class);
        resources.add(com.rokittech.ecommerce.web.controller.AssignmentController.class);
        resources.add(com.rokittech.ecommerce.web.controller.AuthController.class);
        resources.add(com.rokittech.ecommerce.web.controller.CatalogController.class);
        resources.add(com.rokittech.ecommerce.web.controller.CustomerController.class);
        resources.add(com.rokittech.ecommerce.web.controller.OrderController.class);
        resources.add(com.rokittech.ecommerce.web.controller.OrganizationController.class);
        resources.add(com.rokittech.ecommerce.web.controller.PaymentController.class);
        resources.add(com.rokittech.ecommerce.web.controller.PricingController.class);
        resources.add(com.rokittech.ecommerce.web.controller.ProductController.class);
        resources.add(com.rokittech.ecommerce.web.controller.SearchController.class);
        resources.add(com.rokittech.ecommerce.web.controller.ShippingMethodController.class);
        resources.add(com.rokittech.ecommerce.web.controller.TestController.class);
        resources.add(com.rokittech.ecommerce.web.controller.UserController.class);
    }
    
}
