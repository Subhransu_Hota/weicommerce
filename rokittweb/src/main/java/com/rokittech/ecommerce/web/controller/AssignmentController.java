/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.models.Product;
import com.rokittech.container.ecom.product.defaults.ProductServiceInput;
import com.rokittech.container.ecom.product.defaults.ProductServiceOutput;
import com.rokittech.container.ecom.product.defaults.ProductServiceParams;
import com.rokittech.container.ecom.product.defaults.ProductVocabulary;
import com.rokittech.container.ecom.product.factory.ProductServiceFactory;
import com.rokittech.containers.api.core.Service;
import com.rokittech.ecommerce.web.userobjects.ProductToCatalogAssignmentInput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/assign")
public class AssignmentController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(AssignmentController.class);
    private static final String DB_PRODUCT = "Product";
    
    public AssignmentController() {
    }

    /**
     * 
     * @param requestJson
     * @return 
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/productstocatalogs")
    public String assignProduct(String requestJson) {
        slf4jLogger.info("Assigning product - ");
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        ProductToCatalogAssignmentInput requestInput = new ProductToCatalogAssignmentInput().fromJson(requestJson);
        
        List<String> output = new ArrayList<> ();
        requestInput.getData().stream().map((productCatalog) -> {
            ProductServiceInput input = new ProductServiceInput();
            input.setInfo(requestInput.getInfo());
            Product product = new Product();
            product.setId(productCatalog.getId());
            product.setCatalogList(productCatalog.getCatalogs());
            ProductServiceParams params = new ProductServiceParams();
            DefaultPersist persist = new DefaultPersist();
            params.getProperties().put(ProductVocabulary.COLLECTION.name(), DB_PRODUCT);
            params.getProperties().put(ProductVocabulary.COMMAND.name(), ProductVocabulary.UPDATE.name());
            Service<ProductServiceInput, ProductServiceOutput, ProductServiceParams, DefaultPersist> service 
                    = new ProductServiceFactory(ProductVocabulary.WEI_ECOMMERCE.name(), 1).get();
            service.setParams(params);
            service.setPersist(persist);
            service.setInput(input);
            return service;
        }).map((service) -> {
            service.setOutput(new ProductServiceOutput());
            return service;
        }).forEach((service) -> {
            output.add(service.serve().toJson());
        });
        
        slf4jLogger.debug("Response " + Arrays.toString(output.toArray()));
        
        return Arrays.toString(output.toArray());
    }
}
