/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.userobjects;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Input;
import java.util.List;

/**
 *
 * @CreatedDate 18th Sep 2015
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public final class ProductToCatalogAssignmentInput extends Input<List<ProductToCatalog>> {
    /**
     *
     */
    public ProductToCatalogAssignmentInput() {
        this.init();
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, ProductToCatalogAssignmentInput.class);
    }

    @Override
    public ProductToCatalogAssignmentInput fromJson(String json) {
        return new Gson().fromJson(json, ProductToCatalogAssignmentInput.class);
    }

    @Override
    public final void init() {
        
    }
}
