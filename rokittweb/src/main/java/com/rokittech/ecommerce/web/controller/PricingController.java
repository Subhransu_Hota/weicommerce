/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Pricing;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceInput;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceOutput;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceParams;
import com.rokittech.container.ecom.pricing.defaults.PricingVocabulary;
import com.rokittech.container.ecom.pricing.factory.PricingServiceFactory;
import com.rokittech.containers.api.core.Service;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/pricing")
public class PricingController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PricingController.class);
   
    private static final String DB_PRICING = "Pricing";
    private Info info = new Info (); 
    public PricingController() {
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @param requestJson
     * @return
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/insert")
    public String createPricing(String requestJson) {
        slf4jLogger.info("Creating pricing - ");
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        // This is real mandatory code to move in controller
        PricingServiceParams params = new PricingServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(PricingVocabulary.COLLECTION.name(), DB_PRICING);
        params.getProperties().put(PricingVocabulary.COMMAND.name(), PricingVocabulary.INSERT.name());

        PricingServiceInput input = new PricingServiceInput().fromJson(requestJson);

        Service<PricingServiceInput, PricingServiceOutput, PricingServiceParams, DefaultPersist> service = new PricingServiceFactory("WEI", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PricingServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Creaetd pricing - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    //Delete Customer
     @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/delete/{id}")
    public String deletePricing (@PathParam("id") String _id) {
        slf4jLogger.info("Deleting catalog - ");
        Pricing pricing = new Pricing ();
        pricing.setId(_id);
        slf4jLogger.info("request param: " + pricing.toJson());
        
        PricingServiceParams params = new PricingServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(PricingVocabulary.COLLECTION.name(), DB_PRICING);
        params.getProperties().put(PricingVocabulary.COMMAND.name(), PricingVocabulary.DELETE.name());
        
        PricingServiceInput input = new PricingServiceInput();
        input.setData(pricing);
        input.setInfo(info);

        Service<PricingServiceInput, PricingServiceOutput, PricingServiceParams, DefaultPersist> service = new PricingServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PricingServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Deleted pricing - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/find/{id}")
    public String getPricingById(@PathParam("id") String _id) {
        slf4jLogger.info("get Pricing - ");
        Pricing pricing = new Pricing ();
        pricing.setId(_id);
        slf4jLogger.info("request param: " + pricing.toJson());
         
        // This is real mandatory code to move in controller
        PricingServiceParams params = new PricingServiceParams();
        DefaultPersist persist = new DefaultPersist();

        PricingServiceInput input = new PricingServiceInput();
        input.setData(pricing);
        input.setInfo(info);
        
        params.getProperties().put(PricingVocabulary.COLLECTION.name(), DB_PRICING);
        params.getProperties().put(PricingVocabulary.COMMAND.name(), PricingVocabulary.RETRIEVE.name());

        Service<PricingServiceInput, PricingServiceOutput, PricingServiceParams, DefaultPersist> service = new PricingServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PricingServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("got pricing - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

     @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/update")
    public String updatePricing (String requestJson) {
        slf4jLogger.info("Updating pricing - ");
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        PricingServiceParams params = new PricingServiceParams();
        DefaultPersist persist = new DefaultPersist();

        PricingServiceInput input = new PricingServiceInput().fromJson(requestJson);

        params.getProperties().put(PricingVocabulary.COLLECTION.name(), DB_PRICING);
        params.getProperties().put(PricingVocabulary.COMMAND.name(), PricingVocabulary.UPDATE.name());

        Service<PricingServiceInput, PricingServiceOutput, PricingServiceParams, DefaultPersist> service = new PricingServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PricingServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("updated pricing - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    
}
