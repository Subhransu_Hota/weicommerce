/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Organization;
import com.rokittech.container.ecom.organization.defaults.OrganizationServiceInput;
import com.rokittech.container.ecom.organization.defaults.OrganizationServiceOutput;
import com.rokittech.container.ecom.organization.defaults.OrganizationServiceParams;
import com.rokittech.container.ecom.organization.defaults.OrganizationVocabulary;
import com.rokittech.container.ecom.organization.factory.OrganizationServiceFactory;
import com.rokittech.containers.api.core.Service;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/organization")
public class OrganizationController {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(OrganizationController.class);
    private Info info = new Info();

    public OrganizationController() {
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/insert")
    public String createOrganization(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        OrganizationServiceParams params = new OrganizationServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(OrganizationVocabulary.COLLECTION.name(), "Organization");
        params.getProperties().put(OrganizationVocabulary.COMMAND.name(), OrganizationVocabulary.INSERT.name());

        OrganizationServiceInput input = new OrganizationServiceInput().fromJson(requestJson);

        Service<OrganizationServiceInput, OrganizationServiceOutput, OrganizationServiceParams, DefaultPersist> service = new OrganizationServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrganizationServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Delete Customer
     @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/delete/{id}")
    public String deleteOrganization(@PathParam("id") String _id) {
        slf4jLogger.info("Deleting catalog - ");
        Organization organization = new Organization();
        organization.setId(_id);
        slf4jLogger.info("request param: " + organization.toJson());

        String output = "";

        // This is real mandatory code to move in controller
        OrganizationServiceParams params = new OrganizationServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(OrganizationVocabulary.COLLECTION.name(), "Organization");
        params.getProperties().put(OrganizationVocabulary.COMMAND.name(), OrganizationVocabulary.DELETE.name());

        OrganizationServiceInput input = new OrganizationServiceInput();
        input.setData(organization);
        input.setInfo(info);

        Service<OrganizationServiceInput, OrganizationServiceOutput, OrganizationServiceParams, DefaultPersist> service = new OrganizationServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrganizationServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/find/{id}")
    public String getOrganization(@PathParam("id") String _id) {
        slf4jLogger.info("get organization - ");
        Organization organization = new Organization();
        organization.setId(_id);
        slf4jLogger.info("request param: " + organization.toJson());

        String output = "";
        // This is real mandatory code to move in controller
        OrganizationServiceParams params = new OrganizationServiceParams();
        DefaultPersist persist = new DefaultPersist();

        OrganizationServiceInput input = new OrganizationServiceInput();
        input.setData(organization);
        input.setInfo(info);

        params.getProperties().put(OrganizationVocabulary.COLLECTION.name(), "Organization");
        params.getProperties().put(OrganizationVocabulary.COMMAND.name(), OrganizationVocabulary.RETRIEVE.name());

        Service<OrganizationServiceInput, OrganizationServiceOutput, OrganizationServiceParams, DefaultPersist> service = new OrganizationServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrganizationServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/update")
    public String updateOrganization(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        OrganizationServiceParams params = new OrganizationServiceParams();
        DefaultPersist persist = new DefaultPersist();

        OrganizationServiceInput input = new OrganizationServiceInput().fromJson(requestJson);

        params.getProperties().put(OrganizationVocabulary.COLLECTION.name(), "Organization");
        params.getProperties().put(OrganizationVocabulary.COMMAND.name(), OrganizationVocabulary.UPDATE.name());

        Service<OrganizationServiceInput, OrganizationServiceOutput, OrganizationServiceParams, DefaultPersist> service = new OrganizationServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrganizationServiceOutput());

        // Run service and get output data.
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }
}
