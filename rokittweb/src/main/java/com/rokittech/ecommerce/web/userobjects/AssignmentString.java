/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.userobjects;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class AssignmentString implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    private String _id;
    private String assignmentId;
    private List<String> assignments;
    
    public AssignmentString() {
    }

    public AssignmentString(String _id, String assignmentId, List<String> assignments) {
        this._id = _id;
        this.assignmentId = assignmentId;
        this.assignments = assignments;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(String assignmentId) {
        this.assignmentId = assignmentId;
    }

    public List<String> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<String> assignments) {
        this.assignments = assignments;
    }


    @Override
    public String toJson() {
        return new Gson().toJson(this, AssignmentString.class);
    }

    @Override
    public AssignmentString fromJson(String json) {
        return new Gson().fromJson(json, AssignmentString.class);
    }

    @Override
    public void init() {
        //TODO complete Address init
    }
}
