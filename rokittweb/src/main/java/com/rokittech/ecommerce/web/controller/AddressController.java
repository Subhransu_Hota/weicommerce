/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.address.defaults.AddressServiceInput;
import com.rokittech.container.ecom.address.defaults.AddressServiceOutput;
import com.rokittech.container.ecom.address.defaults.AddressServiceParams;
import com.rokittech.container.ecom.address.defaults.AddressVocabulary;
import com.rokittech.container.ecom.address.factory.AddressServiceFactory;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.containers.api.core.Service;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/address")
public class AddressController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PricingController.class);
    
    private Info info = new Info ();
    
    public AddressController() {
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @param requestJson
     * @return
     */
    // Address CRUD
    //Insert Address
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/insert")
    public String createAddress(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        AddressServiceParams params = new AddressServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AddressVocabulary.COLLECTION.name(), "Address");
        params.getProperties().put(AddressVocabulary.COMMAND.name(), AddressVocabulary.INSERT.name());

        AddressServiceInput input = new AddressServiceInput().fromJson(requestJson);

        Service<AddressServiceInput, AddressServiceOutput, AddressServiceParams, DefaultPersist> service = new AddressServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AddressServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/update")
    public String updateAddress(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        AddressServiceParams params = new AddressServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AddressVocabulary.COLLECTION.name(), "Address");
        params.getProperties().put(AddressVocabulary.COMMAND.name(), AddressVocabulary.UPDATE.name());

        AddressServiceInput input = new AddressServiceInput().fromJson(requestJson);

        Service<AddressServiceInput, AddressServiceOutput, AddressServiceParams, DefaultPersist> service = new AddressServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AddressServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Delete Address
    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/delete/{id}")
    public String deleteAddress(@PathParam("id") String _id) {
        slf4jLogger.info("Deleting user - ");
        Address user = new Address ();
//        user.setId(_id);
        slf4jLogger.info("request param: " + user.toJson());
        
        // This is real mandatory code to move in controller
        AddressServiceParams params = new AddressServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AddressVocabulary.COLLECTION.name(), "Address");
        params.getProperties().put(AddressVocabulary.COMMAND.name(), AddressVocabulary.DELETE.name());

        AddressServiceInput input = new AddressServiceInput();
        input.setData(user);
        input.setInfo(info);

        Service<AddressServiceInput, AddressServiceOutput, AddressServiceParams, DefaultPersist> service = new AddressServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AddressServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Find Address
    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/find/{id}")
    public String findAddress(@PathParam("id") String _id) {
        slf4jLogger.info("Finding user - ");
        Address user = new Address ();
//        user.setId(_id);
        slf4jLogger.info("request param: " + user.toJson());
        
        // This is real mandatory code to move in controller
        AddressServiceParams params = new AddressServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AddressVocabulary.COLLECTION.name(), "Address");
        params.getProperties().put(AddressVocabulary.COMMAND.name(), AddressVocabulary.RETRIEVE.name());

        AddressServiceInput input = new AddressServiceInput();
        input.setData(user);
        input.setInfo(info);

        Service<AddressServiceInput, AddressServiceOutput, AddressServiceParams, DefaultPersist> service = new AddressServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AddressServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }
}
