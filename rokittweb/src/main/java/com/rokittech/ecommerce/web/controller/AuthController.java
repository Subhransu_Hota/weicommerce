/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.auth.defaults.AuthServiceInput;
import com.rokittech.container.ecom.auth.defaults.AuthServiceOutput;
import com.rokittech.container.ecom.auth.defaults.AuthServiceParams;
import com.rokittech.container.ecom.auth.defaults.AuthVocabulary;
import com.rokittech.container.ecom.auth.defaults.LoginStatus;
import com.rokittech.container.ecom.auth.factory.AuthServiceFactory;
import com.rokittech.container.ecom.commons.models.UserCredentials;
import com.rokittech.containers.api.core.Service;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/credentials")
public class AuthController {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(PricingController.class);
    private Info info = new Info();

    public AuthController() {
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @param _id
     * @param request
     * @return
     */
    //Find Address
    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/logout/{id}")
    public String logout(@PathParam("id") String _id, @Context HttpServletRequest request) {

        AuthServiceInput input = new AuthServiceInput();
        input.setInfo(info);
        AuthServiceOutput output = new AuthServiceOutput();
        output.setInfo(input.getInfo());
        LoginStatus status = new LoginStatus();
        status.setMsg("Logout Successfully");
        status.setStatus(true);
        output.setData(status.toJson());

        //TODO remove loggedin user info from session
        request.getSession().invalidate();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output.toJson();
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/login")
    public String login(String requestJson, @Context HttpServletRequest request) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        AuthServiceParams params = new AuthServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AuthVocabulary.COLLECTION.name(), "UserCredentials");
        params.getProperties().put(AuthVocabulary.COMMAND.name(), AuthVocabulary.RETRIEVE.name());

        AuthServiceInput input = new AuthServiceInput().fromJson(requestJson);

        Service<AuthServiceInput, AuthServiceOutput, AuthServiceParams, DefaultPersist> service = new AuthServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AuthServiceOutput());

        // Run service and get output data.
        //
        AuthServiceOutput serviceOutput = service.serve();

        if (serviceOutput.getData().equalsIgnoreCase("[ ]")) {
            LoginStatus status = new LoginStatus();
            status.setMsg("Login Failed: Incorrect userid or password");
            status.setStatus(false);

            serviceOutput.setData(status.toJson());
            return serviceOutput.toJson();

        }

        //TODO temp code to be updated with session object of real container.
        request.getSession().setAttribute("UserName", input.getData().getUserName());
        request.getSession().setAttribute("password", input.getData().getPassword());
        output = serviceOutput.toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    // UserCredentials CRUD
    //Insert UserCredentials
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/insert")
    public String createUserCredentials(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        AuthServiceParams params = new AuthServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AuthVocabulary.COLLECTION.name(), "UserCredentials");
        params.getProperties().put(AuthVocabulary.COMMAND.name(), AuthVocabulary.INSERT.name());

        AuthServiceInput input = new AuthServiceInput().fromJson(requestJson);

        Service<AuthServiceInput, AuthServiceOutput, AuthServiceParams, DefaultPersist> service = new AuthServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AuthServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Update UserCredentials
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/update")
    public String updateUserCredentials(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        AuthServiceParams params = new AuthServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AuthVocabulary.COLLECTION.name(), "UserCredentials");
        params.getProperties().put(AuthVocabulary.COMMAND.name(), AuthVocabulary.UPDATE.name());

        AuthServiceInput input = new AuthServiceInput().fromJson(requestJson);

        Service<AuthServiceInput, AuthServiceOutput, AuthServiceParams, DefaultPersist> service = new AuthServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AuthServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Delete UserCredentials
    //Delete Address
    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/delete/{id}")
    public String deleteUserCredentials(@PathParam("id") String _id) {
        slf4jLogger.info("Deleting user - ");
        UserCredentials userCredentials = new UserCredentials();
//        userCredentials.setId(_id);
        slf4jLogger.info("request param: " + userCredentials.toJson());

        // This is real mandatory code to move in controller
        AuthServiceParams params = new AuthServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AuthVocabulary.COLLECTION.name(), "UserCredentials");
        params.getProperties().put(AuthVocabulary.COMMAND.name(), AuthVocabulary.DELETE.name());

        AuthServiceInput input = new AuthServiceInput();
        input.setData(userCredentials);
        input.setInfo(info);

        Service<AuthServiceInput, AuthServiceOutput, AuthServiceParams, DefaultPersist> service = new AuthServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AuthServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }
    
    //Find Address
    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/find/{id}")
    public String findUserCredentials(@PathParam("id") String _id) {
        slf4jLogger.info("Finding user - ");
        UserCredentials userCredentials = new UserCredentials();
//        userCredentials.setId(_id);
        slf4jLogger.info("request param: " + userCredentials.toJson());

        // This is real mandatory code to move in controller
        AuthServiceParams params = new AuthServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(AuthVocabulary.COLLECTION.name(), "UserCredentials");
        params.getProperties().put(AuthVocabulary.COMMAND.name(), AuthVocabulary.RETRIEVE.name());

        AuthServiceInput input = new AuthServiceInput();
        input.setData(userCredentials);
        input.setInfo(info);

        Service<AuthServiceInput, AuthServiceOutput, AuthServiceParams, DefaultPersist> service = new AuthServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new AuthServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

}
