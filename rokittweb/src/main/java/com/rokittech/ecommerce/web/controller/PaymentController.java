/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceInput;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceOutput;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceParams;
import com.rokittech.container.ecom.payment.factory.PaymentServiceFactory;
import com.rokittech.containers.api.core.Service;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.server.mvc.Viewable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Sanga
 */
@Path("/payment")
public class PaymentController {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(PaymentController.class);
    private Info info = new Info();

    public PaymentController() {
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @param requestJson
     * @return
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/process")

    //Insert Address
    public String process(String requestJson) {

        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error(" invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        slf4jLogger.info(" input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        PaymentServiceParams params = new PaymentServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.initEnv("payment.properties");

        PaymentServiceInput input = new PaymentServiceInput().fromJson(requestJson);

        Service<PaymentServiceInput, PaymentServiceOutput, PaymentServiceParams, DefaultPersist> service
                = new PaymentServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new PaymentServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        slf4jLogger.info("Output Response **********\n" + output);

        return output;
    }

    /**
     *
     * @return
     */
    @POST
    @Path("/success")
    public Viewable success() {
        Viewable vb = new Viewable("/payusuccess");
        return vb;
    }

    /**
     *
     * @return
     */
    @POST
    @Path("/failure")
    public String failure(String requestJson) {
        String output = "";

        slf4jLogger.info("Request data ==> " + requestJson);

        return output;
    }

    /**
     *
     * @return
     */
    @POST
    @Path("/cancel")
    public String cancel(String requestJson) {
        String output = "";

        slf4jLogger.info("Request data ==> " + requestJson);

        return output;
    }
}
