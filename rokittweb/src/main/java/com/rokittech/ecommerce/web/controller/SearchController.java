/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.search.defaults.SearchServiceInput;
import com.rokittech.container.ecom.search.defaults.SearchServiceOutput;
import com.rokittech.container.ecom.search.defaults.SearchServiceParams;
import com.rokittech.container.ecom.search.defaults.SearchVocabulary;
import com.rokittech.container.ecom.search.factory.SearchServiceFactory;
import com.rokittech.containers.api.core.Service;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/search")
public class SearchController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PricingController.class);
   
    private Info info = new Info ();
    
    public SearchController() {
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @param requestJson
     * @return
     */
    // Search CRUD
    //Insert Search
    @POST
    
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public String search(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        SearchServiceParams params = new SearchServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(SearchVocabulary.COLLECTION.name(), "Product");
        params.getProperties().put(SearchVocabulary.COMMAND.name(), SearchVocabulary.QUERY.name());

        SearchServiceInput input = new SearchServiceInput().fromJson(requestJson);

        Service<SearchServiceInput, SearchServiceOutput, SearchServiceParams, DefaultPersist> service = new SearchServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new SearchServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }
}
