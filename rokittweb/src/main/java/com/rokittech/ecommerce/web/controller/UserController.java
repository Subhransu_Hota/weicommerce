/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.user.defaults.UserServiceInput;
import com.rokittech.container.ecom.user.defaults.UserServiceOutput;
import com.rokittech.container.ecom.user.defaults.UserServiceParams;
import com.rokittech.container.ecom.user.defaults.UserVocabulary;
import com.rokittech.container.ecom.user.factory.UserServiceFactory;
import com.rokittech.containers.api.core.Service;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Path("/user")
public class UserController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PricingController.class);
    private Info info = new Info ();
    
    public UserController() {
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @param requestJson
     * @return
     */
    // User CRUD
    //Insert User
     @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/insert")
    public String createUser(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.INSERT.name());

        UserServiceInput input = new UserServiceInput().fromJson(requestJson);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Update User
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/update")
    public String updateUser(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.UPDATE.name());

        UserServiceInput input = new UserServiceInput().fromJson(requestJson);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Delete User
    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/delete/{id}")
    public String deleteUser(@PathParam("id") String _id) {
        slf4jLogger.info("Deleting user - ");
        User user = new User ();
        user.setId(_id);
        slf4jLogger.info("request param: " + user.toJson());
        
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.DELETE.name());

        UserServiceInput input = new UserServiceInput();
        input.setData(user);
        input.setInfo(info);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Find User
    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/find/{id}")
    public String findUser(@PathParam("id") String _id) {
        slf4jLogger.info("Finding user - ");
        User user = new User ();
        user.setId(_id);
        slf4jLogger.info("request param: " + user.toJson());
        
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.RETRIEVE.name());

        UserServiceInput input = new UserServiceInput();
        input.setData(user);
        input.setInfo(info);

        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new UserServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }
}
