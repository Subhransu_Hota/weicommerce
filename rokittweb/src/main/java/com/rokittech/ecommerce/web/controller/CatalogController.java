/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceInput;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceOutput;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceParams;
import com.rokittech.container.ecom.catalog.defaults.CatalogVocabulary;
import com.rokittech.container.ecom.catalog.factory.CatalogServiceFactory;
import com.rokittech.container.ecom.commons.models.Catalog;
import com.rokittech.containers.api.core.Service;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/catalog")
public class CatalogController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(CatalogController.class);
    
    private static final String DB_CATALOG = "Catalog";
    private final Info info = new Info (); 
        
    public CatalogController() {
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @param requestJson
     * @return
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/insert")
    public String createCatalog(String requestJson) {
        slf4jLogger.info("Creating catalog - ");
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        
        // This is real mandatory code to move in controller
        CatalogServiceParams params = new CatalogServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), DB_CATALOG);
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.INSERT.name());

        CatalogServiceInput input = new CatalogServiceInput().fromJson(requestJson);

        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = new CatalogServiceFactory(CatalogVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CatalogServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Creaetd catalog - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/delete/{id}")
    public String deleteCatalog(@PathParam("id") String _id) {
        slf4jLogger.info("Deleting catalog - ");
        Catalog catalog = new Catalog ();
        catalog.setId(_id);
        
        slf4jLogger.info("request param: " + catalog.toJson());
        CatalogServiceParams params = new CatalogServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), DB_CATALOG);
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.DELETE.name());
        
        CatalogServiceInput input = new CatalogServiceInput();
        input.setInfo(info);
        input.setData(catalog);

        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = 
                new CatalogServiceFactory(CatalogVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CatalogServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Deleted catalog - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/find/{id}")
    public String getCatalogById(@PathParam("id") String _id) {
        slf4jLogger.info("Get catalog - ");
        Catalog catalog = new Catalog ();
        catalog.setId(_id);    
        slf4jLogger.info("request param: " + catalog.toJson());
         
        // This is real mandatory code to move in controller
        CatalogServiceParams params = new CatalogServiceParams();
        DefaultPersist persist = new DefaultPersist();

        CatalogServiceInput input = new CatalogServiceInput();
        input.setInfo(info);
        input.setData(catalog);
        
        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), DB_CATALOG);
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.RETRIEVE.name());

        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = new CatalogServiceFactory(CatalogVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CatalogServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("got catalog - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/update")
    public String updateCatalog(String requestJson) {
        slf4jLogger.info("Updating catalog - ");
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        CatalogServiceParams params = new CatalogServiceParams();
        DefaultPersist persist = new DefaultPersist();

        CatalogServiceInput input = new CatalogServiceInput().fromJson(requestJson);

        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), DB_CATALOG);
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.UPDATE.name());

        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = new CatalogServiceFactory(CatalogVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CatalogServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("updated catalog - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/list")
    public String listCatalog(String requestJson) {
        slf4jLogger.info("List catalog - ");
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        CatalogServiceParams params = new CatalogServiceParams();
        DefaultPersist persist = new DefaultPersist();

        CatalogServiceInput input = new CatalogServiceInput().fromJson(requestJson);

        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), DB_CATALOG);
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.LIST.name());

        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = 
                new CatalogServiceFactory(CatalogVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CatalogServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("List catalog - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/getchildren/{id}")
    public String getChildren(@PathParam("id") String _id, String requestJson) {
        slf4jLogger.info("List catalog - ");
        Catalog catalog = new Catalog ();
        catalog.setParentCatalogId(_id); 
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        CatalogServiceParams params = new CatalogServiceParams();
        DefaultPersist persist = new DefaultPersist();

        CatalogServiceInput input = new CatalogServiceInput();
        input.setInfo(info);
        input.setData(catalog);

        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), DB_CATALOG);
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.LIST.name());

        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = 
                new CatalogServiceFactory(CatalogVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CatalogServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("List catalog - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
}
