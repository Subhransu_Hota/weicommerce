/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Customer;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceInput;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceOutput;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceParams;
import com.rokittech.container.ecom.customer.defaults.CustomerVocabulary;
import com.rokittech.container.ecom.customer.factory.CustomerServiceFactory;
import com.rokittech.containers.api.core.Service;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/customer")
public class CustomerController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(CustomerController.class);
    
    private Info info = new Info (); 
    
    public CustomerController() {
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @return
     */
// Customer CRUD
    //Insert Customer
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/insert")
    public String createCustomer(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        CustomerServiceParams params = new CustomerServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(CustomerVocabulary.COLLECTION.name(), "Customer");
        params.getProperties().put(CustomerVocabulary.COMMAND.name(), CustomerVocabulary.INSERT.name());

        CustomerServiceInput input = new CustomerServiceInput().fromJson(requestJson);

        Service<CustomerServiceInput, CustomerServiceOutput, CustomerServiceParams, DefaultPersist> service = new CustomerServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CustomerServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Update Customer
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/update")
    public String updateCustomer(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        CustomerServiceParams params = new CustomerServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(CustomerVocabulary.COLLECTION.name(), "Customer");
        params.getProperties().put(CustomerVocabulary.COMMAND.name(), CustomerVocabulary.UPDATE.name());

        CustomerServiceInput input = new CustomerServiceInput().fromJson(requestJson);

        Service<CustomerServiceInput, CustomerServiceOutput, CustomerServiceParams, DefaultPersist> service = new CustomerServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CustomerServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Delete Customer
     @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/delete/{id}")
    public String deleteCustomer(@PathParam("id") String _id) {
        slf4jLogger.info("Deleting Customer - ");
        Customer customer = new Customer ();
        customer.setId(_id);
        slf4jLogger.info("request param: " + customer.toJson());
         
        // This is real mandatory code to move in controller
        CustomerServiceParams params = new CustomerServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(CustomerVocabulary.COLLECTION.name(), "Customer");
        params.getProperties().put(CustomerVocabulary.COMMAND.name(), CustomerVocabulary.DELETE.name());

        CustomerServiceInput input = new CustomerServiceInput();
        input.setInfo(info);
        input.setData(customer);

        Service<CustomerServiceInput, CustomerServiceOutput, CustomerServiceParams, DefaultPersist> service = new CustomerServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CustomerServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Find Customer
    //Find Address
    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/find/{id}")
    public String findCustomer(@PathParam("id") String _id) {
        slf4jLogger.info("Find Customer - ");
        Customer customer = new Customer ();
        slf4jLogger.info("request param: " + customer.toJson());
        
        String output = "";
        // This is real mandatory code to move in controller
        CustomerServiceParams params = new CustomerServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(CustomerVocabulary.COLLECTION.name(), "Customer");
        params.getProperties().put(CustomerVocabulary.COMMAND.name(), CustomerVocabulary.RETRIEVE.name());

        CustomerServiceInput input = new CustomerServiceInput();
        input.setData(customer);
        input.setInfo(info);

        Service<CustomerServiceInput, CustomerServiceOutput, CustomerServiceParams, DefaultPersist> service = new CustomerServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new CustomerServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }
}
