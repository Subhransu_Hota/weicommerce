/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Basket;
import com.rokittech.container.ecom.commons.models.Counter;
import com.rokittech.container.ecom.commons.models.Order;
import com.rokittech.container.ecom.commons.models.OrderStatusType;
import com.rokittech.container.ecom.counters.defaults.CountersServiceInput;
import com.rokittech.container.ecom.counters.defaults.CountersServiceOutput;
import com.rokittech.container.ecom.counters.defaults.CountersServiceParams;
import com.rokittech.container.ecom.counters.defaults.CountersVocabulary;
import com.rokittech.container.ecom.order.defaults.OrderServiceInput;
import com.rokittech.container.ecom.order.defaults.OrderServiceOutput;
import com.rokittech.container.ecom.order.defaults.OrderServiceParams;
import com.rokittech.container.ecom.order.defaults.OrderVocabulary;
import com.rokittech.container.ecom.order.factory.OrderServiceFactory;
import com.rokittech.containers.api.core.Service;
import com.rokittech.ecommerce.web.userobjects.OrderInput;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/order")
public class OrderController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(OrderController.class);
    private static final String DB_ORDER = "Order";
    private final Info info = new Info (); 
        
    public OrderController() {
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @param requestJson
     * @return
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/insert")
    public String createOrder(String requestJson) {
        slf4jLogger.info("Creating order - ");
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);
        OrderInput orderInput = new OrderInput().fromJson(requestJson);
        
        // This is real mandatory code to move in controller
        OrderServiceParams params = new OrderServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(OrderVocabulary.COLLECTION.name(), DB_ORDER);
        params.getProperties().put(OrderVocabulary.COMMAND.name(), OrderVocabulary.INSERT.name());

        OrderServiceInput input = new OrderServiceInput();
        Basket basket = orderInput.getData();
        Order order = new Order();
        order.setBasketId(basket.getId());
        order.setBillingAddress(basket.getBillingAddress());
        order.setCustomerId(basket.getUser().getId());
        order.setGrossPrice(basket.getTotal());
        order.setNetPrice(basket.getSubTotal());
        order.setTax(basket.getTax());
        order.setLinetems(basket.getLineItems());
        order.setStatus(OrderStatusType.NEW);
        
        order.setOrderNumber(getAutoGenOrderNumber (order.getCustomerId(), orderInput.getInfo()));

        Service<OrderServiceInput, OrderServiceOutput, OrderServiceParams, DefaultPersist> service 
                = new OrderServiceFactory(OrderVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input); 
        service.setOutput(new OrderServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("Creaetd order - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

   

    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/find/{id}")
    public String getOrderById(@PathParam("id") String _id) {
        slf4jLogger.info("Deleting order - ");
        Order order = new Order ();
        order.setId(_id);
        
        slf4jLogger.info("request param: " + order.toJson());
        
        // This is real mandatory code to move in controller
        OrderServiceParams params = new OrderServiceParams();
        DefaultPersist persist = new DefaultPersist();

        OrderServiceInput input = new OrderServiceInput();
        input.setInfo(info);
        input.setData(order);
        
        params.getProperties().put(OrderVocabulary.COLLECTION.name(), DB_ORDER);
        params.getProperties().put(OrderVocabulary.COMMAND.name(), OrderVocabulary.RETRIEVE.name());

        Service<OrderServiceInput, OrderServiceOutput, OrderServiceParams, DefaultPersist> service = new OrderServiceFactory(OrderVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrderServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        slf4jLogger.info("got priduct - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/update")
    public String updateOrder (String requestJson) {
        slf4jLogger.info("Updating order - ");
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        OrderServiceParams params = new OrderServiceParams();
        DefaultPersist persist = new DefaultPersist();

        OrderServiceInput input = new OrderServiceInput().fromJson(requestJson);

        params.getProperties().put(OrderVocabulary.COLLECTION.name(), DB_ORDER);
        params.getProperties().put(OrderVocabulary.COMMAND.name(), OrderVocabulary.UPDATE.name());

        Service<OrderServiceInput, OrderServiceOutput, OrderServiceParams, DefaultPersist> service = new OrderServiceFactory(OrderVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrderServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("updated order - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/list")
    public String listOrder(String requestJson) {
        slf4jLogger.info("List order - ");
        if (!PersistUtils.validateInput(requestJson)) {
            slf4jLogger.error("invalid request!");
            return "{\"ERROR\":\": invalid request!\"}";
        }
        
        slf4jLogger.debug("Request " + requestJson);

        // This is real mandatory code to move in controller
        OrderServiceParams params = new OrderServiceParams();
        DefaultPersist persist = new DefaultPersist();

        OrderServiceInput input = new OrderServiceInput().fromJson(requestJson);

        params.getProperties().put(OrderVocabulary.COLLECTION.name(), DB_ORDER);
        params.getProperties().put(OrderVocabulary.COMMAND.name(), OrderVocabulary.LIST.name());

        Service<OrderServiceInput, OrderServiceOutput, OrderServiceParams, DefaultPersist> service = 
                new OrderServiceFactory(OrderVocabulary.WEI_ECOMMERCE.name(), 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new OrderServiceOutput());

        // Run service and get output data.
        String output = service.serve().toJson();

        slf4jLogger.info("list order - ");
        slf4jLogger.debug("Response " + output);
        
        return output;
    }

    /**
     * 
     * @param info
     * @return 
     */
    private String getAutoGenOrderNumber(String custId, Info info) {
        Counter counter = new Counter ();
        counter.setId("ordernumber");
        CountersServiceInput cinput = new CountersServiceInput();
        cinput.setInfo(info);
        
        cinput.setData(counter);
        
        CountersServiceOutput coutput = new CountersServiceOutput();
        // This is real mandatory code to move in controller
        CountersServiceParams cparams = new CountersServiceParams();
        DefaultPersist cpersist = new DefaultPersist();

        cparams.getProperties().put(CountersVocabulary.COLLECTION.name(), "counters");
        cparams.getProperties().put(CountersVocabulary.COMMAND.name(), CountersVocabulary.COUNTER.name());
        Service<CountersServiceInput, CountersServiceOutput, CountersServiceParams, DefaultPersist> cservice 
                = new OrderServiceFactory(OrderVocabulary.WEI_ECOMMERCE.name(), 1).get();

        cservice.setParams(cparams);
        cservice.setPersist(cpersist);
        cservice.setInput(cinput); 
        cservice.setOutput(coutput);
        // Run service and get output data.
        //
        String cntr_output = cservice.serve().toJson();
        Counter cout = new Counter ().fromJson(cntr_output);
        
        return custId + LocalDate.now().format(DateTimeFormatter.ofPattern("ddMMyyyy")) + cout;
    }
}
