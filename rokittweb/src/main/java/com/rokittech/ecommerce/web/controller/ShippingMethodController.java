/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.ShippingMethod;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodServiceInput;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodServiceOutput;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodServiceParams;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodVocabulary;
import com.rokittech.container.ecom.shippingmethod.factory.ShippingMethodServiceFactory;
import com.rokittech.containers.api.core.Service;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/shippingmethod")
public class ShippingMethodController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(PricingController.class);
    
    private Info info = new Info ();
    
    public ShippingMethodController() {
        info.setVersion(1);
        info.setUserId("test1");
        info.setAppId("wei");
    }

    /**
     *
     * @param requestJson
     * @return
     */
    // ShippingMethod CRUD
    //Insert ShippingMethod
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/insert")
    public String createShippingMethod(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        ShippingMethodServiceParams params = new ShippingMethodServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(ShippingMethodVocabulary.COLLECTION.name(), "ShippingMethod");
        params.getProperties().put(ShippingMethodVocabulary.COMMAND.name(), ShippingMethodVocabulary.INSERT.name());

        ShippingMethodServiceInput input = new ShippingMethodServiceInput().fromJson(requestJson);

        Service<ShippingMethodServiceInput, ShippingMethodServiceOutput, ShippingMethodServiceParams, DefaultPersist> service = new ShippingMethodServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ShippingMethodServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/update")
    public String updateShippingMethod(String requestJson) {
        String output = "";
        if (!PersistUtils.validateInput(requestJson)) {

            System.out.println("*********" + "ERROR " + this.getClass().getName() + " invalid request**********");
            return "{\"ERROR\":\": invalid request!\"}";
        }

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " input request **********\n" + requestJson);
        // This is real mandatory code to move in controller
        ShippingMethodServiceParams params = new ShippingMethodServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(ShippingMethodVocabulary.COLLECTION.name(), "ShippingMethod");
        params.getProperties().put(ShippingMethodVocabulary.COMMAND.name(), ShippingMethodVocabulary.UPDATE.name());

        ShippingMethodServiceInput input = new ShippingMethodServiceInput().fromJson(requestJson);

        Service<ShippingMethodServiceInput, ShippingMethodServiceOutput, ShippingMethodServiceParams, DefaultPersist> service = new ShippingMethodServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ShippingMethodServiceOutput());

        // Run service and get output data.
        //
        output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Delete ShippingMethod
    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/delete/{id}")
    public String deleteShippingMethod(@PathParam("id") String _id) {
        slf4jLogger.info("Deleting user - ");
        ShippingMethod user = new ShippingMethod ();
        user.setId(_id);
        slf4jLogger.info("request param: " + user.toJson());
        
        // This is real mandatory code to move in controller
        ShippingMethodServiceParams params = new ShippingMethodServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(ShippingMethodVocabulary.COLLECTION.name(), "ShippingMethod");
        params.getProperties().put(ShippingMethodVocabulary.COMMAND.name(), ShippingMethodVocabulary.DELETE.name());

        ShippingMethodServiceInput input = new ShippingMethodServiceInput();
        input.setData(user);
        input.setInfo(info);

        Service<ShippingMethodServiceInput, ShippingMethodServiceOutput, ShippingMethodServiceParams, DefaultPersist> service = new ShippingMethodServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ShippingMethodServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }

    //Find ShippingMethod
    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/find/{id}")
    public String findShippingMethod(@PathParam("id") String _id) {
        slf4jLogger.info("Finding user - ");
        ShippingMethod user = new ShippingMethod ();
        user.setId(_id);
        slf4jLogger.info("request param: " + user.toJson());
        
        // This is real mandatory code to move in controller
        ShippingMethodServiceParams params = new ShippingMethodServiceParams();
        DefaultPersist persist = new DefaultPersist();

        params.getProperties().put(ShippingMethodVocabulary.COLLECTION.name(), "ShippingMethod");
        params.getProperties().put(ShippingMethodVocabulary.COMMAND.name(), ShippingMethodVocabulary.RETRIEVE.name());

        ShippingMethodServiceInput input = new ShippingMethodServiceInput();
        input.setData(user);
        input.setInfo(info);

        Service<ShippingMethodServiceInput, ShippingMethodServiceOutput, ShippingMethodServiceParams, DefaultPersist> service = new ShippingMethodServiceFactory("WEI_ECOMMERCE", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(input);
        service.setOutput(new ShippingMethodServiceOutput());

        // Run service and get output data.
        //
        String output = service.serve().toJson();

        //TODO replace  log
        System.out.println("*********" + "INFO " + this.getClass().getName() + " Output Response **********\n" + output);

        return output;
    }
}
