 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.userobjects;


import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class ProductToCatalog implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    private String _id;
    private String productId;
    private List<String> catalogs;
    
    public ProductToCatalog() {
    }

    public ProductToCatalog(String _id, String productId, List<String> catalogs) {
        this._id = _id;
        this.productId = productId;
        this.catalogs = catalogs;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<String> getCatalogs() {
        return catalogs;
    }

    public void setCatalogs(List<String> catalogs) {
        this.catalogs = catalogs;
    }

    
    @Override
    public String toJson() {
        return new Gson().toJson(this, ProductToCatalog.class);
    }

    @Override
    public ProductToCatalog fromJson(String json) {
        return new Gson().fromJson(json, ProductToCatalog.class);
    }

    @Override
    public void init() {
        //TODO complete Address init
    }
}
