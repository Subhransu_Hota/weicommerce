/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.ecommerce.web.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.server.mvc.Viewable;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
@Path("/test")
public class TestController {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(TestController.class);
    /**
     *
     * @return
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public Viewable getIt() {
        Viewable vb = new Viewable("/payment");
        return vb;
    }
}
