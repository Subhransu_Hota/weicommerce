/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.rokittech.containers.api.core.Transit;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alexmy
 */
public class WorkflowUtils {

    public static JestClient getEsClient(String json) {
        // Construct a new Jest client according to configuration via factory
        JestClientFactory factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig.Builder("http://localhost:9200")
                .multiThreaded(true)
                .build());
        JestClient client = factory.getObject();

        return client;
    }

    /**
     *
     * @param transit
     * @param inputMap
     * @param mapper
     * @return
     */
    public static Map<String, Object> mapTransit(Transit transit, Map<String, Object> inputMap, ObjectMapper mapper) {

        int transitsize = transit.size();
        int inputMapSize = inputMap.keySet().size();

        int counter = 0;

        // Clone inputMap to null-valued Map
        //
        HashMap<String, Object> nullInputMap = new HashMap<>();
        inputMap.keySet().forEach(s -> nullInputMap.put(s, null));

        // Iterate from the end of the transit ArrayList
        // to start putting latest values for resulr fields
        //
        for (int i = transitsize - 1; i >= 0; i--) {

            // First extract typeJson (that is JSON of saved in triple object)
            // and convert it into Map
            //
            String typeJson = transit.get(i).getTypeJson();
            Map<String, Object> tripleMap = null;
            try {
                
                String className = transit.get(i).getTypeName();
                Class _class = Class.forName(className);
                
                tripleMap = mapper.convertValue(new Gson().fromJson(typeJson, _class), Map.class);

                // Then try to fill-in nullInputMap Map with values from the current tripleMap
                //
                if (tripleMap != null) {
                    for (String s : tripleMap.keySet()) {
                        if ((nullInputMap.containsKey(s) && nullInputMap.get(s) == null)) {
                            nullInputMap.put(s, tripleMap.get(s));
                            counter++;
                            // Check if nullInputMap is filled up
                            //
                            if (counter > inputMapSize - 1) {
                                break;
                            }
                        }
                    }
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(WorkflowUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return nullInputMap;
    }

    // Private methods to support serve( ) method
    //=========================================================
    /**
     *
     * @param persistData
     * @param workflow
     * @return
     * @throws JsonSyntaxException
     */
    public static String getCurrentState(String persistData, WorkflowTool workflow) throws JsonSyntaxException {
        // Check if persistAsJson is null. If it is null initialize TransitionalOutput
        // and persistence
        //
        if (persistData == null) {
            initTransit(workflow);
        } else {
            try {
                workflow.setTransit(new Gson().fromJson(persistData, Transit.class));
            } catch (RuntimeException e) {
                initTransit(workflow);
            }
        }

        String key = getWorkflowState(workflow.getTransit());

        return key;
    }

    /**
     *
     * @param id
     * @param workflow
     */
    private static void initTransit(WorkflowTool workflow) {
        workflow.setTransit(new Transit());
    }

    /**
     *
     * @param transit
     * @return
     */
    public static String getWorkflowState(Transit transit) {
        String key;

        try {
            key = transit.get(transit.size() - 1).getKey();
        } catch (RuntimeException e) {
            key = null;
        }
        
        return key;
    }
}
