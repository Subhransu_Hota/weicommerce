/*
 * To change this license infor, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base.defaults;

import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 * @author alexmy
 * @param <R>
 */
public abstract class Input<R> implements Json, FromJson, Initializable, Serializable {

    private Info info;
    private R data;

    /**
     * @return the info
     */
    public Info getInfo() {
        return info;
    }

    /**
     * @param info
     */
    public void setInfo(Info info) {
        this.info = info;
    }

    public void setData(R data){
        this.data = data;
    }

    public R getData(){
        return data;
    }
}
