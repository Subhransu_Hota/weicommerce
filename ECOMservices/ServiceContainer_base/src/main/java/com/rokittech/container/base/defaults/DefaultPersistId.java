/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base.defaults;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.Json;

/**
 *
 * @author alexmy
 */
public class DefaultPersistId implements Json {
    
    private String appId;
    private String userId;
    private String service;

    /**
     * @return the appId
     */
    public String getAppId() {
        return appId;
    }

    /**
     * @param appId the appId to set
     */
    public void setAppId(String appId) {
        this.appId = appId;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(String service) {
        this.service = service;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, DefaultPersistId.class);
    }
    
    @Override
    public String toString(){
        return getAppId() + ":" + getService() + ":" + getUserId();
    }
}
