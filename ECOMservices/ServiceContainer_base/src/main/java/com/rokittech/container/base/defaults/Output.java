/*
 * To change this license info, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base.defaults;

import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 * @author alexmy
 * @param <D>
 */
public abstract class Output<D> implements Json, FromJson, Initializable, Serializable {
    
    private static final long serialVersionUID = -6230708294339490620L;
    
    Info info;
    D data;
    
    /**
     * @return the info
     */
    public Info getInfo() {
        return info;
    }

    /**
     * @param header the info to set
     */
    public void setInfo(Info header) {
        this.info = header;
    }

    /**
     * @return the data
     */
    public D getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(D data) {
        this.data = data;
    }

}

