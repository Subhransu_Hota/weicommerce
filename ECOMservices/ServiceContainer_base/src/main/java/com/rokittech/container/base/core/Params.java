/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base.core;

import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.Properties;

/**
 *
 * @author alexmy
 * @param <T>
 */
public interface Params<T> extends Json, FromJson, Initializable, Serializable {
    
    Properties getProperties();
    
    void setProperties(Properties props);
    
    public String getEnvId();
    
    public void setEnvId(String envId);
    
    /**
     * 
     * @return 
     */
    public T getEnv();
    
    /**
     * 
     * @param env 
     */
    public void setEnv(T env);
    
    /**
     * Initialize environment using provided properties
     * 
     * @param props 
     * @return  
     */
    public T initEnv(Properties props);
    
    /**
     * Initializing environment using properties file
     * 
     * @param fileName 
     * @return  
     */
    public T initEnv(String fileName);
    
}
