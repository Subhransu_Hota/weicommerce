/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.organization;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Organization;
import com.rokittech.container.ecom.organization.defaults.OrganizationServiceInput;
import com.rokittech.container.ecom.organization.defaults.OrganizationServiceOutput;
import com.rokittech.container.ecom.organization.defaults.OrganizationServiceParams;
import com.rokittech.container.ecom.organization.defaults.OrganizationVocabulary;
import com.rokittech.container.ecom.organization.factory.OrganizationServiceFactory;
import com.rokittech.containers.api.core.Service;
import java.util.Date;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);

        //This code is temp code of UserAddress for POC
        

        //This User code is temp code for POC
        Organization organization = new Organization();
        organization.setOrganizationId("Rokit");
        organization.setOrganizationName("Rokit Technology");
        organization.setDescription("Rokit Marketplace");
       // This is real mandatory code to move in controller
        OrganizationServiceParams params = new OrganizationServiceParams();
        //params.initEnv("env.properties");
        
//        MongoClient mongo = new MongoClient("weicommerce-backend-dev.elasticbeanstalk.com", 27017);
//        MongoDatabase db = mongo.getDatabase("ecommerce");
//        
//        params.setEnv(db);
        params.getProperties().put(OrganizationVocabulary.COLLECTION.name(), "Organization");
        params.getProperties().put(OrganizationVocabulary.COMMAND.name(), OrganizationVocabulary.INSERT.name());

        DefaultPersist persist = null;//new DefaultPersist();
        //persist.initEnv("env.properties");

//        CrudNoSqlServiceInput json = new CrudNoSqlServiceInput();
        //
        OrganizationServiceInput inputTemp = new OrganizationServiceInput();
        inputTemp.setData(organization);
        inputTemp.setInfo(info);
        
        System.out.println (inputTemp.toJson());

//        CrudNoSqlService_0_0_1 service;        
        Service<OrganizationServiceInput, OrganizationServiceOutput, OrganizationServiceParams, DefaultPersist> service = new OrganizationServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        //service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new OrganizationServiceOutput());

        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
