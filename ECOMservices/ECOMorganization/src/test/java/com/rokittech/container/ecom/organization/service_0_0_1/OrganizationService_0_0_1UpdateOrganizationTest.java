/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.organization.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.models.Organization;
import com.rokittech.container.ecom.organization.defaults.OrganizationServiceInput;
import com.rokittech.container.ecom.organization.defaults.OrganizationServiceOutput;
import com.rokittech.container.ecom.organization.defaults.OrganizationServiceParams;
import com.rokittech.container.ecom.organization.defaults.OrganizationVocabulary;
import com.rokittech.container.ecom.organization.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class OrganizationService_0_0_1UpdateOrganizationTest {

    private static final Logger logger = LoggerFactory.getLogger(OrganizationService_0_0_1UpdateOrganizationTest.class);

    public OrganizationService_0_0_1UpdateOrganizationTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        logger.info("Setup test resouces");

        OrganizationServiceInput input = new OrganizationServiceInput();
        Organization organization = TestUtil.getInstance().createNewOrganizationInDatabase();

        //Update description
        organization.setOrganizationName("updated Organization Name");

        input.setData(organization);
        input.setInfo(TestUtil.getInstance().createInfo());

        OrganizationServiceOutput output = new OrganizationServiceOutput();
        // This is real mandatory code to move in controller
        OrganizationServiceParams params = TestUtil.getInstance().getParams();

        params.getProperties().put(OrganizationVocabulary.COLLECTION.name(), "Organization");
        params.getProperties().put(OrganizationVocabulary.COMMAND.name(), OrganizationVocabulary.UPDATE.name());

        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }

    @After
    public void tearDown() {
        logger.info("Cleanup all test resouces");
    }

    /**
     * Test of getInputAsJson method, of class OrganizationService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        logger.info("Started Execution of getInputAsJson ");

        String result = TestUtil.getInstance().getService().getInputAsJson();
        OrganizationServiceInput input = new OrganizationServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);

        logger.info("Finished Execution of getInputAsJson ");
    }

    /**
     * Test of serve method, of class OrganizationService_0_0_1.
     */
    @Test
    public void testServe() {
        logger.info("Started Execution of serve ");

        OrganizationServiceOutput result = TestUtil.getInstance().getService().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isOrganizationEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                result.getData()));

        logger.info("Finished Execution of serve ");
    }

    /**
     * Test of getOutputAsJson method, of class OrganizationService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        logger.info("Started Execution of getOutputAsJson ");

        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        OrganizationServiceOutput output = new OrganizationServiceOutput().fromJson(result);

        assertTrue("input and output are not same", TestUtil.getInstance().isOrganizationEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                output.getData()));

        logger.info("Finished Execution of getOutputAsJson ");
    }

    /**
     * Test of toJson method, of class OrganizationService_0_0_1.
     */
    @Test
    public void testToJson() {
        logger.info("Started Execution of toJson ");

        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);

        logger.info("Finished Execution of toJson ");
    }
}
