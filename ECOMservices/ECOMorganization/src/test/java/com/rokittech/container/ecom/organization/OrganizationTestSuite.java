package com.rokittech.container.ecom.organization;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.rokittech.container.ecom.organization.service_0_0_1.OrganizationService_0_0_1DeleteOrganizationTest;
import com.rokittech.container.ecom.organization.service_0_0_1.OrganizationService_0_0_1FindByIdOrganizationTest;
import com.rokittech.container.ecom.organization.service_0_0_1.OrganizationService_0_0_1InsertOrganizationTest;
import com.rokittech.container.ecom.organization.service_0_0_1.OrganizationService_0_0_1UpdateOrganizationTest;
import com.rokittech.container.ecom.organization.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    OrganizationService_0_0_1InsertOrganizationTest.class,
    OrganizationService_0_0_1UpdateOrganizationTest.class,
    OrganizationService_0_0_1DeleteOrganizationTest.class,
    OrganizationService_0_0_1FindByIdOrganizationTest.class,})
public class OrganizationTestSuite {

    private static final Logger logger = LoggerFactory.getLogger(OrganizationTestSuite.class);

    @BeforeClass
    public static void setUpClass() throws Exception {

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        logger.info("Initiated Mongo database");

        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        logger.info("Cleanup Mongo database");

        TestUtil.getInstance().cleanup();

    }

}
