package com.rokittech.container.ecom.user;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.rokittech.container.ecom.user.service_0_0_1.UserService_0_0_1DeleteUserTest;
import com.rokittech.container.ecom.user.service_0_0_1.UserService_0_0_1FindByIdUserTest;
import com.rokittech.container.ecom.user.service_0_0_1.UserService_0_0_1InsertUserTest;
import com.rokittech.container.ecom.user.service_0_0_1.UserService_0_0_1UpdateUserTest;
import com.rokittech.container.ecom.user.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    UserService_0_0_1InsertUserTest.class,
    UserService_0_0_1UpdateUserTest.class,
    UserService_0_0_1DeleteUserTest.class,
    UserService_0_0_1FindByIdUserTest.class,})
public class UserTestSuite {

    private static final Logger logger = LoggerFactory.getLogger(UserTestSuite.class);

    @BeforeClass
    public static void setUpClass() throws Exception {

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        logger.info("Initiated Mongo database");
        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        //Cleanup Resources
        logger.info("Cleanup Mongo database");

        TestUtil.getInstance().cleanup();

    }

}
