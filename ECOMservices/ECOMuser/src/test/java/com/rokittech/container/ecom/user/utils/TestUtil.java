/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.user.utils;

import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.container.ecom.commons.models.AddressType;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.commons.models.UserCredentials;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import com.rokittech.container.ecom.user.defaults.UserServiceParams;
import com.rokittech.container.ecom.user.factory.UserServiceFactory;
import com.rokittech.container.ecom.user.service_0_0_1.UserService_0_0_1;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Date;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class TestUtil {
    private final UserService_0_0_1 service;
    private static final Logger logger = LoggerFactory.getLogger(TestUtil.class);

    private static final UserServiceParams params = new UserServiceParams();
    private MongoClient client;
    private MongoServer server;

    private TestUtil() {
        service = (UserService_0_0_1) new UserServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
       ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        //params.initEnv("env.properties");
        logger.info("Mongo Db connection created succesfully");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }

    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public User createUser() {
        Address address = new Address();
        address.setAddressName("sweet home");
        address.setAddressType(AddressType.Billing_And_Shipping);
        address.setCity("Bangalore");
        address.setCreatedDate(new Date());
        
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setUserName("asif");
        userCredentials.setPassword("#123");
        userCredentials.setEmail("email");
        
        User user = new User();
        user.setBillingAddress(Arrays.asList(address));
        user.setShippingAddress(Arrays.asList(address));
        user.setUserCredentials(userCredentials);
        user.setFirstName("Deependra");
        user.setLastName("Kumar");
        user.setCompany("Rokitt");
        user.setCreatedDate(new Date());
        user.setPhoneNo("9845555760");

        return user;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);

        return info;
    }

    public boolean isUserEqual(String input, String output) {
        logger.info("Input :" + input + "\n Output : " + output);
        User inputUser = new User().fromJson(input);
        User outputUser = new User().fromJson(output);

        boolean b = inputUser.getFirstName().equalsIgnoreCase(outputUser.getFirstName())
                && inputUser.getLastName().equalsIgnoreCase(outputUser.getLastName())
                && inputUser.getCompany().equalsIgnoreCase(outputUser.getCompany())
                && inputUser.getPhoneNo().equalsIgnoreCase(outputUser.getPhoneNo());

        return b;
    }

    public UserServiceParams getParams() {
        return params;
    }

    public User createNewUserInDatabase() {

        logger.info("Creating test data");
        Address address = new Address();
        address.setAddressName("sweet home");
        address.setAddressType(AddressType.Billing_And_Shipping);
        address.setCity("Bangalore");
        address.setCreatedDate(new Date());
        
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setUserName("asif");
        userCredentials.setPassword("#123");
        userCredentials.setEmail("email");
        
        User user = new User();
        user.setBillingAddress(Arrays.asList(address));
        user.setShippingAddress(Arrays.asList(address));
        user.setUserCredentials(userCredentials);
        user.setFirstName("Deependra");
        user.setLastName("Kumar");
        user.setCompany("Rokitt");
        user.setCreatedDate(new Date());
        user.setPhoneNo("9845555760");

        DBObject object = (DBObject) JSON.parse(user.toJson());

        client.getDatabase("ecommerce_test").getCollection("User", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId) object.get("_id");
        object.put("_id", objId.toString());

        return new User().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObject(String result, String input) {
        User inputUser = new User ().fromJson(input);
        User outputUser = new User ().fromJson(result);
        
        return inputUser.getId().equalsIgnoreCase(outputUser.getId());
    }

    public void cleanup() {

        logger.info("Cleanup test data");
        client.getDatabase("ecommerce_test").getCollection("User").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    public UserService_0_0_1 getService() {
        return service;
    }

    
    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }
}
