/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.user.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.user.defaults.UserServiceInput;
import com.rokittech.container.ecom.user.defaults.UserServiceOutput;
import com.rokittech.container.ecom.user.defaults.UserServiceParams;
import com.rokittech.container.ecom.user.defaults.UserVocabulary;
import com.rokittech.container.ecom.user.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class UserService_0_0_1DeleteUserTest {

    private static final Logger logger = LoggerFactory.getLogger(UserService_0_0_1DeleteUserTest.class);
    
    public UserService_0_0_1DeleteUserTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        logger.info("Setup test resouces");
        UserServiceInput input = new UserServiceInput();
        User user = TestUtil.getInstance().createNewUserInDatabase();

        input.setData(user);
        input.setInfo(TestUtil.getInstance().createInfo());

        UserServiceOutput output = new UserServiceOutput();
        // This is real mandatory code to move in controller
        UserServiceParams params = TestUtil.getInstance().getParams();

        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.DELETE.name());

        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }

    @After
    public void tearDown() {
        logger.info("Cleanup all test resouces");
    }

    /**
     * Test of getInputAsJson method, of class UserService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        logger.info("Started Execution of getInputAsJson ");

        String result = TestUtil.getInstance().getService().getInputAsJson();
        UserServiceInput input = new UserServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
        logger.info("Finished Execution of getInputAsJson ");
    }

    /**
     * Test of serve method, of class UserService_0_0_1.
     */
    @Test
    public void testServe() {
        logger.info("Started Execution of Serve ");
        UserServiceOutput result = TestUtil.getInstance().getService().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isUserEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                result.getData()));
        logger.info("Finished Execution of Serve ");
    }

    /**
     * Test of getOutputAsJson method, of class UserService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {

        logger.info("Started Execution of getOutputAsJson ");
        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        UserServiceOutput output = new UserServiceOutput().fromJson(result);

        assertTrue("input and output are not same", TestUtil.getInstance().isUserEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                output.getData()));
        logger.info("Finished Execution of getOutputAsJson ");
    }

    /**
     * Test of toJson method, of class UserService_0_0_1.
     */
    @Test
    public void testToJson() {
        logger.info("Started Execution of toJson ");
        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
        logger.info("Finished Execution of toJson ");
    }
}
