/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.user;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.core.PasswordUtil;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.container.ecom.commons.models.AddressType;
import com.rokittech.container.ecom.commons.models.UserCredentials;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.user.defaults.UserServiceInput;
import com.rokittech.container.ecom.user.defaults.UserServiceOutput;
import com.rokittech.container.ecom.user.defaults.UserServiceParams;
import com.rokittech.container.ecom.user.defaults.UserVocabulary;
import com.rokittech.container.ecom.user.factory.UserServiceFactory;
import com.rokittech.containers.api.core.Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);

        //This code is temp code of Address for POC
        List<Address> billingAddress=new ArrayList<Address>();
        Address address = new Address();
        address.setAddressId("billing1");
        address.setStreet1("test");
        address.setStreet2("test2");
        address.setCity("Bangalore");
        address.setState("Karnataka");
        address.setCountry("India");
        address.setZipCode("560037");
        address.setAddressType(AddressType.Billing);
        billingAddress.add(address);

        List<Address> shippingAddress=new ArrayList<Address>();
        Address sAddress = new Address();
        sAddress.setAddressId("shipping1");
        sAddress.setStreet1("test");
        sAddress.setStreet2("test2");
        sAddress.setCity("Bangalore");
        sAddress.setState("Karnataka");
        sAddress.setCountry("India");
        sAddress.setZipCode("560037");
        sAddress.setAddressType(AddressType.Shipping);
        shippingAddress.add(sAddress);
        
        //This User code is temp code for POC
        User user = new User();
        user.setFirstName("Deependra");
        user.setLastName("Kumar");
        user.setCompany("Rokitt");
        user.setCreatedDate(new Date());
        user.setPhoneNo("9845555760");

        //This code is temp code of UserCredentials for POC
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("deependra@gmail.com");
        userCredentials.setHintQuestion("What is your pet name");
        userCredentials.setHintAnswer("Test");
        userCredentials.setUserName(userCredentials.getEmail());
        userCredentials.setResetPassword(true);
        userCredentials.setPassword(PasswordUtil.generateSecurePassword("rokit2015"));
        user.setBillingAddress(billingAddress);
        user.setShippingAddress(shippingAddress);
        user.setUserCredentials(userCredentials);
        System.out.print(user);
        
        // This is real mandatory code to move in controller
        UserServiceParams params = new UserServiceParams();
        //params.initEnv("env.properties");
        params.getProperties().put(UserVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(UserVocabulary.COMMAND.name(), UserVocabulary.INSERT.name());

        DefaultPersist persist = new DefaultPersist();
        //persist.initEnv("env.properties");

//        CrudNoSqlServiceInput json = new CrudNoSqlServiceInput();
        //
        UserServiceInput inputTemp = new UserServiceInput();
        inputTemp.setData(user);
        inputTemp.setInfo(info);

//        CrudNoSqlService_0_0_1 service;        
        Service<UserServiceInput, UserServiceOutput, UserServiceParams, DefaultPersist> service = new UserServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new UserServiceOutput());

        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
