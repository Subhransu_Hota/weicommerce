/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.user.defaults;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.ecom.commons.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public final class UserServiceInput extends Input<User> {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceInput.class);

//    @JsonUnwrapped
//    private String data;
    private String template;

    /**
     *
     */
    public UserServiceInput() {
        this.init();
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, UserServiceInput.class);
    }

    @Override
    public UserServiceInput fromJson(String json) {
        return new Gson().fromJson(json, UserServiceInput.class);
    }

    @Override
    public final void init() {
        this.setTemplate("");
    }

    /**
     *
     * @return gets template
     */
    public String getTemplate() {
        return template;
    }

    /**
     *
     * @param template set template
     */
    public void setTemplate(String template) {
        this.template = template;
    }
}
