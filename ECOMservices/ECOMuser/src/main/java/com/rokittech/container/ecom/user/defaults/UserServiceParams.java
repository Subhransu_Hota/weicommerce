/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.user.defaults;

import com.google.gson.Gson;
import com.rokittech.container.base.core.Params;

import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra
 */
public class UserServiceParams implements Params{

    private static final Logger logger = LoggerFactory.getLogger(UserServiceParams.class);
    private Properties properties;

    private String envId;

    public UserServiceParams() {
        this.properties = new Properties();
    }

    @Override
    public String getEnvId() {
        return envId;
    }

    @Override
    public void setEnvId(String envId) {
        this.envId = envId;
    }

   
    @Override
    public Properties getProperties() {
        return properties == null ? new Properties() : properties;

    }

    @Override
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, UserServiceParams.class);
    }

    @Override
    public Object fromJson(String json) {
        return new Gson().fromJson(json, UserServiceParams.class);
    }

    @Override
    public void init() {
        this.properties = new Properties();

    }

    @Override
    public void setEnv(Object env) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getEnv() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object initEnv(Properties props) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object initEnv(String fileName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
