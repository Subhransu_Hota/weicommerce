/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.productcatalogassignment.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceInput;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceOutput;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceParams;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentVocabulary;
import com.rokittech.container.ecom.productcatalogassignment.utils.TestUtil;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Sanga Hiremath
 * @CreatedDate 28th Sep 2015
 */
public class ProductCatalogAssignmentService_0_0_1InsertTest {

    private static final Logger logger = LoggerFactory.getLogger(ProductCatalogAssignmentService_0_0_1InsertTest.class);

    public ProductCatalogAssignmentService_0_0_1InsertTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        logger.info("Setup test resouces");

        
        ProductCatalogAssignmentServiceInput input = new ProductCatalogAssignmentServiceInput();
        input.setData(TestUtil.getInstance().createProductCatalogAssignment(
                "test_address_" + new Random ().nextInt(Integer.MAX_VALUE)));
        input.setInfo(TestUtil.getInstance().createInfo());

        ProductCatalogAssignmentServiceOutput output = new ProductCatalogAssignmentServiceOutput();
        // This is real mandatory code to move in controller
        ProductCatalogAssignmentServiceParams params = TestUtil.getInstance().getParams();

        params.getProperties().put(ProductCatalogAssignmentVocabulary.COLLECTION.name(), "ProductCatalogAssignment");
        params.getProperties().put(ProductCatalogAssignmentVocabulary.COMMAND.name(), ProductCatalogAssignmentVocabulary.INSERT.name());

        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }

    @After
    public void tearDown() {
        logger.info("Cleanup all test resouces");
    }

    /**
     * Test of getInputAsJson method, of class ProductCatalogAssignmentService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        logger.info("Started Execution of getInputAsJson ");

        String result = TestUtil.getInstance().getService().getInputAsJson();
        ProductCatalogAssignmentServiceInput input = new ProductCatalogAssignmentServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);

        logger.info("Finished Execution of getInputAsJson ");
    }

    /**
     * Test of serve method, of class ProductCatalogAssignmentService_0_0_1.
     */
    @Test
    public void testServe() {
        logger.info("Started Execution of serve ");

        ProductCatalogAssignmentServiceOutput result = TestUtil.getInstance().getService().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isProductCatalogAssignmentEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                result.getData()));

        logger.info("Finished Execution of serve ");
    }

    /**
     * Test of getOutputAsJson method, of class ProductCatalogAssignmentService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        logger.info("Started Execution of getOutputAsJson ");

        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        ProductCatalogAssignmentServiceOutput output = new ProductCatalogAssignmentServiceOutput().fromJson(result);

        assertTrue("input and output are not same", TestUtil.getInstance().isProductCatalogAssignmentEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                output.getData()));

        logger.info("Finished Execution of getOutputAsJson ");
    }

    /**
     * Test of toJson method, of class ProductCatalogAssignmentService_0_0_1.
     */
    @Test
    public void testToJson() {
        logger.info("Started Execution of toJson ");

        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);

        logger.info("Finished Execution of toJson ");
    }
}
