/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.productcatalogassignment.utils;

import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.ProductCatalogAssignment;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceParams;
import com.rokittech.container.ecom.productcatalogassignment.factory.ProductCatalogAssignmentServiceFactory;
import com.rokittech.container.ecom.productcatalogassignment.service_0_0_1.ProductCatalogAssignmentService_0_0_1;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.net.InetSocketAddress;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Sanga Hiremath
 * @CreatedDate 28th Sep 2015
 */
public class TestUtil {

    private static final Logger logger = LoggerFactory.getLogger(TestUtil.class);

    private static final ProductCatalogAssignmentServiceParams params = new ProductCatalogAssignmentServiceParams();
    private final ProductCatalogAssignmentService_0_0_1 service;
    private MongoClient client;
    private MongoServer server;

    private TestUtil() {
        service = (ProductCatalogAssignmentService_0_0_1) new ProductCatalogAssignmentServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverProductCatalogAssignment = server.bind();
        client = new MongoClient(new ServerAddress(serverProductCatalogAssignment));

        //Initiate Mongodb
        ((MongoManager) service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        logger.info("Mongo Db connection created succesfully");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }

    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public ProductCatalogAssignment createProductCatalogAssignment(String userId) {
        ProductCatalogAssignment assignment = new ProductCatalogAssignment();
        assignment.setCatalogId("catalog_id_1");
        assignment.setProductId("product_id_1");
        return assignment;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);

        return info;
    }

    public boolean isProductCatalogAssignmentEqual(String input, String output) {
        logger.info("Input :" + input + "\n Output : " + output);

        ProductCatalogAssignment inputProductCatalogAssignment =
                new ProductCatalogAssignment().fromJson(input);
        ProductCatalogAssignment outputProductCatalogAssignment = 
                new ProductCatalogAssignment().fromJson(output);

        return inputProductCatalogAssignment.getCatalogId().equalsIgnoreCase(outputProductCatalogAssignment.getCatalogId())
                && inputProductCatalogAssignment.getProductId().equals(outputProductCatalogAssignment.getProductId());
    }

    public ProductCatalogAssignmentServiceParams getParams() {
        return params;
    }

    public ProductCatalogAssignment createNewProductCatalogAssignmentInDatabase(String userId) {

        ProductCatalogAssignment assign = new ProductCatalogAssignment();

        assign.setCatalogId("catalog_id_1");
        assign.setProductId("product_jd_1");

        DBObject object = (DBObject) JSON.parse(assign.toJson());

        client.getDatabase("ecommerce_test").getCollection("ProductCatalogAssignment", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId) object.get("_id");
        object.put("_id", objId.toString());

        return new ProductCatalogAssignment().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObject(String result, String input) {
        ProductCatalogAssignment inputProductCatalogAssignment = new ProductCatalogAssignment().fromJson(input);
        ProductCatalogAssignment outputProductCatalogAssignment = new ProductCatalogAssignment().fromJson(result);

        return inputProductCatalogAssignment.getId().equalsIgnoreCase(outputProductCatalogAssignment.getId());
    }

    public void cleanup() {
        logger.info("Cleanup test data");
        client.getDatabase("ecommerce_test").getCollection("ProductCatalogAssignment").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }

    public ProductCatalogAssignmentService_0_0_1 getService() {
        return service;
    }
}