package com.rokittech.container.ecom.productcatalogassignment;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.rokittech.container.ecom.productcatalogassignment.service_0_0_1.ProductCatalogAssignmentService_0_0_1FindByIdTest;
import com.rokittech.container.ecom.productcatalogassignment.service_0_0_1.ProductCatalogAssignmentService_0_0_1InsertTest;
import com.rokittech.container.ecom.productcatalogassignment.service_0_0_1.ProductCatalogAssignmentService_0_0_1UpdateTest;
import com.rokittech.container.ecom.productcatalogassignment.service_0_0_1.ProductCatalogAssignmentService_0_0_1_DeleteTest;
import com.rokittech.container.ecom.productcatalogassignment.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Sanga Hiremath
 * @CreatedDate 28th Sep 2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    ProductCatalogAssignmentService_0_0_1InsertTest.class,
    ProductCatalogAssignmentService_0_0_1UpdateTest.class,
    ProductCatalogAssignmentService_0_0_1_DeleteTest.class,
    ProductCatalogAssignmentService_0_0_1FindByIdTest.class
})
public class ProductCatalogAssignmentTestSuite {

    private static final Logger logger = LoggerFactory.getLogger(ProductCatalogAssignmentTestSuite.class);

    @BeforeClass
    public static void setUpClass() throws Exception {

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        logger.info("Initiated Mongo database");

        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        logger.info("Cleanup Mongo database");

        TestUtil.getInstance().cleanup();

    }

}
