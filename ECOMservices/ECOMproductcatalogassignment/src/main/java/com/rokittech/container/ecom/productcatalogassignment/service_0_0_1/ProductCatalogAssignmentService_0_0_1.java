/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.productcatalogassignment.service_0_0_1;

import com.google.gson.Gson;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceInput;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceOutput;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceParams;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentVocabulary;
import com.rokittech.container.ecom.commons.core.AbstractService;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterManager;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.container.ecom.dbadapter.factory.DBManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Sanga Hiremath
 * @CreatedDate 18th Sep 2015
 */
public class ProductCatalogAssignmentService_0_0_1 extends AbstractService<ProductCatalogAssignmentServiceInput, ProductCatalogAssignmentServiceOutput, ProductCatalogAssignmentServiceParams, DefaultPersist> {

    private static final Logger logger = LoggerFactory.getLogger(ProductCatalogAssignmentService_0_0_1.class);
    private ProductCatalogAssignmentServiceInput input;
    private ProductCatalogAssignmentServiceOutput output;
    private ProductCatalogAssignmentServiceParams params;

    private DefaultPersist persist;

    private String inputAsJson;
    private String outputAsJson;

    private final DBAdapterManager dbclientManager;
        
    public ProductCatalogAssignmentService_0_0_1() {
        dbclientManager = new DBManagerFactory().get();
    }
    
    public ProductCatalogAssignmentService_0_0_1(DBType dbType) {
        dbclientManager = new DBManagerFactory(dbType).get();
    }
    
    @Override
    public String serviceId() {
        return this.getClass().getName();
    }

    @Override
    public String getInputAsJson() {
        logger.info("returning Input Object in the form of JSON");
        return input.toJson();
    }

    @Override
    public String getOutputAsJson() {
        logger.info("returning Output Object in the form of JSON");
        return output.toJson();
    }

    @Override
    public void setInputAsJson(String input) {
        logger.info("Recreating Input object from json");
        this.input = this.input.fromJson(input);
        this.inputAsJson = input;
    }

    @Override
    public void setOutputAsJson(String output) {
        logger.info("Recreating Ouput object from json");
        this.output = this.output.fromJson(output);
        this.outputAsJson = output;
    }

    @Override
    public ProductCatalogAssignmentServiceOutput serve() {//TODO thorows ServiceException
        logger.info("Inside serve() ");
        DBHandlerInput serviceinput = new DBHandlerInput();
        serviceinput.setData(this.getInput().getData().toJson());
        
        DBHandlerOutput serviceoutput = new DBHandlerOutput();
        // This is real mandatory code to move in controller
        DBHandlertParams serviceparams = new DBHandlertParams();
        serviceparams.setCommand(getParams().getProperties().getProperty(ProductCatalogAssignmentVocabulary.COMMAND.name()));
        serviceparams.setEntity(params.getProperties().getProperty(ProductCatalogAssignmentVocabulary.COLLECTION.name()));
        
        dbclientManager.setInput(serviceinput);
        dbclientManager.setOutput(serviceoutput);
        dbclientManager.setParams(serviceparams);
        dbclientManager.setPersist(new DefaultPersist());
        
        DBHandlerOutput serviceOutput = (DBHandlerOutput) dbclientManager.serve();
      
        output.setData(serviceOutput.getData());
        output.setInfo(input.getInfo());

        logger.info("Created output object " + output.getClass());
        
        return output;
    }

    @Override
    public String toJson() {
        logger.info("generating json for service object");
        return new Gson().toJson(this, ProductCatalogAssignmentService_0_0_1.class);
    }

    public ProductCatalogAssignmentServiceInput getInput() {
        return input;
    }

    @Override
    public void setInput(ProductCatalogAssignmentServiceInput input) {
        this.input = input;
    }

    @Override
    public ProductCatalogAssignmentServiceOutput getOutput() {
        return output;
    }

    @Override
    public void setOutput(ProductCatalogAssignmentServiceOutput output) {
        this.output = output;
    }

    @Override
    public ProductCatalogAssignmentServiceParams getParams() {
        return params;
    }

    @Override
    public void setParams(ProductCatalogAssignmentServiceParams params) {
        this.params = params;
    }

    public DBAdapterManager getDbclientManager() {
        return dbclientManager;
    }
    
    

}
