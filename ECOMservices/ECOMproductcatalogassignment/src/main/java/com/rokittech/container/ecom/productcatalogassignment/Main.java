/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.productcatalogassignment;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.ProductCatalogAssignment;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceInput;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceOutput;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentServiceParams;
import com.rokittech.container.ecom.productcatalogassignment.defaults.ProductCatalogAssignmentVocabulary;
import com.rokittech.container.ecom.productcatalogassignment.factory.ProductCatalogAssignmentServiceFactory;
import com.rokittech.containers.api.core.Service;

/**
 *
 * @author Sanga Hiremath
 * @CreatedDate 18th Sep 2015
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);

        //This code is temp code of UserAttribute for POC
        
        //This code is temp code of UserAttribute for POC
        ProductCatalogAssignment assign = new ProductCatalogAssignment();
        assign.setCatalogId("catalog_id_1");
        assign.setProductId("product_id_a");
        
       // This is real mandatory code to move in controller
        ProductCatalogAssignmentServiceParams params = new ProductCatalogAssignmentServiceParams();
        //params.initEnv("env.properties");
        
        
        params.getProperties().put(ProductCatalogAssignmentVocabulary.COLLECTION.name(), "ProductCatalogAssignment");
        params.getProperties().put(ProductCatalogAssignmentVocabulary.COMMAND.name(), ProductCatalogAssignmentVocabulary.INSERT.name());

        DefaultPersist persist = null;//new DefaultPersist();
        //persist.initEnv("env.properties");

//        CrudNoSqlServiceInput json = new CrudNoSqlServiceInput();
        //
        ProductCatalogAssignmentServiceInput inputTemp = new ProductCatalogAssignmentServiceInput();
        inputTemp.setData(assign);
        inputTemp.setInfo(info);
        
        System.out.println (inputTemp.toJson());

//        CrudNoSqlService_0_0_1 service;        
        Service<ProductCatalogAssignmentServiceInput, ProductCatalogAssignmentServiceOutput, ProductCatalogAssignmentServiceParams, DefaultPersist> service = new ProductCatalogAssignmentServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        //service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new ProductCatalogAssignmentServiceOutput());

        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
