/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.auth;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.core.PasswordUtil;
import com.rokittech.container.ecom.commons.models.UserCredentials;
import com.rokittech.container.ecom.auth.defaults.AuthServiceInput;
import com.rokittech.container.ecom.auth.defaults.AuthServiceOutput;
import com.rokittech.container.ecom.auth.defaults.AuthServiceParams;
import com.rokittech.container.ecom.auth.defaults.AuthVocabulary;
import com.rokittech.container.ecom.auth.factory.AuthServiceFactory;
import com.rokittech.containers.api.core.Service;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 7th Oct 2015
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);
        //This code is temp code of UserCredentials for POC
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("deependra@gmail.com");
        userCredentials.setHintQuestion("What is your pet name");
        userCredentials.setHintAnswer("Test");
        userCredentials.setUserName(userCredentials.getEmail());
        userCredentials.setResetPassword(true);
        userCredentials.setPassword(PasswordUtil.generateSecurePassword("rokit2015"));

        // This is real mandatory code to move in controller
        AuthServiceParams params = new AuthServiceParams();
        params.getProperties().put(AuthVocabulary.COLLECTION.name(), "UserCredentials");
        params.getProperties().put(AuthVocabulary.COMMAND.name(), AuthVocabulary.INSERT.name());

        DefaultPersist persist = new DefaultPersist();
        //persist.initEnv("env.properties");

//        CrudNoSqlServiceInput json = new CrudNoSqlServiceInput();
        //
        AuthServiceInput inputTemp = new AuthServiceInput();
        inputTemp.setData(userCredentials);
        inputTemp.setInfo(info);

//        CrudNoSqlService_0_0_1 service;        
        Service<AuthServiceInput, AuthServiceOutput, AuthServiceParams, DefaultPersist> service = new AuthServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new AuthServiceOutput());
        System.out.println (inputTemp.toJson());
        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
