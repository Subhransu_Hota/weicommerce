/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.auth.defaults;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 * @author deependr
 */
public class LoginStatus implements Json, FromJson, Initializable, Serializable {

    private boolean status;
    private String msg;
    private String userName;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public LoginStatus() {
        this.status=false;
        this.msg="Looged in failed";
        this.userName="";
    }

    public LoginStatus(boolean status, String msg, String userName) {
        this.status = status;
        this.msg = msg;
        this.userName = userName;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, LoginStatus.class);
    }

    @Override
    public Object fromJson(String json) {
        return new Gson().fromJson(json, LoginStatus.class);
    }

    @Override
    public void init() {
        this.status=false;
        this.msg="Looged in failed";
        this.userName="";
    }

}
