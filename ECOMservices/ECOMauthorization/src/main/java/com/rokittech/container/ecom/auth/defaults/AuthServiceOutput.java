/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.auth.defaults;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Output;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class AuthServiceOutput extends Output {

    private static final Logger logger = LoggerFactory.getLogger(AuthServiceOutput.class);
    @JsonUnwrapped
    private String data;

    public AuthServiceOutput() {
        logger.info("inside constructor");
    }

    /**
     *
     * @return get Data
     */
    public String getData() {
        return data;
    }

    /**
     *
     * @param data set Data
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     *
     * @param data constructor
     */
    public AuthServiceOutput(String data) {
        this.data = data;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, AuthServiceOutput.class);
    }

    @Override
    public AuthServiceOutput fromJson(String json) {
        return new Gson().fromJson(json, AuthServiceOutput.class);
    }

    @Override
    public void init() {

    }
}
