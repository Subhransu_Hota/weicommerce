/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.auth.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.auth.defaults.AuthServiceInput;
import com.rokittech.container.ecom.auth.defaults.AuthServiceOutput;
import com.rokittech.container.ecom.auth.defaults.AuthServiceParams;
import com.rokittech.container.ecom.auth.defaults.AuthVocabulary;
import com.rokittech.container.ecom.auth.utils.TestUtil;
import com.rokittech.container.ecom.commons.models.UserCredentials;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class AuthService_0_0_1FindByIdAuthTest {

    private static final Logger logger = LoggerFactory.getLogger(AuthService_0_0_1FindByIdAuthTest.class);
    public AuthService_0_0_1FindByIdAuthTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        logger.info("Setup test resouces");
        AuthServiceInput input = new AuthServiceInput();
        UserCredentials userCredentials = TestUtil.getInstance().createNewUserCredentialsInDatabase(
                "test_address_" + new Random ().nextInt(Integer.MAX_VALUE));

        input.setData(userCredentials);
        input.setInfo(TestUtil.getInstance().createInfo());

        AuthServiceOutput output = new AuthServiceOutput();
        // This is real mandatory code to move in controller
        AuthServiceParams params = TestUtil.getInstance().getParams();

        params.getProperties().put(AuthVocabulary.COLLECTION.name(), "UserCredentials");
        params.getProperties().put(AuthVocabulary.COMMAND.name(), AuthVocabulary.RETRIEVE.name());

        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }

    @After
    public void tearDown() {
        logger.info("Cleanup all test resouces");
    }

    /**
     * Test of getInputAsJson method, of class UserCredentialsService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        logger.info("Started Execution of getInputAsJson ");
        String result = TestUtil.getInstance().getService().getInputAsJson();
        AuthServiceInput input = new AuthServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
        logger.info("Finished Execution of getInputAsJson ");
    }

    /**
     * Test of serve method, of class UserCredentialsService_0_0_1.
     */
    @Test
    public void testServe() {
        logger.info("Started Execution of Serve ");
        AuthServiceOutput result = TestUtil.getInstance().getService().serve();

        assertNotEquals("Result does not have object ",
                TestUtil.getInstance().hasInputObject(result.getData(), TestUtil.getInstance().getService().getInput().getData().toJson()));
        logger.info("Finished Execution of Serve ");
    }

    /**
     * Test of getOutputAsJson method, of class UserCredentialsService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {

        logger.info("Started Execution of getOutputAsJson ");
        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        AuthServiceOutput output = new AuthServiceOutput().fromJson(result);

        assertNotEquals("Result does not have object ",
                TestUtil.getInstance().hasInputObject(output.getData(), TestUtil.getInstance().getService().getInput().getData().toJson()));
        logger.info("Finished Execution of getOutputAsJson ");
    }

    /**
     * Test of toJson method, of class UserCredentialsService_0_0_1.
     */
    @Test
    public void testToJson() {
        logger.info("Started Execution of toJson ");

        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
        logger.info("Finished Execution of toJson ");
    }
}
