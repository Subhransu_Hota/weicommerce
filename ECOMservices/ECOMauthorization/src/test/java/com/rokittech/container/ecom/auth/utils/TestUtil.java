/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.auth.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.auth.defaults.AuthServiceParams;
import com.rokittech.container.ecom.auth.factory.AuthServiceFactory;
import com.rokittech.container.ecom.auth.service_0_0_1.AuthService_0_0_1;
import com.rokittech.container.ecom.commons.core.PasswordUtil;
import com.rokittech.container.ecom.commons.models.UserCredentials;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.List;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class TestUtil {

    private static final Logger logger = LoggerFactory.getLogger(TestUtil.class);
    private final AuthService_0_0_1 service;
    private static final AuthServiceParams params = new AuthServiceParams();
    private MongoClient client;
    private MongoServer server;

    private TestUtil() {
        service = (AuthService_0_0_1) new AuthServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
        ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        //params.initEnv("env.properties");
        logger.info("Mongo Db connection created succesfully");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }

    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public UserCredentials createUserCredentials(String userId) {
        //This code is temp code of UserCredentials for POC
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("deependra@gmail.com");
        userCredentials.setHintQuestion("What is your pet name");
        userCredentials.setHintAnswer("Test");
        userCredentials.setUserName(userCredentials.getEmail());
        userCredentials.setResetPassword(true);
        userCredentials.setPassword(PasswordUtil.generateSecurePassword("rokit2015"));
        
        
        return userCredentials;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);

        return info;
    }

    public boolean isUserCredentialsEqual(String input, String output) {
        logger.info("Input :" + input + "\n Output : " + output);
        UserCredentials inputUserCredentials = new UserCredentials().fromJson(input);
        UserCredentials outputUserCredentials = new UserCredentials().fromJson(output);

        boolean b = inputUserCredentials.getEmail().equalsIgnoreCase(outputUserCredentials.getEmail())
                && inputUserCredentials.getUserName().equalsIgnoreCase(outputUserCredentials.getUserName())
                && inputUserCredentials.getHintQuestion().equalsIgnoreCase(outputUserCredentials.getHintQuestion())
                && inputUserCredentials.getHintAnswer().equalsIgnoreCase(outputUserCredentials.getHintAnswer());
                //&& inputUserCredentials.getUserId().equalsIgnoreCase(outputUserCredentials.getUserId());

        return b;
    }

    public AuthServiceParams getParams() {
        return params;
    }

    public UserCredentials createNewUserCredentialsInDatabase(String userId) {

        logger.info("Creating test data");
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("deependra@gmail.com");
        userCredentials.setHintQuestion("What is your pet name");
        userCredentials.setHintAnswer("Test");
        userCredentials.setUserName(userCredentials.getEmail());
        userCredentials.setResetPassword(true);
        userCredentials.setPassword(PasswordUtil.generateSecurePassword("rokit2015"));
        //userCredentials.setUserId(userId);

        DBObject object = (DBObject) JSON.parse(userCredentials.toJson());
        object.put("is_deleted", false);

        client.getDatabase("ecommerce_test").getCollection("UserCredentials", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId) object.get("_id");
        object.put("_id", objId.toString());

        return new UserCredentials().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObject(String result, String input) {

        UserCredentials inputUserCredentials = new UserCredentials().fromJson(input);

        Type listOfTestObject = new TypeToken<List<UserCredentials>>() {
        }.getType();
        List<UserCredentials> list = new Gson().fromJson(result, listOfTestObject);

        boolean flag = false;
        for (UserCredentials catalog : list) {
            if (catalog.getUserName().equalsIgnoreCase(inputUserCredentials.getUserName())) {
                flag = true;
                break;
            }
        }

        return flag;
    }

    public void cleanup() {

        logger.info("Cleanup test data");
        client.getDatabase("ecommerce_test").getCollection("UserCredentials").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    public AuthService_0_0_1 getService() {
        return service;
    }

    
    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }
}
