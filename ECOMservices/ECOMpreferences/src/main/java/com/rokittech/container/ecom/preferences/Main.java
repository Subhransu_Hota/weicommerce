/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.preferences;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Preferences;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceInput;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceOutput;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceParams;
import com.rokittech.container.ecom.preferences.defaults.PreferencesVocabulary;
import com.rokittech.container.ecom.preferences.factory.PreferencesServiceFactory;
import com.rokittech.containers.api.core.Service;

/**
 * 
 * @author asifaham
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);

        Preferences preferences = new Preferences();
       // This is real mandatory code to move in controller
        PreferencesServiceParams params = new PreferencesServiceParams();
        //params.initEnv("env.properties");
        
        //MongoClient mongo = new MongoClient("weicommerce-backend-dev.elasticbeanstalk.com", 27017);
        //MongoClient mongo = new MongoClient("localhost", 27017);
        //MongoDatabase db = mongo.getDatabase("ecommerce");
        
        //params.setEnv(db);
        params.getProperties().put(PreferencesVocabulary.COLLECTION.name(), "Preferences");
        params.getProperties().put(PreferencesVocabulary.COMMAND.name(), PreferencesVocabulary.INSERT.name());

        DefaultPersist persist = new DefaultPersist();
        //persist.initEnv("env.properties");

        PreferencesServiceInput inputTemp = new PreferencesServiceInput();
        inputTemp.setData(preferences);
        inputTemp.setInfo(info);
        
        System.out.println (inputTemp.toJson());

        Service<PreferencesServiceInput, PreferencesServiceOutput, PreferencesServiceParams, DefaultPersist> service = new PreferencesServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new PreferencesServiceOutput());

        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
