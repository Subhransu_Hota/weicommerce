package com.rokittech.container.ecom.preferences.factory;

import com.google.inject.Provider;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.base.defaults.Output;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceParams;
import com.rokittech.container.ecom.preferences.defaults.PreferencesVocabulary;
import com.rokittech.container.ecom.preferences.service_0_0_1.PreferencesService_0_0_1;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class PreferencesServiceFactory implements Provider<Service<Input, Output, PreferencesServiceParams, DefaultPersist>> {

    private static final Logger logger = LoggerFactory.getLogger(PreferencesServiceFactory.class);
    private Service service;

    /**
     * This is the place where you are managing different versions
     *
     * @param version
     */
//    @Inject
    public PreferencesServiceFactory(int version) {
        this(null, version);

    }

    public PreferencesServiceFactory(String appId, int version) {
        logger.info("Will create the instance of service based on appID: " + appId + " Version: " + version);
        if (appId != null) {
            PreferencesVocabulary app = PreferencesVocabulary.valueOf(appId.toUpperCase());
            switch (app) {
                case WEI_ECOMMERCE:
                    weiEcommerce(version);
                    break;
                case TEST_ECOMMERCE:
                    testEcommerce(version);
                    break;
                default:
                    weiEcommerce( version);
                    break;
            }
        } else {
            weiEcommerce(version);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Service get() {
        return service;
    }

    private void weiEcommerce(int version) {

        switch (version) {
            case 1:
                this.service = new PreferencesService_0_0_1();

                break;
            case 2:

                break;
            default:

                break;

        }
    }

    private void testEcommerce(int version) {
        switch (version) {
            case 1:
                this.service = new PreferencesService_0_0_1(DBType.TEST);
            	
                break;
            case 2:

                break;
            default:

                break;

        }
    }

}
