/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.preferences.defaults;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.ecom.commons.models.Preferences;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public final class PreferencesServiceInput extends Input<Preferences> {

//    @JsonUnwrapped
//    private String data;
    private String template;

    /**
     *
     */
    public PreferencesServiceInput() {
        this.init();
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, PreferencesServiceInput.class);
    }

    @Override
    public PreferencesServiceInput fromJson(String json) {
        return new Gson().fromJson(json, PreferencesServiceInput.class);
    }

    @Override
    public final void init() {
        this.setTemplate("");
    }

    /**
     *
     * @return gets template
     */
    public String getTemplate() {
        return template;
    }

    /**
     *
     * @param template set template
     */
    public void setTemplate(String template) {
        this.template = template;
    }
}
