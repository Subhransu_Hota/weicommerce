/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.preferences.service_0_0_1;

import com.google.gson.Gson;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.core.AbstractService;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterManager;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.container.ecom.dbadapter.factory.DBManagerFactory;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceInput;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceOutput;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceParams;
import com.rokittech.container.ecom.preferences.defaults.PreferencesVocabulary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class PreferencesService_0_0_1 extends AbstractService<PreferencesServiceInput, PreferencesServiceOutput, PreferencesServiceParams, DefaultPersist> {

    private static final Logger logger = LoggerFactory.getLogger(PreferencesService_0_0_1.class);
    private PreferencesServiceInput input;
    private PreferencesServiceOutput output;
    private PreferencesServiceParams params;

    private DefaultPersist persist;

    private String inputAsJson;
    private String outputAsJson;

    private final DBAdapterManager dbclientManager;
        
    public PreferencesService_0_0_1() {
        dbclientManager = new DBManagerFactory().get();
    }
    
    public PreferencesService_0_0_1(DBType dbType) {
        dbclientManager = new DBManagerFactory(dbType).get();
    }
    
    @Override
    public String serviceId() {
        return this.getClass().getName();
    }

    @Override
    public String getInputAsJson() {
        logger.info("returning Input Object in the form of JSON");
        return input.toJson();
    }

    @Override
    public String getOutputAsJson() {
        logger.info("returning Output Object in the form of JSON");
        return output.toJson();
    }

    @Override
    public void setInputAsJson(String input) {
        logger.info("Recreating Input object from json");
        this.input = this.input.fromJson(input);
        this.inputAsJson = input;
    }

    @Override
    public void setOutputAsJson(String output) {
        logger.info("Recreating Ouput object from json");
        this.output = this.output.fromJson(output);
        this.outputAsJson = output;
    }

    @Override
    public PreferencesServiceOutput serve() {//TODO thorows ServiceException
        logger.info("Inside serve() ");
        DBHandlerInput serviceinput = new DBHandlerInput();
        serviceinput.setData(this.getInput().getData().toJson());
        
        DBHandlerOutput serviceoutput = new DBHandlerOutput();
        // This is real mandatory code to move in controller
        DBHandlertParams serviceparams = new DBHandlertParams();
        serviceparams.setCommand(getParams().getProperties().getProperty(PreferencesVocabulary.COMMAND.name()));
        serviceparams.setEntity(params.getProperties().getProperty(PreferencesVocabulary.COLLECTION.name()));
        
        dbclientManager.setInput(serviceinput);
        dbclientManager.setOutput(serviceoutput);
        dbclientManager.setParams(serviceparams);
        dbclientManager.setPersist(new DefaultPersist());
        
        DBHandlerOutput serviceOutput = (DBHandlerOutput) dbclientManager.serve();
      
        output.setData(serviceOutput.getData());
        output.setInfo(input.getInfo());

        logger.info("Created output object " + output.getClass());
        
        return output;
    }

    @Override
    public String toJson() {
        logger.info("generating json for service object");
        return new Gson().toJson(this, PreferencesService_0_0_1.class);
    }

    public PreferencesServiceInput getInput() {
        return input;
    }

    @Override
    public void setInput(PreferencesServiceInput input) {
        this.input = input;
    }

    @Override
    public PreferencesServiceOutput getOutput() {
        return output;
    }

    @Override
    public void setOutput(PreferencesServiceOutput output) {
        this.output = output;
    }

    @Override
    public PreferencesServiceParams getParams() {
        return params;
    }

    @Override
    public void setParams(PreferencesServiceParams params) {
        this.params = params;
    }

    public DBAdapterManager getDbclientManager() {
        return dbclientManager;
    }
    
    

}
