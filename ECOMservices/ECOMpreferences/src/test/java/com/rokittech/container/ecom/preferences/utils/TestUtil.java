/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.preferences.utils;

import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Preferences;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceParams;
import com.rokittech.container.ecom.preferences.factory.PreferencesServiceFactory;
import com.rokittech.container.ecom.preferences.service_0_0_1.PreferencesService_0_0_1;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.net.InetSocketAddress;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class TestUtil {

    private static final Logger logger = LoggerFactory.getLogger(TestUtil.class);

    private static final PreferencesServiceParams params = new PreferencesServiceParams();
    private final PreferencesService_0_0_1 service;
    private MongoClient client;
    private MongoServer server;

    private TestUtil() {
        service = (PreferencesService_0_0_1) new PreferencesServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
        ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        logger.info("Mongo Db connection created succesfully");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }

    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public Preferences createPreferences() {
        Preferences preferences = new Preferences();
        preferences.setName("property1");
        preferences.setValue("value1");
        preferences.setDescription("descr");

        return preferences;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);

        return info;
    }

    public boolean isPreferencesEqual(String input, String output) {
        logger.info("Input :" + input + "\n Output : " + output);

        Preferences inputPreferences = (Preferences) new Preferences().fromJson(input);
        Preferences outputPreferences = (Preferences) new Preferences().fromJson(output);

        boolean b = inputPreferences.getName().equalsIgnoreCase(outputPreferences.getName())
                && inputPreferences.getValue().equalsIgnoreCase(outputPreferences.getValue())
                && inputPreferences.getDescription().equalsIgnoreCase(outputPreferences.getDescription());

        return b;
    }

    public PreferencesServiceParams getParams() {
        return params;
    }

    public Preferences createNewPreferencesInDatabase(String userId) {

        Preferences preferences = new Preferences();
        preferences.setName("property1");
        preferences.setValue("value1");
        preferences.setDescription("descr");

        DBObject object = (DBObject) JSON.parse(preferences.toJson());
        object.put("is_deleted", false);

        client.getDatabase("ecommerce_test").getCollection("Preferences", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId) object.get("_id");
        object.put("_id", objId.toString());

        return new Preferences().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObject(String result, String input) {
        Preferences inputPreferences = new Preferences ().fromJson(input);
        Preferences outputPreferences = new Preferences ().fromJson(result);
        
        return inputPreferences.getId().equalsIgnoreCase(outputPreferences.getId());
    }

    public void cleanup() {
        logger.info("Cleanup test data");
        client.getDatabase("ecommerce_test").getCollection("Preferences").drop();
        client.getDatabase("ecommerce_test").getCollection("Product").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }

    public PreferencesService_0_0_1 getService() {
        return service;
    }
    
    
}
