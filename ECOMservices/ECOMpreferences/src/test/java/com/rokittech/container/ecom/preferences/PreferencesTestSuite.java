package com.rokittech.container.ecom.preferences;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.rokittech.container.ecom.preferences.service_0_0_1.PreferencesService_0_0_1DeletePreferencesTest;
import com.rokittech.container.ecom.preferences.service_0_0_1.PreferencesService_0_0_1FindByIdPreferencesTest;
import com.rokittech.container.ecom.preferences.service_0_0_1.PreferencesService_0_0_1InsertPreferencesTest;
import com.rokittech.container.ecom.preferences.service_0_0_1.PreferencesService_0_0_1UpdatePreferencesTest;
import com.rokittech.container.ecom.preferences.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    PreferencesService_0_0_1InsertPreferencesTest.class,
    PreferencesService_0_0_1UpdatePreferencesTest.class,
    PreferencesService_0_0_1DeletePreferencesTest.class,
    PreferencesService_0_0_1FindByIdPreferencesTest.class,})
public class PreferencesTestSuite {

    private static final Logger logger = LoggerFactory.getLogger(PreferencesTestSuite.class);

    @BeforeClass
    public static void setUpClass() throws Exception {

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        logger.info("Initiated Mongo database");

        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        logger.info("Cleanup Mongo database");

        TestUtil.getInstance().cleanup();

    }

}
