/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.preferences.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceInput;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceOutput;
import com.rokittech.container.ecom.preferences.defaults.PreferencesServiceParams;
import com.rokittech.container.ecom.preferences.defaults.PreferencesVocabulary;
import com.rokittech.container.ecom.preferences.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class PreferencesService_0_0_1InsertPreferencesTest {

    private static final Logger logger = LoggerFactory.getLogger(PreferencesService_0_0_1InsertPreferencesTest.class);

    public PreferencesService_0_0_1InsertPreferencesTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        logger.info("Setup test resouces");

        
        PreferencesServiceInput input = new PreferencesServiceInput();
        input.setData(TestUtil.getInstance().createPreferences());
        input.setInfo(TestUtil.getInstance().createInfo());

        PreferencesServiceOutput output = new PreferencesServiceOutput();
        // This is real mandatory code to move in controller
        PreferencesServiceParams params = TestUtil.getInstance().getParams();

        params.getProperties().put(PreferencesVocabulary.COLLECTION.name(), "Preferences");
        params.getProperties().put(PreferencesVocabulary.COMMAND.name(), PreferencesVocabulary.INSERT.name());

        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }

    @After
    public void tearDown() {
        logger.info("Cleanup all test resouces");
    }

    /**
     * Test of getInputAsJson method, of class PreferencesService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        logger.info("Started Execution of getInputAsJson ");

        String result = TestUtil.getInstance().getService().getInputAsJson();
        PreferencesServiceInput input = new PreferencesServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);

        logger.info("Finished Execution of getInputAsJson ");
    }

    /**
     * Test of serve method, of class PreferencesService_0_0_1.
     */
    @Test
    public void testServe() {
        logger.info("Started Execution of serve ");

        PreferencesServiceOutput result = TestUtil.getInstance().getService().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isPreferencesEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                result.getData()));

        logger.info("Finished Execution of serve ");
    }

    /**
     * Test of getOutputAsJson method, of class PreferencesService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        logger.info("Started Execution of getOutputAsJson ");

        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        PreferencesServiceOutput output = new PreferencesServiceOutput().fromJson(result);

        assertTrue("input and output are not same", TestUtil.getInstance().isPreferencesEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                output.getData()));

        logger.info("Finished Execution of getOutputAsJson ");
    }

    /**
     * Test of toJson method, of class PreferencesService_0_0_1.
     */
    @Test
    public void testToJson() {
        logger.info("Started Execution of toJson ");

        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);

        logger.info("Finished Execution of toJson ");
    }
}
