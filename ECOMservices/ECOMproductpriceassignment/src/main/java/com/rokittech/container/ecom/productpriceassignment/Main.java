/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.productpriceassignment;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.ProductPriceAssignment;
import com.rokittech.container.ecom.productpriceassignment.defaults.ProductPriceAssignmentServiceInput;
import com.rokittech.container.ecom.productpriceassignment.defaults.ProductPriceAssignmentServiceOutput;
import com.rokittech.container.ecom.productpriceassignment.defaults.ProductPriceAssignmentServiceParams;
import com.rokittech.container.ecom.productpriceassignment.defaults.ProductPriceAssignmentVocabulary;
import com.rokittech.container.ecom.productpriceassignment.factory.ProductPriceAssignmentServiceFactory;
import com.rokittech.containers.api.core.Service;

/**
 *
 * @author Sanga Hiremath
 * @CreatedDate 18th Sep 2015
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);

        //This code is temp code of UserAttribute for POC
        
        //This code is temp code of UserAttribute for POC
        ProductPriceAssignment assign = new ProductPriceAssignment();
        assign.setPriceId("catalog_id_1");
        assign.setProductId("product_id_a");
        
       // This is real mandatory code to move in controller
        ProductPriceAssignmentServiceParams params = new ProductPriceAssignmentServiceParams();
        //params.initEnv("env.properties");
        
        
        params.getProperties().put(ProductPriceAssignmentVocabulary.COLLECTION.name(), "ProductPriceAssignment");
        params.getProperties().put(ProductPriceAssignmentVocabulary.COMMAND.name(), ProductPriceAssignmentVocabulary.INSERT.name());

        DefaultPersist persist = null;//new DefaultPersist();
        //persist.initEnv("env.properties");

//        CrudNoSqlServiceInput json = new CrudNoSqlServiceInput();
        //
        ProductPriceAssignmentServiceInput inputTemp = new ProductPriceAssignmentServiceInput();
        inputTemp.setData(assign);
        inputTemp.setInfo(info);
        
        System.out.println (inputTemp.toJson());

//        CrudNoSqlService_0_0_1 service;        
        Service<ProductPriceAssignmentServiceInput, ProductPriceAssignmentServiceOutput, ProductPriceAssignmentServiceParams, DefaultPersist> service = new ProductPriceAssignmentServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        //service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new ProductPriceAssignmentServiceOutput());

        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
