/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.productpriceassignment.defaults;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Output;

/**
 *
 * @author Sanga Hiremath
 * @CreatedDate 18th Sep 2015
 */
public class ProductPriceAssignmentServiceOutput extends Output{
    
    @JsonUnwrapped
    private String data;

    public ProductPriceAssignmentServiceOutput() {
        
    }

    /**
     * 
     * @return get Data
     */
    public String getData() {
        return data;
    }

    /**
     * 
     * @param data set Data
     */
    public void setData(String data) {
        this.data = data;
    }
    
    /**
     * 
     * @param data constructor
     */
    public ProductPriceAssignmentServiceOutput(String data){
        this.data = data;
    }
    
    @Override
    public String toJson() {
        return new Gson().toJson(this,ProductPriceAssignmentServiceOutput.class);
    }

    @Override
    public ProductPriceAssignmentServiceOutput fromJson(String json) {
        return new Gson().fromJson(json,ProductPriceAssignmentServiceOutput.class);
    }

    @Override
    public void init() {
        
    }
}
