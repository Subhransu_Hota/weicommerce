package com.rokittech.container.ecom.productpriceassignment.factory;

import javax.inject.Inject;

import com.google.inject.Provider;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.base.defaults.Output;
import com.rokittech.container.ecom.productpriceassignment.defaults.ProductPriceAssignmentServiceParams;
import com.rokittech.container.ecom.productpriceassignment.defaults.ProductPriceAssignmentVocabulary;
import com.rokittech.container.ecom.productpriceassignment.service_0_0_1.ProductPriceAssignmentService_0_0_1;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Sanga Hiremath
 * @CreatedDate 18th Sep 2015
 */
public class ProductPriceAssignmentServiceFactory implements Provider<Service<Input, Output, ProductPriceAssignmentServiceParams, DefaultPersist>> {

    private static final Logger logger = LoggerFactory.getLogger(ProductPriceAssignmentServiceFactory.class);
    private Service service;

    /**
     * This is the place where you are managing different versions
     *
     * @param version
     */
//    @Inject
    public ProductPriceAssignmentServiceFactory(int version) {
        this(null, version);

    }

    public ProductPriceAssignmentServiceFactory(String appId, int version) {
        logger.info("Will create the instance of service based on appID: " + appId + " Version: " + version);
        if (appId != null) {
            ProductPriceAssignmentVocabulary app = ProductPriceAssignmentVocabulary.valueOf(appId.toUpperCase());
            switch (app) {
                case WEI_ECOMMERCE:
                    weiEcommerce(version);
                    break;
                case TEST_ECOMMERCE:
                    testEcommerce(version);
                    break;
                default:
                    weiEcommerce( version);
                    break;
            }
        } else {
            weiEcommerce(version);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Service get() {
        return service;
    }

    private void weiEcommerce(int version) {

        switch (version) {
            case 1:
                this.service = new ProductPriceAssignmentService_0_0_1();

                break;
            case 2:

                break;
            default:

                break;

        }
    }

    private void testEcommerce(int version) {
        switch (version) {
            case 1:
                this.service = new ProductPriceAssignmentService_0_0_1(DBType.TEST);
            	
                break;
            case 2:

                break;
            default:

                break;

        }
    }

}
