package com.rokittech.container.ecom.importing.defaults;

public enum ImportVocabulary {
	
	COMMAND,
	COLLECTION,
	MONGODB,
	HAZELCAST,
	WEI_ECOMMERCE,
    TEST_ECOMMERCE,
    VALIDATE_IMPORT,
    IMPORT_UPSERT,
    RETRIEVE,
    DB_DUMP_CREATION,
    DB_DUMP_RESTORE;

}
