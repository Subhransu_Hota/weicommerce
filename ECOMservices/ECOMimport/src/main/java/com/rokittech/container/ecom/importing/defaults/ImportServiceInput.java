package com.rokittech.container.ecom.importing.defaults;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Input;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public final class ImportServiceInput extends Input {
	
	private static final Logger slf4jLogger = LoggerFactory.getLogger(ImportServiceInput.class);
    /**
     *
     */
	
	public ImportServiceInput() { 
        slf4jLogger.info("inside constructor");
        this.init();
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, ImportServiceInput.class);
    }

    @Override
    public ImportServiceInput fromJson(String json) {
        return new Gson().fromJson(json, ImportServiceInput.class);
    }

    @Override
    public final void init() {
    }

}
