package com.rokittech.container.ecom.importing.service_0_0_1;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.core.AbstractService;
import com.rokittech.container.ecom.commons.models.Organization;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterManager;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.factory.DBManagerFactory;
import com.rokittech.container.ecom.importing.defaults.ImportServiceInput;
import com.rokittech.container.ecom.importing.defaults.ImportServiceOutput;
import com.rokittech.container.ecom.importing.defaults.ImportServiceParams;
import com.rokittech.container.ecom.importing.defaults.ImportVocabulary;
import com.rokittech.container.ecom.importing.utils.FieldsInformation;

import au.com.bytecode.opencsv.CSVReader;


public class ImportService_0_0_1 extends AbstractService<ImportServiceInput, ImportServiceOutput, ImportServiceParams, DefaultPersist>{
	
	private static final Logger slf4jLogger = LoggerFactory.getLogger(ImportService_0_0_1.class);
    private ImportServiceInput input;
    private ImportServiceOutput output;
    private ImportServiceParams params;
    
    private String resultMessage = "";

    private final DBAdapterManager dbclientManager;
        
    public ImportService_0_0_1() {
        dbclientManager = new DBManagerFactory().get();
    }

    public ImportService_0_0_1(DBHandlertParams.DBType dbType) {
        dbclientManager = new DBManagerFactory(dbType).get();
    }
    
    @Override
    public String serviceId() {
        return this.getClass().getName();
    }

    @Override
    public String getInputAsJson() {
        slf4jLogger.info("returning Input Object in the form of JSON");
        
        return input.toJson();
    }

    @Override
    public String getOutputAsJson() {
        slf4jLogger.info("returning Output Object in the form of JSON");
        return output.toJson();
    }

    @Override
    public void setInputAsJson(String input) {
        slf4jLogger.info("Recreating Input object from json");
        this.input = this.input.fromJson(input);
    }

    @Override
    public void setOutputAsJson(String output) {
        slf4jLogger.info("Recreating Ouput object from json");
        this.output = this.output.fromJson(output);
    }

	@Override
	public ImportServiceOutput serve() {
		slf4jLogger.info("Inside serve() ");
		
		String newFilePath = "";

		String collectionName = params.getProperties().getProperty(ImportVocabulary.COLLECTION.name());
		String filePath = getInput().getData().toString();
		filePath = filePath.substring(filePath.indexOf("=") + 1, filePath.length() - 1);
		
		AWSCredentials credentials = new BasicAWSCredentials("AKIAI4LX6EPGIBQKIJCA", "eLwTJOHzHPeWOHD9XShWZPk0B5kg60yTz34vxzHM");
		AmazonS3 s3client = new AmazonS3Client(credentials);
		String tempLocation = "fileLocation/"+filePath;
		S3Object s3object = s3client.getObject(new GetObjectRequest("weicommerce-dev",tempLocation));
		
		InputStream stream = s3object.getObjectContent();
		
		byte[] content = new byte[1028];
		BufferedOutputStream outputStream = null;
		try {
		 newFilePath = "/opt/recsweb/"+filePath;
		 outputStream = new BufferedOutputStream(new FileOutputStream(newFilePath));
	
		int bytesRead;
			while ((bytesRead = stream.read(content)) != -1) {
			outputStream.write(content, 0, bytesRead);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	//	Boolean isValidFile = isValidFile(filePath);

	//	if (isValidFile) {
			List<FieldsInformation> fieldsList = organizationValidFieldsList(newFilePath, collectionName);
			if (!fieldsList.isEmpty()) {
				List<Organization> list = prepareOrganizationObjsFromCSVData(newFilePath, collectionName, fieldsList);
				String temp = "";
				if(!list.isEmpty()){
				for (int i = 0; i < list.size(); i++) {
					temp = temp + "|" + list.get(i).toJson();
				}

				DBHandlerOutput serviceoutput = new DBHandlerOutput();
				DBHandlerInput serviceinput = new DBHandlerInput();

				serviceinput.setData(temp);

				DBHandlertParams serviceparams = new DBHandlertParams();
				serviceparams.setCommand(getParams().getProperties().getProperty(ImportVocabulary.COMMAND.name()));
				serviceparams.setEntity(params.getProperties().getProperty(ImportVocabulary.COLLECTION.name()));

				dbclientManager.setInput(serviceinput);
				dbclientManager.setOutput(serviceoutput);
				dbclientManager.setParams(serviceparams);
				dbclientManager.setPersist(new DefaultPersist());

				DBHandlerOutput serviceOutput = (DBHandlerOutput) dbclientManager.serve();

				resultMessage = "true, the given csv file is imported succesfully";
				output.setData(resultMessage);

				slf4jLogger.info("Created output object " + output.getClass());
			}
				
			}
	//	}
		output.setData(resultMessage);
		return output;
	}

    @Override
    public String toJson() {
        slf4jLogger.info("generating json for service object");
        return new Gson().toJson(this, ImportService_0_0_1.class);
    }

    @Override
    public ImportServiceInput getInput() {
        return input;
    }

    @Override
    public void setInput(ImportServiceInput input) {
        this.input = input;
    }

    @Override
    public ImportServiceOutput getOutput() {
        return output;
    }

    @Override
    public void setOutput(ImportServiceOutput output) {
        this.output = output;
    }

    @Override
    public ImportServiceParams getParams() {
        return params;
    }

    public DBAdapterManager getDbclientManager() {
        return dbclientManager;
    }

    
    @Override
    public void setParams(ImportServiceParams params) {
        this.params = params;
    }
    
	private Boolean isValidFile(String filePath) {
		boolean isValid = true;
		String fileExtension = filePath.substring(filePath.indexOf(".") + 1);
		if (!(fileExtension.equals("csv") || fileExtension.equals("xml"))) {
			isValid = false;
			resultMessage = "false, the given file is neither CSV nor XML";
		}
		return isValid;
	}
    
	private List<FieldsInformation> organizationValidFieldsList(String filePath, String collectionName) {
		List<FieldsInformation> fieldsList = new ArrayList<FieldsInformation>();
		File dataFile = new File(filePath);
		if (!dataFile.exists())
			return fieldsList;
		if (collectionName.equalsIgnoreCase("Organization")) {

			CSVReader parser;
			try {
				parser = new CSVReader(new FileReader(dataFile), '|');
				String[] firstRow = parser.readNext();
				String[] organizationFields = { "organizationId*", "organizationName*", "description", "createdDate*" };

				List<String> headersList = new ArrayList(Arrays.asList(firstRow));
				for (int i = 0; i < organizationFields.length; i++) {
					if (organizationFields[i].contains("*")) {
						String temp = organizationFields[i].substring(0, organizationFields[i].length() - 1);
						if (!headersList.contains(temp)) {
							resultMessage = "Mandatory header is missed from csv file";
							return fieldsList;
						}

					}
				}

				String temp = "";
				for (int i = 0; i < firstRow.length; i++) {
					FieldsInformation information = new FieldsInformation();

					for (int j = 0; j < organizationFields.length; j++) {
						if (organizationFields[j].contains("*")) {
							temp = organizationFields[j].substring(0, organizationFields[j].length() - 1);
							if (firstRow[i].equals(temp)) {
								information.setOrder(j + 1);
								information.setType("1");
								information.setFieldName(temp);
								break;
							}
						} else {
							temp = organizationFields[j];
							if (firstRow[i].equals(temp)) {
								information.setOrder(j + 1);
								information.setType("0");
								information.setFieldName(temp);
								break;
							}
						}
					}

					if (information.getType().equals("1") && information.getOrder() == 0) {
						resultMessage = "Value missed for Mandatory field(s) is missed from csv file";
						fieldsList.clear();
						return fieldsList;
					}

					fieldsList.add(information);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return fieldsList;
	}
    
	private List<Organization> prepareOrganizationObjsFromCSVData(String filePath, String collectionName,
			List<FieldsInformation> list) {

		List<Organization> organizations = new ArrayList<Organization>();
		File dataFile = new File(filePath);
		try {
			CSVReader parser = new CSVReader(new FileReader(dataFile), '|');
			List<String[]> alltRows = parser.readAll();
			for (int i = 1; i < alltRows.size(); i++) {
				Organization organization = new Organization();
				for (int j =0; j <list.size();j++) {
					switch (list.get(j).getOrder() - 1) {

					case 0:
						String value = alltRows.get(i)[j];
						if (list.get(j).getType().equals("1")) {

							if (value == null || value.isEmpty()) {
								resultMessage = "One of the mandatory value is missed from given csv file";
								organizations.clear();
							    return organizations;
							} else {
								organization.setOrganizationId(value);
							}
						} else {
							if(value != null)
								organization.setOrganizationId(value);
						}
						break;

					case 1:
						String value1 = alltRows.get(i)[j];
						if (list.get(j).getType().equals("1")) {

							if (value1 == null || value1.isEmpty()) {
								resultMessage = "One of the mandatory value is missed from given csv file";
								organizations.clear();
							    return organizations;
							} else {
								organization.setOrganizationName(value1);
							}
						} else {
							if(value1 != null)
								organization.setOrganizationName(value1);
						}
						break;

					case 2:
						String value2 = alltRows.get(i)[j];
						if (list.get(j).getType().equals("1")) {

							if (value2 == null || value2.isEmpty()) {
								resultMessage = "One of the mandatory value is missed from given csv file";
								organizations.clear();
							    return organizations;
							} else {
								organization.setDescription(value2);
							}
						} else {
							if(value2 != null)
								organization.setDescription(value2);
							
						}
						break;

					case 3:
						String value3 = alltRows.get(i)[j];
						boolean isValidDate = isValidDate(value3);
						if (list.get(j).getType().equals("1")) {
							if (!isValidDate) {
								resultMessage = "One of the mandatory value is missed from given csv file";
							    organizations.clear();
							    return organizations;
							
							} else {
								organization.setCreatedDate(new Date(value3));
							}
						} else {
							if(isValidDate)
								organization.setCreatedDate(new Date(value3));
							
						}
						break;
					default:
						break;
					}
				}
				
				organizations.add(organization);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return organizations;
	}
	
	private boolean isValidDate(String input) {
		boolean isValid = false;
		try {
			Date date = new Date(input);
			if (date != null)
				isValid = true;
		} catch (Exception e) {
		//	e.printStackTrace();
		}
		return isValid;
	}
 
}
