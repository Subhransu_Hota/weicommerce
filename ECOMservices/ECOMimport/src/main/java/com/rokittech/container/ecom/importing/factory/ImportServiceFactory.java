package com.rokittech.container.ecom.importing.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Provider;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.base.defaults.Output;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.container.ecom.importing.defaults.ImportServiceParams;
import com.rokittech.container.ecom.importing.defaults.ImportVocabulary;
import com.rokittech.container.ecom.importing.service_0_0_1.ImportService_0_0_1;
import com.rokittech.containers.api.core.Service;

public class ImportServiceFactory implements Provider<Service<Input, Output, 
ImportServiceParams, DefaultPersist>>{
	
private static final Logger slf4jLogger = LoggerFactory.getLogger(ImportServiceFactory.class);
    
    private Service service;

    /**
     * This is the place where you are managing different versions
     *
     * @param version
     */
//    @Inject
    public ImportServiceFactory(int version) {
        this(null, version);

    }

   
    public ImportServiceFactory(String appId, int version) {
        slf4jLogger.info ("Will create the instance of service based on appID: " + appId + " Version: " + version);
        if (appId != null) {
            ImportVocabulary app = ImportVocabulary.valueOf(appId.toUpperCase());
            switch (app) {
                case WEI_ECOMMERCE:
                    weiEcommerce(version);
                    break;
                case TEST_ECOMMERCE:
                    testEcommerce(version);
                    break;
                default:
                    weiEcommerce(version);
                    break;
            }
        } else {
            weiEcommerce(version);
        }
        
        
        }
    

    /**
     *
     * @return
     */
    @Override
    public Service get() {
        return service;
    }

    private void weiEcommerce(int version) {

        switch (version) {
            case 1:
                this.service = new ImportService_0_0_1();
                break;
            case 2:
                break;
            default:
                break;

        }
    }
    
    private void testEcommerce(int version) {

        switch (version) {
            case 1:
                this.service = new ImportService_0_0_1(DBType.TEST);
                break;
            case 2:
                break;
            default:
                break;
        }
    }
}
