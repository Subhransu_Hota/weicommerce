package com.rokittech.container.ecom.importing.utils;

public class FieldsInformation {
	
	private int order;
	private String type;
	private String fieldName;
	
	public FieldsInformation(){
		
	}
	
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}	
}
