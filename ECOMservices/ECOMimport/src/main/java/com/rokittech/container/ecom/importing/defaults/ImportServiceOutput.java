package com.rokittech.container.ecom.importing.defaults;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Output;

public final class ImportServiceOutput extends Output {

	private static final Logger slf4jLogger = LoggerFactory.getLogger(ImportServiceOutput.class);
    @JsonUnwrapped
    private String data;

    public ImportServiceOutput() {
        slf4jLogger.info("inside constructor");
    }

    /**
     * 
     * @return get Data
     */
    public String getData() {
        return data;
    }

    /**
     * 
     * @param data set Data
     */
    public void setData(String data) {
        this.data = data;
    }
    
    /**
     * 
     * @param data constructor
     */
    public ImportServiceOutput(String data){
        this.data = data;
    }
    
    @Override
    public String toJson() {
        return new Gson().toJson(this,ImportServiceOutput.class);
    }

    @Override
    public ImportServiceOutput fromJson(String json) {
        return new Gson().fromJson(json,ImportServiceOutput.class);
    }

    @Override
    public void init() {
        
    }
}
