package com.rokittech.container.ecom.importing.defaults;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.rokittech.container.base.core.Params;

public class ImportServiceParams implements Params{

	private static final Logger slf4jLogger = LoggerFactory.getLogger(ImportServiceParams.class);
    private Properties properties;
    private String envId;

    public ImportServiceParams() {
        this.properties = new Properties();
    }

    @Override
    public String getEnvId() {
        return envId;
    }

    @Override
    public void setEnvId(String envId) {
        this.envId = envId;
    }

    

    @Override
    public Properties getProperties() {
        return properties == null ? new Properties() : properties;
    }

    @Override
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, ImportServiceParams.class);
    }

    @Override
    public Object fromJson(String json) {
        return new Gson().fromJson(json, ImportServiceParams.class);
    }

    @Override
    public void init() {
        this.properties = new Properties();
    }

    @Override
    public Object getEnv() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setEnv(Object env) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object initEnv(Properties props) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object initEnv(String fileName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
