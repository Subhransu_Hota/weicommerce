/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter.mongo.manager;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.rokittech.container.base.defaults.DefaultPersist;

import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterManager;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterVocabulary;
import com.rokittech.container.ecom.dbadapter.mongo.handler.MongoHandler;
import com.rokittech.container.ecom.dbadapter.mongo.handler.DefaultMongoHandler;
import com.rokittech.container.ecom.dbadapter.mongo.handler.QueryMongoHandler;
import com.rokittech.containers.api.core.TransitTriple;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author deependr
 */
public class MongoManager implements DBAdapterManager<DBHandlerInput, DBHandlerOutput, DBHandlertParams, DefaultPersist> {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(MongoManager.class);
    public static final String MONGO_URL = "url";
    public static final String MONGO_PORT = "port";
    public static final String MONGO_DATABASENAME = "databasename";
    public static final String MONGO_DB_USERNAME = "username";
    public static final String MONGO_DB_PASSWORD = "password";
    
    private DBHandlerInput input;
    private DBHandlerOutput output;
    private DBHandlertParams params;
    private DefaultPersist persist;

    private MongoDatabase db;

    public MongoDatabase getDb() {
        return db;
    }

    public void setDb(MongoDatabase db) {
        this.db = db;
    }

    private MongoClient mongo;

    
    @Override
    public String serviceId() {
        return this.getClass().getName();
    }

    @Override
    public String getInputAsJson() {
        slf4jLogger.info("returning Input Object in the form of JSON");

        return input.toJson();
    }

    @Override
    public String getOutputAsJson() {
        slf4jLogger.info("returning Output Object in the form of JSON");
        return output.toJson();
    }

    @Override
    public void setInputAsJson(String input) {
        slf4jLogger.info("Recreating Input object from json");
        this.input = this.input.fromJson(input);
    }

    @Override
    public void setOutputAsJson(String output) {
        slf4jLogger.info("Recreating Ouput object from json");
        this.output = this.output.fromJson(output);
    }

    @Override
    public DBHandlerOutput serve() {
        slf4jLogger.info("Inside serve() ");
        //TODO thorows ServiceException
        TransitTriple inputTriple = new TransitTriple();

        String commandName = params.getCommand();
        slf4jLogger.info("Creating input triple using command-name: " + commandName
                + "type: " + this.getInput().getData().getClass().getName()
                + "typeJson:  *****");

        inputTriple.setKey(commandName);
        inputTriple.setType(this.getInput().getData().getClass().getName());
        inputTriple.setTypeJson(this.getInput().getData());

        // Creat instance of CommandTool and specify command for execution.
        MongoHandler handler;
        
        if (DBAdapterVocabulary.QUERY.name().equalsIgnoreCase(commandName)) {
            handler= new QueryMongoHandler(inputTriple, commandName);
        }else{
            handler=new DefaultMongoHandler(inputTriple, commandName);
        }
        handler.setDb(db);

        slf4jLogger.info("Will make operation call to : "
                + params.getEntity());

        handler.setCollection(params.getEntity());
        //crudTool.setTemplate(params.getProperties().getProperty(DaoVocabulary.TEMPLATE.name()));

        // Run service with provided commandTool
        TransitTriple outputTriple = handler.apply();
        //TODO Throw ServiceException in case of invalid output
        //if(outputTriple == null)  throw new ServiceException(EcommerceErrorCodes.SERVICE_ERROR_INVALIDOUTPUT, "Invalid service output");
        output.setData(outputTriple.getTypeJson());
        output.setInfo(input.getInfo());

        slf4jLogger.info("Created output object " + output.getClass());

        if (output.getData().equalsIgnoreCase("[]")) {
            output.setIsEmpty(true);
        }

        if (mongo != null)
            mongo.close();

        this.setOutput(output);
        return output;
    }

    @Override
    public DBHandlerInput getInput() {
        return input;
    }

    @Override
    public void setInput(DBHandlerInput input) {
        this.input = input;
    }

    @Override
    public DBHandlerOutput getOutput() {
        return output;
    }

    @Override
    public void setOutput(DBHandlerOutput output) {
        this.output = output;
    }

    @Override
    public DBHandlertParams getParams() {
        return params;
    }

    @Override
    public void setParams(DBHandlertParams params) {
        this.params = params;
    }

    @Override
    public DefaultPersist getPersist() {
        return this.persist;
    }

    @Override
    public void setPersist(DefaultPersist persist) {
        this.persist = persist;
    }

    @Override
    public void createFromProperties(Properties properties) {
        slf4jLogger.info("Read all properties from file");
        if (db == null) {
            String mongodbUrl;
            int mongodbPort;
            String mongodbName;
            String mongodbUserName;
            String mongodbPassword;
            mongodbUrl = properties.getProperty(MONGO_URL);
            mongodbPort = new Integer(properties.getProperty(MONGO_PORT));
            mongodbName = properties.getProperty(MONGO_DATABASENAME);
            mongodbUserName = properties.getProperty(MONGO_DB_USERNAME);
            mongodbPassword = properties.getProperty(MONGO_DB_PASSWORD);

            mongo = new MongoClient(mongodbUrl, mongodbPort);

            db = mongo.getDatabase(mongodbName);
            if (db == null) {
                slf4jLogger.error("Mongo db connection creation failed");

            }
            slf4jLogger.info("Mongo db connection created successfully");
        }
    }
}
