/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter.mongo.handler;

//import com.rokittech.container.base.core.WorkflowTool;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterVocabulary;
import com.rokittech.containers.api.core.TransitTriple;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra
 */
public class QueryMongoHandler implements MongoHandler {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(QueryMongoHandler.class);

    private String command;
    private List<TransitTriple> stack;

    private String template;
    private String collection;
    private Map<String, String> attributeMap;

    private MongoDatabase db;

    /**
     *
     *
     * @param triple
     * @param command
     */
    public QueryMongoHandler(TransitTriple triple, String command) {
        slf4jLogger.info("Creating CRUDMongoTool: " + triple.getKey() + " command:" + command);
        stack = new ArrayList<>();
        stack.add(triple);
        this.command = command;
    }

    public String toolId() {
        return this.getClass().getName();
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    /**
     * @return the stack
     */
    public List<TransitTriple> getStack() {
        return stack;
    }

    /**
     * @param stack the stack to set
     */
    public void setStack(List<TransitTriple> stack) {
        this.stack = stack;
    }

    /**
     *
     * @return
     */
    public TransitTriple query() {
        slf4jLogger.info("inside execute " + command);
        String json = stack.get(stack.size() - 1).getTypeJson();

        final ArrayList<Document> listOfObjects = new ArrayList<>();
        DBObject dbObject = (DBObject) JSON.parse(json);
        MongoCollection mongoCollection
                = db.getCollection(collection);

        BasicDBObject condition1 = new BasicDBObject("productId", java.util.regex.Pattern.compile(dbObject.get("keyword").toString()));
        BasicDBObject condition2 = new BasicDBObject("name", java.util.regex.Pattern.compile(dbObject.get("keyword").toString()));
        BasicDBObject condition3 = new BasicDBObject("shortDescription", java.util.regex.Pattern.compile(dbObject.get("keyword").toString()));
        BasicDBObject condition4 = new BasicDBObject("catalogList", java.util.regex.Pattern.compile(dbObject.get("keyword").toString()));
        //BasicDBObject condition5 = new BasicDBObject("catalogList", java.util.regex.Pattern.compile(dbObject.get("keyword").toString()));
        BasicDBList orCondition = new BasicDBList();
        orCondition.add(condition1);
        orCondition.add(condition2);
        orCondition.add(condition3);
        orCondition.add(condition4);
        //orCondition.add(condition5);
        BasicDBObject query = new BasicDBObject("$or", orCondition);
        //query.put("$elemMatch", condition4);
        System.out.print(query);
        mongoCollection.find(query).forEach((Block<Document>) document -> {
            document.put("_id", document.get("_id").toString());
            listOfObjects.add(document);
        });
        slf4jLogger.info("returning " + listOfObjects.size() + " objects");

        slf4jLogger.info("Creating out triple using command-name: " + command
                + " type: " + dbObject.getClass().getName()
                + " typeJson:  *****");
        
        return getTriple(listOfObjects);
    }

    public TransitTriple search() {
        slf4jLogger.info("inside execute " + command);
        String json = stack.get(stack.size() - 1).getTypeJson();

        DBObject dbObject = (DBObject) JSON.parse(json);
        MongoCollection mongoCollection
                = db.getCollection(collection);

        BasicDBObject condition1 = new BasicDBObject("productId", dbObject.get("keyword").toString());
        BasicDBObject condition2 = new BasicDBObject("name", dbObject.get("keyword").toString());
        BasicDBObject condition3 = new BasicDBObject("shortDescription", dbObject.get("keyword").toString());
        BasicDBList orCondition = new BasicDBList();
        orCondition.add(condition1);
        orCondition.add(condition2);
        orCondition.add(condition3);
        BasicDBObject query = new BasicDBObject("$or", orCondition);

        final ArrayList<Document> listOfObjects = new ArrayList<>();

        mongoCollection.find(query).forEach((Block<Document>) document -> {
            document.put("_id", document.get("_id").toString());
            listOfObjects.add(document);
        });
        slf4jLogger.info("returning " + listOfObjects.size() + " objects");

        slf4jLogger.info("Creating out triple using command-name: " + command
                + " type: " + dbObject.getClass().getName()
                + " typeJson:  *****");
        Document document = new Document();
        if (listOfObjects.size() > 0) {
            document = listOfObjects.get(0);
        }
        return getTriple(document);
    }

    /**
     *
     * @param dbObject
     * @return
     */
    private TransitTriple getTriple(Object object) {
        TransitTriple triple = new TransitTriple();
        triple.setKey(command);
        triple.setType(object.getClass().getName());
        triple.setTypeJson(JSON.serialize(object));
        stack.add(triple);

        if (stack == null || stack.isEmpty()) {
            return null;
        } else {
            triple = stack.get(stack.size() - 1);
            return triple;
        }
    }

    /**
     * @return the db
     */
    public MongoDatabase getDb() {
        return db;
    }

    /**
     * @param db the db to set
     */
    public void setDb(MongoDatabase db) {
        this.db = db;
    }

    /**
     * @return the attributeMap
     */
    public Map<String, String> getAttributeMap() {
        return attributeMap;
    }

    /**
     * @param attributeMap the attributeMap to set
     */
    public void setAttributeMap(Map<String, String> attributeMap) {
        this.attributeMap = attributeMap;
    }

    /**
     * @return the template
     */
    public String getTemplate() {
        return template;
    }

    /**
     * @param template the template to set
     */
    public void setTemplate(String template) {
        this.template = template;
    }

    /**
     * @return the collection
     */
    public String getCollection() {
        return collection;
    }

    /**
     * @param collection the collection to set
     */
    public void setCollection(String collection) {
        this.collection = collection;
    }

    @Override
    public TransitTriple apply() {
        if (DBAdapterVocabulary.QUERY.name().equalsIgnoreCase(command)) {
            return this.query();
        }
        return null;
    }
}
