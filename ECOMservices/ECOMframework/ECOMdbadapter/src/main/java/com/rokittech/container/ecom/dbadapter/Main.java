/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.factory.DBManagerFactory;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterManager;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;



/**
 *
 * @author alexmylnikov
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
      
       
        DBHandlertParams params = new DBHandlertParams();
        params.setCommand("INSERT");
        params.setEntity("Catalog");
        
        Properties props = new Properties();
        InputStream in = Main.class.getClassLoader().getResourceAsStream("default.properties");
        try {
            //        System.out.println("Read all properties from file");
            props.load(in);
        } catch (IOException ex) {
        }
        
        params.setProperties(props);
        
        DefaultPersist persist = new DefaultPersist();
        DBHandlerInput inputTemp = new DBHandlerInput();

        inputTemp.setData("");
        
        System.out.println ("***" + inputTemp.toJson());
        DBAdapterManager<DBHandlerInput, DBHandlerOutput, DBHandlertParams, DefaultPersist> dbclientManager 
                = new DBManagerFactory().get();

        dbclientManager.setParams(params);
        dbclientManager.setPersist(persist);
        dbclientManager.setInput(inputTemp.fromJson(inputTemp.toJson()));
        dbclientManager.setOutput(new DBHandlerOutput());

        System.out.println(dbclientManager.serve().getData());
         
        
        System.out.println(dbclientManager.getOutput().getData());
    }

}
