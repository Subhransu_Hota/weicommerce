/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter.defaults;

import java.util.Properties;

/**
 *
 * @author deependr
 * @param <I>
 * @param <O>
 * @param <P>
 * @param <S>
 */
public interface DBAdapterManager<I, O, P, S> {
    String serviceId();

    String getInputAsJson();
    I getInput();
    
    String getOutputAsJson();
    
    O getOutput();
   
    P getParams();
    
    S getPersist();
    
    void setInputAsJson(String input);
    void setInput(I input);
    
    void setOutputAsJson(String output);
    void setOutput(O output);
    
    void setParams(P params);
   
    void setPersist(S persist);
    
    O serve();
    
    void createFromProperties (Properties properties);
}
