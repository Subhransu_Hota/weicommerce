/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter.mongo.handler;

import com.mongodb.client.MongoDatabase;
import com.rokittech.containers.api.core.TransitTriple;

/**
 *
 * @author deependr
 */
public interface MongoHandler {
    public TransitTriple apply();
    public void setDb(MongoDatabase db);
    public void setCollection(String collection);
    
}
