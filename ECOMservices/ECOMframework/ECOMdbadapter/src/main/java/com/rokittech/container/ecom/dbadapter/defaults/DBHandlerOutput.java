/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter.defaults;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Output;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author deependr
 */
public class DBHandlerOutput extends Output {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(DBHandlerOutput.class);
    @JsonUnwrapped
    private boolean isEmpty = false;
    private String data;

    public boolean isIsEmpty() {
        return isEmpty;
    }

    public void setIsEmpty(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }

    public DBHandlerOutput() {
        slf4jLogger.info("inside constructor");
    }

    /**
     *
     * @return get Data
     */
    public String getData() {
        return data;
    }

    /**
     *
     * @param data set Data
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     *
     * @param data constructor
     */
    public DBHandlerOutput(String data) {
        this.data = data;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, DBHandlerOutput.class);
    }

    @Override
    public DBHandlerOutput fromJson(String json) {
        return new Gson().fromJson(json, DBHandlerOutput.class);
    }

    @Override
    public void init() {

    }
}
