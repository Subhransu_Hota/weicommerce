package com.rokittech.container.ecom.dbadapter.factory;

import com.google.inject.Provider;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterManager;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class DBManagerFactory implements Provider<DBAdapterManager<DBHandlerInput, DBHandlerOutput, 
        DBHandlertParams, DefaultPersist>> {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(DBManagerFactory.class);
    
    private DBAdapterManager dbclientManager;

    public DBManagerFactory(DBType dbType) {
        slf4jLogger.info ("Will create the instance of database service based on dbTtype: " + dbType.name());
        initService(dbType);
    }

    public DBManagerFactory() {
        slf4jLogger.info ("Will create the instance of database service based on dbTtype: " + DBType.DEFAULT.name());
        initService(DBType.QUERY);
        Properties properties = new Properties ();
        
        try {
            properties.load(DBManagerFactory.class.getResourceAsStream("/default.properties"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        dbclientManager.createFromProperties(properties);
    }
    
    /**
     * 
     * @param dbType 
     */
    public void initService(DBType dbType){
       
       if (DBType.QUERY.equals(dbType) || DBType.DEFAULT.equals(dbType) || DBType.MONGO.equals(dbType) || DBType.TEST.equals(dbType)){
            dbclientManager = new MongoManager();
        }
    }
    
    /**
     *
     * @return
     */
    @Override
    public DBAdapterManager get() {
        return dbclientManager;
    }
}
