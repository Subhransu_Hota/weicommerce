/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter.defaults;

/**
 *
 * @author deependr
 */
public enum DBAdapterVocabulary {
    INSERT,
    RETRIEVE,
    RETRIEVE_WITH_ROLES,
    UPDATE,
    DELETE,
    LIST,
    COMMAND,
    MONGODB,
    COLLECTIONID,
    QUERY,
    COUNTER,
    UPSERT,
    FIND,
    AUTH,
    IMPORT_UPSERT,
    EXPORT;
}
