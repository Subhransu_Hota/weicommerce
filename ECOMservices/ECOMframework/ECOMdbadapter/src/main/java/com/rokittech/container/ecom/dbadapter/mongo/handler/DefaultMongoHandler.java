/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter.mongo.handler;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
//import com.rokittech.container.base.core.WorkflowTool;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.util.JSON;
import com.rokittech.container.ecom.commons.models.Permission;
import com.rokittech.container.ecom.commons.models.Role;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterVocabulary;
import com.rokittech.container.ecom.dbadapter.utils.ExportUtil;
import com.rokittech.containers.api.core.TransitTriple;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.lucene.queryparser.surround.parser.Token;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alexmy
 */
public class DefaultMongoHandler implements MongoHandler {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(DefaultMongoHandler.class);

    private String command;
    private List<TransitTriple> stack;

    private String template;
    private String collection;
    private Map<String, String> attributeMap;

    private MongoDatabase db;

    /**
     *
     *
     * @param triple
     * @param command
     */
    public DefaultMongoHandler(TransitTriple triple, String command) {
        slf4jLogger.info("Creating CRUDMongoTool: " + triple.getKey() + " command:" + command);
        stack = new ArrayList<>();
        stack.add(triple);
        this.command = command;
    }

    public String toolId() {
        return this.getClass().getName();
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    /**
     * @return the stack
     */
    public List<TransitTriple> getStack() {
        return stack;
    }

    /**
     * @param stack the stack to set
     */
    public void setStack(List<TransitTriple> stack) {
        this.stack = stack;
    }

    /**
     *
     * @return
     */
    public TransitTriple insert() {
        slf4jLogger.info("inside apply: will execute " + command);

        String json = stack.get(stack.size() - 1).getTypeJson();
        DBObject dbObject = (DBObject) JSON.parse(json);
        MongoCollection<DBObject> mongoCollection = db.getCollection(collection, DBObject.class);

        mongoCollection.insertOne(dbObject);

        ObjectId objId = (ObjectId) dbObject.get("_id");

        slf4jLogger.info("_id: " + objId.toString());
        dbObject.put("_id", objId.toString());

        slf4jLogger.info("Creating out triple using command-name: Create "
                + " type: " + dbObject.getClass().getName()
                + " typeJson:  *****");

        return getTriple(dbObject);
    }

    /**
     *
     * @return
     */
    public TransitTriple update() {
        slf4jLogger.info("inside execute " + command);
        String json = stack.get(stack.size() - 1).getTypeJson();

        DBObject dbObject = (DBObject) JSON.parse(json);
        String _id = (String) dbObject.get("_id");
        MongoCollection<DBObject> mongoCollection = db.getCollection(collection, DBObject.class);
        BasicDBObject query = new BasicDBObject();
        query.put("_id", new ObjectId(_id));

        dbObject.removeField("_id");
        UpdateResult result = mongoCollection.updateOne(query, new BasicDBObject("$set", dbObject));

        slf4jLogger.info("Modified records: " + result.getMatchedCount());
        dbObject.put("_id", _id);

        slf4jLogger.info("Creating out triple using command-name: " + command
                + " type: " + dbObject.getClass().getName()
                + " typeJson:  *****");

        return getTriple(dbObject);
    }

    /**
     *
     * @return
     */
    public TransitTriple upsert() {
        slf4jLogger.info("inside execute " + command);
        String json = stack.get(stack.size() - 1).getTypeJson();

        DBObject dbObject = (DBObject) JSON.parse(json);
        String _id = (String) dbObject.get("_id");
        MongoCollection<DBObject> mongoCollection = db.getCollection(collection, DBObject.class);
        BasicDBObject searchQuery = new BasicDBObject().append("_id", _id);

        UpdateOptions updateOptions = new UpdateOptions();
        updateOptions.upsert(Boolean.TRUE);
        UpdateResult result = mongoCollection.updateOne(searchQuery, new BasicDBObject("$set", dbObject), updateOptions);

        slf4jLogger.info("Modified records: " + result.getMatchedCount());
        dbObject.put("_id", _id);

        slf4jLogger.info("Creating out triple using command-name: " + command
                + " type: " + dbObject.getClass().getName()
                + " typeJson:  *****");

        return getTriple(dbObject);
    }

    /**
     *
     * @return
     */
    public TransitTriple delete() {
        slf4jLogger.info("inside execute " + command);
        String json = stack.get(stack.size() - 1).getTypeJson();

        DBObject dbObject = (DBObject) JSON.parse(json);
        MongoCollection<DBObject> mongoCollection
                = db.getCollection(collection, DBObject.class);

        String _id = (String) dbObject.get("_id");
        BasicDBObject query = new BasicDBObject();
        query.put("_id", new ObjectId(_id));

        DeleteResult result = mongoCollection.deleteOne(query);
        slf4jLogger.info("deleted records: " + result.getDeletedCount());

        slf4jLogger.info("Creating out triple using command-name: " + command
                + " type: " + dbObject.getClass().getName()
                + " typeJson:  *****");

        return getTriple(dbObject);
    }

    /**
     *
     * @return
     */
    public TransitTriple retrieve() {
        slf4jLogger.info("inside execute " + command);
        String json = stack.get(stack.size() - 1).getTypeJson();

        DBObject dbObject = (DBObject) JSON.parse(json);
        MongoCollection mongoCollection
                = db.getCollection(collection);
        BasicDBObject query = new BasicDBObject();
        query.put("_id", new ObjectId((String) dbObject.get("_id")));

        final ArrayList<Document> listOfObjects = new ArrayList<>();

        mongoCollection.find(query).forEach((Block<Document>) document -> {
            document.put("_id", document.get("_id").toString());    
            listOfObjects.add(document);
        });

        slf4jLogger.info("returning " + listOfObjects.size() + " objects");

        slf4jLogger.info("Creating out triple using command-name: " + command
                + " type: " + dbObject.getClass().getName()
                + " typeJson:  *****");
        Document document = new Document();
        if (listOfObjects.size() > 0) {
            document = listOfObjects.get(0);
        }
        return getTriple(document);
    }
    
    private TransitTriple retrieveUserwithRoles() {
        slf4jLogger.info("inside execute " + command);
        String json = stack.get(stack.size() - 1).getTypeJson();

        DBObject dbObject = (DBObject) JSON.parse(json);
        MongoCollection mongoCollection
                = db.getCollection(collection);
        BasicDBObject query = new BasicDBObject();
        query.put("_id", new ObjectId((String) dbObject.get("_id")));

        final ArrayList<Document> listOfObjects = new ArrayList<>();

        mongoCollection.find(query).forEach((Block<Document>) document -> {
            document.put("_id", document.get("_id").toString());
            listOfObjects.add(document);
        });

        slf4jLogger.info("returning " + listOfObjects.size() + " objects");

        Document document = new Document();
        if (listOfObjects.size() > 0) {
            document = listOfObjects.get(0);
            Set<ObjectId> queryParams = new HashSet<ObjectId>();
            
            List<String> roleIds = (List<String>) document.get("roles");
            if(roleIds != null){
                for(String id : roleIds){
                    queryParams.add(new ObjectId(id));
                }
                slf4jLogger.info("querying for roles " + queryParams);
                mongoCollection = db.getCollection("Role");

                query.clear();
                query.put("_id", new BasicDBObject("$in", queryParams));

                Map<String, String> userPermissionsMap = new HashMap<String, String>();

                mongoCollection.find(query).forEach((Block<Document>) refDocument -> {
                    refDocument.put("_id", refDocument.get("_id").toString());
                    Role role = new Role().fromJson(refDocument.toJson());
                    Set<Permission> permissions = role.getPermissions();
                    for(Permission permission : permissions){
                        userPermissionsMap.put(permission.getName(), permission.getName());
                    }
                });

                slf4jLogger.info("returning permissions with size " + userPermissionsMap.size());

                //combine user with roles
                document.put("permissions", userPermissionsMap);
            }
        }
        
        slf4jLogger.info("Creating out triple using command-name: " + command
                + " type: " + dbObject.getClass().getName()
                + " typeJson:  *****");
        
        return getTriple(document);
    }


    /**
     *
     * @return
     */
    public TransitTriple counter() {
        slf4jLogger.info("inside execute " + command);
        String json = stack.get(stack.size() - 1).getTypeJson();

        DBObject dbObject = (DBObject) JSON.parse(json);
        MongoCollection mongoCollection
                = db.getCollection(collection);

        String idValue = (String) dbObject.get("_id");
        Object object = mongoCollection.findOneAndUpdate(new Document("_id", idValue), new Document("$inc", new Document("counter", 1)));
        
        slf4jLogger.info("next counter is : " + object.toString());
        return getTriple(object);
    }
    /**
     *
     * @return
     */
    public TransitTriple find() {
        slf4jLogger.info("inside execute " + command);
        String json = stack.get(stack.size() - 1).getTypeJson();

        BasicDBObject dbObject = (BasicDBObject) JSON.parse(json);
        MongoCollection mongoCollection = db.getCollection(collection);

        final List<Document> listOfObjects = new ArrayList<>();
        mongoCollection.find(dbObject).forEach((Block<Document>) document -> {
            document.put("_id", document.get("_id").toString());
            listOfObjects.add(document);
        });

        slf4jLogger.info("returning " + listOfObjects.size() + " objects");

        slf4jLogger.info("Creating out triple using command-name: " + command
                + " type: " + dbObject.getClass().getName()
                + " typeJson:  *****");
        Document document = new Document ();
        if (listOfObjects.size() > 0) {
            document = listOfObjects.get(0);
        }
        return getTriple(document);
    }

    /**
     *
     * @return
     */
    public TransitTriple list() {
        String json = stack.get(stack.size() - 1).getTypeJson();

        DBObject dbObject = (DBObject) JSON.parse(json);
        MongoCollection mongoCollection
                = db.getCollection(collection);
        BasicDBObject query = new BasicDBObject();
        final ArrayList<Document> listOfObjects = new ArrayList<>();
        query.putAll(dbObject);

        mongoCollection.find(query).forEach((Block<Document>) document -> {
            document.put("_id", document.get("_id").toString());
            listOfObjects.add(document);
        });

        slf4jLogger.info("returning " + listOfObjects.size() + " objects");

        slf4jLogger.info("Creating out triple using command-name: " + command
                + " type: " + dbObject.getClass().getName()
                + " typeJson:  *****");

        return getTriple(listOfObjects);
    }
    
    private TransitTriple authenticate() {
        slf4jLogger.info("inside execute " + command);
        String json = stack.get(stack.size() - 1).getTypeJson();

        User user = new User().fromJson(json);
        MongoCollection mongoCollection
                = db.getCollection(collection);

        BasicDBObject query = new BasicDBObject("userCredentials.userName", user.getUserCredentials().getUserName());
        query.put("userCredentials.password", user.getUserCredentials().getPassword());
        
        final ArrayList<Document> listOfObjects = new ArrayList<>();

        mongoCollection.find(query).forEach((Block<Document>) document -> {
            document.put("_id", document.get("_id").toString());
            listOfObjects.add(document);
        });
        slf4jLogger.info("returning " + listOfObjects.size() + " objects");

        slf4jLogger.info("Creating out triple using command-name: " + command
                + " type: " + user.getClass().getName()
                + " typeJson:  *****");
        Document document = new Document();
        if (listOfObjects.size() > 0) {
            document = listOfObjects.get(0);
        }
        return getTriple(document);
    }

    /**
     *
     * @return
     */
    public TransitTriple getMongoId() {
        slf4jLogger.info("inside execute " + command);

        Document document = new Document("_id", new ObjectId().toString());

        return getTriple(document);
    }

    /**
     *
     * @param object
     * @return
     */
    private TransitTriple getTriple(Object object) {
        TransitTriple triple = new TransitTriple();
        triple.setKey(command);
        triple.setType(object.getClass().getName());
        triple.setTypeJson(JSON.serialize(object));
        stack.add(triple);

        if (stack == null || stack.isEmpty()) {
            return null;
        } else {
            triple = stack.get(stack.size() - 1);
            return triple;
        }
    }
    
    
    /**
     * This method is used to import the given collection
     * 
     * @return
     */
	public TransitTriple importUpsert() {
		slf4jLogger.info("inside import upsert execute: " + command);
		String json = stack.get(stack.size() - 1).getTypeJson();
		json = json.substring(1, json.length());
		StringTokenizer tokens = new StringTokenizer(json, "|");
		DBObject dbObject = null;
		while (tokens.hasMoreElements()) {
			dbObject = (DBObject) JSON.parse(tokens.nextToken());
			String key = dbObject.keySet().iterator().next();
			String value = (String) dbObject.get(key);
			MongoCollection<DBObject> mongoCollection = db.getCollection(collection, DBObject.class);
			BasicDBObject searchQuery = new BasicDBObject().append(key, value);
			UpdateOptions updateOptions = new UpdateOptions();
			updateOptions.upsert(Boolean.TRUE);
			UpdateResult result = mongoCollection.updateOne(searchQuery, new BasicDBObject("$set", dbObject),
					updateOptions);

			slf4jLogger.info("Modified records: " + result.getMatchedCount());
			dbObject.put(key, value);
		}
		slf4jLogger.info("Creating out triple using command-name: " + command + " type: "
				+ dbObject.getClass().getName() + " typeJson:  *****");

		return getTriple("CSV Data imported successfully");
	}


	public TransitTriple exportToCSV() {
		slf4jLogger.info("inside execute " + command);
		String json = stack.get(stack.size() - 1).getTypeJson();
		Date date= new java.util.Date();
		String str = new Timestamp(date.getTime()).toString();
		str = str.substring(0, str.lastIndexOf("."));
		str = str.replace(" ", "-");
		str = str.replace(":", "");
		//String filePath = json + "/" + collection + "_"+str + ".csv";
		String fileName = collection + "_"+str + ".csv";
		String tempFilePath = "/opt/recsweb"+ "/" + collection + "_"+str + ".csv";
		slf4jLogger.info("temp path :"+tempFilePath);
		File dataFile = new File(tempFilePath);
		
	
		if (!collection.equalsIgnoreCase("system.indexes")) {
			MongoCollection collectionTemp = db.getCollection(collection);
			MongoCursor<Document> cursorDoc = collectionTemp.find().iterator();
			
			ExportUtil exportUtil = new ExportUtil();
			List<Map<String, String>> listOfObjects = new ArrayList<Map<String, String>>();
            Map<String, String> nestedMap = new LinkedHashMap<String, String>();
            List<String> distinctKeys = new ArrayList<String>();
			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dataFile), "utf-8"));
			
				while (cursorDoc.hasNext()) {
					Map<String, String> map = new LinkedHashMap<String, String>();
					Document collectionElement = cursorDoc.next();
					Set<String> keySet = collectionElement.keySet();
					String[] arr = new String[keySet.size()];
					arr = keySet.toArray(arr);
					
					for(int i=1; i < arr.length; i++){
						Object value = collectionElement.get(arr[i]);
						
						if(value.toString().startsWith("[")){
							List list = (List) value;
							exportUtil.setKeyValueMap(map);
							exportUtil.setDistnictKeys(distinctKeys);
						    exportUtil = getKeyValueFromList(list, arr[i], exportUtil);
						}
						else if(value.toString().startsWith("Document{")){
							Document doc = (Document)value;
							exportUtil.setKeyValueMap(map);
							exportUtil.setDistnictKeys(distinctKeys);
							exportUtil = getKeyAndValueFromDoc(doc, arr[i], exportUtil);	
							
						}else{
							map.put(arr[i].toString(), value.toString());
							if(!distinctKeys.contains(arr[i].toString()))
								distinctKeys.add(arr[i].toString());
							exportUtil.setKeyValueMap(map);
						}
					}
					listOfObjects.add(exportUtil.getKeyValueMap());	
				}
				
				String line = "";
				for(String itr : distinctKeys){
					line = line+itr+"|";
				}
				line = line.substring(0, line.length()-1);
				writer.write(line);
				writer.newLine();
				
				for(Map<String, String> itr : listOfObjects){
				 line = "";
				    for(String itr1 : distinctKeys){
				    	String val= itr.get(itr1);
				    	if(val != null)
			        		line = line+val+"|";
			        	else
			        		line = line+"  "+"|";
			        }
			        line = line.substring(0, line.length()-1);
				    writer.write(line);
					writer.newLine();
			    }
				
				writer.close(); 
				AWSCredentials credentials = new BasicAWSCredentials("AKIAI4LX6EPGIBQKIJCA", "eLwTJOHzHPeWOHD9XShWZPk0B5kg60yTz34vxzHM");
				AmazonS3 s3client = new AmazonS3Client(credentials);
				String fileName1 = "fileLocation/export"+"/"+fileName;
				slf4jLogger.info("file name: "+fileName1);
				
				s3client.putObject(new PutObjectRequest("weicommerce-dev", fileName1, dataFile));
			} catch (UnsupportedEncodingException | FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} /*finally {
				try {
		//			writer.close();
		//		} catch (IOException e) {
		//			e.printStackTrace();
				} */
		
		}
		return getTriple("Data is exported to csv file successfully");
	}
	
	 private ExportUtil getKeyAndValueFromDoc(Document doc, String parentFieldName, ExportUtil exportUtil){
	
		 Map<String, String> keyValues1 = exportUtil.getKeyValueMap();
		 List<String> distnictKeys = exportUtil.getDistnictKeys();
		 
		 Set<String> keySet = doc.keySet();
		 String[] arr = new String[keySet.size()];
			arr = keySet.toArray(arr);
			for(int i=0; i < arr.length; i++){
				Object value = doc.get(arr[i]);
				if(value.toString().startsWith("[")){
					List list = (List) value;
				    exportUtil = getKeyValueFromList(list, arr[i], exportUtil);
				}
				else if(value.toString().startsWith("Document{")){
					Document doc1 = (Document)value;
					String temp = parentFieldName+"."+arr[i].toString();
					exportUtil.setKeyValueMap(keyValues1);
					exportUtil = getKeyAndValueFromDoc(doc1, temp, exportUtil);
				}else{
					String temp = parentFieldName+"."+arr[i].toString();
					keyValues1.put(temp, value.toString());
					if(!distnictKeys.contains(temp))
						distnictKeys.add(temp);
				}
				System.out.println(value);
			}
		 return exportUtil;
	 }
	 
	 private ExportUtil getKeyValueFromList(List list, String parentFieldName, ExportUtil exportUtil){
	
		 for(int j = 0; j < list.size(); j++){
			 if(list.get(j) instanceof String){
				 String key = parentFieldName+(j+1);
				 exportUtil.getKeyValueMap().put(key, list.get(j).toString());
				 if(!exportUtil.getDistnictKeys().contains(key))
					 exportUtil.getDistnictKeys().add(key);
				 
			 }else{
				Document doc1 = (Document)list.get(j);
				exportUtil = getKeyAndValueFromDoc(doc1, parentFieldName+(j+1), exportUtil);
			 }
			}
		 
		 return exportUtil;
	 }
    
    
    

    /**
     * @return the db
     */
    public MongoDatabase getDb() {
        return db;
    }

    /**
     * @param db the db to set
     */
    public void setDb(MongoDatabase db) {
        this.db = db;
    }

    /**
     * @return the attributeMap
     */
    public Map<String, String> getAttributeMap() {
        return attributeMap;
    }

    /**
     * @param attributeMap the attributeMap to set
     */
    public void setAttributeMap(Map<String, String> attributeMap) {
        this.attributeMap = attributeMap;
    }

    /**
     * @return the template
     */
    public String getTemplate() {
        return template;
    }

    /**
     * @param template the template to set
     */
    public void setTemplate(String template) {
        this.template = template;
    }

    /**
     * @return the collection
     */
    public String getCollection() {
        return collection;
    }

    /**
     * @param collection the collection to set
     */
    public void setCollection(String collection) {
        this.collection = collection;
    }

    @Override
    public TransitTriple apply() {
        if (DBAdapterVocabulary.INSERT.name().equalsIgnoreCase(command)) {
            return this.insert();
        } else if (DBAdapterVocabulary.UPDATE.name().equalsIgnoreCase(command)) {
            return this.update();
        } else if (DBAdapterVocabulary.DELETE.name().equalsIgnoreCase(command)) {
            return this.delete();
        } else if (DBAdapterVocabulary.RETRIEVE.name().equalsIgnoreCase(command)) {
            return this.retrieve();
        } else if (DBAdapterVocabulary.RETRIEVE_WITH_ROLES.name().equalsIgnoreCase(command)) {
            return this.retrieveUserwithRoles();
        } else if (DBAdapterVocabulary.LIST.name().equalsIgnoreCase(command)) {
            return this.list();
        } else if (DBAdapterVocabulary.COLLECTIONID.name().equalsIgnoreCase(command)) {
            return this.getMongoId();
        } else if (DBAdapterVocabulary.COUNTER.name().equalsIgnoreCase(command)) {
            return this.counter();
        } else if (DBAdapterVocabulary.UPSERT.name().equalsIgnoreCase(command)) {
            return this.upsert();
        } else if (DBAdapterVocabulary.FIND.name().equalsIgnoreCase(command)) {
            return this.find();
        } else if (DBAdapterVocabulary.AUTH.name().equalsIgnoreCase(command)) {
            return this.authenticate();
        }else if (DBAdapterVocabulary.IMPORT_UPSERT.name().equalsIgnoreCase(command)) {
            return this.importUpsert();
        }else if (DBAdapterVocabulary.EXPORT.name().equalsIgnoreCase(command)) {
            return this.exportToCSV();
        } 

        return null;
    }

}
