/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter.defaults;


import java.util.Properties;

/**
 *
 * @author Deependra
 */
public class DBHandlertParams{

    
    private Properties properties;
    
    private String command;
    private String entity;

    /**
     * 
     * @param props
     */
    public DBHandlertParams( Properties props) {
        this.properties = props;
    }
   
    
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    
    public enum DBType {
        TEST,
        MONGO,
        QUERY,
        SEARCH,
        DEFAULT;
    }
    /**
     * 
     */
    public DBHandlertParams() {
       
    }
    
    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties props) {
        properties = props;
    }
}