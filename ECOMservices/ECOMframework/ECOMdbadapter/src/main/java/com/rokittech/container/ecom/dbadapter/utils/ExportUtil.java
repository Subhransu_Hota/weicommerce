package com.rokittech.container.ecom.dbadapter.utils;

import java.util.List;
import java.util.Map;

public class ExportUtil {
	
	private Map<String, String> keyValueMap;
	private List<String> distnictKeys;
	
	public ExportUtil(){
		
	}
	
	public Map<String, String> getKeyValueMap() {
		return keyValueMap;
	}
	public void setKeyValueMap(Map<String, String> keyValueMap) {
		this.keyValueMap = keyValueMap;
	}
	public List<String> getDistnictKeys() {
		return distnictKeys;
	}
	public void setDistnictKeys(List<String> distnictKeys) {
		this.distnictKeys = distnictKeys;
	}
	
	
	

}
