/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.container.ecom.commons.models.AddressType;
import com.rokittech.container.ecom.commons.models.Catalog;
import com.rokittech.container.ecom.commons.models.Counter;
import com.rokittech.container.ecom.commons.models.Permission;
import com.rokittech.container.ecom.commons.models.Role;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.commons.models.UserCredentials;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.factory.DBManagerFactory;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.bson.types.ObjectId;

/**
 *
 * @author sangamesh
 */
public class TestUtil {
    private static final DBHandlertParams params = new DBHandlertParams();
    private MongoClient client;
    private MongoServer server;
    
    MongoManager mongoManager;
    
    DBManagerFactory factory;
    
    private TestUtil() {
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        factory = new DBManagerFactory(DBHandlertParams.DBType.DEFAULT);
        mongoManager = (MongoManager) factory.get();
        
        //Initiate Mongodb
        mongoManager.setDb(client.getDatabase("ecommerce_test"));
        //params.initEnv("env.properties");
    }

    public MongoManager getMongoManager() {
        return mongoManager;
    }

    public void setMongoManager(MongoManager mongoManager) {
        this.mongoManager = mongoManager;
    }

    
    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }
    
    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public Catalog createCatalog(String catalogId) {
        Catalog catalog = new Catalog ();
        catalog.setCatalogId(catalogId);
        catalog.setDescription("descr");
        catalog.setName("name");
        catalog.setParentCatalogId("0");
        
        return catalog;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        return info;
    }

    public boolean isCatalogEqual(String input, String output) {
        System.out.println ("input: " + input);
        System.out.println("output: " + output);
        
        Catalog inputCatalog = new Catalog ().fromJson(input);
        Catalog outputCatalog = new Catalog ().fromJson(output);
        
        boolean b =  inputCatalog.getCatalogId().equalsIgnoreCase(outputCatalog.getCatalogId()) &&
                inputCatalog.getDescription().equalsIgnoreCase(outputCatalog.getDescription()) &&
                inputCatalog.getName().equalsIgnoreCase(outputCatalog.getName()) &&
                inputCatalog.getParentCatalogId().equalsIgnoreCase(outputCatalog.getParentCatalogId());
        
        return b;
    }

    public DBHandlertParams getParams() {
        return params;
    }
    
    public Role createNewRoleInDatabase(String roleName) {
        Set<Permission> permissions = new HashSet<>(Arrays.asList(
                new Permission("order_update", "update order"), new Permission("order_delete", "delete order"), new Permission("user_update", "update user")));
        Role role = new Role();
        role.setName(roleName);
        role.setDescription("descrp");
        role.setPermissions(permissions);
        DBObject object = (DBObject) JSON.parse(role.toJson());

        client.getDatabase("ecommerce_test").getCollection("Role", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId) object.get("_id");
        object.put("_id", objId.toString());

        return new Role().fromJson(JSON.serialize(object));
    }
    
    public User createNewUserInDatabase(Set<String> roles) {

        Address address = new Address();
        address.setAddressName("sweet home");
        address.setAddressType(AddressType.Billing_And_Shipping);
        address.setCity("Bangalore");
        address.setCreatedDate(new Date());
        
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setUserName("asif");
        userCredentials.setPassword("#123");
        userCredentials.setEmail("email");
        
        User user = new User();
        user.setBillingAddress(Arrays.asList(address));
        user.setShippingAddress(Arrays.asList(address));
        user.setUserCredentials(userCredentials);
        user.setFirstName("Deependra");
        user.setLastName("Kumar");
        user.setCompany("Rokitt");
        user.setCreatedDate(new Date());
        user.setPhoneNo("9845555760");
        user.setRoles(roles);

        DBObject object = (DBObject) JSON.parse(user.toJson());

        client.getDatabase("ecommerce_test").getCollection("User", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId) object.get("_id");
        object.put("_id", objId.toString());

        return new User().fromJson(JSON.serialize(object));
    }

    public Catalog createNewCategoryInDatabase (String catalogId) {
        Catalog catalog = new Catalog ();
        catalog.setCatalogId(catalogId);
        catalog.setDescription("descr");
        catalog.setName("name");
        catalog.setParentCatalogId("0");
        
        DBObject object = (DBObject) JSON.parse(catalog.toJson());
        
        mongoManager.getDb().getCollection("Catalog", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId)object.get("_id");
        object.put("_id", objId.toString());
        
        return new Catalog ().fromJson(JSON.serialize(object));
    }
    
    public Counter createCounterInDatabase () {
        Counter counter = new Counter ();
        counter.setId("ordernumber");
        counter.setCounter(10000);
        DBObject object = (DBObject) JSON.parse(counter.toJson());
        
        mongoManager.getDb().getCollection("counters", DBObject.class).insertOne(object);
        
        return counter;
    }

    public boolean hasInputObjectInList(String result, String input) {
        Catalog inputCatalog = new Catalog ().fromJson(input);
        
        Type listOfTestObject = new TypeToken<List<Catalog>>(){}.getType();
        List<Catalog> list = new Gson().fromJson(result, listOfTestObject);
        
        boolean flag = false;
        for (Catalog catalog : list) {
            if (catalog.getId().equalsIgnoreCase(inputCatalog.getId())){
                flag = true;
                break;
            }
        }
        
        return flag;
    }
    
    public boolean hasInputObject(String result, String input) {
        Catalog inputCatalog = new Catalog ().fromJson(input);
        Catalog outputCatalog = new Catalog ().fromJson(result);
        
        return inputCatalog.getId().equalsIgnoreCase(outputCatalog.getId());
    }
    

    public void cleanup() {
        MongoDatabase mongoDatabase = mongoManager.getDb();
        mongoDatabase.getCollection("Catalog").drop();
        mongoDatabase.getCollection("Product").drop();
        mongoDatabase.drop();

        client.close();
        server.shutdown();
    }

    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }
}
