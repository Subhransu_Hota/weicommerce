/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.models.Role;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.commons.models.UserProfile;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.utils.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author asifaham
 */
public class DBAdapter_0_0_1FindUserWithRolesTest {
    
    public DBAdapter_0_0_1FindUserWithRolesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
       
        DBHandlerInput input = new DBHandlerInput();
        Role role1 = TestUtil.getInstance ().createNewRoleInDatabase ("Role1");
        Role role2 = TestUtil.getInstance ().createNewRoleInDatabase ("Role2");
        Set<String> roles = new HashSet<String>();
        roles.add(new ObjectId(role1.getId()).toString());
        roles.add(new ObjectId(role2.getId()).toString());
        User user = TestUtil.getInstance ().createNewUserInDatabase (roles);
        input.setData(user.toJson());
        
        input.setInfo(TestUtil.getInstance().createInfo());
        
        DBHandlerOutput output = new DBHandlerOutput();
        
        DBHandlertParams params = TestUtil.getInstance().getParams ();
        params.setCommand("RETRIEVE_WITH_ROLES");
        params.setEntity("User");
        
        TestUtil.getInstance().getMongoManager().setInput(input);
        TestUtil.getInstance().getMongoManager().setOutput(output);
        TestUtil.getInstance().getMongoManager().setParams(params);
        TestUtil.getInstance().getMongoManager().setPersist(new DefaultPersist());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInputAsJson method, of class UserService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        System.out.println("getInputAsJson");
        String result = TestUtil.getInstance().getMongoManager().getInputAsJson();
        DBHandlerInput input = new DBHandlerInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
    }

    /**
     * Test of serve method, of class UserService_0_0_1.
     */
    @Test
    public void testServe() {
        System.out.println("serve");
        DBHandlerOutput result = TestUtil.getInstance().getMongoManager().serve();
        
        User inputUser = new User().fromJson(TestUtil.getInstance().getMongoManager().getInput().getData());
        UserProfile outputUser = new UserProfile().fromJson(result.getData());
        
        assertNotEquals("Result does not have object ", 
                inputUser.getId().equalsIgnoreCase(outputUser.getId()));
    }

    /**
     * Test of getOutputAsJson method, of class UserService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        System.out.println("getOutputAsJson");
        
        DBHandlerOutput result = TestUtil.getInstance().getMongoManager().serve();
        DBHandlerOutput output = new DBHandlerOutput().fromJson(result.getData());
        
        assertNotNull("Test Failed: Output As Json is null", 
                output);
    }
    
    /**
     * Test of toJson method, of class UserService_0_0_1.
     */
    @Test
    public void testToJson() {
        System.out.println("toJson");
        String json = TestUtil.getInstance().getMongoManager().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
    }
}
