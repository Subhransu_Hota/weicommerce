/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.utils.TestUtil;
import com.rokittech.container.ecom.commons.models.Catalog;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sangamesh
 */
public class DBAdapter_0_0_1UpdateCategoryTest {
    public DBAdapter_0_0_1UpdateCategoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DBHandlerInput input = new DBHandlerInput();
        Catalog catalog = TestUtil.getInstance ().createNewCategoryInDatabase (
                "test_category_" + new Random ().nextInt(Integer.MAX_VALUE));
        
        //Update description
        catalog.setDescription("updated description");
        
        input.setData(catalog.toJson());
        
        input.setInfo(TestUtil.getInstance().createInfo());
        
        DBHandlerOutput output = new DBHandlerOutput();
        // This is real mandatory code to move in controller
        DBHandlertParams params = TestUtil.getInstance().getParams ();
        params.setCommand("UPDATE");
        params.setEntity("Catalog");
        
        TestUtil.getInstance().getMongoManager().setInput(input);
        TestUtil.getInstance().getMongoManager().setOutput(output);
        TestUtil.getInstance().getMongoManager().setParams(params);
        TestUtil.getInstance().getMongoManager().setPersist(new DefaultPersist());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInputAsJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        System.out.println("getInputAsJson");
        String result = TestUtil.getInstance().getMongoManager().getInputAsJson();
        DBHandlerInput input = new DBHandlerInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
    }

    /**
     * Test of serve method, of class CatalogService_0_0_1.
     */
    @Test
    public void testServe() {
        System.out.println("serve");
        DBHandlerOutput result = TestUtil.getInstance().getMongoManager().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isCatalogEqual (
                TestUtil.getInstance().getMongoManager().getInput().getData(),
                result.getData()));
    }

    /**
     * Test of getOutputAsJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        System.out.println("getOutputAsJson");
        
        TestUtil.getInstance().getMongoManager().serve();
        String result = TestUtil.getInstance().getMongoManager().getOutputAsJson();
        DBHandlerOutput output = new DBHandlerOutput().fromJson(result);
        
        assertTrue("input and output are not same", TestUtil.getInstance().isCatalogEqual (
                TestUtil.getInstance().getMongoManager().getInput().getData(),
                output.getData()));
    }
    
    /**
     * Test of toJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testToJson() {
        System.out.println("toJson");
        String json = TestUtil.getInstance().getMongoManager().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
    }
}
