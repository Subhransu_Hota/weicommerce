/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter;

import com.rokittech.container.ecom.dbadapter.service_0_0_1.DBAdapter_0_0_1FindUserWithRolesTest;
import com.rokittech.container.ecom.dbadapter.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author asifaham
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    DBAdapter_0_0_1FindUserWithRolesTest.class,
})
public class UserTestSuite {
    
    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Initiated Mongo database");

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Dropping tables... ");
        TestUtil.getInstance().cleanup ();
        
    }
}
