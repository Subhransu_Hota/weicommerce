/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.dbadapter.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.utils.TestUtil;
import com.rokittech.container.ecom.commons.models.Counter;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterVocabulary;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sangamesh
 */
public class DBAdapter_0_0_1CounterTest {
    public DBAdapter_0_0_1CounterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Counter counter = TestUtil.getInstance ().createCounterInDatabase();
        
        DBHandlerInput input = new DBHandlerInput();
        input.setData(counter.toJson());
        
        input.setInfo(TestUtil.getInstance().createInfo());
        
        DBHandlerOutput output = new DBHandlerOutput();
        // This is real mandatory code to move in controller
        DBHandlertParams params = TestUtil.getInstance().getParams ();
        params.setCommand(DBAdapterVocabulary.COUNTER.name());
        
        params.setEntity("counters");
        
        TestUtil.getInstance().getMongoManager().setInput(input);
        TestUtil.getInstance().getMongoManager().setOutput(output);
        TestUtil.getInstance().getMongoManager().setParams(params);
        TestUtil.getInstance().getMongoManager().setPersist(new DefaultPersist());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of serve method, of class CatalogService_0_0_1.
     */
    @Test
    public void testServe() {
        System.out.println("serve");
        DBHandlerOutput result = null;
        for (int i = 0; i < 10; i++) {
            result = TestUtil.getInstance().getMongoManager().serve();
            Counter counter = new Counter ().fromJson(result.getData());
            System.out.println ("output is " + counter.getCounter());
        }
        
        assertNotEquals("Result does not have object ", 
                TestUtil.getInstance().hasInputObject (result.getData(), TestUtil.getInstance().getMongoManager().getInput().getData()));
    }
}
