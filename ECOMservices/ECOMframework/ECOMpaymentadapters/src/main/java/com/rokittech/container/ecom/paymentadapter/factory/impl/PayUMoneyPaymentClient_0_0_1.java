/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.paymentadapter.factory.impl;

import com.rokittech.container.ecom.commons.models.Payment;
import com.rokittech.container.ecom.commons.models.PaymentResp;
import com.rokittech.container.ecom.paymentadapter.client.PaymentClient;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.slf4j.LoggerFactory;

/**
 *
 * key: hash: txnid: service_provider: amount: firstname: email: phone:
 * productinfo: surl: furl: lastname: curl: address1: address2: city: state:
 * country: zipcode: udf1: udf2: udf3: udf4: udf5: pg:
 *
 * @author sangamesh
 */
public class PayUMoneyPaymentClient_0_0_1 implements PaymentClient {

    private static final org.slf4j.Logger slf4jLogger
            = LoggerFactory.getLogger(PayUMoneyPaymentClient_0_0_1.class);

    private Payment input;
    private PaymentResp output;

    private Properties properties;

    public PayUMoneyPaymentClient_0_0_1() {
        
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
    
    @Override
    public PaymentResp getFormData() {
        if (validateInput() && validateProps()) {
            //buildMap
            Map<String, String> params = buildParams();
            output = new PaymentResp();
            output.setHost(properties.getProperty("host"));
            output.setFormData(params);
        }
        
        return output;
    }

    @Override
    public void setInput(Payment payment) {
        this.input = payment;
    }

    @Override
    public Payment getInput() {
        return input;
    }

    @Override
    public void setOutput(PaymentResp paymentResp) {
        output = paymentResp;
    }

    @Override
    public PaymentResp getOutput() {
        return output;
    }

    private boolean validateInput() {
        if ((input != null)
                && (input.getAmount() > 0)
                && (null != input.getFirstname()) && (!input.getFirstname().isEmpty())
                && (null != input.getEmail()) && (!input.getEmail().isEmpty())
                && (null != input.getPhone()) && (!input.getPhone().isEmpty())
                && (null != input.getProductInfo()) && (!input.getProductInfo().isEmpty())) {
            return true;
        }

        return false;
    }

    private boolean validateProps() {
        if (properties != null) {
            String host = properties.getProperty("host");
            String key = properties.getProperty("MerchantKey");
            String surl = properties.getProperty("surl");
            String furl = properties.getProperty("furl");
            String curl = properties.getProperty("curl");
            String salt = properties.getProperty("MerchantSalt");
            String provider = properties.getProperty("service_provider");

            if ((null != key) && (!key.isEmpty())
                    && (null != host) && (!host.isEmpty())
                    && (null != surl) && (!surl.isEmpty())
                    && (null != furl) && (!furl.isEmpty())
                    && (null != curl) && (!curl.isEmpty())
                    && (null != salt) && (!salt.isEmpty())
                    && (null != provider) && (!provider.isEmpty())) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @return
     */
    private Map<String, String> buildParams() {
        Map<String, String> params = new HashMap<>();

        //Calculate txnid 
//        Random rand = new Random();
//        String rndm = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
        //String txnid = hashCal("SHA-256", rndm).substring(0, 20);
        String txnid = input.getUserDefinedFields().get("orderId");
        params.put("txnid", txnid);
        params.put("key", properties.getProperty("MerchantKey"));
        params.put("service_provider", properties.getProperty("service_provider"));
        params.put("surl", properties.getProperty("surl"));
        params.put("furl", properties.getProperty("furl"));
        params.put("curl", properties.getProperty("curl"));

        //front-end params - Mandatory
        params.put("amount", String.valueOf(input.getAmount()));
        params.put("firstname", input.getFirstname());
        params.put("email", input.getEmail());
        params.put("phone", input.getPhone());
        params.put("productinfo", input.getProductInfo());

        //front-end params - Optional
        params.put("lastname", ((null != input.getLastname()) && (!input.getLastname().isEmpty())) ? input.getLastname() : "");
        params.put("address1", ((null != input.getAddress1()) && (!input.getAddress1().isEmpty())) ? input.getAddress1() : "");
        params.put("address2", ((null != input.getAddress2()) && (!input.getAddress2().isEmpty())) ? input.getAddress2() : "");
        params.put("city", ((null != input.getPhone()) && (!input.getPhone().isEmpty())) ? input.getPhone() : "");
        params.put("state", ((null != input.getState()) && (!input.getState().isEmpty())) ? input.getState() : "");
        params.put("country", ((null != input.getCountry()) && (!input.getCountry().isEmpty())) ? input.getCountry() : "");
        params.put("zipcode", ((null != input.getZipcode()) && (!input.getZipcode().isEmpty())) ? input.getZipcode() : "");

        params.put("udf1", getUdfParams("udf1"));
        params.put("udf2", getUdfParams("udf2"));
        params.put("udf3", getUdfParams("udf3"));
        params.put("udf4", getUdfParams("udf4"));
        params.put("udf5", getUdfParams("udf5"));
        params.put("pg", getUdfParams("pg"));

        String hashString = "";

        String hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        String[] hashVarSeq = hashSequence.split("\\|");

        for (String part : hashVarSeq) {
            hashString = (empty(params.get(part))) ? hashString.concat("") : hashString.concat(params.get(part));
            hashString = hashString.concat("|");
        }

        String salt = properties.getProperty("MerchantSalt");
        hashString = hashString.concat(salt);

        String hash = hashCal("SHA-512", hashString);
        params.put("hash", hash);
        
        return params;
    }

    private String getUdfParams(String udfParam) {
        return ((null != input.getUserDefinedFields()) && (null != input.getUserDefinedFields().get(udfParam)) && (!input.getUserDefinedFields().get(udfParam).isEmpty())) ? input.getUserDefinedFields().get(udfParam) : "";
    }

    private boolean empty(String s) {
        if (s == null || s.trim().equals("")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param type
     * @param str
     * @return
     */
    private String hashCal(String type, String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();

            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }

        } catch (NoSuchAlgorithmException nsae) {
        }

        return hexString.toString();
    }
}
