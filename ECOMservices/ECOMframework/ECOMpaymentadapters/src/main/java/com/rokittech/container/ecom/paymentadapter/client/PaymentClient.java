/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.paymentadapter.client;

import com.rokittech.container.ecom.commons.models.Payment;
import com.rokittech.container.ecom.commons.models.PaymentResp;
import java.util.Properties;

/**
 *
 * @author sangamesh
 */
public interface PaymentClient {
    public PaymentResp getFormData ();
    public void setInput (Payment payment);
    public Payment getInput ();
    public void setOutput (PaymentResp paymentResp);
    public PaymentResp getOutput ();
    public void setProperties (Properties properties);
}