/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.paymentadapter.client;

/**
 *
 * @author deependr
 */
public enum PaymentVocabulary {
    GETFORM,
    SUCCESS,
    FAILURE,
    PAYUMONEY,
    WEI_ECOMMERCE,
    COMMAND,
    TEST_ECOMMERCE;
}
