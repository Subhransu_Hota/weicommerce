/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.paymentadapter.client.factory;

import com.google.inject.Provider;
import com.rokittech.container.ecom.paymentadapter.client.PaymentClient;
import com.rokittech.container.ecom.paymentadapter.client.PaymentVocabulary;
import com.rokittech.container.ecom.paymentadapter.factory.impl.PayUMoneyPaymentClient_0_0_1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sangamesh
 */
public class PaymentClientFactory implements Provider<PaymentClient> {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(PaymentClientFactory.class);
    
    private PaymentClient paymentClient;

    /**
     * This is the place where you are managing different versions
     *
     * @param version
     */
//    @Inject
    public PaymentClientFactory(int version) {
        this(null, version);
    }

   public PaymentClientFactory(String paymentType, int version) {
        slf4jLogger.info ("Will create the instance of service based on appID: " + paymentType + " Version: " + version);
        if (paymentType != null) {
            PaymentVocabulary app = PaymentVocabulary.valueOf(paymentType.toUpperCase());
            switch (app) {
                case PAYUMONEY:
                    payuMoneyClient(version);
                    break;
            }
        } else {
            payuMoneyClient(version);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public PaymentClient get() {
        return paymentClient;
    }

    private void payuMoneyClient(int version) {

        switch (version) {
            case 1:
                this.paymentClient = new PayUMoneyPaymentClient_0_0_1();
            	
                break;
            case 2:

                break;
            default:

                break;

        }
    }
}
