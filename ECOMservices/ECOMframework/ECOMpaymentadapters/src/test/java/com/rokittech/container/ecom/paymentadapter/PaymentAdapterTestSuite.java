/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.paymentadapter;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author sangamesh
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    
})

public class PaymentAdapterTestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Initiated Mongo database");

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
       
    }

    @After
    public void tearDown() throws Exception {
        
        
    }
}
