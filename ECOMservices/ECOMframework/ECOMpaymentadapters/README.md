# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

####Quick summary####

This is initial commit of the ROKITT e-commerce services.

The objectives of this project:

* To develop e-commerce service (ECS) that will provide secure and reliable payment and e-commerce services for all ROKITT applications;
* Provide simple and unambiguous communication between ROKITT applications and ECS;
* Data exchange protocol has to be very minimal and include: security taken, user ID, application ID and JSON formatted message body;
* APIs have to provide uniform request and response for any third party service providers, they also should isolate ROKITT applications from any special configurations and request/response formatting requirements exposed by different service providers;
* ECS has to be open for new order management and payment scenarios and workflows, and be ready to accepting new third party service providers as well;
* At the same time ECS should seamlessly work with different JSON message’s formats provided by different applications.

 
####Version####

 [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Run git clone command

git clone https://Aaexmy@bitbucket.org/Aaexmy/recommerce.git

Go to the project directory and open project in your IDE


* Configuration

No configuration required

* Dependencies

All dependencies provided by maven

* Database configuration

It is a skeleton project and it doesn't have any database connections

* How to run tests

http://localhost:4567/payment?serviceVersion=1&firstName=FirstName&lastName=LastName

To run version 2 of this service change serviceVersion parameter

http://localhost:4567/payment?serviceVersion=2&firstName=FirstName&lastName=LastName

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact