/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.mail;

import com.rokittech.container.ecom.commons.pdf.PDFGenerator;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author deependr
 */
public class SendEMail {

    public static void sendEmail(String sendToEmailId, String subjectSufix, String messageBody, String attachment) {
        final String username = "deependra.kumar@rokittech.com";
        final String password = "Rokit2015";

        Properties props = new Properties();
        InputStream in = PDFGenerator.class.getClassLoader().getResourceAsStream("env.properties");
        try {
            props.load(in);
        } catch (IOException ex) {
        }

        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", props.getProperty("mail.smtp.host").trim());
        properties.put("mail.smtp.port", props.getProperty("mail.smtp.port").trim());
        Session session = Session.getInstance(properties);
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(props.getProperty("mail.sentfrom").trim()));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(sendToEmailId));
            message.setSubject(props.getProperty("mail.subjectPrefix").trim() + subjectSufix);
            message.setText(messageBody);
            Transport.send(message);
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }

}
