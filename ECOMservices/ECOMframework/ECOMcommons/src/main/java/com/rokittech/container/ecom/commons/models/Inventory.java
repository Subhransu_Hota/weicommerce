/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Entity for inventory
 * 
 * @author asif
 */
public class Inventory implements Json, FromJson, Initializable, Serializable {

    private String _id;
    private String sellerId;
    private String productId;
    private Integer quantity;

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this,Inventory.class);
    }

    @Override
    public Inventory fromJson(String json) {
        return new Gson().fromJson(json,Inventory.class);
    }

    @Override
    public void init() {
        _id = "";
        sellerId = "";
        productId = "";
        quantity = null;
    }
    
}
