package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.container.ecom.commons.core.Money;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Shopping Basket Object
 *
 * @author Deependra Kumar
 */
@Getter @Setter
@SuppressWarnings("serial")
public class Basket implements Json, FromJson, Initializable, Serializable {

    public static final String resource_error = "OrderErrorUiLabels";
    
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private String _id;
    private User user;
    private Address billingAddress;
    private List<LineItem> lineItems;

    public PaymentType paymentType;
    public Payment payment;
    private String status;
    private Money tax;
    private Money subTotal;
    private Money total;
    private Date createdDate;

    /**
     * don't allow empty constructor
     */
    public Basket() {
    }

    public Basket(LineItem item, User user) {

        this.user = user;
        if (this.lineItems == null) {
            this.lineItems = new ArrayList<>();
        }

        this.lineItems.add(item);
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }
    
    @Override
    public Basket fromJson(String json) {
        return new Gson().fromJson(json, Basket.class);
    }

    @Override
    public void init() {
        //
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, Basket.class);
    }
}
