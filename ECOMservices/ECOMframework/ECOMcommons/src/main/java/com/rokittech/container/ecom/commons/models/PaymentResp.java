/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author sangamesh
 */
public class PaymentResp  implements Json, FromJson, Initializable, Serializable {
    private String host;
    
    private Map<String, String> formData;
        
    
    @Override
    public String toJson() {
        return new Gson().toJson(this, PaymentResp.class);
    }

    @Override
    public PaymentResp fromJson(String json) {
        return new Gson().fromJson(json, PaymentResp.class);
    }

    @Override
    public void init() {
        //TODO complete Address init
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Map<String, String> getFormData() {
        return formData;
    }

    public void setFormData(Map<String, String> formData) {
        this.formData = formData;
    }
}
