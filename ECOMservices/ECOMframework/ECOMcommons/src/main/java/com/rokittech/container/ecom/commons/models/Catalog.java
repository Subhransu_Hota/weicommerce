/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 * Entity for catalogs
 * 
 * @author sangamesh
 */
public class Catalog implements Json, FromJson, Initializable, Serializable {

    private String _id;
    private String catalogId;
    private String name;
    private String description;
    private String parentCatalogId;

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParentCatalogId() {
        return parentCatalogId;
    }

    public void setParentCatalogId(String parentCatalogId) {
        this.parentCatalogId = parentCatalogId;
    }
    
    @Override
    public String toJson() {
        return new Gson().toJson(this,Catalog.class);
    }

    @Override
    public Catalog fromJson(String json) {
        return new Gson().fromJson(json,Catalog.class);
    }

    @Override
    public void init() {
        _id = "";
        catalogId = "";
        name = "";
        description = "";
        parentCatalogId = "";
    }
    
}
