/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * Entity for sellers.
 * 
 * @author asifahammed
 */
public class Seller implements Json, FromJson, Initializable, Serializable {

    private String _id;
    private String name;
    private String organizationName;
    private String description;
    private String companyUrl;
    private String panNo;
    private String tinNo;
    private String tanNo;
    private String phoneNo;
    private boolean verified;
    private Date createdDate;
    private AccountStatus status;
    private List<Address> businessAddress;
    private String defaultBusinessAddressId;
    private User user;
    
    public Seller() {
    }

    public Seller(String _id, String name, String organizationName, String description, String companyUrl, String panNo, String tinNo, String tanNo, String phoneNo, boolean verified, Date createdDate, AccountStatus status, List<Address> businessAddress, String defaultBusinessAddressId, User user) {
        this._id = _id;
        this.name = name;
        this.organizationName = organizationName;
        this.description = description;
        this.companyUrl = companyUrl;
        this.panNo = panNo;
        this.tinNo = tinNo;
        this.tanNo = tanNo;
        this.phoneNo = phoneNo;
        this.verified = verified;
        this.createdDate = createdDate;
        this.status = status;
        this.businessAddress = businessAddress;
        this.defaultBusinessAddressId = defaultBusinessAddressId;
        this.user = user;
    }
    
    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompanyUrl() {
        return companyUrl;
    }

    public void setCompanyUrl(String companyUrl) {
        this.companyUrl = companyUrl;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getTinNo() {
        return tinNo;
    }

    public void setTinNo(String tinNo) {
        this.tinNo = tinNo;
    }

    public String getTanNo() {
        return tanNo;
    }

    public void setTanNo(String tanNo) {
        this.tanNo = tanNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public List<Address> getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(List<Address> businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getDefaultBusinessAddressId() {
        return defaultBusinessAddressId;
    }

    public void setDefaultBusinessAddressId(String defaultBusinessAddressId) {
        this.defaultBusinessAddressId = defaultBusinessAddressId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    
    
    @Override
    public String toJson() {
        return new Gson().toJson(this, Seller.class);
    }

    @Override
    public Seller fromJson(String json) {
        return new Gson().fromJson(json, Seller.class);
    }

    @Override
    public void init() {
    }
    
}

