package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 * Shopping Basket Address ref Object
 *
 * @author Deependra Kumar
 */
@SuppressWarnings("serial")
public class BasketAddressRef implements Json, FromJson, Initializable, Serializable {

    private String basketId;
    private Address address;
    private String addressType;

    public String getBasketId() {
        return basketId;
    }

    public void setBasketId(String basketId) {
        this.basketId = basketId;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    
    @Override
    public Object fromJson(String json) {
        return new Gson().fromJson(json, BasketAddressRef.class);
    }

    @Override
    public void init() {
        //
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, BasketAddressRef.class);
    }

}
