/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author asifahammed
 */
@EqualsAndHashCode
@ToString
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class Permission implements Json, FromJson, Initializable, Serializable{
    
    private String name;
    
    private String description;

    @Override
    public String toJson() {
        return new Gson().toJson(this, Permission.class);
    }

    @Override
    public Permission fromJson(String json) {
        return new Gson().fromJson(json, Permission.class);
    }

    @Override
    public void init() {
        
    }
}
