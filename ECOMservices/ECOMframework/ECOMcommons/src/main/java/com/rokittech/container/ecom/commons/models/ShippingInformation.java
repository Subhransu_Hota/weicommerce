/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 */
public class ShippingInformation implements Json, FromJson, Initializable, Serializable {
    Address shippingAddress;

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
    
    @Override
    public String toJson() {
        return new Gson().toJson(this, ShippingInformation.class);
    }

    @Override
    public ShippingInformation fromJson(String json) {
        return new Gson().fromJson(json, ShippingInformation.class);
    }

    @Override
    public void init() {
        //TODO complete Address init
    }
    
}
