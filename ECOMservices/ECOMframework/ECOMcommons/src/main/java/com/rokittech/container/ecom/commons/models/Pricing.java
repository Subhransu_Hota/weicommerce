/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.container.ecom.commons.core.Money;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 * Entity for catalogs
 * 
 * @author sangamesh
 */
public class Pricing implements Json, FromJson, Initializable, Serializable {
    private String _id;
    
    private String pricingId; 
    private PricingPurposeType pricingPurposeType; 
    private PricingType pricingType; 
    private Money money;
    private String pricingFromDate; 
    private String pricingToDate;
        
    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getPricingId() {
        return pricingId;
    }

    public void setPricingId(String pricingId) {
        this.pricingId = pricingId;
    }

    public PricingPurposeType getPricingPurposeType() {
        return pricingPurposeType;
    }

    public void setPricingPurposeType(PricingPurposeType pricingPurposeType) {
        this.pricingPurposeType = pricingPurposeType;
    }

    public PricingType getPricingType() {
        return pricingType;
    }

    public void setPricingType(PricingType pricingType) {
        this.pricingType = pricingType;
    }

    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }

    public String getPricingFromDate() {
        return pricingFromDate;
    }

    public void setPricingFromDate(String pricingFromDate) {
        this.pricingFromDate = pricingFromDate;
    }

    public String getPricingToDate() {
        return pricingToDate;
    }

    public void setPricingToDate(String pricingToDate) {
        this.pricingToDate = pricingToDate;
    }
    
    @Override
    public String toJson() {
        return new Gson().toJson(this,Pricing.class);
    }

    @Override
    public Pricing fromJson(String json) {
        return new Gson().fromJson(json,Pricing.class);
    }

    @Override
    public void init() {
        _id = "";
    }
    
   
public enum PricingPurposeType {
   PURCHASE,
   RECURRING_CHARGE,
   USAGE_CHARGE,
   COMPONENT_PRICE;
}

public enum PricingType {
    LIST_PRICE,
    DEFAULT_PRICE,
    COST_PRICE,
    MINIMUM_PRICE,
    MAXIMUM_PRICE,
    PROMO_PRICE,
    COMPETITIVE_PRICE,
    WHOLESALE_PRICE,
    SPECIAL_PROMO_PRICE,
    BOX_PRICE,
    MINIMUM_ORDER_PRICE;
}

}

