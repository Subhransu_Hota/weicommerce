/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class UserCredentials implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    private String userName;
    private String password;
    private String email;
    private String hintQuestion;
    private String hintAnswer;
    private Date lastLogin;
    private Date createdDate;
    private boolean resetPassword;

    public UserCredentials() {
    }

    public UserCredentials(String userName, String password, String email, String hintQuestion, String hintAnswer, Date lastLogin, boolean resetPassword) {
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.hintQuestion = hintQuestion;
        this.hintAnswer = hintAnswer;
        this.lastLogin = lastLogin;
        this.resetPassword = resetPassword;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); 
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHintQuestion() {
        return hintQuestion;
    }

    public void setHintQuestion(String hintQuestion) {
        this.hintQuestion = hintQuestion;
    }

    public String getHintAnswer() {
        return hintAnswer;
    }

    public void setHintAnswer(String hintAnswer) {
        this.hintAnswer = hintAnswer;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public boolean isResetPassword() {
        return resetPassword;
    }

    public void setResetPassword(boolean resetPassword) {
        this.resetPassword = resetPassword;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, UserCredentials.class);
    }

    @Override
    public UserCredentials fromJson(String json) {
        return new Gson().fromJson(json, UserCredentials.class);
    }

    @Override
    public void init() {
        //TODO complete UserCredentials init
    }

}
