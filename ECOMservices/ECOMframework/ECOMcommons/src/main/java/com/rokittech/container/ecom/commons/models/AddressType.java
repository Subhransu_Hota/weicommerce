/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

/**
 *
 * @author deependr
 */
public enum AddressType {

    Billing,
    Shipping,
    Billing_And_Shipping,
    Business

}
