/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class Phone implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    private String _id;
    private String profileId;
    private String countryCode;
    private String areaCode;
    private String phoneNo;
    private Date createdDate;

    public Phone() {
    }

    public Phone(String _id, String profileId, String countryCode, String areaCode, String phoneNo, Date createdDate) {
        this._id = _id;
        this.profileId = profileId;
        this.countryCode = countryCode;
        this.areaCode = areaCode;
        this.phoneNo = phoneNo;
        this.createdDate = createdDate;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, Phone.class);
    }

    @Override
    public Object fromJson(String json) {
        return new Gson().fromJson(json, Phone.class);
    }

    @Override
    public void init() {
        //TODO complete PhoneInfo init
    }

}
