/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.container.ecom.commons.core.Money;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *{id: "8390483290", orderno:"12345-<date>", customerid:"", billingaddress:"", 
 * orderitems:[{lineitem:"{}", shippingaddress:{}, shippinginfo:{}},
 * {lineitem:"{}", shippingaddress:{}, shippinginfo:{}}], itemtotalprice:"", tax:"", 
 * ordertotalprice:"", status:"", paymentinfo:""}

 */
@Getter @Setter
@NoArgsConstructor
public class Order implements Json, FromJson, Initializable, Serializable {
    
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private String _id;
    private String orderNumber;
    private String customerId;
    private String basketId;
    private Address billingAddress;
    private List<LineItem> linetems;
    private Money netPrice;
    private Money tax;
    private Money grossPrice;
    private OrderStatusType status;
    private PaymentInformation paymentInformation;
    private Date createdDate;

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, Order.class);
    }

    @Override
    public Order fromJson(String json) {
        return new Gson().fromJson(json, Order.class);
    }

    @Override
    public void init() {
        //TODO complete PhoneInfo init
    }
}
