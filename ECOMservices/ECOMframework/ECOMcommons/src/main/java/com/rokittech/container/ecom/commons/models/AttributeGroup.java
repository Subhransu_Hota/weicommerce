/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class AttributeGroup implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    private String _id;
    private String name;
    private List<String> attributes;

    public AttributeGroup() {
    }

    public AttributeGroup(String _id, List<String> attributes) {
        this._id = _id;
        this.attributes = attributes;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, AttributeGroup.class);
    }

    @Override
    public AttributeGroup fromJson(String json) {
        return new Gson().fromJson(json, AttributeGroup.class);
    }

    @Override
    public void init() {
        //TODO complete Address init
    }
}
