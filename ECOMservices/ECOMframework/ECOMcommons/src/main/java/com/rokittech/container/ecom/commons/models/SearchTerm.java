/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class SearchTerm implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    private String keyword;

    public SearchTerm() {
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }


    @Override
    public String toJson() {
        return new Gson().toJson(this, SearchTerm.class);
    }

    @Override
    public SearchTerm fromJson(String json) {
        return new Gson().fromJson(json, SearchTerm.class);
    }

    @Override
    public void init() {
        //TODO complete Address init
    }


}
