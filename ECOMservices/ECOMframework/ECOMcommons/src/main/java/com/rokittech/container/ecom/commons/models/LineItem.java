/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.container.ecom.commons.core.Money;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *{id: "8390483290", orderno:"12345-<date>", customerid:"", billingaddress:"", 
 * orderitems:[{lineitem:"{}", shippingaddress:{}, shippinginfo:{}},
 * {lineitem:"{}", shippingaddress:{}, shippinginfo:{}}], itemtotalprice:"", tax:"", 
 * ordertotalprice:"", status:"", paymentinfo:""}

 */
public class LineItem implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    private String _id;
    private LineItemType type = LineItemType.Order;
    private String productSKU;
    private String productId;
    private String productName;
    private double quantity;
    private Money itemNetPrice;
    private Money itemTax;
    private Money itemGrossPrice; 
    private Money netPrice;
    private Money tax;
    private Money grossPrice; 
    private Address shippingAddress;
    private ShippingMethod shippingMethod;    

    public LineItem() {
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public ShippingMethod getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public String getProductSKU() {
        return productSKU;
    }

    public void setProductSKU(String productSKU) {
        this.productSKU = productSKU;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Money getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(Money netPrice) {
        this.netPrice = netPrice;
    }

    public Money getTax() {
        return tax;
    }

    public void setTax(Money tax) {
        this.tax = tax;
    }

    public Money getGrossPrice() {
        return grossPrice;
    }

    public void setGrossPrice(Money grossPrice) {
        this.grossPrice = grossPrice;
    }

    public LineItemType getType() {
        return type;
    }

    public void setType(LineItemType type) {
        this.type = type;
    }

    public Money getItemNetPrice() {
        return itemNetPrice;
    }

    public void setItemNetPrice(Money itemNetPrice) {
        this.itemNetPrice = itemNetPrice;
    }

    public Money getItemTax() {
        return itemTax;
    }

    public void setItemTax(Money itemTax) {
        this.itemTax = itemTax;
    }

    public Money getItemGrossPrice() {
        return itemGrossPrice;
    }

    public void setItemGrossPrice(Money itemGrossPrice) {
        this.itemGrossPrice = itemGrossPrice;
    }
    
    @Override
    public String toJson() {
        return new Gson().toJson(this, LineItem.class);
    }

    @Override
    public Object fromJson(String json) {
        return new Gson().fromJson(json, LineItem.class);
    }

    @Override
    public void init() {
        //TODO complete PhoneInfo init
    }

}
