/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author deependr
 */
public class PDFGenerator {

    public static void generatePDF(String orderNo) throws DocumentException, FileNotFoundException, IOException {

        Properties props = new Properties();
        InputStream in = PDFGenerator.class.getClassLoader().getResourceAsStream("env.properties");
        try {
            props.load(in);
        } catch (IOException ex) {
        }
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(props.getProperty("pdfinvoicelocation") + orderNo + "Invoice.pdf"));
        document.open();
        XMLWorkerHelper.getInstance().parseXHtml(writer, document,
                new FileInputStream(props.getProperty("pdfinvoicelocation") + orderNo + "Invoice.html"));
        document.close();

    }

}
