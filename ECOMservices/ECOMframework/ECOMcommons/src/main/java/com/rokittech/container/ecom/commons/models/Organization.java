/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class Organization implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    private String _id;
    private String organizationId;
    private String organizationName;
    private String description;
    private Date createdDate;

    public Organization() {
    }

    public Organization(String _id, String organizationId, String organizationName, String description) {
        this._id = _id;
        this.organizationId = organizationId;
        this.organizationName = organizationName;
        this.description = description;

    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, Organization.class);
    }

    @Override
    public Organization fromJson(String json) {
        return new Gson().fromJson(json, Organization.class);
    }

    @Override
    public void init() {
        setCreatedDate(new Date());

    }

}
