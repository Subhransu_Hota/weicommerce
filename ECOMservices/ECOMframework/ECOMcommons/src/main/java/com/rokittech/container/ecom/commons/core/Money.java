/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.core;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.Currency;
import java.util.Locale;

/**
 *
 * @author sangamesh
 */
public class Money implements Json, FromJson, Initializable, Serializable {

    private Locale locale;
    private double amount;
    private Currency currency;

    public Money(Locale locale, double amount) {
        this.locale = locale;
        this.amount = amount;
        this.currency = Currency.getInstance(locale);
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
 
    
    @Override
    public String toJson() {
        return new Gson().toJson(this,Money.class);
    }

    @Override
    public Object fromJson(String json) {
        return new Gson().fromJson(json,Money.class);
    }

    @Override
    public void init() {
        
    }
}
