/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class ProductAttribute implements Json, FromJson, Initializable, Serializable {
    private String name;
    private String value;
    private AttributeType attributeType;

    public ProductAttribute() {
    }

    public ProductAttribute(String id, String name, String value, AttributeType attributeType) {
        this.name = name;
        this.value = value;
        this.attributeType = attributeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public AttributeType getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(AttributeType attributeType) {
        this.attributeType = attributeType;
    }

    
    @Override
    public String toJson() {
        return new Gson().toJson(this, ProductAttribute.class);
    }

    @Override
    public ProductAttribute fromJson(String json) {
        return new Gson().fromJson(json, ProductAttribute.class);
    }

    @Override
    public void init() {
        //TODO complete Address init
    }

}
