/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.List;

/**
 */
public class ShippingMethod implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    private String _id;
    private String name;
    private String description;
    private String tracking_url;
    private List<ShippingMethodItemType> itemTypes;
    private List<ShippingMethodZip> zones;
    
    private ShippingMethodCalcType shippingCalcType;
    private double shippingCalcValue; 
    
    public ShippingMethod() {
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTracking_url() {
        return tracking_url;
    }

    public void setTracking_url(String tracking_url) {
        this.tracking_url = tracking_url;
    }

    public List<ShippingMethodItemType> getItemTypes() {
        return itemTypes;
    }

    public void setItemTypes(List<ShippingMethodItemType> itemTypes) {
        this.itemTypes = itemTypes;
    }

    public List<ShippingMethodZip> getZones() {
        return zones;
    }

    public void setZones(List<ShippingMethodZip> zones) {
        this.zones = zones;
    }

    public ShippingMethodCalcType getShippingCalcType() {
        return shippingCalcType;
    }

    public void setShippingCalcType(ShippingMethodCalcType shippingCalcType) {
        this.shippingCalcType = shippingCalcType;
    }

    public double getShippingCalcValue() {
        return shippingCalcValue;
    }

    public void setShippingCalcValue(double shippingCalcValue) {
        this.shippingCalcValue = shippingCalcValue;
    }

    
    @Override
    public String toJson() {
        return new Gson().toJson(this, ShippingMethod.class);
    }

    @Override
    public ShippingMethod fromJson(String json) {
        return new Gson().fromJson(json, ShippingMethod.class);
    }

    @Override
    public void init() {
        //TODO complete Address init
    }
}
