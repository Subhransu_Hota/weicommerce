/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sangamesh
 */
public class ShippingMethodZip implements Json, FromJson, Initializable, Serializable {
    private String _id;
    private String name;
    private String zipCode;
    private List<String> areas;
    
    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public List<String> getAreas() {
        return areas;
    }

    public void setAreas(List<String> areas) {
        this.areas = areas;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this,ShippingMethodZip.class);
    }

    @Override
    public ShippingMethodZip fromJson(String json) {
        return new Gson().fromJson(json,ShippingMethodZip.class);
    }
    
    @Override
    public void init() {
        
    }
}
