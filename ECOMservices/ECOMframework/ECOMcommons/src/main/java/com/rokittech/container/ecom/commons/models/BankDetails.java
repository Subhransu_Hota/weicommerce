/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 * Bank details of seller.
 * 
 * @author asifahammed
 */
public class BankDetails implements Json, FromJson, Initializable, Serializable {
    
    private String _id;
    private String userId;
    private String accountHolderName;
    private String accountNumber;
    private String ifscCode;
    private String bankName;
    private String city;
    private String branch;
    private String state;

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    public BankDetails(){
        
    }
    
    public BankDetails(String _id, String userId, String accountHolderName, String accountNumber, String ifscCode, String bankName, String city, String branch, String state) {
        this._id = _id;
        this.userId = userId;
        this.accountHolderName = accountHolderName;
        this.accountNumber = accountNumber;
        this.ifscCode = ifscCode;
        this.bankName = bankName;
        this.city = city;
        this.branch = branch;
        this.state = state;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, BankDetails.class);
    }

    @Override
    public Object fromJson(String json) {
        return new Gson().fromJson(json, BankDetails.class);
    }

    @Override
    public void init() {
        this._id = "";
        this.userId = "";
        this.accountHolderName = "";
        this.accountNumber = "";
        this.ifscCode = "";
        this.bankName = "";
        this.city = "";
        this.branch = "";
        this.state = "";
    }

    
    
    
}
