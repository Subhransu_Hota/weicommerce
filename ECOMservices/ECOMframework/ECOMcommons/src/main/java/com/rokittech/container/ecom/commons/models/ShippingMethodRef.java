package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 * Shopping Method Ref
 */
@SuppressWarnings("serial")
public class ShippingMethodRef implements Json, FromJson, Initializable, Serializable {

    private String basketId;
    ShippingMethod shippingMethod;

    public String getBasketId() {
        return basketId;
    }

    public void setBasketId(String basketId) {
        this.basketId = basketId;
    }

    public ShippingMethod getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    @Override
    public Object fromJson(String json) {
        return new Gson().fromJson(json, ShippingMethodRef.class);
    }

    @Override
    public void init() {
        //
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, ShippingMethodRef.class);
    }

}
