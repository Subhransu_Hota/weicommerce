/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

/**
 *
 * @author asifahammed
 */
public enum AccountStatus {
    
    Pending,
    Active,
    Blocked
    
}
