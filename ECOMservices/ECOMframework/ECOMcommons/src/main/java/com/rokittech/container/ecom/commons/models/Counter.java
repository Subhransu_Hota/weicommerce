/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 * Entity for catalogs
 * 
 * @author sangamesh
 */
public class Counter implements Json, FromJson, Initializable, Serializable {
    private String _id;
    private long counter;
        
    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public long getCounter() {
        return counter;
    }

    public void setCounter(long counter) {
        this.counter = counter;
    }
    
    @Override
    public String toJson() {
        return new Gson().toJson(this,Counter.class);
    }

    @Override
    public Counter fromJson(String json) {
        return new Gson().fromJson(json,Counter.class);
    }

    @Override
    public void init() {
        _id = "";
    }
    
}

