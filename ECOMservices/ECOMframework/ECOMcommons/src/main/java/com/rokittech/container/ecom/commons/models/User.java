/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.container.ecom.commons.core.AbstractUser;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

/**
 * DB User Entity class.
 * 
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
@Getter @Setter
public class User extends AbstractUser implements Json, FromJson, Initializable, Serializable {

    private UserCredentials userCredentials;

    public User() {
    }

    public User(String _id, String firstName, String middleName, String lastName,
            String company, UserType userType, String phoneNo, String url, List<Address> billingAddress,
            List<Address> shippingAddress, String defaultBillingAddressId, String defaultShippingAddressId,
            UserCredentials userCredentials, Set<String> roles, Date createdDate) {
        this._id = _id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.company = company;
        this.userType = userType;
        this.phoneNo = phoneNo;
        this.url = url;
        this.billingAddress = billingAddress;
        this.shippingAddress = shippingAddress;
        this.defaultBillingAddressId = defaultBillingAddressId;
        this.defaultShippingAddressId = defaultShippingAddressId;
        this.userCredentials = userCredentials;
        this.roles = roles;
        this.createdDate = createdDate;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, User.class);
    }

    @Override
    public User fromJson(String json) {
        return new Gson().fromJson(json, User.class);
    }

    @Override
    public void init() {
        //Todo init
    }

}
