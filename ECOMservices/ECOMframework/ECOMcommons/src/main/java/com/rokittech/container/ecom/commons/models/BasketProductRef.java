package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 * Shopping Basket Object
 *
 * @author Deependra Kumar
 */
@SuppressWarnings("serial")
public class BasketProductRef implements Json, FromJson, Initializable, Serializable {

    private String basketId;
    private String productId;
    private String userId;
    private double qty;

    public String getProductId() {
        return productId;
    }

    public String getBasketId() {
        return basketId;
    }

    public void setBasketId(String basketId) {
        this.basketId = basketId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    @Override
    public Object fromJson(String json) {
        return new Gson().fromJson(json, BasketProductRef.class);
    }

    @Override
    public void init() {
        //
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, BasketProductRef.class);
    }

}
