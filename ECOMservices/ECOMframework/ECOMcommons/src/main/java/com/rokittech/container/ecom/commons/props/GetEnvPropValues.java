package com.rokittech.container.ecom.commons.props;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GetEnvPropValues {

	InputStream inputStream;
	
	public String getPropValues(String property) throws IOException {
          String value = "";
		try {
			Properties prop = new Properties();
			String propFileName = "config.properties";
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
			value = prop.getProperty(property);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			inputStream.close();
		}
		return value;
	}
	
}
