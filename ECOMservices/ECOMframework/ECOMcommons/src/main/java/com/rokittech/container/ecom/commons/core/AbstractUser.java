/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.core;

import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.container.ecom.commons.models.UserType;
import java.util.Date;
import java.util.List;
import java.util.Set;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author asifaham
 */
@Getter @Setter
public abstract class AbstractUser {
    
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    protected String _id;
    
    protected String firstName;
    
    protected String middleName;
    
    protected String lastName;
    
    protected String company;
    
    protected UserType userType;
    
    protected String phoneNo;
    
    protected String url;
    
    protected List<Address> billingAddress;
    
    protected List<Address> shippingAddress;
    
    protected String defaultBillingAddressId;
    
    protected String defaultShippingAddressId;
    
    protected Set<String> roles;
    
    protected Date createdDate;
}
