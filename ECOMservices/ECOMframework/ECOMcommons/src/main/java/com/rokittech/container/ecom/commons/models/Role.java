/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author asifahammed
 */
@EqualsAndHashCode(exclude="_id")
@ToString(exclude="_id")
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class Role implements Json, FromJson, Initializable, Serializable{
    
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private String _id;
    
    private String name;
    
    private String description;
    
    private Set<Permission> permissions;

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, Role.class);
    }

    @Override
    public Role fromJson(String json) {
        return new Gson().fromJson(json, Role.class);
    }

    @Override
    public void init() {
        this.permissions = new HashSet<Permission>();
    }
    
}
