/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 * @author asifaham
 */
public class Preferences implements Json, FromJson, Initializable, Serializable {
    
    private String _id;
    private String name;
    private String value;
    private String description; 
    
    public Preferences() {
    }

    public Preferences(String _id, String name, String value, String description) {
        this._id = _id;
        this.name = name;
        this.value = value;
        this.description = description;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
    
    
    @Override
    public String toJson() {
        return new Gson().toJson(this, Preferences.class);
    }

    @Override
    public Preferences fromJson(String json) {
        return new Gson().fromJson(json, Preferences.class);
    }

    @Override
    public void init() {
         
    }
    
}
