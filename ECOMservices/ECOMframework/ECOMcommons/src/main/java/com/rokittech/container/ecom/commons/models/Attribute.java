/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class Attribute implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    private String _id;
    private String name;
    private AttributeType attributeType;
    private String attributeGroupId;

    public Attribute() {
    }

    public Attribute(String _id, String name, String attributeGroupId) {
        this._id = _id;
        this.name = name;
        this.attributeGroupId = attributeGroupId;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AttributeType getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(AttributeType attributeType) {
        this.attributeType = attributeType;
    }

    public String getAttributeGroupId() {
        return attributeGroupId;
    }

    public void setAttributeGroupId(String attributeGroupId) {
        this.attributeGroupId = attributeGroupId;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, Attribute.class);
    }

    @Override
    public Attribute fromJson(String json) {
        return new Gson().fromJson(json, Attribute.class);
    }

    @Override
    public void init() {
        //TODO complete Address init
    }

}
