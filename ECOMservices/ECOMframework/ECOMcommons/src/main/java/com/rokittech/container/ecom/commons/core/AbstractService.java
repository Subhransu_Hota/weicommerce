/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.core;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.Service;

/**
 *
 * @author deependr
 * @param <I>
 * @param <O>
 * @param <P>
 * @param <S>
 */
public abstract class AbstractService<I, O, P, S> implements Service<I, O, P, S> {

    private I input;
    private O output;
    private P params;
    private S persist;

    private String inputAsJson;
    private String outputAsJson;

    @Override
    public String serviceId() {
        return this.getClass().getName();
    }

    @Override
    public I getInput() {
        return input;
    }

    @Override
    public O getOutput() {
        return output;
    }

    @Override
    public P getParams() {
        return params;
    }

    @Override
    public S getPersist() {
        return persist;
    }

    @Override
    public void setInput(I input) {
        this.input = input;
    }

    @Override
    public void setOutput(O output) {
        this.output = output;
    }

    @Override
    public void setParams(P params) {
        this.params = params;
    }

    @Override
    public void setPersist(S persist) {
        this.persist = persist;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, AbstractService.class);
    }

}
