/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.Date;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
public class Customer implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private String _id;
    private String ownerId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String company;
    private UserType userType;
    private String phoneNo;
    private String url;
    private Date createdDate;

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, Customer.class);
    }

    @Override
    public Customer fromJson(String json) {
        return new Gson().fromJson(json, Customer.class);
    }

    @Override
    public void init() {
        //TODO complete User init
    }

}
