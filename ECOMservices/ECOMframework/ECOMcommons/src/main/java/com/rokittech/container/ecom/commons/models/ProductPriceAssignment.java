/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class ProductPriceAssignment implements Json, FromJson, Initializable, Serializable {

    // User credential attributes
    private String _id;
    private String productId;
    private String priceId;
    
    public ProductPriceAssignment() {
    }

    public ProductPriceAssignment(String _id, String productId, String priceId) {
        this._id = _id;
        this.productId = productId;
        this.priceId = priceId;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, ProductPriceAssignment.class);
    }

    @Override
    public ProductPriceAssignment fromJson(String json) {
        return new Gson().fromJson(json, ProductPriceAssignment.class);
    }

    @Override
    public void init() {
        //TODO complete Address init
    }
}
