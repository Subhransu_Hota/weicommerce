/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author deependr
 */
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentInformation implements Json, FromJson, Initializable, Serializable {
    
    private PaymentType paymentType;
    
    private Payment payment;

    @Override
    public String toJson() {
        return new Gson().toJson(this, PaymentInformation.class);
    }

    @Override
    public PaymentInformation fromJson(String json) {
        return new Gson().fromJson(json, PaymentInformation.class);
    }

    @Override
    public void init() {
    
    }
}
