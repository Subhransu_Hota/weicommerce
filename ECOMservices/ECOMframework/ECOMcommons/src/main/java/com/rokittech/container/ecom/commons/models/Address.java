/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.commons.models;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author asifaham
 */
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class Address implements Json, FromJson, Initializable, Serializable {

    private String addressId;
    private String addressName;
    protected String firstName;
    protected String middleName;
    protected String lastName;
    private String street1;
    private String street2;
    private String city;
    private String state;
    private String country;
    private String zipCode;
    private Date createdDate;
    private String addressType;

    @Override
    public String toJson() {
        return new Gson().toJson(this, Address.class);
    }

    @Override
    public Address fromJson(String json) {
        return new Gson().fromJson(json, Address.class);
    }

    @Override
    public void init() {
        //TODO complete Address init
    }

    public void setAddressType(AddressType addressType) {
        this.addressType=addressType.name();
    }

}
