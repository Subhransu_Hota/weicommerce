/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core.json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.rokittech.containers.api.core.TransitTriple;
import java.lang.reflect.Type;

/**
 *
 * @author alexmy
 */
public class TransitTripleSerializer implements JsonSerializer<TransitTriple> {

    @Override
    public JsonElement serialize(TransitTriple src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject jsonObject = new JsonObject();

        jsonObject = assign(jsonObject, "type", src.getTypeName());
        jsonObject = assign(jsonObject, "key", src.getKey());
        jsonObject = assign(jsonObject, "typeJson", src.getTypeJson());
        
        return jsonObject;
    }

    private JsonObject assign(JsonObject jsonObject, String name, Object value) {

        if (value != null) {
            if (value instanceof Boolean) {
                jsonObject.addProperty(name, (Boolean) value);
            } else if (value instanceof Number) {
                jsonObject.addProperty(name, (Number) value);
            } else if (value instanceof String) {
                jsonObject.addProperty(name, (String) value);
            } else if (value instanceof Character) {
                jsonObject.addProperty(name, (Character) value);
            }
        }

        return jsonObject;
    }
}
