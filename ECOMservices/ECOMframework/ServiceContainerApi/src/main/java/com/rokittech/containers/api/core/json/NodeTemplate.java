/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core.json;

/**
 * The structure of NodeTemplate needs some additional work,
 * it is very preliminary. 
 * I created it to provide support for CreateIndexTool.
 * 
 * One of the way of implementing settings for this tool is:
 * 
 *      NodeTemplate template = new NodeTemplate();
 *      JestClient client = factory.getObject();
 * 
 *      String settings = new Gson().toJson(template.getIndex(), template.getIndex().class);
 *      
 *      client.execute(new CreateIndex.Builder("articles")
 *          .settings(ImmutableSettings.builder()
 *          .loadFromSource(settings).build().getAsMap()).build());
 *      
 * 
 * @author alexmy
 */

public class NodeTemplate {
    
    private Network network;
    private Settings settings;
    private Gateway gateway;

    /**
     * @return the network
     */
    public Network getNetwork() {
        return this.network = network == null ? new Network() : network;
    }

    /**
     * @param network the network to set
     */
    public void setNetwork(Network network) {
        this.network = network;
    }

    /**
     * @return the index
     */
    public Settings getSettings() {
        return this.settings = settings == null ? new Settings() : settings;
    }

    /**
     * @param settings
     */
    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * @return the gateway
     */
    public Gateway getGateway() {
        return this.gateway = gateway == null ? new Gateway() : gateway;
    }

    /**
     * @param gateway the gateway to set
     */
    public void setGateway(Gateway gateway) {
        this.gateway = gateway;
    }

    public static class Network {

        private String host;
        
        /**
         * @return the host
         */
        public String getHost() {
            return host;
        }

        /**
         * @param host the host to set
         */
        public void setHost(String host) {
            this.host = host;
        }
    }

    public static class Settings {

        private Store store;
        private int number_of_shards;
        private int number_of_replicas;
       
        /**
         * @return the store
         */
        public Store getStore() {
            return this.store = store == null ? new Store() : store;
        }

        /**
         * @param store the store to set
         */
        public void setStore(Store store) {
            this.store = store;
        }

        /**
         * @return the number_of_shards
         */
        public int getNumber_of_shards() {
            return number_of_shards;
        }

        /**
         * @param number_of_shards the number_of_shards to set
         */
        public void setNumber_of_shards(int number_of_shards) {
            this.number_of_shards = number_of_shards;
        }

        /**
         * @return the number_of_replicas
         */
        public int getNumber_of_replicas() {
            return number_of_replicas;
        }
 
        /**
         * 
         * @param number_of_replicas 
         */
        public void setNumber_of_replicas(int number_of_replicas) {
            this.number_of_replicas = number_of_replicas;
        }
    }

    public static class Store {

        private String type;
     
        /**
         * @return the type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type the type to set
         */
        public void setType(String type) {
            this.type = type;
        }
    }

    public static class Gateway {

        private String type;
       
        /**
         * @return the type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type the type to set
         */
        public void setType(String type) {
            this.type = type;
        }
    }
    
}
