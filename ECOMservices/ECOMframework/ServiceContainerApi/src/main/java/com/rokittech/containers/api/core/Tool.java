/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core;

/**
 *
 * @author alexmy
 * @param <I>
 * @param <O>
 * @param <P>
 */
public interface Tool<I, O, P> {
          
    O apply();
    
    String toolId();

    /**
     * @return the input
     */
    public I getInput();

    /**
     * @param input the input to set
     */
    public void setInput(I input);

    /**
     * @return the params
     */
    public P getParams();

    /**
     * @param params the params to set
     */
    public void setParams(P params);
    
}
