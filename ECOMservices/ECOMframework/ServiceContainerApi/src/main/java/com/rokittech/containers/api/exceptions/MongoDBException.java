/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.exceptions;

/**
 *
 * @author deependr
 */
public class MongoDBException extends ECommerceException{

    public MongoDBException() {
    }

    public MongoDBException(String message) {
        super(message);
    }

    public MongoDBException(Throwable cause) {
        super(cause);
    }

    public MongoDBException(String errorCode, String message) {
        super(errorCode, message);
    }

    public MongoDBException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
    
}
