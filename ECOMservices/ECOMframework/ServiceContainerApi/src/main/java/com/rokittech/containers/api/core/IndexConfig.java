/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core;

import com.google.gson.Gson;

/**
 *
 * @author alexmy
 */
public class IndexConfig implements Json{
    private String indexName;
    private String indexAlias;
    private IndexSettings indexSettings;

    /**
     * @return the indexName
     */
    public String getIndexName() {
        return indexName;
    }

    /**
     * @param indexName the indexName to set
     */
    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    /**
     * @return the indexAlias
     */
    public String getIndexAlias() {
        return indexAlias;
    }

    /**
     * @param indexAlias the indexAlias to set
     */
    public void setIndexAlias(String indexAlias) {
        this.indexAlias = indexAlias;
    }

    /**
     * @return the indexSettings
     */
    public IndexSettings getIndexSettings() {
        return indexSettings;
    }

    /**
     * @param indexSettings the indexSettings to set
     */
    public void setIndexSettings(IndexSettings indexSettings) {
        this.indexSettings = indexSettings;
    }

    public String toJson() {
        return new Gson().toJson(this, IndexConfig.class);
    }
}
