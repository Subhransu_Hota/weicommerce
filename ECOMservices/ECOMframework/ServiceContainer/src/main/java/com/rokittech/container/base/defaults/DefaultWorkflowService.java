package com.rokittech.container.base.defaults;


import com.rokittech.containers.api.core.Service;
import com.rokittech.containers.api.core.Transit;

//import com.google.inject.Singleton;
//@Singleton
public abstract class DefaultWorkflowService<I,O,P,S,W> implements Service<I,O,P,S> {

    private I input;
    private O output;
    private P params;
    private S persist;

//    private Workflow_shoppingcart workflow;
    public String inputAsJson;
    public String outputAsJson;

    public Transit transit;


    public W workflow;

    // This is the place where you have to put your code
    //==========================================================================
    abstract public Output serve(W workflow);

    /**
     * @return the workflow
     */
    public W getWorkflow() {
        return workflow;
    }

    /**
     * @param workflow the workflow to set
     */
    public void setWorkflow(W workflow) {
        this.workflow = workflow;
    }

    /**
     * @return the inputAsJson
     */
    @Override
    public String getInputAsJson() {
        return inputAsJson;
    }

    /**
     * @param inputAsJson the inputAsJson to set
     */
    @Override
    public void setInputAsJson(String inputAsJson) {
        this.inputAsJson = inputAsJson;
    }

    /**
     * @return the outputAsJson
     */
    @Override
    public String getOutputAsJson() {
        return outputAsJson;
    }

    /**
     * @param outputAsJson the outputAsJson to set
     */
    @Override
    public void setOutputAsJson(String outputAsJson) {
        this.outputAsJson = outputAsJson;
    }

    /**
     * @return the persist
     */
    @Override
    public S getPersist() {
        return persist;
    }

    /**
     * @param persist the persist to set
     */
    @Override
    public void setPersist(S persist) {
        this.persist = persist;
    }

    /**
     * @return the input
     */
    @Override
    public I getInput() {
        return input;
    }

    /**
     * @param input the input to set
     */
    @Override
    public void setInput(I input) {
        this.input = input;
    }

    /**
     * @return the output
     */
    @Override
    public O getOutput() {
        return output;
    }

    /**
     * @param output the output to set
     */
    @Override
    public void setOutput(O output) {
        this.output = output;
    }

    /**
     * @return the params
     */
    @Override
    public P getParams() {
        return params;
    }

    /**
     * @param params the params to set
     */
    @Override
    public void setParams(P params) {
        this.params = params;
    }
}
