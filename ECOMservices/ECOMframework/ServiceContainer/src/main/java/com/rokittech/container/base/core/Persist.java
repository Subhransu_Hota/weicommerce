/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base.core;

import com.rokittech.containers.api.exceptions.HazelcastException;
import java.util.List;
import java.util.Properties;
import java.util.TreeMap;

/**
 *
 * @author alexmy
 * @param <T> - Persistence storage;
 * @param <I> - Persistence Entry Id; 
 * @param <D> - Data to be persisted;
 */
public interface Persist<T, I, D> {
   
    // List support
    //
    public Boolean push2List(I id, D data);
    
    public D pullFromListHead(I id, int index);
    
    public D pullFromListTail(I id, int index);
    
    public D pullLastFromList(I id);
    
    public List<D> pullAllFromList(I id);
    
    public Boolean deleteItemFromList(I id, int index);
    
    public Boolean deleteAllFromList(I id);
    
    
    /**
     * 
     * @return 
     */  
    public String getEnvId();
    
    /**
     * 
     * @param envId 
     */
    public void setEnvId(String envId);
    
    /**
     * 
     * @return 
     */
    public T getEnv();
    
    /**
     * 
     * @param env 
     */
    public void setEnv(T env);
    
    /**
     * Initialize environment using provided properties
     * 
     * @param props 
     * @return  
     * @throws com.rokittech.containers.api.exceptions.HazelcastException  
     */
    public T initEnv(Properties props) throws HazelcastException;
    
    /**
     * Initializing environment using properties file
     * 
     * @param fileName 
     * @return  
     * @throws com.rokittech.containers.api.exceptions.HazelcastException  
     */
    public T initEnv(String fileName) throws HazelcastException;
    
}
