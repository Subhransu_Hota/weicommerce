/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base.defaults;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.Json;

/**
 *
 * @author alexmy
 */
public class Info implements Json {

        private Integer version;
        private String userId;
        private String appId;
        private String serviceId;

        @Override
        public String toJson() {
            return new Gson().toJson(this, Info.class);
        }

        /**
         * @return the version
         */
        public Integer getVersion() {
            return version;
        }

        /**
         * @param version the version to set
         */
        public void setVersion(Integer version) {
            this.version = version;
        }

        /**
         * @return the userId
         */
        public String getUserId() {
            return userId;
        }

        /**
         * @param userId the userId to set
         */
        public void setUserId(String userId) {
            this.userId = userId;
        }

        /**
         * @return the appId
         */
        public String getAppId() {
            return appId;
        }

        /**
         * @param appId the appId to set
         */
        public void setAppId(String appId) {
            this.appId = appId;
        }

    /**
     * @return the serviceId
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * @param serviceId the serviceId to set
     */
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
    }
