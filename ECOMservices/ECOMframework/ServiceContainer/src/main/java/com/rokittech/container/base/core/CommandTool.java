/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base.core;

import com.rokittech.containers.api.core.Tool;

/**
 *
 * @author alexmy
 * @param <I> - WorkflowTool Input
 * @param <O> - WorkflowTool Output
 * @param <P> - WorkflowTool Params
 */
public abstract class CommandTool<I, O, P> implements Tool<I, O, P> {
    
    public I input;
    public O output;
    public P params;
   
    @Override
    public I getInput(){
        return this.input;
    }

    /**
     */
    @Override
    public void setInput(I input){
        this.input = input;
    }

    /**
     * @return the workflowOutput
     */
    public O getOutput(){
        return this.output;
    }

    /**
     * @param output the output to set
     */
    public void setOutput(O output){
        this.output = output;
    }

    /**
     * @return the workflowParams
     */
    @Override
    abstract public P getParams();

    /**
     */
    @Override
    abstract public void setParams(P params);    
    
}
