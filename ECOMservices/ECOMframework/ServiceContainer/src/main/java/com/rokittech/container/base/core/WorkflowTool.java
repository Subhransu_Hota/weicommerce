/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base.core;

import com.rokittech.containers.api.core.Tool;
import com.rokittech.containers.api.core.Transit;

/**
 *
 * @author alexmy
 * @param <I> - WorkflowTool Input
 * @param <O> - WorkflowTool Output
 * @param <P> - WorkflowTool Params
 */
public abstract class WorkflowTool<I, O, P> implements Tool<I, O, P> {
    
    public I input;
    public O output;
    public P params;

    public String inputAsJson;
    public String outputAsJson;
    public String paramsAsJson;
    public String persistAsJson;

    private Transit transit;
    
    @Override
    public I getInput(){
        return this.input;
    }

    /**
     */
    @Override
    public void setInput(I input){
        this.input = input;
    }

    /**
     * @return the workflowOutput
     */
    public O getOutput(){
        return this.output;
    }

    /**
     * @param output the output to set
     */
    public void setOutput(O output){
        this.output = output;
    }

    /**
     * @return the workflowParams
     */
    @Override
    abstract public P getParams();

    /**
     */
    @Override
    abstract public void setParams(P params);

    /**
     * @return the inputAsJson
     */
    public String getInputAsJson() {
        return inputAsJson;
    }

    /**
     * @param inputAsJson the inputAsJson to set
     */
    public void setInputAsJson(String inputAsJson) {
        this.inputAsJson = inputAsJson;
    }

    /**
     * @return the outputAsJson
     */
    public String getOutputAsJson() {
        return outputAsJson;
    }

    /**
     * @param outputAsJson the outputAsJson to set
     */
    public void setOutputAsJson(String outputAsJson) {
        this.outputAsJson = outputAsJson;
    }

    /**
     * @return the paramsAsJson
     */
    public String getParamsAsJson() {
        return paramsAsJson;
    }

    /**
     * @param paramsAsJson the paramsAsJson to set
     */
    public void setParamsAsJson(String paramsAsJson) {
        this.paramsAsJson = paramsAsJson;
    }

    /**
     * @return the persistAsJson
     */
    public String getPersistAsJson() {
        return persistAsJson;
    }

    /**
     * @param persistAsJson the persistAsJson to set
     */
    public void setPersistAsJson(String persistAsJson) {
        this.persistAsJson = persistAsJson;
    }

    /**
     * @return the transit
     */
    public Transit getTransit() {
        return transit;
    }

    /**
     * @param transit the transit to set
     */
    public void setTransit(Transit transit) {
        this.transit = transit;
    }    
}
