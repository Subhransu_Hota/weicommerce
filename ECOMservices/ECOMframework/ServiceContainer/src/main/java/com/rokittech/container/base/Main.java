/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.rokittech.container.base.core.WorkflowUtils;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.IndexApi;
import com.rokittech.containers.api.core.IndexApiImpl;
import com.rokittech.containers.api.core.IndexConfig;
import com.rokittech.containers.api.core.IndexSettings;
import com.rokittech.containers.api.core.Initializable;
import com.rokittech.containers.api.core.Json;
import com.rokittech.containers.api.core.Transit;
import com.rokittech.containers.api.core.TransitTriple;
import java.util.Map;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

/**
 *
 * @author alexmylnikov
 */
public class Main {

    public static void main(String[] args) {

        Person person = new Person();
        person.setAge(68);
        person.setFirstName("Alex");
        person.setLastName("Mylnikov");

        TransitTriple persontriple = new TransitTriple();
        persontriple.setKey("key");
        persontriple.setType(Person.class.getName());
        persontriple.setTypeJson(person.toJson());

        String json = persontriple.toJson();

        System.out.println("toJson: " + json);

        TransitTriple outtriple = persontriple.fromJson(json);

        System.out.println("fromJson: " + outtriple.toJson());

        Transit transit = new Transit();

        transit.add(persontriple);
     
//        Person persona = new Person();
//        persona.setAge(23);
//        persona.setFirstName("Masha");
//        persona.setLastName("Mylnikov");
//
//        transit.add(new TransitTriple("key1", Person.class.getName(), persona.toJson()));
//
//        String json1 = transit.toJson();
//
//        System.out.println(json1);

        Office office = new Office();
        office.setOfficeName("ROKITT");
        office.setOfficeAddress(new Address());
        office.getOfficeAddress().setAddress("158 Hudson Street");
        office.getOfficeAddress().setCity("Jersey City");
        office.getOfficeAddress().setState("NJ");
        office.getOfficeAddress().setCountry("US");
        
        TransitTriple officetriple = new TransitTriple();
        officetriple.setKey("key1");
        officetriple.setType(Office.class.getName());
        officetriple.setTypeJson(office.toJson());

        json = officetriple.toJson();

        System.out.println("toJson: " + json);
        
        transit.add(officetriple);
        
        Assignment assignment = new Assignment();
        
        ObjectMapper mapper = new ObjectMapper(); // create once, reuse
        
        Map<String, Object> fieldMap = mapper.convertValue(assignment, Map.class);
        
        fieldMap = WorkflowUtils.mapTransit(transit, fieldMap, mapper);
        
        assignment = mapper.convertValue(fieldMap, Assignment.class);
        
        System.out.println(assignment.toJson());
    }

    private static void settings() {
        // Setting up Client
        //
        Settings settings = ImmutableSettings.settingsBuilder()
                .put("cluster.name", "elasticsearch")
                .put("client.transport.sniff", true)
                .build();

        Client client = new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress("localhost", 9300));

        // Setting up Index
        //
        IndexConfig indexConfig = new IndexConfig();

        indexConfig.setIndexName("test");
        indexConfig.setIndexAlias("IndexAlias");

        IndexSettings indexSettings = new IndexSettings();
        indexSettings.setNumber_of_replicas(3);
        indexSettings.setNumber_of_shards(3);

        indexConfig.setIndexSettings(indexSettings);

        System.out.println(indexConfig.toJson());

        // Call IndexApi.createIndex() method to create index        
        //
        IndexApi api = new IndexApiImpl();

        if (!api.isIndexExists(client, indexConfig.getIndexName())) {
            api.createIndex(client, indexConfig.toJson());
        }

        // Look at index settings
        // All settings
        String allSettings = api.getIndexSettings(client, indexConfig.toJson());

        System.out.println(allSettings);

        // Setting with specified name
        //
        String setting = api.getIndexSetting(client, indexConfig.toJson(), "number_of_shards");

        System.out.println(setting);

    }

    @JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY)
    static class Person implements Json, FromJson<Person>, Initializable {

        private String firstName;
        private String lastName;
        private Integer age;
        
        public Person() {
            init();
        }

        /**
         * @return the firstName
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName the firstName to set
         */
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        /**
         * @return the lastName
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * @param lastName the lastName to set
         */
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        /**
         * @return the age
         */
        public Integer getAge() {
            return age;
        }

        /**
         * @param age the age to set
         */
        public void setAge(Integer age) {
            this.age = age;
        }

        @Override
        public String toJson() {
            return new Gson().toJson(this, Person.class);
        }
        
        @Override
        public Person fromJson(String json){
            return new Gson().fromJson(json, Person.class);
        }

        @Override
        public final void init() {
            this.setAge(68);
            this.setFirstName("Alex");
            this.setLastName("Mylnikov");
        }
    }

    @JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY)
    static class Office implements Json, FromJson<Office>, Initializable {
        private String officeName;
        
        @JsonUnwrapped
        private Address officeAddress;
        
        public Office() {
            init();
        }

        /**
         * @return the officeName
         */
        public String getOfficeName() {
            return officeName;
        }

        /**
         * @param officeName the officeName to set
         */
        public void setOfficeName(String officeName) {
            this.officeName = officeName;
        }

        /**
         * @return the officeAddress
         */
        public Address getOfficeAddress() {
            return officeAddress;
        }

        /**
         * @param officeAddress the officeAddress to set
         */
        public void setOfficeAddress(Address officeAddress) {
            this.officeAddress = officeAddress;
        }

        @Override
        public String toJson() {
            return new Gson().toJson(this, Office.class);
        }

        @Override
        public Office fromJson(String json){
            return new Gson().fromJson(json, Office.class);
        }

        @Override
        public final void init() {
            Address address = new Address();
            address.init();
            this.setOfficeAddress(address);
            
            this.setOfficeName("");
        }

    }

    @JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY)
    static class Address implements Json, FromJson<Address>, Initializable {
        
        private String address;
        private String city;
        private String state;
        private String country;
        
        public Address() {
            init();
        }

        /**
         * @return the address
         */
        public String getAddress() {
            return address;
        }

        /**
         * @param address the address to set
         */
        public void setAddress(String address) {
            this.address = address;
        }

        /**
         * @return the city
         */
        public String getCity() {
            return city;
        }

        /**
         * @param city the city to set
         */
        public void setCity(String city) {
            this.city = city;
        }

        /**
         * @return the state
         */
        public String getState() {
            return state;
        }

        /**
         * @param state the state to set
         */
        public void setState(String state) {
            this.state = state;
        }

        /**
         * @return the country
         */
        public String getCountry() {
            return country;
        }

        /**
         * @param country the country to set
         */
        public void setCountry(String country) {
            this.country = country;
        }

        @Override
        public String toJson() {
            return new Gson().toJson(this, Address.class);
        }

        @Override
        public Address fromJson(String json){
            return new Gson().fromJson(json, Address.class);
        }

        @Override
        public final void init() {
            this.setAddress("");
            this.setCity("");
            this.setState("");
            this.setCountry("");
        }

    }
    
    @JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY)
    static class Assignment implements Json, FromJson<Assignment>, Initializable {
        
        @JsonUnwrapped
        private Person person;
        
        @JsonUnwrapped
        private Office office;
        
        public Assignment() {
            init();
        }
        
        public Assignment(Person person, Office office){
            this.person = person;
            this.office = office;
            
        }

        /**
         * @return the person
         */
        public Person getPerson() {
            return person;
        }

        /**
         * @param person the person to set
         */
        public void setPerson(Person person) {
            this.person = person;
        }

        /**
         * @return the office
         */
        public Office getOffice() {
            return office;
        }

        /**
         * @param office the office to set
         */
        public void setOffice(Office office) {
            this.office = office;
        }

        //
        @Override
        public String toJson() {
            return new Gson().toJson(this, Assignment.class);
        }

        @Override
        public Assignment fromJson(String json){
            return new Gson().fromJson(json, Assignment.class);
        }

        @Override
        public final void init() {
            
            person = new Person();
            person.init();
            this.setPerson(person);
            
            office = new Office();
            office.init();
            this.setOffice(office);
        }
    }

}
