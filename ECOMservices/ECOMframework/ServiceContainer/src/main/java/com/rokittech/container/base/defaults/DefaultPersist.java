/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base.defaults;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientAwsConfig;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.ringbuffer.Ringbuffer;
import com.rokittech.container.base.core.Persist;
import com.rokittech.container.base.core.PersistUtils;
import com.rokittech.containers.api.exceptions.HazelcastException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alexmy
 */
public class DefaultPersist implements Persist<HazelcastInstance, String, String> {

    public static final String HAZELCAST = "hazelcast";
    public static final String ACCESS_KEY = "accesskey";
    public static final String SECRET_KEY = "secretkey";
    public static final String TIMEOUT = "timeout";
    public static final String REGION = "region";
    public static final String TAG_KEY = "tagkey";
    public static final String TAG_VALUE = "tagvalue";
    public static final String USER_ID = "userid";
    public static final String PASSWORD = "password";
    public static final String CAPACITY = "capacity";

    private HazelcastInstance hz;
    private String envId;
    private Integer capacity = 1000;

    // List base persistence support
    //=========================================================
    /**
     *
     * @param id
     * @param data
     * @return
     */
    @Override
    public Boolean push2List(String id, String data) {
        Boolean success = false;

        if (hz != null) {
            List<String> list = hz.getList(id);
            success = PersistUtils.addToEnd(list, data);
        }

        return success;
    }

    /**
     *
     * @param id
     * @param index
     * @return
     */
    @Override
    public String pullFromListHead(String id, int index) {

        String item = null;
        if (hz != null) {
            List<String> list = hz.getList(id);
            item = PersistUtils.getFromHead(list, index);
        }

        return item;
    }

    /**
     *
     * @param id
     * @param index
     * @return
     */
    @Override
    public String pullFromListTail(String id, int index) {
        String item = null;

        if (hz != null) {
            List<String> list = hz.getList(id);
            item = PersistUtils.getFromTail(list, index);
        }

        return item;
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public String pullLastFromList(String id) {

        String item = null;

        if (hz != null) {
            List<String> list = hz.getList(id);
            item = PersistUtils.getLast(list);
        }

        return item;
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public List<String> pullAllFromList(String id) {
        List<String> items = new ArrayList<>();

        if (hz != null) {

            Ringbuffer<String> ring = hz.getRingbuffer(id);

            if (ring != null) {
                long sequence = ring.headSequence();

                while (true && items != null) {
                    try {
                        String item = ring.readOne(sequence);
                        if (item != null) {
                            items.add(item);
                        }
                        sequence++;
                    } catch (InterruptedException ex) {
                        items = null;
                        Logger.getLogger(DefaultPersist.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

        return items;
    }

    /**
     *
     * @param id
     * @param index
     * @return
     */
    @Override
    public Boolean deleteItemFromList(String id, int index) {
        Boolean success = false;

        if (hz != null) {
            List<String> list = hz.getList(id);
            success = PersistUtils.delete(list, index);
        }

        return success;
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public Boolean deleteAllFromList(String id) {
        Boolean success = false;

        if (hz != null) {
            List<String> list = hz.getList(id);
            success = PersistUtils.deleteAll(list);
        }

        return success;
    }

    
    // Environment management
    //=========================================================
    /**
     *
     * @return
     */
    @Override
    public HazelcastInstance getEnv() {
        return hz;
    }

    /**
     *
     * @param env
     */
    @Override
    public void setEnv(HazelcastInstance env) {
        this.hz = env;
    }

    /**
     *
     * @param props
     * @return
     */
    @Override
    public HazelcastInstance initEnv(Properties props)  throws HazelcastException  {

        this.hz = configHz(props);

        return hz;
    }

    /**
     *
     * @param fileName
     * @return
     * @throws com.rokittech.containers.api.exceptions.HazelcastException
     */
    @Override
    public HazelcastInstance initEnv(String fileName) throws HazelcastException {
        Properties props = new Properties();

        InputStream in = this.getClass().getClassLoader().getResourceAsStream(fileName);
//        System.out.println("Read all properties from file");
        try {
            props.load(in);
        } catch (IOException e) {
        }

        this.hz = configHz(props);
        if (props.getProperty(CAPACITY) != null) {
            this.capacity = Integer.valueOf(props.getProperty(CAPACITY));
        }

        return hz;
    }

    /**
     *
     * @param props
     * @return
     * @throws NumberFormatException
     */
    private HazelcastInstance configHz(Properties props) throws HazelcastException {

        ClientConfig clientConfig = new ClientConfig();
        ClientNetworkConfig clientNetworkConfig = new ClientNetworkConfig();
        ClientAwsConfig awsConfig = new ClientAwsConfig();
        HazelcastInstance _hz = null;
        
        try {
            awsConfig.setEnabled(true);
            awsConfig.setAccessKey(props.getProperty(ACCESS_KEY));
            awsConfig.setSecretKey(props.getProperty(SECRET_KEY));
            awsConfig.setInsideAws(false);
            awsConfig.setConnectionTimeoutSeconds(Integer.valueOf(props.getProperty(TIMEOUT)));
            awsConfig.setRegion(props.getProperty(REGION));
            awsConfig.setTagKey(props.getProperty(TAG_KEY));
            awsConfig.setTagValue(props.getProperty(TAG_VALUE));
            clientConfig.setGroupConfig(new GroupConfig(props.getProperty(USER_ID), props.getProperty(PASSWORD)));
            clientConfig.setNetworkConfig(clientNetworkConfig.setAwsConfig(awsConfig));
            _hz = HazelcastClient.newHazelcastClient(clientConfig);
        } catch (NumberFormatException | IllegalStateException e) {
           throw new HazelcastException ("Error: ", e.getMessage()); 
        } catch (Exception e){
            throw new HazelcastException ("Error: ", e.getMessage()); 
        }
        
        return _hz;
    }

    /**
     * @return the envId
     */
    @Override
    public String getEnvId() {
        return envId;
    }

    /**
     * @param envId the envId to set
     */
    @Override
    public void setEnvId(String envId) {
        this.envId = envId;
    }

    /**
     * @return the capacity
     */
    public Integer getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

}
