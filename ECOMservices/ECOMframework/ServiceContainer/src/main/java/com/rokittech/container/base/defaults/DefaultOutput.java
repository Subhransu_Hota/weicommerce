/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base.defaults;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.Json;

/**
 *
 * @author alexmy
 */
public class DefaultOutput implements Json {
    
    private String greeting;

    @Override
    public String toJson() {
        return new Gson().toJson(this, DefaultOutput.class);
    }    

    /**
     * @return the greeting
     */
    public String getGreeting() {
        return greeting;
    }

    /**
     * @param greeting the greeting to set
     */
    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }
}
