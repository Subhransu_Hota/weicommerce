/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.base.defaults;

import com.google.gson.Gson;
import com.rokittech.container.base.core.Params;
import com.rokittech.containers.api.core.Json;
//import com.rokittech.rtk.pay.RokittPay;
import java.util.Properties;

/**
 *
 * @author alexmy
 */
public class DefaultParams implements Json, Params<Object> {

    Properties properties;
    
    // RokitPay Env for BrainTree wrapper
//    private RokittPay rokittPay;
    private String envId;
    
    // BrainTree constants
    public static final String BRAINTREE_ENV = "braintreeenv";
    public static final String BRAINTREE_MERCHANT_ID = "braintreemerchantid";
    public static final String BRAINTREE_PUBLIC_KEY = "braintreepublickey";
    public static final String BRAINTREE_PRIVATE_KEY = "braintreeprivatekey";
    public static final String ROKITT_PAY = "rokittpay";
    
    // Payment Service URL
    private String serviceUrl;
    private String contentType;
    
    public static final String SERVICE_URL = "serviceurl";
    public static final String CONTENT_TYPE = "contenttype";
    
    // PostgreSql connection
    private String postgreSqlUrl;
    private String postgreSqlUserId;
    private String postgreSqlUserPsw;
    
    // Setting up environment
    //==========================================================================
    /**
     * 
     * @return 
     */
    @Override
    public Object getEnv() {
//        return this.rokittPay;
        return null;
    }

    /**
     * 
     * @param env 
     */
    @Override
    public void setEnv(Object env) {
        //this.rokittPay = env;
    }

    @Override
    public Object initEnv(Properties props) {
//        BTPaymentVenueFactory factory = new BTPaymentVenueFactory(
//                props.getProperty(BRAINTREE_ENV),
//                props.getProperty(BRAINTREE_MERCHANT_ID),
//                props.getProperty(BRAINTREE_PUBLIC_KEY),
//                props.getProperty(BRAINTREE_PRIVATE_KEY)
//        );
//
//        PaymentVenue paymentVenue = factory.create();
//        paymentVenue.connect();
//        
//        this.rokittPay = new RokittPay(factory);
//        
//        this.setContentType(props.getProperty(CONTENT_TYPE));
//        this.setServiceUrl(props.getProperty(SERVICE_URL));
//
//        return rokittPay;
        return null;
    }

    /**
     * Reads properties from the file assuming that file is in resources
     * 
     * @param fileName
     * @return 
     */
    @Override
    public Object initEnv(String fileName) {
        
//        Properties props = new Properties();
//        
//        InputStream in = this.getClass().getClassLoader().getResourceAsStream(fileName);
////        System.out.println("Read all properties from file");
//        
//        try {
//            props.load(in);
//        } catch (IOException e) {
//        }
//        
//        BTPaymentVenueFactory factory = new BTPaymentVenueFactory(
//                props.getProperty(BRAINTREE_ENV),
//                props.getProperty(BRAINTREE_MERCHANT_ID),
//                props.getProperty(BRAINTREE_PUBLIC_KEY),
//                props.getProperty(BRAINTREE_PRIVATE_KEY)
//        );
//
//        PaymentVenue paymentVenue = factory.create();
//        paymentVenue.connect();
//
//        this.rokittPay = new RokittPay(factory);
//         
//        this.setContentType(props.getProperty(CONTENT_TYPE));
//        this.setServiceUrl(props.getProperty(SERVICE_URL));
//
//        return rokittPay;
        return null;
    }
    
    /**
     * @return the envId
     */
    @Override
    public String getEnvId() {
        return envId;
    }

    /**
     * @param envId the envId to set
     */
    @Override
    public void setEnvId(String envId) {
        this.envId = envId;
    }

    // Redis connection
    @Override
    public String toJson() {
        return new Gson().toJson(this, DefaultParams.class);
    }
    
    //==========================================================================
    // Additional properties
    //==========================================================================

    /**
     * @return the postgreSqlUrl
     */
    public String getPostgreSqlUrl() {
        return postgreSqlUrl;
    }

    /**
     * @param postgreSqlUrl the postgreSqlUrl to set
     */
    public void setPostgreSqlUrl(String postgreSqlUrl) {
        this.postgreSqlUrl = postgreSqlUrl;
    }

    /**
     * @return the postgreSqlUserId
     */
    public String getPostgreSqlUserId() {
        return postgreSqlUserId;
    }

    /**
     * @param postgreSqlUserId the postgreSqlUserId to set
     */
    public void setPostgreSqlUserId(String postgreSqlUserId) {
        this.postgreSqlUserId = postgreSqlUserId;
    }

    /**
     * @return the postgreSqlUserPsw
     */
    public String getPostgreSqlUserPsw() {
        return postgreSqlUserPsw;
    }

    /**
     * @param postgreSqlUserPsw the postgreSqlUserPsw to set
     */
    public void setPostgreSqlUserPsw(String postgreSqlUserPsw) {
        this.postgreSqlUserPsw = postgreSqlUserPsw;
    }

    

    /**
     * @return the serviceUrl
     */
    public String getServiceUrl() {
        return serviceUrl;
    }

    /**
     * @param serviceUrl the serviceUrl to set
     */
    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public String getContentType() {
        
        return contentType;
    }

    /**
     * @param contentType the contentType to set
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public Properties getProperties() {
        return this.properties == null ? new Properties() : properties;
    }

    @Override
    public void setProperties(Properties props) {
        this.properties = props;
    }

    @Override
    public Object fromJson(String json) {
        return new Gson().fromJson(json, DefaultParams.class);
    }

    @Override
    public void init() {
        this.properties = new Properties();
    }

}
