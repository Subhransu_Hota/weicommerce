/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.payment.service_0_0_1;

import com.google.gson.Gson;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.core.AbstractService;
import com.rokittech.container.ecom.commons.models.Payment;
import com.rokittech.container.ecom.paymentadapter.client.PaymentClient;

import com.rokittech.container.ecom.payment.defaults.PaymentServiceInput;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceOutput;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceParams;
import com.rokittech.container.ecom.paymentadapter.client.factory.PaymentClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 
 */
public class PaymentService_0_0_1 extends AbstractService<PaymentServiceInput, PaymentServiceOutput, PaymentServiceParams, DefaultPersist> {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(PaymentService_0_0_1.class);
    private PaymentServiceInput input;
    private PaymentServiceOutput output;
    private PaymentServiceParams params;

        
    public PaymentService_0_0_1() {
        
    }
    
    @Override
    public String serviceId() {
        return this.getClass().getName();
    }

    @Override
    public String getInputAsJson() {
        slf4jLogger.info("returning Input Object in the form of JSON");
        
        return input.toJson();
    }

    @Override
    public String getOutputAsJson() {
        slf4jLogger.info("returning Output Object in the form of JSON");
        return output.toJson();
    }

    @Override
    public void setInputAsJson(String input) {
        slf4jLogger.info("Recreating Input object from json");
        this.input = this.input.fromJson(input);
    }

    @Override
    public void setOutputAsJson(String output) {
        slf4jLogger.info("Recreating Ouput object from json");
        this.output = this.output.fromJson(output);
    }

    @Override
    public PaymentServiceOutput serve() {
        slf4jLogger.info("Inside serve() ");
        Payment payment = input.getData();
        PaymentClient client = new PaymentClientFactory(payment.getPaymentType().name(), 1).get();
        client.setInput(payment);
        client.setProperties(params.getProperties());
        
        output.setInfo(input.getInfo());
        output.setData(client.getFormData().toJson());
        slf4jLogger.info("Created output object " + output.getClass());
        
        return output;
    }

    @Override
    public String toJson() {
        slf4jLogger.info("generating json for service object");
        return new Gson().toJson(this, PaymentService_0_0_1.class);
    }

    @Override
    public PaymentServiceInput getInput() {
        return input;
    }

    @Override
    public void setInput(PaymentServiceInput input) {
        this.input = input;
    }

    @Override
    public PaymentServiceOutput getOutput() {
        return output;
    }

    @Override
    public void setOutput(PaymentServiceOutput output) {
        this.output = output;
    }

    @Override
    public PaymentServiceParams getParams() {
        return params;
    }

    @Override
    public void setParams(PaymentServiceParams params) {
        this.params = params;
    }
}
