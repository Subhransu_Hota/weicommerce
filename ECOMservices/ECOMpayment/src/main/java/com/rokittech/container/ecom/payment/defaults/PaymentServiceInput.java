/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.payment.defaults;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.ecom.commons.models.Payment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author deependr
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public final class PaymentServiceInput extends Input<Payment> {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(PaymentServiceInput.class);
    /**
     *
     */
    public PaymentServiceInput() { 
        slf4jLogger.info("inside constructor");
        this.init();
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, PaymentServiceInput.class);
    }

    @Override
    public PaymentServiceInput fromJson(String json) {
        return new Gson().fromJson(json, PaymentServiceInput.class);
    }

    @Override
    public final void init() {
    }
}
