package com.rokittech.container.ecom.payment.factory;

import com.google.inject.Provider;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.base.defaults.Output;
import com.rokittech.container.ecom.payment.service_0_0_1.PaymentService_0_0_1;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceParams;
import com.rokittech.container.ecom.paymentadapter.client.PaymentVocabulary;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PaymentServiceFactory implements Provider<Service<Input, Output, 
        PaymentServiceParams, DefaultPersist>> {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(PaymentServiceFactory.class);
    
    private Service service;

    /**
     * This is the place where you are managing different versions
     *
     * @param version
     */
//    @Inject
    public PaymentServiceFactory(int version) {
        this(null, version);
    }

   public PaymentServiceFactory(String paymentType, int version) {
        slf4jLogger.info ("Will create the instance of service based on appID: " + paymentType + " Version: " + version);
        if (paymentType != null) {
            PaymentVocabulary app = PaymentVocabulary.valueOf(paymentType.toUpperCase());
            switch (app) {
                case WEI_ECOMMERCE:
                    payuMoneyClient(version);
                    break;
                
                case TEST_ECOMMERCE:
                    payuMoneyClient(version);
                    break;
            }
        } else {
            payuMoneyClient(version);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Service get() {
        return service;
    }

    private void payuMoneyClient(int version) {

        switch (version) {
            case 1:
                this.service = new PaymentService_0_0_1();
            	
                break;
            case 2:

                break;
            default:

                break;

        }
    }
    
}
