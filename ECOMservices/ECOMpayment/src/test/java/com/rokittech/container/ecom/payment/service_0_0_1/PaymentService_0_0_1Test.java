/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.payment.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.models.Payment;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceInput;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceOutput;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceParams;
import com.rokittech.container.ecom.payment.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sangamesh
 */
public class PaymentService_0_0_1Test {
    
    public PaymentService_0_0_1Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        PaymentServiceInput input = new PaymentServiceInput();
        Payment payment = TestUtil.getInstance ().createPayment();
        
        
        input.setData(payment);
        input.setInfo(TestUtil.getInstance().createInfo());
        System.out.println("JSON Request==> " + input.toJson());
        
        PaymentServiceOutput output = new PaymentServiceOutput();
        // This is real mandatory code to move in controller
        PaymentServiceParams params = TestUtil.getInstance().getParams ();
       
        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of serve method, of class PaymentService_0_0_1.
     */
    @Test
    public void testServe() {
        System.out.println("serve");
        PaymentServiceOutput result = TestUtil.getInstance().getService().serve();
        
        System.out.println("JSON Response==> " + result.getData());
        assertNotEquals("Result does not have object ", 
                TestUtil.getInstance().checkHash (result.getData()));
    
    }
}
