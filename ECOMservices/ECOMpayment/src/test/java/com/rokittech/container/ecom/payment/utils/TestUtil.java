/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.payment.utils;

import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Payment;
import com.rokittech.container.ecom.commons.models.PaymentResp;
import com.rokittech.container.ecom.commons.models.PaymentType;
import com.rokittech.container.ecom.payment.defaults.PaymentServiceParams;
import com.rokittech.container.ecom.payment.factory.PaymentServiceFactory;
import com.rokittech.container.ecom.payment.service_0_0_1.PaymentService_0_0_1;
import java.util.HashMap;

/**
 *
 * @author sangamesh
 */
public class TestUtil {
    private final PaymentService_0_0_1 service;
    private static final PaymentServiceParams params = new PaymentServiceParams();
    
    private TestUtil() {
        service = (PaymentService_0_0_1) new PaymentServiceFactory("test_ecommerce", 1).get();
        params.initEnv("payment.properties");
    }

    
    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public Payment createPayment() {
        Payment payment = new Payment ();
        payment.setAmount(1000.00);
        payment.setFirstname("test");
        payment.setEmail("test@test.com");
        payment.setPhone("9008064007");
        payment.setProductInfo("product info");
        payment.setPaymentType(PaymentType.PAYUMONEY);
        HashMap<String, String> userDefinedFields = new HashMap<String, String>();
        userDefinedFields.put("orderId", "5786781681y211a");
        payment.setUserDefinedFields(userDefinedFields);
        return payment;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        return info;
    }

    

    public PaymentServiceParams getParams() {
        return params;
    }

    public PaymentService_0_0_1 getService() {
        return service;
    }

    public boolean checkHash(String data) {
        PaymentResp resp = new PaymentResp().fromJson(data);
        
        return (resp.getFormData().containsKey("hash"));
    }
    
    private static class TestDataCreationHolder {
        private static final TestUtil INSTANCE = new TestUtil();
    }
}
