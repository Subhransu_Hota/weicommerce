/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.order.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.models.Order;
import com.rokittech.container.ecom.order.defaults.OrderServiceInput;
import com.rokittech.container.ecom.order.defaults.OrderServiceOutput;
import com.rokittech.container.ecom.order.defaults.OrderServiceParams;
import com.rokittech.container.ecom.order.defaults.OrderVocabulary;
import com.rokittech.container.ecom.order.utils.TestUtil;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sangamesh
 */
public class OrderService_0_0_1FindByIdOrderTest {
    
    public OrderService_0_0_1FindByIdOrderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        OrderServiceInput input = new OrderServiceInput();
        Order order = TestUtil.getInstance ().createNewOrderInDatabase (
                "test_order_" + new Random ().nextInt(Integer.MAX_VALUE));
        
        input.setData(order);
        input.setInfo(TestUtil.getInstance().createInfo());
        
        OrderServiceOutput output = new OrderServiceOutput();
        // This is real mandatory code to move in controller
        OrderServiceParams params = TestUtil.getInstance().getParams ();
       
        params.getProperties().put(OrderVocabulary.COLLECTION.name(), "Order");
        params.getProperties().put(OrderVocabulary.COMMAND.name(), OrderVocabulary.RETRIEVE.name());
        
        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInputAsJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        System.out.println("getInputAsJson");
        String result = TestUtil.getInstance().getService().getInputAsJson();
        OrderServiceInput input = new OrderServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
    }

    /**
     * Test of serve method, of class CatalogService_0_0_1.
     */
    @Test
    public void testServe() {
        System.out.println("serve");
        OrderServiceOutput result = TestUtil.getInstance().getService().serve();
        
        assertNotEquals("Result does not have object ", 
                TestUtil.getInstance().hasInputObject (result.getData(), TestUtil.getInstance().getService().getInput().getData().toJson()));
    }

    /**
     * Test of getOutputAsJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        System.out.println("getOutputAsJson");
        
        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        OrderServiceOutput output = new OrderServiceOutput().fromJson(result);
        
        assertNotEquals("Result does not have object ", 
                TestUtil.getInstance().hasInputObject (output.getData(), TestUtil.getInstance().getService().getInput().getData().toJson()));
    }
    
    /**
     * Test of toJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testToJson() {
        System.out.println("toJson");
        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
    }
}
