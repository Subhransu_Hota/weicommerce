/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.order;
import com.rokittech.container.ecom.order.service_0_0_1.OrderService_0_0_1DeleteOrderTest;
import com.rokittech.container.ecom.order.service_0_0_1.OrderService_0_0_1FindByIdOrderTest;
import com.rokittech.container.ecom.order.service_0_0_1.OrderService_0_0_1InsertOrderTest;
import com.rokittech.container.ecom.order.service_0_0_1.OrderService_0_0_1UpdateOrderTest;
import com.rokittech.container.ecom.order.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author sangamesh
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    OrderService_0_0_1InsertOrderTest.class,
    OrderService_0_0_1UpdateOrderTest.class,
    OrderService_0_0_1FindByIdOrderTest.class,
    OrderService_0_0_1DeleteOrderTest.class
})

public class OrderTestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Initiated Mongo database");

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Dropping tables... ");
        TestUtil.getInstance().cleanup ();
        
    }
}
