/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.order.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.order.defaults.OrderServiceInput;
import com.rokittech.container.ecom.order.defaults.OrderServiceOutput;
import com.rokittech.container.ecom.order.defaults.OrderServiceParams;
import com.rokittech.container.ecom.order.defaults.OrderVocabulary;
import com.rokittech.container.ecom.order.utils.TestUtil;
import com.rokittech.container.ecom.commons.models.Order;
import com.rokittech.container.ecom.commons.models.OrderStatusType;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sangamesh
 */
public class OrderService_0_0_1UpdateOrderTest {

    public OrderService_0_0_1UpdateOrderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        OrderServiceInput input = new OrderServiceInput();
        Order order = TestUtil.getInstance().createNewOrderInDatabase(
                "test_order_" + new Random().nextInt(Integer.MAX_VALUE));

        
        
        
        Order p = new Order ();
        p.setId(order.getId());
        p.setOrderNumber(order.getOrderNumber());
        p.setStatus(OrderStatusType.PENDING);
        input.setData(p);
        input.setInfo(TestUtil.getInstance().createInfo());
        
        System.out.println("setup.. " + input.toJson());
        
        OrderServiceOutput output = new OrderServiceOutput();
        // This is real mandatory code to move in controller
        OrderServiceParams params = TestUtil.getInstance().getParams();
        
        params.getProperties().put(OrderVocabulary.COLLECTION.name(), "Order");
        params.getProperties().put(OrderVocabulary.COMMAND.name(), OrderVocabulary.UPDATE.name());
        
        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInputAsJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        System.out.println("getInputAsJson");
        String result = TestUtil.getInstance().getService().getInputAsJson();
        OrderServiceInput input = new OrderServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
    }

    /**
     * Test of serve method, of class CatalogService_0_0_1.
     */
    @Test
    public void testServe() {
        System.out.println("serving .. " + TestUtil.getInstance().getService().getInput().getData().toJson());
        OrderServiceOutput result = TestUtil.getInstance().getService().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isOrderEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                result.getData()));
    }

    /**
     * Test of getOutputAsJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        System.out.println("getOutputAsJson");
        
        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        OrderServiceOutput output = new OrderServiceOutput().fromJson(result);
        
        assertTrue("input and output are not same", TestUtil.getInstance().isOrderEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                output.getData()));
    }

    /**
     * Test of toJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testToJson() {
        System.out.println("toJson");
        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
    }
}
