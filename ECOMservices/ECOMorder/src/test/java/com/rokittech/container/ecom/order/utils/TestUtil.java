/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.order.utils;

import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.core.Money;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.container.ecom.commons.models.AddressType;
import com.rokittech.container.ecom.commons.models.Counter;
import com.rokittech.container.ecom.commons.models.Order;
import com.rokittech.container.ecom.commons.models.OrderStatusType;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import com.rokittech.container.ecom.order.defaults.OrderServiceParams;
import com.rokittech.container.ecom.order.factory.OrderServiceFactory;
import com.rokittech.container.ecom.order.service_0_0_1.OrderService_0_0_1;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.net.InetSocketAddress;
import java.util.Locale;
import org.bson.types.ObjectId;

/**
 *
 * @author sangamesh
 */
public class TestUtil {
    private final OrderService_0_0_1 service;
    private static final OrderServiceParams params = new OrderServiceParams();
    private MongoClient client;
    private MongoServer server;
    private static long counter;
    
    private TestUtil() {
        service = (OrderService_0_0_1) new OrderServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
        ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        counter = createCounterInDatabase().getCounter();
        //params.initEnv("env.properties");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }
    
    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }
    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        return info;
        
    }

    public OrderServiceParams getParams() {
        return params;
    }

    public boolean hasInputObject(String result, String input) {
        Order oi = new Order ().fromJson(input);
        Order output = new Order ().fromJson(result);
        
        return oi.getId().equalsIgnoreCase(output.getId());
    }

    public Order createOrder(String string) {
        Order order = new Order();
        Address ba = new Address();
        ba.setAddressName("ba1");
        ba.setAddressType(AddressType.Billing);
        ba.setCity("city");
        ba.setCountry("country");
        ba.setState("state");
        ba.setStreet1("state1");
        ba.setStreet2("state2");

        ba.setZipCode("12345");
        order.setBillingAddress(ba);
        order.setCustomerId("cust_id");
        order.setBasketId("basket_id");
        order.setGrossPrice(new Money(new Locale("en", "IN"), 1140.00));
        order.setNetPrice(new Money(new Locale("en", "IN"), 1000.00));
        order.setTax(new Money(new Locale("en", "IN"), 140.00));
        order.setStatus(OrderStatusType.NEW);
        
        order.setOrderNumber("" + counter);
        return order;
    }

    public boolean isOrderEqual(String input, String output) {
        System.out.println ("input: " + input);
        System.out.println("output: " + output);
        
        Order inputOrder = new Order ().fromJson(input);
        Order outputOrder = new Order ().fromJson(output);
        
        boolean b =  inputOrder.getOrderNumber().equalsIgnoreCase(outputOrder.getOrderNumber());
        
        return b;
    }

    public Order createNewOrderInDatabase(String string) {
        Order order = createOrder(string);
        
        DBObject object = (DBObject) JSON.parse(order.toJson());
        object.put("is_deleted", false);
        
        client.getDatabase("ecommerce_test").getCollection("Catalog", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId)object.get("_id");
        object.put("_id", objId.toString());
        
        return new Order ().fromJson(JSON.serialize(object));
    
    }

    public void cleanup() {
        client.getDatabase("ecommerce_test").getCollection("Catalog").drop();
        client.getDatabase("ecommerce_test").getCollection("Order").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    public OrderService_0_0_1 getService() {
        return service;
    }

    
    private Counter createCounterInDatabase () {
        Counter counter = new Counter ();
        counter.setId("ordernumber");
        counter.setCounter(10000);
        DBObject object = (DBObject) JSON.parse(counter.toJson());
        
        client.getDatabase("ecommerce_test").getCollection("counters", DBObject.class).insertOne(object);
        
        return counter;
    }
    
    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }
}
