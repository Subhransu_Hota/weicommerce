/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.order.defaults;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.ecom.commons.models.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author deependr
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public final class OrderServiceInput extends Input<Order> {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(OrderServiceInput.class);
    /**
     *
     */
    public OrderServiceInput() { 
        slf4jLogger.info("inside constructor");
        this.init();
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, OrderServiceInput.class);
    }

    @Override
    public OrderServiceInput fromJson(String json) {
        return new Gson().fromJson(json, OrderServiceInput.class);
    }

    @Override
    public final void init() {
    }
}
