/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.permission.service_0_0_1;

import com.rokittech.container.ecom.commons.models.Permission;
import com.rokittech.container.ecom.permission.defaults.PermissionService;
import com.rokittech.container.ecom.permission.defaults.Permissions;
import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

/**
 *
 * @author asifaham
 */
public class PermissionService_0_0_1 implements PermissionService {
    
    private static final Logger logger = LoggerFactory.getLogger(PermissionService_0_0_1.class);
    
    private final String PERMISSION_PROPERTIES = "/permissions.yml";
    
    @Override
    public Permissions serve() {
        final  Constructor constructor = new Constructor(Permissions.class);
        final TypeDescription typeDescription = new TypeDescription(Permissions.class);
        typeDescription.putListPropertyType("permissions", Permission.class);
        constructor.addTypeDescription(typeDescription);
        final Yaml yaml = new Yaml(constructor);
        final InputStream inputStream = PermissionService_0_0_1.class.getResourceAsStream(this.PERMISSION_PROPERTIES);
        final Permissions permissions = (Permissions) yaml.load(inputStream);
        
        return permissions;
    } 
}
