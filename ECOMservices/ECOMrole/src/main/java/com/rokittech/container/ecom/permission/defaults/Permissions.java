/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.permission.defaults;

import com.google.gson.Gson;
import com.rokittech.container.ecom.commons.models.Permission;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Json;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author asifaham
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class Permissions implements Json, FromJson{

    private Set<Permission> permissions;

    @Override
    public String toJson() {
        return new Gson().toJson(this, Permissions.class);
    }

    @Override
    public Permissions fromJson(String json) {
        return new Gson().fromJson(json, Permissions.class);
    }
}
