/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.permission.factory;

import com.rokittech.container.ecom.permission.defaults.PermissionService;
import com.rokittech.container.ecom.permission.defaults.PermissionVocabulary;
import com.rokittech.container.ecom.permission.service_0_0_1.PermissionService_0_0_1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author asifaham
 */
public class PermissionServiceFactory {
    
    private static final Logger logger = LoggerFactory.getLogger(PermissionServiceFactory.class);
    private String appId;
    private int version;
    
    private PermissionService service;

    /**
     * This is the place where you are managing different versions
     *
     * @param version
     */
    public PermissionServiceFactory(int version) {
        this.appId = null;
        this.version = version;
    }

    public PermissionServiceFactory(String appId, int version) {
        logger.info("Will create the instance of permission service based on appID: " + appId + " Version: " + version);
        if (appId != null) {
            PermissionVocabulary app = PermissionVocabulary.valueOf(appId.toUpperCase());
            switch (app) {
                case WEI_ECOMMERCE:
                    weiEcommerce(version);
                    break;
                case TEST_ECOMMERCE:
                    testEcommerce(version);
                    break;
                default:
                    weiEcommerce(version);
                    break;
            }
        } else {
            weiEcommerce(version);
        }
    }

    /**
     *
     * @return
     */
    public PermissionService get() {
        return service;
    }

    private void weiEcommerce(int version) {

        switch (version) {
            case 1:
                this.service = new PermissionService_0_0_1();

                break;
            case 2:

                break;
            default:

                break;

        }
    }

    private void testEcommerce(int version) {
        switch (version) {
            case 1:
                this.service = new PermissionService_0_0_1();
                break;
            case 2:

                break;
            default:

                break;
        }
    }
}
