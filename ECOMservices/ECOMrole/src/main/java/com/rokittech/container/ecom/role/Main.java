/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.role;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Permission;
import com.rokittech.container.ecom.commons.models.Role;
import com.rokittech.container.ecom.role.defaults.RoleServiceInput;
import com.rokittech.container.ecom.role.defaults.RoleServiceOutput;
import com.rokittech.container.ecom.role.defaults.RoleServiceParams;
import com.rokittech.container.ecom.role.defaults.RoleVocabulary;
import com.rokittech.container.ecom.role.factory.RoleServiceFactory;
import com.rokittech.containers.api.core.Service;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author asifahammed
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);
        
        //Temp code for poc.
        Set<Permission> permissions = new HashSet<>(Arrays.asList(
                new Permission("order_update", "update order"), new Permission("order_delete", "delete order"), new Permission("user_update", "update user")));
        Role role = new Role();
        role.setName("Order Manager");
        role.setDescription("Manages order");
        role.setPermissions(permissions);
        
        // This is real mandatory code to move in controller
        RoleServiceParams params = new RoleServiceParams();
        params.initEnv("env.properties");
        params.getProperties().put(RoleVocabulary.COLLECTION.name(), "Role");
        params.getProperties().put(RoleVocabulary.COMMAND.name(), RoleVocabulary.INSERT.name());

        DefaultPersist persist = new DefaultPersist();
        //persist.initEnv("env.properties");

//        CrudNoSqlServiceInput json = new CrudNoSqlServiceInput();
        //
        RoleServiceInput inputTemp = new RoleServiceInput();
        inputTemp.setData(role);
        inputTemp.setInfo(info);

//        CrudNoSqlService_0_0_1 service;        
        Service<RoleServiceInput, RoleServiceOutput, RoleServiceParams, DefaultPersist> service = new RoleServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new RoleServiceOutput());

        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
