package com.rokittech.container.ecom.role;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.rokittech.container.ecom.role.service_0_0_1.RoleService_0_0_1DeleteRoleTest;
import com.rokittech.container.ecom.role.service_0_0_1.RoleService_0_0_1FindByIdRoleTest;
import com.rokittech.container.ecom.role.service_0_0_1.RoleService_0_0_1InsertRoleTest;
import com.rokittech.container.ecom.role.service_0_0_1.RoleService_0_0_1UpdateRoleTest;
import com.rokittech.container.ecom.role.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    RoleService_0_0_1InsertRoleTest.class,
    RoleService_0_0_1UpdateRoleTest.class,
    RoleService_0_0_1DeleteRoleTest.class,
    RoleService_0_0_1FindByIdRoleTest.class,})
public class RoleTestSuite {

    private static final Logger logger = LoggerFactory.getLogger(RoleTestSuite.class);

    @BeforeClass
    public static void setUpClass() throws Exception {

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        logger.info("Initiated Mongo database");
        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        //Cleanup Resources
        logger.info("Cleanup Mongo database");

        TestUtil.getInstance().cleanup();

    }

}
