/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.role.utils;

import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Permission;
import com.rokittech.container.ecom.commons.models.Role;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import com.rokittech.container.ecom.role.defaults.RoleServiceParams;
import com.rokittech.container.ecom.role.factory.RoleServiceFactory;
import com.rokittech.container.ecom.role.service_0_0_1.RoleService_0_0_1;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class TestUtil {
    private final RoleService_0_0_1 service;
    private static final Logger logger = LoggerFactory.getLogger(TestUtil.class);

    private static final RoleServiceParams params = new RoleServiceParams();
    private MongoClient client;
    private MongoServer server;

    private TestUtil() {
        service = (RoleService_0_0_1) new RoleServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
       ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        //params.initEnv("env.properties");
        logger.info("Mongo Db connection created succesfully");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }

    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public Role createRole() {
        Set<Permission> permissions = new HashSet<>(Arrays.asList(
                new Permission("order_update", "update order"), new Permission("order_delete", "delete order"), new Permission("user_update", "update user")));
        Role role = new Role();
        role.setName("Order Manager");
        role.setDescription("Manages order");
        role.setPermissions(permissions);
        
        return role;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);

        return info;
    }

    public boolean isRoleEqual(String input, String output) {
        logger.info("Input :" + input + "\n Output : " + output);
        Role inputRole = new Role().fromJson(input);
        Role outputRole = new Role().fromJson(output);

        boolean b = inputRole.getDescription().equalsIgnoreCase(outputRole.getDescription())
                && inputRole.getPermissions().equals(outputRole.getPermissions())
                && inputRole.getName().equalsIgnoreCase(outputRole.getName());

        return b;
    }

    public RoleServiceParams getParams() {
        return params;
    }

    public Role createNewRoleInDatabase() {

        logger.info("Creating test data");
        Set<Permission> permissions = new HashSet<>(Arrays.asList(
                new Permission("order_update", "update order"), new Permission("order_delete", "delete order"), new Permission("user_update", "update user")));
        Role role = new Role();
        role.setName("Order Manager");
        role.setDescription("Manages order");
        role.setPermissions(permissions);

        DBObject object = (DBObject) JSON.parse(role.toJson());
        object.put("is_deleted", false);

        client.getDatabase("ecommerce_test").getCollection("Role", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId) object.get("_id");
        object.put("_id", objId.toString());

        return new Role().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObject(String result, String input) {
        Role inputRole = new Role ().fromJson(input);
        Role outputRole = new Role ().fromJson(result);
        
        return inputRole.getId().equalsIgnoreCase(outputRole.getId());
    }

    public void cleanup() {

        logger.info("Cleanup test data");
        client.getDatabase("ecommerce_test").getCollection("Role").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    public RoleService_0_0_1 getService() {
        return service;
    }

    
    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }
}
