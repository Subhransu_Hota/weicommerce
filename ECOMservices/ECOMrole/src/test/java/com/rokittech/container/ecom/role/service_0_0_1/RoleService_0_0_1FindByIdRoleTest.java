/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.role.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.models.Role;
import com.rokittech.container.ecom.role.defaults.RoleServiceInput;
import com.rokittech.container.ecom.role.defaults.RoleServiceOutput;
import com.rokittech.container.ecom.role.defaults.RoleServiceParams;
import com.rokittech.container.ecom.role.defaults.RoleVocabulary;
import com.rokittech.container.ecom.role.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class RoleService_0_0_1FindByIdRoleTest {

    private static final Logger logger = LoggerFactory.getLogger(RoleService_0_0_1FindByIdRoleTest.class);
    
    public RoleService_0_0_1FindByIdRoleTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        logger.info("Setup test resouces");
        RoleServiceInput input = new RoleServiceInput();
        Role role = TestUtil.getInstance().createNewRoleInDatabase();

        input.setData(role);
        input.setInfo(TestUtil.getInstance().createInfo());

        RoleServiceOutput output = new RoleServiceOutput();
        // This is real mandatory code to move in controller
        RoleServiceParams params = TestUtil.getInstance().getParams();

        params.getProperties().put(RoleVocabulary.COLLECTION.name(), "Role");
        params.getProperties().put(RoleVocabulary.COMMAND.name(), RoleVocabulary.RETRIEVE.name());

        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }

    @After
    public void tearDown() {
        logger.info("Cleanup all test resouces");
    }

    /**
     * Test of getInputAsJson method, of class RoleService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        logger.info("Started Execution of getInputAsJson ");
        String result = TestUtil.getInstance().getService().getInputAsJson();
        RoleServiceInput input = new RoleServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
        logger.info("Finished Execution of getInputAsJson ");
    }

    /**
     * Test of serve method, of class RoleService_0_0_1.
     */
    @Test
    public void testServe() {
        logger.info("Started Execution of Serve ");
        RoleServiceOutput result = TestUtil.getInstance().getService().serve();

        assertNotEquals("Result does not have object ",
                TestUtil.getInstance().hasInputObject(result.getData(), TestUtil.getInstance().getService().getInput().getData().toJson()));
        logger.info("Finished Execution of Serve ");
    }

    /**
     * Test of getOutputAsJson method, of class RoleService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {

        logger.info("Started Execution of getOutputAsJson ");
        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        RoleServiceOutput output = new RoleServiceOutput().fromJson(result);

        assertNotEquals("Result does not have object ",
                TestUtil.getInstance().hasInputObject(output.getData(), TestUtil.getInstance().getService().getInput().getData().toJson()));
        logger.info("Finished Execution of getOutputAsJson ");
    }

    /**
     * Test of toJson method, of class RoleService_0_0_1.
     */
    @Test
    public void testToJson() {
        logger.info("Started Execution of toJson ");

        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
        logger.info("Finished Execution of toJson ");
    }
}
