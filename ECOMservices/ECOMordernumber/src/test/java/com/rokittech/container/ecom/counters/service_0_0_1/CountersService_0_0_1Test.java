/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.counters.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.models.Counter;
import com.rokittech.container.ecom.counters.defaults.CountersServiceInput;
import com.rokittech.container.ecom.counters.defaults.CountersServiceOutput;
import com.rokittech.container.ecom.counters.defaults.CountersServiceParams;
import com.rokittech.container.ecom.counters.defaults.CountersVocabulary;
import com.rokittech.container.ecom.counters.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class CountersService_0_0_1Test {

    private static final Logger logger = LoggerFactory.getLogger(CountersService_0_0_1Test.class);
    public CountersService_0_0_1Test() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        logger.info("Setup test resouces");
        Counter counter = TestUtil.getInstance ().createCounterInDatabase();
        CountersServiceInput input = new CountersServiceInput();
        input.setInfo(TestUtil.getInstance().createInfo());
        input.setData(counter);
        
        CountersServiceOutput output = new CountersServiceOutput();
        // This is real mandatory code to move in controller
        CountersServiceParams params = TestUtil.getInstance().getParams();

        params.getProperties().put(CountersVocabulary.COLLECTION.name(), "counters");
        params.getProperties().put(CountersVocabulary.COMMAND.name(), CountersVocabulary.COUNTER.name());

        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }

    @After
    public void tearDown() {
        logger.info("Cleanup all test resouces");
    }

    /**
     * Test of serve method, of class AttributeService_0_0_1.
     */
    @Test
    public void testServe() {

        logger.info("Started Execution of serve ");

        CountersServiceOutput result = TestUtil.getInstance().getService().serve();
        Counter counter = new Counter ().fromJson (result.getData());
        assertNotNull("Test Failed: Input As Json is null", counter);
        
        logger.info("Finished Execution of serve: " + counter.getCounter());
    }
}
