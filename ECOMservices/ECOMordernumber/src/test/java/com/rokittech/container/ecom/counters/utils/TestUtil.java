/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.counters.utils;

import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.counters.defaults.CountersServiceParams;
import com.rokittech.container.ecom.counters.factory.CountersServiceFactory;
import com.rokittech.container.ecom.counters.service_0_0_1.CountersService_0_0_1;
import com.rokittech.container.ecom.commons.models.Attribute;
import com.rokittech.container.ecom.commons.models.Counter;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.net.InetSocketAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class TestUtil {

    private static final Logger logger = LoggerFactory.getLogger(TestUtil.class);

    private static final CountersServiceParams params = new CountersServiceParams();
    private final CountersService_0_0_1 service;
    private MongoClient client;
    private MongoServer server;

    private TestUtil() {
        service = (CountersService_0_0_1) new CountersServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAttribute = server.bind();
        client = new MongoClient(new ServerAddress(serverAttribute));

        //Initiate Mongodb
        ((MongoManager) service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        logger.info("Mongo Db connection created succesfully");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }

    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);

        return info;
    }

    public Counter createCounterInDatabase () {
        Counter counter = new Counter ();
        counter.setId("ordernumber");
        counter.setCounter(10000);
        DBObject object = (DBObject) JSON.parse(counter.toJson());
        
        client.getDatabase("ecommerce_test").getCollection("counters", DBObject.class).insertOne(object);
        
        return counter;
    }

    public CountersServiceParams getParams() {
        return params;
    }

    public boolean hasInputObject(String result, String input) {
        Attribute inputAttribute = new Attribute().fromJson(input);
        Attribute outputAttribute = new Attribute().fromJson(result);

        return inputAttribute.getId().equalsIgnoreCase(outputAttribute.getId());
    }

    public void cleanup() {
        logger.info("Cleanup test data");
        client.getDatabase("ecommerce_test").getCollection("counters").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }


    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }

    public CountersService_0_0_1 getService() {
        return service;
    }
}