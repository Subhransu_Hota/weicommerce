/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.counters.defaults;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.ecom.commons.models.Counter;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public final class CountersServiceInput extends Input<Counter> {

//    @JsonUnwrapped
//    private String data;
    private String template;

    /**
     *
     */
    public CountersServiceInput() {
        this.init();
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, CountersServiceInput.class);
    }

    @Override
    public CountersServiceInput fromJson(String json) {
        return new Gson().fromJson(json, CountersServiceInput.class);
    }

    @Override
    public final void init() {
        this.setTemplate("");
    }

    /**
     *
     * @return gets template
     */
    public String getTemplate() {
        return template;
    }

    /**
     *
     * @param template set template
     */
    public void setTemplate(String template) {
        this.template = template;
    }
}
