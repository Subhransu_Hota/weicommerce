/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.address;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.address.defaults.AddressServiceInput;
import com.rokittech.container.ecom.address.defaults.AddressServiceOutput;
import com.rokittech.container.ecom.address.defaults.AddressServiceParams;
import com.rokittech.container.ecom.address.defaults.AddressVocabulary;
import com.rokittech.container.ecom.address.factory.AddressServiceFactory;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.container.ecom.commons.models.AddressType;
import com.rokittech.containers.api.core.Service;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);

        //This code is temp code of UserAddress for POC
        
        // Temp User Id 
        String userId ="560b7d980e8543233407bc2a";
        //This code is temp code of UserAddress for POC
        Address address = new Address();
        address.setStreet1("test");
        address.setStreet2("test2");
        address.setCity("Bangalore");
        address.setState("Karnataka");
        address.setCountry("India");
        address.setZipCode("560037");
        address.setAddressType(AddressType.Billing);
       // This is real mandatory code to move in controller
        AddressServiceParams params = new AddressServiceParams();
        //params.initEnv("env.properties");
        
        //MongoClient mongo = new MongoClient("weicommerce-backend-dev.elasticbeanstalk.com", 27017);
        MongoClient mongo = new MongoClient("localhost", 27017);
        MongoDatabase db = mongo.getDatabase("ecommerce");
        
        params.setEnv(db);
        params.getProperties().put(AddressVocabulary.COLLECTION.name(), "Address");
        params.getProperties().put(AddressVocabulary.COMMAND.name(), AddressVocabulary.INSERT.name());

        DefaultPersist persist = null;//new DefaultPersist();
        //persist.initEnv("env.properties");

//        CrudNoSqlServiceInput json = new CrudNoSqlServiceInput();
        //
        AddressServiceInput inputTemp = new AddressServiceInput();
        inputTemp.setData(address);
        inputTemp.setInfo(info);
        
        System.out.println (inputTemp.toJson());

//        CrudNoSqlService_0_0_1 service;        
        Service<AddressServiceInput, AddressServiceOutput, AddressServiceParams, DefaultPersist> service = new AddressServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        //service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new AddressServiceOutput());

        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
