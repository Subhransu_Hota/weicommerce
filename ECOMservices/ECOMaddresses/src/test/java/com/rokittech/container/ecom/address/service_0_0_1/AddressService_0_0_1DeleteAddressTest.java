/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.address.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.address.defaults.AddressServiceInput;
import com.rokittech.container.ecom.address.defaults.AddressServiceOutput;
import com.rokittech.container.ecom.address.defaults.AddressServiceParams;
import com.rokittech.container.ecom.address.defaults.AddressVocabulary;
import com.rokittech.container.ecom.address.utils.TestUtil;
import com.rokittech.container.ecom.commons.models.Address;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class AddressService_0_0_1DeleteAddressTest {

    private static final Logger logger = LoggerFactory.getLogger(AddressService_0_0_1DeleteAddressTest.class);

    public AddressService_0_0_1DeleteAddressTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        logger.info("Setup test resouces");
        
        AddressServiceInput input = new AddressServiceInput();
        Address address = TestUtil.getInstance().createNewAddressInDatabase(
                "test_address_" + new Random ().nextInt(Integer.MAX_VALUE));

        input.setData(address);
        input.setInfo(TestUtil.getInstance().createInfo());

        AddressServiceOutput output = new AddressServiceOutput();
        // This is real mandatory code to move in controller
        AddressServiceParams params = TestUtil.getInstance().getParams();

        params.getProperties().put(AddressVocabulary.COLLECTION.name(), "Address");
        params.getProperties().put(AddressVocabulary.COMMAND.name(), AddressVocabulary.DELETE.name());

        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }

    @After
    public void tearDown() {
        logger.info("Cleanup all test resouces");
    }

    /**
     * Test of getInputAsJson method, of class AddressService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        logger.info("Started Execution of getInputAsJson ");

        System.out.println("getInputAsJson");
        String result = TestUtil.getInstance().getService().getInputAsJson();
        AddressServiceInput input = new AddressServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);

        logger.info("Finished Execution of getInputAsJson ");
    }

    /**
     * Test of serve method, of class AddressService_0_0_1.
     */
    @Test
    public void testServe() {
        logger.info("Started Execution of serve ");

        System.out.println("serve");
        AddressServiceOutput result = TestUtil.getInstance().getService().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isAddressEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                result.getData()));
        logger.info("Finished Execution of serve ");
    }

    /**
     * Test of getOutputAsJson method, of class AddressService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        logger.info("Started Execution of getOutputAsJson ");

        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        AddressServiceOutput output = new AddressServiceOutput().fromJson(result);

        assertTrue("input and output are not same", TestUtil.getInstance().isAddressEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                output.getData()));

        logger.info("Finished Execution of getOutputAsJson ");
    }

    /**
     * Test of toJson method, of class AddressService_0_0_1.
     */
    @Test
    public void testToJson() {
        logger.info("Started Execution of toJson ");

        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
        logger.info("Finished Execution of toJson ");
    }
}
