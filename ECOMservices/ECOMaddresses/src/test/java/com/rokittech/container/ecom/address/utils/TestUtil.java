/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.address.utils;

import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.address.defaults.AddressServiceParams;
import com.rokittech.container.ecom.address.factory.AddressServiceFactory;
import com.rokittech.container.ecom.address.service_0_0_1.AddressService_0_0_1;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.net.InetSocketAddress;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class TestUtil {

    private static final Logger logger = LoggerFactory.getLogger(TestUtil.class);

    private static final AddressServiceParams params = new AddressServiceParams();
    private final AddressService_0_0_1 service;
    private MongoClient client;
    private MongoServer server;

    private TestUtil() {
        service = (AddressService_0_0_1) new AddressServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
        ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        logger.info("Mongo Db connection created succesfully");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }

    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public Address createAddress(String addressId) {
        Address address = new Address();
        address.setAddressId(addressId);
        address.setStreet1("test");
        address.setStreet2("test2");
        address.setCity("Bangalore");
        address.setState("Karnataka");
        address.setCountry("India");
        address.setZipCode("560037");

        return address;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);

        return info;
    }

    public boolean isAddressEqual(String input, String output) {
        logger.info("Input :" + input + "\n Output : " + output);

        Address inputAddress = new Address().fromJson(input);
        Address outputAddress = new Address().fromJson(output);

        boolean b = inputAddress.getCity().equalsIgnoreCase(outputAddress.getCity())
                && inputAddress.getState().equalsIgnoreCase(outputAddress.getState())
                && inputAddress.getAddressId().equalsIgnoreCase(outputAddress.getAddressId());

        return b;
    }

    public AddressServiceParams getParams() {
        return params;
    }

    public Address createNewAddressInDatabase(String addressId) {

        Address address = new Address();
        address.setStreet1("test");
        address.setStreet2("test2");
        address.setCity("Bangalore");
        address.setState("Karnataka");
        address.setCountry("India");
        address.setZipCode("560037");
        address.setAddressId(addressId);

        DBObject object = (DBObject) JSON.parse(address.toJson());
        object.put("is_deleted", false);

        client.getDatabase("ecommerce_test").getCollection("Address", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId) object.get("_id");
        object.put("_id", objId.toString());

        return new Address().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObject(String result, String input) {
        Address inputAddress = new Address ().fromJson(input);
        Address outputAddress = new Address ().fromJson(result);
        
        return inputAddress.getAddressId().equalsIgnoreCase(outputAddress.getAddressId());
    }

    public void cleanup() {
        logger.info("Cleanup test data");
        client.getDatabase("ecommerce_test").getCollection("Address").drop();
        client.getDatabase("ecommerce_test").getCollection("Product").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }

    public AddressService_0_0_1 getService() {
        return service;
    }
    
    
}
