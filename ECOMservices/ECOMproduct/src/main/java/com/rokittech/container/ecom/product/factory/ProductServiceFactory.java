package com.rokittech.container.ecom.product.factory;

import javax.inject.Inject;

import com.google.inject.Provider;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.base.defaults.Output;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.container.ecom.product.service_0_0_1.ProductService_0_0_1;
import com.rokittech.container.ecom.product.defaults.ProductServiceParams;
import com.rokittech.container.ecom.product.defaults.ProductVocabulary;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductServiceFactory implements Provider<Service<Input, Output, 
        ProductServiceParams, DefaultPersist>> {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(ProductServiceFactory.class);
    
    private Service service;

    /**
     * This is the place where you are managing different versions
     *
     * @param version
     */
//    @Inject
    public ProductServiceFactory(int version) {
        this(null, version);

    }

    public ProductServiceFactory(String appId, int version) {
        slf4jLogger.info ("Will create the instance of service based on appID: " + appId + " Version: " + version);
        if (appId != null) {
            ProductVocabulary app = ProductVocabulary.valueOf(appId.toUpperCase());
            switch (app) {
                case WEI_ECOMMERCE:
                    weiEcommerce(version);
                    break;
                case TEST_ECOMMERCE:
                    testEcommerce(version);
                    break;
                default:
                    weiEcommerce(version);
                    break;
            }
        } else {
            weiEcommerce(version);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Service get() {
        return service;
    }

    private void weiEcommerce(int version) {

        switch (version) {
            case 1:
                this.service = new ProductService_0_0_1();
            	
                break;
            case 2:

                break;
            default:

                break;

        }
    }
    
    private void testEcommerce(int version) {

        switch (version) {
            case 1:
                this.service = new ProductService_0_0_1(DBType.TEST);
            	
                break;
            case 2:

                break;
            default:

                break;

        }
    }
}
