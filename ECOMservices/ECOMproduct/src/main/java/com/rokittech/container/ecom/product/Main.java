/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.product;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.core.Money;
import com.rokittech.container.ecom.commons.models.Pricing;
import com.rokittech.container.ecom.commons.models.Product;
import com.rokittech.container.ecom.product.defaults.ProductServiceInput;
import com.rokittech.container.ecom.product.defaults.ProductServiceOutput;
import com.rokittech.container.ecom.product.defaults.ProductServiceParams;
import com.rokittech.container.ecom.product.defaults.ProductVocabulary;
import com.rokittech.container.ecom.product.factory.ProductServiceFactory;
import com.rokittech.containers.api.core.Service;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;



/**
 *
 * @author alexmylnikov
 */
public class Main {

    public static void main(String[] args) {
        
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        String date = new Date().toString();
        Product product = new Product ();
        product.setProductId("Product_1");
        product.setEndDate(date);
        Money money = new Money(new Locale("en", "IN"), 10.00);
        product.setLongDescr("Long description");
        product.setManufactureName("manufacturer name");
        product.setName("priduct name");
        product.setShortDescr("short description");
        product.setStartDate(date);
        product.setStockCount(10);
        product.setVisibleFlag(true);
        
        //
        Pricing price1 = new Pricing();
        price1.setMoney(money);
        price1.setPricingFromDate(date);
        price1.setPricingToDate(date);
        price1.setPricingId("Price 1");
        price1.setPricingPurposeType(Pricing.PricingPurposeType.PURCHASE);
        price1.setPricingType(Pricing.PricingType.PROMO_PRICE);
        
        Pricing price2 = new Pricing().fromJson(price1.toJson());
        price2.setPricingId("Price 2");
        product.setPricingList(Arrays.asList(price1, price2));
        
        // This is real mandatory code to move in controller
        ProductServiceParams params = new ProductServiceParams();
        
        params.getProperties().put(ProductVocabulary.COLLECTION.name(), "Product");
        params.getProperties().put(ProductVocabulary.COMMAND.name(), ProductVocabulary.INSERT.name());

        DefaultPersist persist = new DefaultPersist();
        ProductServiceInput inputTemp = new ProductServiceInput();
        inputTemp.setData(product);
        inputTemp.setInfo(info);
      
        System.out.println ("***" + inputTemp.toJson());
        Service<ProductServiceInput, ProductServiceOutput, ProductServiceParams, DefaultPersist> service = new ProductServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new ProductServiceOutput());

        System.out.println(service.serve().getData());
         
        params.getProperties().put(ProductVocabulary.COMMAND.name(), ProductVocabulary.RETRIEVE.name());
        Product product1 = new Product ();
        product1.setProductId("product_1");
        
        inputTemp.setData(product1);
               
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        
        System.out.println(service.getOutput().getData());
    }

}
