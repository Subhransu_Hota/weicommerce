/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.product;
import com.rokittech.container.ecom.product.service_0_0_1.ProductService_0_0_1DeleteProductTest;
import com.rokittech.container.ecom.product.service_0_0_1.ProductService_0_0_1FindByIdProductTest;
import com.rokittech.container.ecom.product.service_0_0_1.ProductService_0_0_1InsertProductTest;
import com.rokittech.container.ecom.product.service_0_0_1.ProductService_0_0_1UpdateProductTest;
import com.rokittech.container.ecom.product.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author sangamesh
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    ProductService_0_0_1InsertProductTest.class,
    ProductService_0_0_1UpdateProductTest.class,
    ProductService_0_0_1FindByIdProductTest.class,
    ProductService_0_0_1DeleteProductTest.class
})

public class ProductTestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Initiated Mongo database");

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Dropping tables... ");
        TestUtil.getInstance().cleanup ();
        
    }
}
