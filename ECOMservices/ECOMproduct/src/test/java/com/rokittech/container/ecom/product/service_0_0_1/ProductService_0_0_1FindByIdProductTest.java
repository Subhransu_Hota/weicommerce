/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.product.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.product.defaults.ProductServiceInput;
import com.rokittech.container.ecom.product.defaults.ProductServiceOutput;
import com.rokittech.container.ecom.product.defaults.ProductServiceParams;
import com.rokittech.container.ecom.product.defaults.ProductVocabulary;
import com.rokittech.container.ecom.product.factory.ProductServiceFactory;
import com.rokittech.container.ecom.product.utils.TestUtil;
import com.rokittech.container.ecom.commons.models.Product;
import com.rokittech.containers.api.core.Service;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sangamesh
 */
public class ProductService_0_0_1FindByIdProductTest {
    
    public ProductService_0_0_1FindByIdProductTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ProductServiceInput input = new ProductServiceInput();
        Product product = TestUtil.getInstance ().createNewProductInDatabase (
                "test_product_" + new Random ().nextInt(Integer.MAX_VALUE));
        
        input.setData(product);
        input.setInfo(TestUtil.getInstance().createInfo());
        
        ProductServiceOutput output = new ProductServiceOutput();
        // This is real mandatory code to move in controller
        ProductServiceParams params = TestUtil.getInstance().getParams ();
       
        params.getProperties().put(ProductVocabulary.COLLECTION.name(), "Product");
        params.getProperties().put(ProductVocabulary.COMMAND.name(), ProductVocabulary.RETRIEVE.name());
        
        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInputAsJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        System.out.println("getInputAsJson");
        String result = TestUtil.getInstance().getService().getInputAsJson();
        ProductServiceInput input = new ProductServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
    }

    /**
     * Test of serve method, of class CatalogService_0_0_1.
     */
    @Test
    public void testServe() {
        System.out.println("serve");
        ProductServiceOutput result = TestUtil.getInstance().getService().serve();
        
        assertNotEquals("Result does not have object ", 
                TestUtil.getInstance().hasInputObject (result.getData(), TestUtil.getInstance().getService().getInput().getData().toJson()));
    }

    /**
     * Test of getOutputAsJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        System.out.println("getOutputAsJson");
        
        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        ProductServiceOutput output = new ProductServiceOutput().fromJson(result);
        
        assertNotEquals("Result does not have object ", 
                TestUtil.getInstance().hasInputObject (output.getData(), TestUtil.getInstance().getService().getInput().getData().toJson()));
    }
    
    /**
     * Test of toJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testToJson() {
        System.out.println("toJson");
        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
    }
}
