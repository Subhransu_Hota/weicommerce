/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.product.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.product.defaults.ProductServiceParams;
import com.rokittech.container.ecom.commons.core.Money;
import com.rokittech.container.ecom.commons.models.Pricing;
import com.rokittech.container.ecom.commons.models.Product;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import com.rokittech.container.ecom.product.factory.ProductServiceFactory;
import com.rokittech.container.ecom.product.service_0_0_1.ProductService_0_0_1;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.bson.types.ObjectId;

/**
 *
 * @author sangamesh
 */
public class TestUtil {
    private final ProductService_0_0_1 service;
    private static final ProductServiceParams params = new ProductServiceParams();
    private MongoClient client;
    private MongoServer server;
    
    private TestUtil() {
        service = (ProductService_0_0_1) new ProductServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
        ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        //params.initEnv("env.properties");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }
    
    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

 
    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        return info;
    }

   

    public ProductServiceParams getParams() {
        return params;
    }


    public boolean hasInputObject(String result, String input) {
        Product inputProduct = new Product ().fromJson(input);
        Product outputProduct = new Product ().fromJson(result);
        
        return inputProduct.getId().equalsIgnoreCase(outputProduct.getId());
    }

    
    public boolean hasInputObjectInList(String result, String input) {
        Product inputCatalog = new Product ().fromJson(input);
        
        Type listOfTestObject = new TypeToken<List<Product>>(){}.getType();
        List<Product> list = new Gson().fromJson(result, listOfTestObject);
        
        boolean flag = false;
        for (Product Product : list) {
            if (Product.getId().equalsIgnoreCase(inputCatalog.getId())){
                flag = true;
                break;
            }
        }
        
        return flag;
    }
    
    public Product createProduct(String string) {
        Product product = new Product ();
        product.setProductId(string);
        product.setEndDate(new Date ().toString());
        Money listPrice = new Money(new Locale("en", "IN"), 10.00);
        Pricing price1 = new Pricing();
        price1.setMoney(listPrice);
        price1.setPricingId("pricing_id_1");
        price1.setPricingPurposeType(Pricing.PricingPurposeType.PURCHASE);
        price1.setPricingToDate(new Date ().toString());
        price1.setPricingFromDate(new Date ().toString());
        product.setPricingList(new ArrayList<>(Arrays.asList(price1)));
        product.setLongDescr("Long description");
        product.setManufactureName("manufacturer name");
        product.setName("priduct name");
        product.setShortDescr("short description");
        product.setStartDate(new Date ().toString());
        product.setStockCount(10);
        product.setVisibleFlag(true);
        
        return product;
    }

    public boolean isProductEqual(String input, String output) {
        System.out.println ("input: " + input);
        System.out.println("output: " + output);
        
        Product inputProduct = new Product ().fromJson(input);
        Product outputProduct = new Product ().fromJson(output);
        
        boolean b =  inputProduct.getProductId().equalsIgnoreCase(outputProduct.getProductId());
        
        return b;
    }

    public Product createNewProductInDatabase(String string) {
        Product product = new Product ();
        product.setProductId(string);
        product.setEndDate(new Date ().toString());
        Money listPrice = new Money(new Locale("en", "IN"), 10.00);
        Pricing price1 = new Pricing();
        price1.setMoney(listPrice);
        price1.setPricingId("pricing_id_1");
        price1.setPricingPurposeType(Pricing.PricingPurposeType.PURCHASE);
        price1.setPricingToDate(new Date ().toString());
        price1.setPricingFromDate(new Date ().toString());
        product.setPricingList(new ArrayList<>(Arrays.asList(price1)));
        product.setLongDescr("Long description");
        product.setManufactureName("manufacturer name");
        product.setName("priduct name");
        product.setShortDescr("short description");
        product.setStartDate(new Date ().toString());
        product.setStockCount(10);
        product.setVisibleFlag(true);
        
        DBObject object = (DBObject) JSON.parse(product.toJson());
        object.put("is_deleted", false);
        
        client.getDatabase("ecommerce_test").getCollection("Catalog", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId)object.get("_id");
        object.put("_id", objId.toString());
        
        return new Product ().fromJson(JSON.serialize(object));
    
    }

    public void cleanup() {
        client.getDatabase("ecommerce_test").getCollection("Catalog").drop();
        client.getDatabase("ecommerce_test").getCollection("Product").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    public ProductService_0_0_1 getService() {
        return service;
    }
    
    
    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }
}
