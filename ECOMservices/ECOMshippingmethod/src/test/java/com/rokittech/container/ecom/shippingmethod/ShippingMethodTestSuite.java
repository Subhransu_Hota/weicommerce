package com.rokittech.container.ecom.shippingmethod;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.rokittech.container.ecom.shippingmethod.service_0_0_1.ShippingMethodService_0_0_1DeleteAttributeTest;
import com.rokittech.container.ecom.shippingmethod.service_0_0_1.ShippingMethodService_0_0_1FindByIdAttributeTest;
import com.rokittech.container.ecom.shippingmethod.service_0_0_1.ShippingMethodService_0_0_1InsertAttributeTest;
import com.rokittech.container.ecom.shippingmethod.service_0_0_1.ShippingMethodService_0_0_1UpdateAttributeTest;
import com.rokittech.container.ecom.shippingmethod.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Sanga
 * @CreatedDate 28th Sep 2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    ShippingMethodService_0_0_1InsertAttributeTest.class,
    ShippingMethodService_0_0_1UpdateAttributeTest.class,
    ShippingMethodService_0_0_1DeleteAttributeTest.class,
    ShippingMethodService_0_0_1FindByIdAttributeTest.class,})
public class ShippingMethodTestSuite {

    private static final Logger logger = LoggerFactory.getLogger(ShippingMethodTestSuite.class);

    @BeforeClass
    public static void setUpClass() throws Exception {

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        logger.info("Initiated Mongo database");

        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        logger.info("Cleanup Mongo database");

        TestUtil.getInstance().cleanup();

    }

}
