/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.shippingmethod.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.models.ShippingMethod;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodServiceInput;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodServiceOutput;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodServiceParams;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodVocabulary;
import com.rokittech.container.ecom.shippingmethod.utils.TestUtil;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Sanga
 * @CreatedDate 28th Sep 2015
 */
public class ShippingMethodService_0_0_1FindByIdAttributeTest {

    private static final Logger logger = LoggerFactory.getLogger(ShippingMethodService_0_0_1FindByIdAttributeTest.class);
    public ShippingMethodService_0_0_1FindByIdAttributeTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        logger.info("Setup test resouces");
        ShippingMethodServiceInput input = new ShippingMethodServiceInput();
        ShippingMethod address = TestUtil.getInstance().createNewShippingMethodInDatabase(
        "test_address_" + new Random ().nextInt(Integer.MAX_VALUE));

        input.setData(address);
        input.setInfo(TestUtil.getInstance().createInfo());

        ShippingMethodServiceOutput output = new ShippingMethodServiceOutput();
        // This is real mandatory code to move in controller
        ShippingMethodServiceParams params = TestUtil.getInstance().getParams();

        params.getProperties().put(ShippingMethodVocabulary.COLLECTION.name(), "Attribute");
        params.getProperties().put(ShippingMethodVocabulary.COMMAND.name(), ShippingMethodVocabulary.RETRIEVE.name());

        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }

    @After
    public void tearDown() {
        logger.info("Cleanup all test resouces");
    }

    /**
     * Test of getInputAsJson method, of class AttributeService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {

        logger.info("Started Execution of getInputAsJson ");

        String result = TestUtil.getInstance().getService().getInputAsJson();
        ShippingMethodServiceInput input = new ShippingMethodServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);

        logger.info("Finished Execution of getInputAsJson ");

    }

    /**
     * Test of serve method, of class AttributeService_0_0_1.
     */
    @Test
    public void testServe() {

        logger.info("Started Execution of serve ");

        ShippingMethodServiceOutput result = TestUtil.getInstance().getService().serve();

        assertNotEquals("Result does not have object ",
                TestUtil.getInstance().hasInputObject(result.getData(), TestUtil.getInstance().getService().getInput().getData().toJson()));

        logger.info("Finished Execution of serve ");

    }

    /**
     * Test of getOutputAsJson method, of class AttributeService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {

        logger.info("Started Execution of getOutputAsJson ");

        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        ShippingMethodServiceOutput output = new ShippingMethodServiceOutput().fromJson(result);

        assertNotEquals("Result does not have object ",
                TestUtil.getInstance().hasInputObject(output.getData(), TestUtil.getInstance().getService().getInput().getData().toJson()));

        logger.info("Finished Execution of getOutputAsJson ");

    }

    /**
     * Test of toJson method, of class AttributeService_0_0_1.
     */
    @Test
    public void testToJson() {

        logger.info("Started Execution of toJson ");

        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);

        logger.info("Finished Execution of toJson ");

    }
}
