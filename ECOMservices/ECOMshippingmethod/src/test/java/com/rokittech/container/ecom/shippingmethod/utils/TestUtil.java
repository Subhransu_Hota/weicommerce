/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.shippingmethod.utils;

import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.ShippingMethodCalcType;
import com.rokittech.container.ecom.commons.models.ShippingMethodItemType;
import com.rokittech.container.ecom.commons.models.ShippingMethod;
import com.rokittech.container.ecom.commons.models.ShippingMethodZip;
import com.rokittech.container.ecom.shippingmethod.defaults.ShippingMethodServiceParams;
import com.rokittech.container.ecom.shippingmethod.factory.ShippingMethodServiceFactory;
import com.rokittech.container.ecom.shippingmethod.service_0_0_1.ShippingMethodService_0_0_1;

import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.List;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Sanga
 * @CreatedDate 28th Sep 2015
 */
public class TestUtil {

    private static final Logger logger = LoggerFactory.getLogger(TestUtil.class);

    private static final ShippingMethodServiceParams params = new ShippingMethodServiceParams();
    private final ShippingMethodService_0_0_1 service;
    private MongoClient client;
    private MongoServer server;

    private TestUtil() {
        service = (ShippingMethodService_0_0_1) new ShippingMethodServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAttribute = server.bind();
        client = new MongoClient(new ServerAddress(serverAttribute));

        //Initiate Mongodb
        ((MongoManager) service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        logger.info("Mongo Db connection created succesfully");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }

    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public ShippingMethod createShippingMethod(String userId) {
        ShippingMethod sm = new ShippingMethod();
        
        sm.setName("sm_1");
        sm.setDescription("Shipping method 1");
        List<ShippingMethodItemType> it = new ArrayList <> ();
        it.add(ShippingMethodItemType.DEFAULT);
        sm.setItemTypes(it);
        sm.setShippingCalcType(ShippingMethodCalcType.FLAT_PERCENT);
        sm.setShippingCalcValue(10);
        sm.setTracking_url("sample_url");
        List<ShippingMethodZip> zones = new ArrayList<>();
        ShippingMethodZip zip = new ShippingMethodZip();
        zip.setAreas(new ArrayList<>(asList ("Area 1")));
        zip.setName("Area 1");
        zip.setZipCode("000000");
        zones.add(zip);
        sm.setZones(zones);
        
        return sm;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);

        return info;
    }

    public boolean isShippingMethodEqual(String input, String output) {
        logger.info("Input :" + input + "\n Output : " + output);

        ShippingMethod inputAttribute = new ShippingMethod().fromJson(input);
        ShippingMethod outputAttribute = new ShippingMethod().fromJson(output);

        return inputAttribute.getName().equalsIgnoreCase(outputAttribute.getName());
    }

    public ShippingMethodServiceParams getParams() {
        return params;
    }

    public ShippingMethod createNewShippingMethodInDatabase(String userId) {

        ShippingMethod sm = new ShippingMethod();

        sm.setName("sm_1");
        sm.setDescription("Shipping method 1");
        List<ShippingMethodItemType> it = new ArrayList <> ();
        it.add(ShippingMethodItemType.DEFAULT);
        sm.setItemTypes(it);
        sm.setShippingCalcType(ShippingMethodCalcType.FLAT_PERCENT);
        sm.setShippingCalcValue(10);
        sm.setTracking_url("sample_url");
        List<ShippingMethodZip> zones = new ArrayList<>();
        ShippingMethodZip zip = new ShippingMethodZip();
        zip.setAreas(new ArrayList<>(asList ("Area 1")));
        zip.setName("Area 1");
        zip.setZipCode("000000");
        zones.add(zip);
        sm.setZones(zones);
        
        DBObject object = (DBObject) JSON.parse(sm.toJson());

        client.getDatabase("ecommerce_test").getCollection("Attribute", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId) object.get("_id");
        object.put("_id", objId.toString());

        return new ShippingMethod().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObject(String result, String input) {
        ShippingMethod inputAttribute = new ShippingMethod().fromJson(input);
        ShippingMethod outputAttribute = new ShippingMethod().fromJson(result);

        return inputAttribute.getId().equalsIgnoreCase(outputAttribute.getId());
    }

    public void cleanup() {
        logger.info("Cleanup test data");
        client.getDatabase("ecommerce_test").getCollection("Attribute").drop();
        client.getDatabase("ecommerce_test").getCollection("Product").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }

    public ShippingMethodService_0_0_1 getService() {
        return service;
    }
}