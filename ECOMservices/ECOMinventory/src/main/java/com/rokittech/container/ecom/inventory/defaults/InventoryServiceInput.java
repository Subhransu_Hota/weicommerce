/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.inventory.defaults;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.ecom.commons.models.Inventory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author asif
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public final class InventoryServiceInput extends Input<Inventory> {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(InventoryServiceInput.class);
    /**
     *
     */
    public InventoryServiceInput() { 
        slf4jLogger.info("inside constructor");
        this.init();
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, InventoryServiceInput.class);
    }

    @Override
    public InventoryServiceInput fromJson(String json) {
        return new Gson().fromJson(json, InventoryServiceInput.class);
    }

    @Override
    public final void init() {
    }
}
