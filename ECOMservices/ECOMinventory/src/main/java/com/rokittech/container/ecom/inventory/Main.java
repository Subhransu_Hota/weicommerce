/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.inventory;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceInput;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceOutput;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceParams;
import com.rokittech.container.ecom.inventory.defaults.InventoryVocabulary;
import com.rokittech.container.ecom.inventory.factory.InventoryServiceFactory;
import com.rokittech.container.ecom.commons.models.Inventory;
import com.rokittech.containers.api.core.Service;
import java.util.HashMap;
import java.util.Map;



/**
 *
 * @author alexmylnikov
 */
public class Main {

    public static void main(String[] args) {
        
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        Inventory inventory = new Inventory ();
        inventory.setSellerId("123");
        inventory.setProductId("XYZAPIR12");
        inventory.setQuantity(100);
        
        System.out.println(inventory.toJson());
        
        // This is real mandatory code to move in controller
        InventoryServiceParams params = new InventoryServiceParams();
        
//        MongoClient mongo = new MongoClient("weicommerce-backend-dev.elasticbeanstalk.com", 27017);
//        MongoDatabase db = mongo.getDatabase("ecommerce");
//        
//        //params.initEnv("env.properties");
//        params.setEnv(db);
        params.getProperties().put(InventoryVocabulary.COLLECTION.name(), "Inventory");
        params.getProperties().put(InventoryVocabulary.COMMAND.name(), InventoryVocabulary.INSERT.name());

        DefaultPersist persist = new DefaultPersist();
        InventoryServiceInput inputTemp = new InventoryServiceInput();
        inputTemp.setData(inventory);
        inputTemp.setInfo(info);
      
        System.out.println ("***" + inputTemp.toJson());
        Service<InventoryServiceInput, InventoryServiceOutput, InventoryServiceParams, DefaultPersist> service = new InventoryServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new InventoryServiceOutput());

        System.out.println(service.serve().getData());
         
        params.getProperties().put(InventoryVocabulary.COMMAND.name(), InventoryVocabulary.RETRIEVE.name());
        Inventory inventory1 = new Inventory ();
        inputTemp.setData(inventory1);
               
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        
        System.out.println(service.getOutput().getData());
    }

}
