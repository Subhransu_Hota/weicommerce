/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.inventory.service_0_0_1;

import com.google.gson.Gson;
import com.rokittech.container.base.defaults.DefaultPersist;

import com.rokittech.container.ecom.commons.core.AbstractService;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterManager;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.container.ecom.dbadapter.factory.DBManagerFactory;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceInput;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceOutput;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceParams;
import com.rokittech.container.ecom.inventory.defaults.InventoryVocabulary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author asif
 */
public class InventoryService_0_0_1 extends AbstractService<InventoryServiceInput, InventoryServiceOutput, InventoryServiceParams, DefaultPersist> {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(InventoryService_0_0_1.class);
    private InventoryServiceInput input;
    private InventoryServiceOutput output;
    private InventoryServiceParams params;
    private final DBAdapterManager dbclientManager;
        
    public InventoryService_0_0_1() {
        dbclientManager = new DBManagerFactory().get();
    }
    
    public InventoryService_0_0_1(DBType dbType) {
        dbclientManager = new DBManagerFactory(dbType).get();
    }

    public DBAdapterManager getDbclientManager() {
        return dbclientManager;
    }
   
    @Override
    public String serviceId() {
        return this.getClass().getName();
    }

    @Override
    public String getInputAsJson() {
        slf4jLogger.info("returning Input Object in the form of JSON");
        
        return input.toJson();
    }

    @Override
    public String getOutputAsJson() {
        slf4jLogger.info("returning Output Object in the form of JSON");
        return output.toJson();
    }

    @Override
    public void setInputAsJson(String input) {
        slf4jLogger.info("Recreating Input object from json");
        this.input = this.input.fromJson(input);
    }

    @Override
    public void setOutputAsJson(String output) {
        slf4jLogger.info("Recreating Ouput object from json");
        this.output = this.output.fromJson(output);
    }

    @Override
    public InventoryServiceOutput serve() {
        slf4jLogger.info("Inside serve() ");
        DBHandlerInput serviceinput = new DBHandlerInput();
        serviceinput.setData(this.getInput().getData().toJson());
        
        DBHandlerOutput serviceoutput = new DBHandlerOutput();
        // This is real mandatory code to move in controller
        DBHandlertParams serviceparams = new DBHandlertParams();
        serviceparams.setCommand(getParams().getProperties().getProperty(InventoryVocabulary.COMMAND.name()));
        serviceparams.setEntity(params.getProperties().getProperty(InventoryVocabulary.COLLECTION.name()));
        
        dbclientManager.setInput(serviceinput);
        dbclientManager.setOutput(serviceoutput);
        dbclientManager.setParams(serviceparams);
        dbclientManager.setPersist(new DefaultPersist());
        
        DBHandlerOutput serviceOutput = (DBHandlerOutput) dbclientManager.serve();
      
        output.setData(serviceOutput.getData());
        output.setInfo(input.getInfo());

        slf4jLogger.info("Created output object " + output.getClass());
        
        return output;
    }

    @Override
    public String toJson() {
        slf4jLogger.info("generating json for service object");
        return new Gson().toJson(this, InventoryService_0_0_1.class);
    }

    @Override
    public InventoryServiceInput getInput() {
        return input;
    }

    @Override
    public void setInput(InventoryServiceInput input) {
        this.input = input;
    }

    @Override
    public InventoryServiceOutput getOutput() {
        return output;
    }

    @Override
    public void setOutput(InventoryServiceOutput output) {
        this.output = output;
    }

    @Override
    public InventoryServiceParams getParams() {
        return params;
    }

    @Override
    public void setParams(InventoryServiceParams params) {
        this.params = params;
    }
}
