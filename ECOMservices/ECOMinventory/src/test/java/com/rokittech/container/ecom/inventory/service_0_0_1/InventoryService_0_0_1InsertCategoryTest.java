/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.inventory.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceInput;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceOutput;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceParams;
import com.rokittech.container.ecom.inventory.defaults.InventoryVocabulary;
import com.rokittech.container.ecom.inventory.utils.TestUtil;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author asif
 */
public class InventoryService_0_0_1InsertCategoryTest {
    public InventoryService_0_0_1InsertCategoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        InventoryServiceInput input = new InventoryServiceInput();
        input.setData(TestUtil.getInstance ().createInventory (
                "test_category_" + new Random ().nextInt(Integer.MAX_VALUE)));
        input.setInfo(TestUtil.getInstance().createInfo());
        
        InventoryServiceOutput output = new InventoryServiceOutput();
        // This is real mandatory code to move in controller
        InventoryServiceParams params = TestUtil.getInstance().getParams ();
       
        params.getProperties().put(InventoryVocabulary.COLLECTION.name(), "Inventory");
        params.getProperties().put(InventoryVocabulary.COMMAND.name(), InventoryVocabulary.INSERT.name());
        
        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInputAsJson method, of class InventoryService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        System.out.println("getInputAsJson");
        String result = TestUtil.getInstance().getService().getInputAsJson();
        InventoryServiceInput input = new InventoryServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
    }

    /**
     * Test of serve method, of class InventoryService_0_0_1.
     */
    @Test
    public void testServe() {
        System.out.println("serve");
        InventoryServiceOutput result = TestUtil.getInstance().getService().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isInventoryEqual (
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                result.getData()));
    }

    /**
     * Test of getOutputAsJson method, of class InventoryService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        System.out.println("getOutputAsJson");
        
        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        InventoryServiceOutput output = new InventoryServiceOutput().fromJson(result);
        
        assertTrue("input and output are not same", TestUtil.getInstance().isInventoryEqual (
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                output.getData()));
    }
    
    /**
     * Test of toJson method, of class InventoryService_0_0_1.
     */
    @Test
    public void testToJson() {
        System.out.println("toJson");
        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
    }
}
