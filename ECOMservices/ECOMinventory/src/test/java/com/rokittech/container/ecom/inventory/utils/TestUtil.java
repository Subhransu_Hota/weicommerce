/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.inventory.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Inventory;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import com.rokittech.container.ecom.inventory.defaults.InventoryServiceParams;
import com.rokittech.container.ecom.inventory.factory.InventoryServiceFactory;
import com.rokittech.container.ecom.inventory.service_0_0_1.InventoryService_0_0_1;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bson.types.ObjectId;

/**
 *
 * @author asif
 */
public class TestUtil {
    private static final InventoryServiceParams params = new InventoryServiceParams();
    private final InventoryService_0_0_1 service;
    
    private MongoClient client;
    private MongoServer server;
    
    private TestUtil() {
        service = (InventoryService_0_0_1) new InventoryServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
        ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
    }

    public InventoryService_0_0_1 getService() {
        return service;
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }
    
    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public Inventory createInventory(String inventoryId) {
        return getInventory();
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        return info;
    }

    public boolean isInventoryEqual(String input, String output) {
        System.out.println ("input: " + input);
        System.out.println("output: " + output);
        
        Inventory inputInventory = new Inventory ().fromJson(input);
        Inventory outputInventory = new Inventory ().fromJson(output);
        
        boolean b =  inputInventory.getSellerId().equalsIgnoreCase(outputInventory.getSellerId()) &&
                        inputInventory.getProductId().equalsIgnoreCase(outputInventory.getProductId()) &&
                        inputInventory.getQuantity() == outputInventory.getQuantity();
        
        return b;
    }

    public InventoryServiceParams getParams() {
        return params;
    }

    public Inventory createNewInventoryInDatabase(String inventoryId) {
        DBObject object = (DBObject) JSON.parse(getInventory().toJson());
        object.put("is_deleted", false);
        
        client.getDatabase("ecommerce_test").getCollection("Inventory", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId)object.get("_id");
        object.put("_id", objId.toString());
        
        return new Inventory ().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObjectInList(String result, String input) {
        Inventory inputInventory = new Inventory ().fromJson(input);
        
        Type listOfTestObject = new TypeToken<List<Inventory>>(){}.getType();
        List<Inventory> list = new Gson().fromJson(result, listOfTestObject);
        
        boolean flag = false;
        for (Inventory catalog : list) {
            if (catalog.getId().equalsIgnoreCase(inputInventory.getId())){
                flag = true;
                break;
            }
        }
        
        return flag;
    }
    
    public boolean hasInputObject(String result, String input) {
        Inventory inputInventory = new Inventory ().fromJson(input);
        Inventory outputInventory = new Inventory ().fromJson(result);
        
        return inputInventory.getId().equalsIgnoreCase(outputInventory.getId());
    }

    public void cleanup() {
        client.getDatabase("ecommerce_test").getCollection("Inventory").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }
    
    
    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }

    private Inventory getInventory() {
        Inventory inventory = new Inventory ();
        inventory.setSellerId("123");
        inventory.setProductId("XYZAPIR12");
        inventory.setQuantity(100);

        return inventory;
    }
}
