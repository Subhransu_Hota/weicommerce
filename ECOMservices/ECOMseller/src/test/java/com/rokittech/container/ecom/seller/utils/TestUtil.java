/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.seller.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.container.ecom.commons.models.AddressType;
import com.rokittech.container.ecom.commons.models.Seller;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.commons.models.UserCredentials;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import com.rokittech.container.ecom.seller.defaults.SellerServiceParams;
import com.rokittech.container.ecom.seller.factory.SellerServiceFactory;
import com.rokittech.container.ecom.seller.service_0_0_1.SellerService_0_0_1;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.bson.types.ObjectId;

/**
 *
 * @author sangamesh
 */
public class TestUtil {
    private static final SellerServiceParams params = new SellerServiceParams();
    private final SellerService_0_0_1 service;
    
    private MongoClient client;
    private MongoServer server;
    
    private TestUtil() {
        service = (SellerService_0_0_1) new SellerServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
        ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
    }

    public SellerService_0_0_1 getService() {
        return service;
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }
    
    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public Seller createSeller(String sellerId) {
        Seller seller = new Seller ();
        seller.setName(sellerId);
        seller.setDescription("descr");
        seller.setOrganizationName("Samsung India Pvt Ltd");
        seller.setTanNo("TAT787545");
        seller.setPanNo("EWIPS6476");
        seller.setTinNo("TIN67856");
        seller.setCreatedDate(new Date());
        seller.setPhoneNo("9845555760");
        
        return seller;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        return info;
    }

    public boolean isSellerEqual(String input, String output) {
        System.out.println ("input: " + input);
        System.out.println("output: " + output);
        
        Seller inputSeller = new Seller ().fromJson(input);
        Seller outputSeller = new Seller ().fromJson(output);
        
        boolean b =  inputSeller.getName().equalsIgnoreCase(outputSeller.getName()) &&
                inputSeller.getDescription().equalsIgnoreCase(outputSeller.getDescription()) &&
                inputSeller.getOrganizationName().equalsIgnoreCase(outputSeller.getOrganizationName()) &&
                inputSeller.getPhoneNo().equalsIgnoreCase(outputSeller.getPhoneNo());        
        return b;
    }

    public SellerServiceParams getParams() {
        return params;
    }

    public Seller createNewSellerInDatabase (String sellerId) {
        Address address = new Address();
        address.setAddressName("sweet home");
        address.setAddressType(AddressType.Billing_And_Shipping);
        address.setCity("Bangalore");
        address.setCreatedDate(new Date());
        
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setUserName("asif");
        userCredentials.setPassword("#123");
        userCredentials.setEmail("email");
        
        User user = new User();
        user.setFirstName("Asif");
        user.setLastName("Ahammed");
        user.setBillingAddress(Arrays.asList(address));
        user.setShippingAddress(Arrays.asList(address));
        user.setUserCredentials(userCredentials);
        
        Seller seller = new Seller();
        seller.setName("Samsung");
        seller.setDescription("descr");
        seller.setOrganizationName("Samsung India Pvt Ltd");
        seller.setTanNo("TAN787545");
        seller.setPanNo("EWIPS6476");
        seller.setTinNo("TIN67856");
        seller.setCreatedDate(new Date());
        seller.setPhoneNo("9845555760");
        seller.setUser(user);
        DBObject object = (DBObject) JSON.parse(seller.toJson());
        object.put("is_deleted", false);
        
        client.getDatabase("ecommerce_test").getCollection("Seller", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId)object.get("_id");
        object.put("_id", objId.toString());
        
        return new Seller ().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObjectInList(String result, String input) {
        Seller inputSeller = new Seller ().fromJson(input);
        
        Type listOfTestObject = new TypeToken<List<Seller>>(){}.getType();
        List<Seller> list = new Gson().fromJson(result, listOfTestObject);
        
        boolean flag = false;
        for (Seller seller : list) {
            if (seller.getId().equalsIgnoreCase(inputSeller.getId())){
                flag = true;
                break;
            }
        }
        
        return flag;
    }
    
    public boolean hasInputObject(String result, String input) {
        Seller inputSeller = new Seller ().fromJson(input);
        Seller outputSeller = new Seller ().fromJson(result);
        
        return inputSeller.getId().equalsIgnoreCase(outputSeller.getId());
    }

    public void cleanup() {
        client.getDatabase("ecommerce_test").getCollection("Seller").drop();
        client.getDatabase("ecommerce_test").getCollection("Product").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }
    
    
    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }
}
