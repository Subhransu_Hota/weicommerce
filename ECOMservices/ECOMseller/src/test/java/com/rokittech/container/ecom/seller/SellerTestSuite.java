/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.seller;

import com.rokittech.container.ecom.seller.service_0_0_1.SellerService_0_0_1DeleteSellerTest;
import com.rokittech.container.ecom.seller.service_0_0_1.SellerService_0_0_1FindByIdSellerTest;
import com.rokittech.container.ecom.seller.service_0_0_1.SellerService_0_0_1InsertSellerTest;
import com.rokittech.container.ecom.seller.service_0_0_1.SellerService_0_0_1ListSellerTest;
import com.rokittech.container.ecom.seller.service_0_0_1.SellerService_0_0_1UpdateSellerTest;
import com.rokittech.container.ecom.seller.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author sangamesh
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    SellerService_0_0_1InsertSellerTest.class,
    SellerService_0_0_1UpdateSellerTest.class,
    SellerService_0_0_1DeleteSellerTest.class,
    SellerService_0_0_1FindByIdSellerTest.class,
    SellerService_0_0_1ListSellerTest.class
})

public class SellerTestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Initiated Mongo database");

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Dropping tables... ");
        TestUtil.getInstance().cleanup ();
        
    }
}
