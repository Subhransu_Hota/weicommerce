/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.seller.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.seller.defaults.SellerServiceInput;
import com.rokittech.container.ecom.seller.defaults.SellerServiceOutput;
import com.rokittech.container.ecom.seller.defaults.SellerServiceParams;
import com.rokittech.container.ecom.seller.defaults.SellerVocabulary;
import com.rokittech.container.ecom.seller.utils.TestUtil;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sangamesh
 */
public class SellerService_0_0_1InsertSellerTest {
    public SellerService_0_0_1InsertSellerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        SellerServiceInput input = new SellerServiceInput();
        input.setData(TestUtil.getInstance ().createSeller (
                "test_seller_" + new Random ().nextInt(Integer.MAX_VALUE)));
        input.setInfo(TestUtil.getInstance().createInfo());
        
        SellerServiceOutput output = new SellerServiceOutput();
        // This is real mandatory code to move in controller
        SellerServiceParams params = TestUtil.getInstance().getParams ();
       
        params.getProperties().put(SellerVocabulary.COLLECTION.name(), "Seller");
        params.getProperties().put(SellerVocabulary.COMMAND.name(), SellerVocabulary.INSERT.name());
        
        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInputAsJson method, of class SellerService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        System.out.println("getInputAsJson");
        String result = TestUtil.getInstance().getService().getInputAsJson();
        SellerServiceInput input = new SellerServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
    }

    /**
     * Test of serve method, of class SellerService_0_0_1.
     */
    @Test
    public void testServe() {
        System.out.println("serve");
        SellerServiceOutput result = TestUtil.getInstance().getService().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isSellerEqual (
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                result.getData()));
    }

    /**
     * Test of getOutputAsJson method, of class SellerService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        System.out.println("getOutputAsJson");
        
        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        SellerServiceOutput output = new SellerServiceOutput().fromJson(result);
        
        assertTrue("input and output are not same", TestUtil.getInstance().isSellerEqual (
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                output.getData()));
    }
    
    /**
     * Test of toJson method, of class SellerService_0_0_1.
     */
    @Test
    public void testToJson() {
        System.out.println("toJson");
        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
    }
}
