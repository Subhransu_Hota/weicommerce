package com.rokittech.container.ecom.seller.factory;


import com.google.inject.Provider;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.base.defaults.Output;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.container.ecom.seller.defaults.SellerServiceParams;
import com.rokittech.container.ecom.seller.defaults.SellerVocabulary;
import com.rokittech.container.ecom.seller.service_0_0_1.SellerService_0_0_1;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author asifaham
 */
public class SellerServiceFactory implements Provider<Service<Input, Output, 
        SellerServiceParams, DefaultPersist>> {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(SellerServiceFactory.class);
    
    private Service service;

    /**
     * This is the place where you are managing different versions
     *
     * @param version
     */
//    @Inject
    public SellerServiceFactory(int version) {
        this(null, version);

    }

    public SellerServiceFactory(String appId, int version) {
        slf4jLogger.info ("Will create the instance of service based on appID: " + appId + " Version: " + version);
        if (appId != null) {
            SellerVocabulary app = SellerVocabulary.valueOf(appId.toUpperCase());
            switch (app) {
                case WEI_ECOMMERCE:
                    weiEcommerce(version);
                    break;
                case TEST_ECOMMERCE:
                    testEcommerce(version);
                    break;
                default:
                    weiEcommerce(version);
                    break;
            }
        } else {
            weiEcommerce(version);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Service get() {
        return service;
    }

    private void weiEcommerce(int version) {
        switch (version) {
            case 1:
                this.service = new SellerService_0_0_1();
            	
                break;
            case 2:

                break;
            default:

                break;

        }
    }
    
    private void testEcommerce(int version) {
        switch (version) {
            case 1:
                this.service = new SellerService_0_0_1(DBType.TEST);
            	
                break;
            case 2:

                break;
            default:

                break;

        }
    }
}
