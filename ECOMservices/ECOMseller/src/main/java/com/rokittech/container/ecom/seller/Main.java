/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.seller;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.container.ecom.commons.models.AddressType;
import com.rokittech.container.ecom.commons.models.Seller;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.container.ecom.commons.models.UserCredentials;
import com.rokittech.container.ecom.commons.models.UserType;
import com.rokittech.container.ecom.seller.defaults.SellerServiceInput;
import com.rokittech.container.ecom.seller.defaults.SellerServiceOutput;
import com.rokittech.container.ecom.seller.defaults.SellerServiceParams;
import com.rokittech.container.ecom.seller.defaults.SellerVocabulary;
import com.rokittech.container.ecom.seller.factory.SellerServiceFactory;
import com.rokittech.containers.api.core.Service;
import java.util.Arrays;
import java.util.Date;



/**
 * Temporary code.
 * 
 * @author asifaham
 */
public class Main {

    public static void main(String[] args) {
        
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        Address address = new Address();
        address.setAddressName("sweet home");
        address.setAddressType(AddressType.Billing_And_Shipping);
        address.setCity("Bangalore");
        address.setCreatedDate(new Date());
        
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setUserName("asif");
        userCredentials.setPassword("#123");
        userCredentials.setEmail("email");
        
        User user = new User();
        user.setFirstName("Asif");
        user.setLastName("Ahammed");
        user.setBillingAddress(Arrays.asList(address));
        user.setShippingAddress(Arrays.asList(address));
        user.setUserCredentials(userCredentials);
        user.setUserType(UserType.Seller);
        
        Seller seller = new Seller();
        seller.setName("Samsung");
        seller.setDescription("descr");
        seller.setOrganizationName("Samsung India Pvt Ltd");
        seller.setTanNo("TAN787545");
        seller.setPanNo("EWIPS6476");
        seller.setTinNo("TIN67856");
        seller.setCreatedDate(new Date());
        seller.setPhoneNo("9845555760");
        seller.setUser(user);
        
        // This is real mandatory code to move in controller
        SellerServiceParams params = new SellerServiceParams();
        
//        MongoClient mongo = new MongoClient("weicommerce-backend-dev.elasticbeanstalk.com", 27017);
//        MongoDatabase db = mongo.getDatabase("ecommerce");
//        
//        params.initEnv("env.properties");
//        params.setEnv(db);
        params.getProperties().put(SellerVocabulary.COLLECTION.name(), "Seller");
        params.getProperties().put(SellerVocabulary.COMMAND.name(), SellerVocabulary.INSERT.name());

        DefaultPersist persist = new DefaultPersist();
        SellerServiceInput inputTemp = new SellerServiceInput();
        inputTemp.setData(seller);
        inputTemp.setInfo(info);
        
      
        System.out.println ("***" + inputTemp.toJson());
        Service<SellerServiceInput, SellerServiceOutput, SellerServiceParams, DefaultPersist> service = new SellerServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new SellerServiceOutput());

        System.out.println(service.serve().getData());
        
    }

}
