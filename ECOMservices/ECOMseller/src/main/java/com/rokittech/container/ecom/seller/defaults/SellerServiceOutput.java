/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.seller.defaults;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Output;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author asifahammed
 */
public class SellerServiceOutput extends Output{
    private static final Logger slf4jLogger = LoggerFactory.getLogger(SellerServiceOutput.class);
    @JsonUnwrapped
    private String data;

    public SellerServiceOutput() {
        slf4jLogger.info("inside constructor");
    }

    /**
     * 
     * @return get Data
     */
    public String getData() {
        return data;
    }

    /**
     * 
     * @param data set Data
     */
    public void setData(String data) {
        this.data = data;
    }
    
    /**
     * 
     * @param data constructor
     */
    public SellerServiceOutput(String data){
        this.data = data;
    }
    
    @Override
    public String toJson() {
        return new Gson().toJson(this,SellerServiceOutput.class);
    }

    @Override
    public SellerServiceOutput fromJson(String json) {
        return new Gson().fromJson(json,SellerServiceOutput.class);
    }

    @Override
    public void init() {
        
    }
}
