/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.seller.defaults;

import com.google.gson.Gson;
import com.rokittech.container.base.core.Params;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author asifahammed
 */
public class SellerServiceParams implements Params {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(SellerServiceParams.class);
    private Properties properties;
    private String envId;
    
    public SellerServiceParams() {
        this.properties = new Properties();
    }

    @Override
    public String getEnvId() {
        return envId;
    }

    @Override
    public void setEnvId(String envId) {
        this.envId = envId;
    }

    @Override
    public Properties getProperties() {
        return properties == null ? new Properties() : properties;
    }

    @Override
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, SellerServiceParams.class);
    }

    @Override
    public Object fromJson(String json) {
        return new Gson().fromJson(json, SellerServiceParams.class);
    }

    @Override
    public void init() {
        this.properties = new Properties();
    }

    public Object getEnv() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setEnv(Object env) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Object initEnv(Properties props) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Object initEnv(String fileName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
