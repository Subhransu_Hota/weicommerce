/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.seller.defaults;

/**
 *
 * @author asifaham
 */
public enum SellerVocabulary {
    INSERT,
    RETRIEVE,
    UPDATE,
    DELETE,
    LIST,
    COLLECTION,
    LIMIT, 
    COMMAND,
    ECOMMERCE,
    WEI_ECOMMERCE,
    TEST_ECOMMERCE,
    MONGODB,
    HAZELCAST,
    USERS, 
    QUERY,
    TEMPLATE;
}
