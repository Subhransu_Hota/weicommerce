/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.seller.service_0_0_1;

import com.google.gson.Gson;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.core.AbstractService;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterManager;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.container.ecom.dbadapter.factory.DBManagerFactory;
import com.rokittech.container.ecom.seller.defaults.SellerServiceInput;
import com.rokittech.container.ecom.seller.defaults.SellerServiceOutput;
import com.rokittech.container.ecom.seller.defaults.SellerServiceParams;
import com.rokittech.container.ecom.seller.defaults.SellerVocabulary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author asifaham
 */
public class SellerService_0_0_1 extends AbstractService<SellerServiceInput, SellerServiceOutput, SellerServiceParams, DefaultPersist> {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(SellerService_0_0_1.class);
    private SellerServiceInput input;
    private SellerServiceOutput output;
    private SellerServiceParams params;
    private final DBAdapterManager dbclientManager;
        
    public SellerService_0_0_1() {
        dbclientManager = new DBManagerFactory().get();
    }
    
    public SellerService_0_0_1(DBType dbType) {
        dbclientManager = new DBManagerFactory(dbType).get();
    }

    public DBAdapterManager getDbclientManager() {
        return dbclientManager;
    }
   
    @Override
    public String serviceId() {
        return this.getClass().getName();
    }

    @Override
    public String getInputAsJson() {
        slf4jLogger.info("returning Input Object in the form of JSON");
        
        return input.toJson();
    }

    @Override
    public String getOutputAsJson() {
        slf4jLogger.info("returning Output Object in the form of JSON");
        return output.toJson();
    }

    @Override
    public void setInputAsJson(String input) {
        slf4jLogger.info("Recreating Input object from json");
        this.input = this.input.fromJson(input);
    }

    @Override
    public void setOutputAsJson(String output) {
        slf4jLogger.info("Recreating Ouput object from json");
        this.output = this.output.fromJson(output);
    }

    @Override
    public SellerServiceOutput serve() {
        slf4jLogger.info("Inside serve() ");
        DBHandlerInput serviceinput = new DBHandlerInput();
        serviceinput.setData(this.getInput().getData().toJson());
        
        DBHandlerOutput serviceoutput = new DBHandlerOutput();
        // This is real mandatory code to move in controller
        DBHandlertParams serviceparams = new DBHandlertParams();
        serviceparams.setCommand(getParams().getProperties().getProperty(SellerVocabulary.COMMAND.name()));
        serviceparams.setEntity(params.getProperties().getProperty(SellerVocabulary.COLLECTION.name()));
        
        dbclientManager.setInput(serviceinput);
        dbclientManager.setOutput(serviceoutput);
        dbclientManager.setParams(serviceparams);
        dbclientManager.setPersist(new DefaultPersist());
        
        DBHandlerOutput serviceOutput = (DBHandlerOutput) dbclientManager.serve();
      
        output.setData(serviceOutput.getData());
        output.setInfo(input.getInfo());

        slf4jLogger.info("Created output object " + output.getClass());
        
        return output;
    }

    @Override
    public String toJson() {
        slf4jLogger.info("generating json for service object");
        return new Gson().toJson(this, SellerService_0_0_1.class);
    }

    @Override
    public SellerServiceInput getInput() {
        return input;
    }

    @Override
    public void setInput(SellerServiceInput input) {
        this.input = input;
    }

    @Override
    public SellerServiceOutput getOutput() {
        return output;
    }

    @Override
    public void setOutput(SellerServiceOutput output) {
        this.output = output;
    }

    @Override
    public SellerServiceParams getParams() {
        return params;
    }

    @Override
    public void setParams(SellerServiceParams params) {
        this.params = params;
    }
}
