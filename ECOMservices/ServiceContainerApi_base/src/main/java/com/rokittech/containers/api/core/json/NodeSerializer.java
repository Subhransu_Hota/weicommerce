/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core.json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;

/**
 *
 * @author alexmy
 */
public class NodeSerializer implements JsonSerializer<NodeTemplate> {

    @Override
    public JsonElement serialize(NodeTemplate src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject jsonObject = new JsonObject();

        jsonObject = assign(jsonObject, "settings.number_of_shards", src.getSettings().getNumber_of_shards());
        jsonObject = assign(jsonObject, "settings.number_of_replicas", src.getSettings().getNumber_of_replicas());
        jsonObject = assign(jsonObject, "settings.store.type", src.getSettings().getStore().getType());

        jsonObject = assign(jsonObject, "gateway.type", src.getGateway().getType());
        jsonObject = assign(jsonObject, "network.host", src.getNetwork().getHost());
        
//        jsonObject = assign(jsonObject, "settings", src.getSettings());

        return jsonObject;
    }

    private JsonObject assign(JsonObject jsonObject, String name, Object value) {

        if (value != null) {
            if (value instanceof Boolean) {
                jsonObject.addProperty(name, (Boolean) value);
            } else if (value instanceof Number) {
                jsonObject.addProperty(name, (Number) value);
            } else if (value instanceof String) {
                jsonObject.addProperty(name, (String) value);
            } else if (value instanceof Character) {
                jsonObject.addProperty(name, (Character) value);
            }
        }

        return jsonObject;
    }
}
