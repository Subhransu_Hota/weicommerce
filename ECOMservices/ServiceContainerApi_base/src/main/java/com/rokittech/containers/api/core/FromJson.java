/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core;

/**
 *
 * @author alexmy
 * @param <T>
 */
public interface FromJson <T> {
    T fromJson(String json);
}
