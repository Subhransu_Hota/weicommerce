package com.rokittech.containers.api.core;

public interface Service <I, O, P, S> extends Json {
    
    String serviceId();

    String getInputAsJson();
    I getInput();
    
    String getOutputAsJson();
    
    O getOutput();
   
    P getParams();
    
    S getPersist();
    
    void setInputAsJson(String input);
    void setInput(I input);
    
    void setOutputAsJson(String output);
    void setOutput(O output);
    
    void setParams(P params);
   
    void setPersist(S persist);
    
    O serve();
    
    /**
     *
     * @param tool
     * @return
     */
    default O serve(Tool tool){
        // Perform processing and return result as Output object;
        // . . . . . . .
        //
        return getOutput();
    }
}