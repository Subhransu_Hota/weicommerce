/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

/**
 *
 * @author alexmy
 */
public class NodeDeserializer implements JsonDeserializer<NodeTemplate> {

    @Override
    public NodeTemplate deserialize(JsonElement json, Type type, JsonDeserializationContext jdc) throws JsonParseException {
        NodeTemplate template = new NodeTemplate();

        JsonObject jobject = (JsonObject) json;

        if (jobject.get("gateway.type") != null) {
            template.getGateway().setType(jobject.get("gateway.type").getAsString());
        }
        if (jobject.get("settings.number_of_replicas") != null) {
            template.getSettings().setNumber_of_replicas(jobject.get("settings.number_of_replicas").getAsInt());
        }
        if (jobject.get("settings.store.type") != null) {
            template.getSettings().getStore().setType(jobject.get("settings.store.type").getAsString());
        }
        if (jobject.get("settings.number_of_shards") != null) {
            template.getSettings().setNumber_of_shards(jobject.get("settings.number_of_shards").getAsInt());
        }
        if (jobject.get("network.host") != null) {
            template.getNetwork().setHost(jobject.get("network.host").getAsString());
        }
//        if (jobject.get("settings") != null) {
//            template.getNetwork().setHost(jobject.get("settings").getAsString());
//        }

        return template;
    }

}
