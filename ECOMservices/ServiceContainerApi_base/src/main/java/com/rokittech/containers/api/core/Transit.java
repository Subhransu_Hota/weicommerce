/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core;

import com.google.gson.Gson;
import java.util.ArrayList;

/**
 *
 * @author alexmy
 */
public class Transit extends ArrayList<TransitTriple> implements Json {

    @Override
    public String toJson() {
        if(this.isEmpty()) {
            return "{}";
        } else {
//            Gson gson = new GsonBuilder().registerTypeAdapter(
//                TransitTriple.class, 
//                new TransitTripleSerializer())
//                .setPrettyPrinting().create();
            
            return new Gson().toJson(this, Transit.class);
        }
    }

}

