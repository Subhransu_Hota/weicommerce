/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core;

import com.google.gson.Gson;
import java.util.List;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.indices.IndexMissingException;

/**
 *
 * @author alexmy
 */
public class IndexApiImpl implements IndexApi {

    @Override
    public void createIndex(Client client, String config) {

        IndexConfig indexConfig = new Gson().fromJson(config, IndexConfig.class);
        IndexSettings indexSettings = indexConfig.getIndexSettings();

        CreateIndexRequestBuilder createIndexRequestBuilder
                = client.admin().indices().prepareCreate(indexConfig.getIndexName());
        createIndexRequestBuilder.setSettings(indexSettings.toJson());

        CreateIndexResponse indexResponse = createIndexRequestBuilder.execute().actionGet();

    }

    @Override
    public void updateIndexSettings(Client client, String config, String settings) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateDocumentTypeMapping(Client client, String config, String documentType, boolean parentRelationship) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setupAllIndices(Client client, boolean parentRelationship) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isIndexExists(Client client, String indexName) {
        
        return client.admin().indices().prepareExists(indexName).execute().actionGet().isExists();
    }

    @Override
    public boolean deleteIndex(Client client, String indexName) {
//        logger.info("Deleting index " + indexName);
        DeleteIndexRequest request = new DeleteIndexRequest(indexName);
        final IndicesAdminClient adminClient = client.admin().indices();
        try {
            DeleteIndexResponse response = adminClient.delete(request).actionGet();
            if (response.isAcknowledged()) {
                return true;
            }
        } catch (IndexMissingException e) {
//            logger.info("No such index: " + indexName);            
        }
        
        return false;
    }

    @Override
    public String getIndexSetting(Client client, String config, String settingName) {
        
        IndexConfig indexConfig = new Gson().fromJson(config, IndexConfig.class);
        String indexName = indexConfig.getIndexName();
        
        return client.admin().indices().prepareGetSettings(indexName).get()
                .getSetting(indexName, settingName);
    }

    @Override
    public String getIndexSettings(Client client, String config) {
         
        IndexConfig indexConfig = new Gson().fromJson(config, IndexConfig.class);
        String indexName = indexConfig.getIndexName();
        
        return ""; //client.admin().indices().prepareGetSettings(indexName).get().getContext()..getIndexToSettings().toString();
    }

    @Override
    public boolean isAliasExists(Client client, String indexAliasName) {
        return isIndexExists(client, indexAliasName);
    }

    @Override
    public List<String> analyzeText(Client client, String indexAliasName, String analyzer, String[] tokenFilters, String text) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
