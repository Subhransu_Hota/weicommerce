/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.exceptions;

/**
 *
 * @author deependr
 */
public class EcommerceErrorCodes {

    //TODO finalize error codes
    //Default Error Code
    public static String DefaultCode = "EC000000";

    //HAZELCAST Related Error Code
    public static String HAZELCAST_ERROR_SYSTEMDOWN = "EC00H001";
    public static String HAZELCAST_ERROR_DATANOTFOUND = "EC00H002";
    public static String HAZELCAST_ERROR_INCORRECTDATA = "EC00H003";
    public static String HAZELCAST_ERROR_CONNECTION = "EC00H003";
    public static String HAZELCAST_ERROR_WRONGCONFIG = "EC00H005";
    public static String HAZELCAST_ERROR_AUTHENTICATION = "EC00H006";

    //MONGO DB Related Error Code
    public static String MONGO_ERROR_SYSTEMDOWN = "EC00M001";
    public static String MONGO_ERROR_DATANOTFOUND = "EC00M002";
    public static String MONGO_ERROR_INCORRECTDATA = "EC00M003";
    public static String MONGO_ERROR_CONNECTION = "EC00M003";
    public static String MONGO_ERROR_WRONGCONFIG = "EC00M005";

    //Service Related Error Code
    public static String SERVICE_ERROR_INVALIDINPUT = "EC00S001";
    public static String SERVICE_ERROR_INVALIDOUTPUT = "EC00S002";
    public static String SERVICE_ERROR_PARSING = "EC00S003";

    //Request Validation Related Error Code
    public static String SERVICE_ERROR_INVALIDREQUEST = "EC00V001";
    public static String SERVICE_ERROR_INCORRECTREQUEST = "EC00V002";
    public static String SERVICE_ERROR_MISSINGREQUIREDINFO = "EC00V003";

    //TOOLS Related Error Code
    public static String TOOL_ERROR_INVALIDINPUT = "EC00T001";
    public static String TOOL_ERROR_INVALIDOUTPUT = "EC00T002";
    public static String TOOL_ERROR_INVALIDSTATE = "EC00T003";
}
