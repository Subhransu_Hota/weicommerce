/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.exceptions;

/**
 *
 * @author deependr
 */
public class HazelcastException extends ECommerceException {

    public HazelcastException() {
        super();
    }

    public HazelcastException(String errorCode, String message) {
        super(errorCode, message);

    }

    public HazelcastException(String message, Throwable cause) {
        super(message, cause);
    }

    public HazelcastException(String message) {
        super(message);
    }

    public HazelcastException(Throwable cause) {
        super(cause);
    }

    @Override
    public void setErrorCode(String errorCode) {
        super.setErrorCode(errorCode); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getLocalizedMessage() {
        return super.getLocalizedMessage(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setMessage(String message) {
        super.setMessage(message); //To change body of generated methods, choose Tools | Templates.
    }

}
