/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.google.gson.Gson;

/**
 *
 * @author alexmy
 */
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY)
public class Pair implements Json, FromJson<Pair>, Initializable {
    
    String key;
    Object value;
    
    public Pair() {
        init();
    }

    public Pair(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    /**
     * 
     * @return 
     */
    public Object getValue() {
        return value;
    }
    
    /**
     * 
     * @return 
     */
    public String getKey() {
        return key;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, Pair.class);
    }

    @Override
    public Pair fromJson(String json) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public final void init() {
        this.key = "name";
        this.value = "";
                
    }
    
}
