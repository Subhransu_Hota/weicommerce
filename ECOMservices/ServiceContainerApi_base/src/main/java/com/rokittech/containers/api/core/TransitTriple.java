/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core;

import com.google.gson.Gson;

/**
 *
 * @author alexmylnikov
 */
public class TransitTriple implements Json, FromJson<TransitTriple>, Initializable {
    
    private String key;
    private String typeName;
    private String typeJson;
    
    public TransitTriple() {
        init();
    }
    
    public TransitTriple(String key, String typeName, String typeJson){
        this.key = key;
        this.typeName = typeName;
        this.typeJson = typeJson;
        
    }
    
    @Override
    public final void init() {
        this.key = "";
        this.typeName = "";
        this.typeJson = "{}";
    }

    /**
     * 
     * @return JSON presentation of this class implementation
     */
    @Override
    public String toJson() {
//        Gson gson = new GsonBuilder().registerTypeAdapter(
//                Class.class, 
//                new TransitTripleSerializer())
//                .setPrettyPrinting().create();
        
        return new Gson().toJson(this, TransitTriple.class);
    }
    
    @Override
    public TransitTriple fromJson(String json){
//        GsonBuilder gsonBuilder = new GsonBuilder();
////        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
//        gsonBuilder.registerTypeAdapter(TransitTriple.class, new TransitTripleDeserializer());
//        Gson gson = gsonBuilder.create();
        return new Gson().fromJson(json, TransitTriple.class);
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the type
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * @param typeName
     */
    public void setType(String typeName) {
        this.typeName = typeName;
    }

    /**
     * @return the JSON of the given type implementation
     */
    public String getTypeJson() {
        return typeJson;
    }

    /**
     * @param typeJson the typeJson to set
     */
    public void setTypeJson(String typeJson) {
        this.typeJson = typeJson;
    }

}
