/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.exceptions;

import com.google.gson.Gson;
import com.rokittech.containers.api.core.FromJson;
import com.rokittech.containers.api.core.Json;
import java.io.Serializable;

/**
 *
 * @author deependr
 */
public class ECommerceException extends Exception implements Json, FromJson, Serializable{

    private String message = null;

    // Default Ststus code E000000 is Unknown Exception
    private String errorCode = "E000000";

    public ECommerceException() {
        super();
    }

    public ECommerceException(String message) {
        super(message);
        this.message = message;
    }

    public ECommerceException(String errorCode, String message) {
        super(message);
        this.message = message;
        this.errorCode = errorCode;
    }

    public ECommerceException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {

        // TODO Log implementation
        return "Error Code : " + errorCode + "\nMessage : " + message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public ECommerceException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, ECommerceException.class);
    }

    @Override
    public Object fromJson(String json) {
         return new Gson().fromJson(json, ECommerceException.class);
    }

    

}
