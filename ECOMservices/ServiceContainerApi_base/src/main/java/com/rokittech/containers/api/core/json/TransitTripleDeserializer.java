/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.rokittech.containers.api.core.TransitTriple;
import java.lang.reflect.Type;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alexmy
 */
public class TransitTripleDeserializer implements JsonDeserializer<TransitTriple> {

    @Override
    public TransitTriple deserialize(JsonElement json, Type type, JsonDeserializationContext jdc) throws JsonParseException {
        
        TransitTriple template = new TransitTriple();

        JsonObject jobject = (JsonObject) json;

        if (jobject.get("key") != null) {
            template.setKey(jobject.get("key").getAsString());
        }
        if (jobject.get("typeJson") != null) {
            template.setTypeJson(jobject.get("typeJson").getAsString());
        }
        if (jobject.get("typeName") != null) {
            template.setTypeJson(jobject.get("typeName").getAsString());
        }
        

        return template;
    }

}
