/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core;

/**
 *
 * @author alexmy
 */
import java.util.List;
import org.elasticsearch.client.Client;


public interface IndexApi {

    void createIndex(Client client, String config);

    void updateIndexSettings(Client client, String config, String settings);

    void updateDocumentTypeMapping(Client client, String config, String documentType, boolean parentRelationship);

    void setupAllIndices(Client client, boolean parentRelationship);

    boolean isIndexExists(Client client, String indexName);

    boolean deleteIndex(Client client, String indexName);

    String getIndexSetting(Client client, String config, String settingName);
    
    String getIndexSettings(Client client, String config);

    boolean isAliasExists(Client client, String indexAliasName);

    List<String> analyzeText(Client client, String indexAliasName, String analyzer, String[] tokenFilters, String text);

}
