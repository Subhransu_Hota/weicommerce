/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.core;

import com.google.gson.Gson;

/**
 *
 * @author alexmy
 */
public class IndexSettings implements Json {
    
    private int number_of_shards;
    private int number_of_replicas;

    /**
     * @return the number_of_shards
     */
    public int getNumber_of_shards() {
        return number_of_shards;
    }

    /**
     * @param number_of_shards the number_of_shards to set
     */
    public void setNumber_of_shards(int number_of_shards) {
        this.number_of_shards = number_of_shards;
    }

    /**
     * @return the number_of_replicas
     */
    public int getNumber_of_replicas() {
        return number_of_replicas;
    }

    /**
     * @param number_of_replicas the number_of_replicas to set
     */
    public void setNumber_of_replicas(int number_of_replicas) {
        this.number_of_replicas = number_of_replicas;
    }

    public String toJson() {
        return new Gson().toJson(this, IndexSettings.class);
    }
}
