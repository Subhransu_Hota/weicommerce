/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.containers.api.exceptions;

/**
 *
 * @author deependr
 */
public class RequestValidationException extends ECommerceException{

    public RequestValidationException() {
    }

    public RequestValidationException(String message) {
        super(message);
    }

    public RequestValidationException(Throwable cause) {
        super(cause);
    }

    public RequestValidationException(String errorCode, String message) {
        super(errorCode, message);
    }

    public RequestValidationException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
}
