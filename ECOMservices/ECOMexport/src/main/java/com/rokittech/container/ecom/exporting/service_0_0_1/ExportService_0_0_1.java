package com.rokittech.container.ecom.exporting.service_0_0_1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.core.AbstractService;
import com.rokittech.container.ecom.commons.models.Organization;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterManager;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.factory.DBManagerFactory;
import com.rokittech.container.ecom.exporting.defaults.ExportServiceInput;
import com.rokittech.container.ecom.exporting.defaults.ExportServiceOutput;
import com.rokittech.container.ecom.exporting.defaults.ExportServiceParams;
import com.rokittech.container.ecom.exporting.defaults.ExportVocabulary;

import au.com.bytecode.opencsv.CSVReader;

public class ExportService_0_0_1 extends AbstractService<ExportServiceInput, ExportServiceOutput, ExportServiceParams, DefaultPersist>{
	
	private static final Logger slf4jLogger = LoggerFactory.getLogger(ExportService_0_0_1.class);
    private ExportServiceInput input;
    private ExportServiceOutput output;
    private ExportServiceParams params;
    
    private String resultMessage = "";

    private final DBAdapterManager dbclientManager;
        
    public ExportService_0_0_1() {
        dbclientManager = new DBManagerFactory().get();
    }

    public ExportService_0_0_1(DBHandlertParams.DBType dbType) {
        dbclientManager = new DBManagerFactory(dbType).get();
    }
    
    @Override
    public String serviceId() {
        return this.getClass().getName();
    }

    @Override
    public String getInputAsJson() {
        slf4jLogger.info("returning Input Object in the form of JSON");
        
        return input.toJson();
    }

    @Override
    public String getOutputAsJson() {
        slf4jLogger.info("returning Output Object in the form of JSON");
        return output.toJson();
    }

    @Override
    public void setInputAsJson(String input) {
        slf4jLogger.info("Recreating Input object from json");
        this.input = this.input.fromJson(input);
    }

    @Override
    public void setOutputAsJson(String output) {
        slf4jLogger.info("Recreating Ouput object from json");
        this.output = this.output.fromJson(output);
    }

	@Override
	public ExportServiceOutput serve() {
		slf4jLogger.info("Inside serve() ");

		String collectionName = params.getProperties().getProperty(ExportVocabulary.COLLECTION.name());
		String filePath = getInput().getData().toString();
		filePath = filePath.substring(filePath.indexOf("=") + 1, filePath.length() - 1);
		
			DBHandlerOutput serviceoutput = new DBHandlerOutput();
			
				DBHandlerInput serviceinput = new DBHandlerInput();
				serviceinput.setData(filePath);

				DBHandlertParams serviceparams = new DBHandlertParams();
				serviceparams.setCommand(getParams().getProperties().getProperty(ExportVocabulary.COMMAND.name()));
				serviceparams.setEntity(params.getProperties().getProperty(ExportVocabulary.COLLECTION.name()));

				dbclientManager.setInput(serviceinput);
				dbclientManager.setOutput(serviceoutput);
				dbclientManager.setParams(serviceparams);
				dbclientManager.setPersist(new DefaultPersist());

				DBHandlerOutput serviceOutput = (DBHandlerOutput) dbclientManager.serve();
			

			// output.setData(serviceoutput.getData());
			// output.setInfo(input.getInfo());
			
			output.setData(serviceOutput.getData());

			slf4jLogger.info("Created output object " + output.getClass());
			return output;
		} 
		

	

    @Override
    public String toJson() {
        slf4jLogger.info("generating json for service object");
        return new Gson().toJson(this, ExportService_0_0_1.class);
    }

    @Override
    public ExportServiceInput getInput() {
        return input;
    }

    @Override
    public void setInput(ExportServiceInput input) {
        this.input = input;
    }

    @Override
    public ExportServiceOutput getOutput() {
        return output;
    }

    @Override
    public void setOutput(ExportServiceOutput output) {
        this.output = output;
    }

    @Override
    public ExportServiceParams getParams() {
        return params;
    }

    public DBAdapterManager getDbclientManager() {
        return dbclientManager;
    }

    
    @Override
    public void setParams(ExportServiceParams params) {
        this.params = params;
    }
    
	
    
	
    

}

