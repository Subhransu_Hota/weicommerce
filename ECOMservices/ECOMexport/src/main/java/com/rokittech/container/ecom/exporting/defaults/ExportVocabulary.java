package com.rokittech.container.ecom.exporting.defaults;

public enum ExportVocabulary {
	
	COLLECTION,
	COMMAND,
	EXPORT,
	WEI_ECOMMERCE,
	VALIDATE_IMPORT,
	TEST_ECOMMERCE,
	MONGODB,
	HAZELCAST,
	RETRIEVE;
	

}
