package com.rokittech.container.ecom.exporting.defaults;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Output;


public final class ExportServiceOutput extends Output {

	private static final Logger slf4jLogger = LoggerFactory.getLogger(ExportServiceOutput.class);
    @JsonUnwrapped
    private String data;

    public ExportServiceOutput() {
        slf4jLogger.info("inside constructor");
    }

    /**
     * 
     * @return get Data
     */
    public String getData() {
        return data;
    }

    /**
     * 
     * @param data set Data
     */
    public void setData(String data) {
        this.data = data;
    }
    
    /**
     * 
     * @param data constructor
     */
    public ExportServiceOutput(String data){
        this.data = data;
    }
    
    @Override
    public String toJson() {
        return new Gson().toJson(this,ExportServiceOutput.class);
    }

    @Override
    public ExportServiceOutput fromJson(String json) {
        return new Gson().fromJson(json,ExportServiceOutput.class);
    }

    @Override
    public void init() {
        
    }


}
