package com.rokittech.container.ecom.exporting.defaults;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Input;


public final class ExportServiceInput extends Input {
	
	private static final Logger slf4jLogger = LoggerFactory.getLogger(ExportServiceInput.class);
    /**
     *
     */
	
	public ExportServiceInput() { 
        slf4jLogger.info("inside constructor");
        this.init();
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, ExportServiceInput.class);
    }

    @Override
    public ExportServiceInput fromJson(String json) {
        return new Gson().fromJson(json, ExportServiceInput.class);
    }

    @Override
    public final void init() {
    }

}


