/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.basket;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.basket.defaults.BasketServiceInput;
import com.rokittech.container.ecom.basket.defaults.BasketServiceOutput;
import com.rokittech.container.ecom.basket.defaults.BasketServiceParams;
import com.rokittech.container.ecom.basket.defaults.BasketVocabulary;
import com.rokittech.container.ecom.basket.factory.BasketServiceFactory;
import com.rokittech.container.ecom.commons.models.Basket;
import com.rokittech.container.ecom.commons.models.BasketProductRef;
import com.rokittech.container.ecom.commons.models.LineItem;
import com.rokittech.container.ecom.commons.models.User;
import com.rokittech.containers.api.core.Service;
import java.util.Date;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);

        
        //User
        BasketProductRef productRef= new BasketProductRef();
        
        productRef.setUserId("564daf9706354d172e803b0f");
        productRef.setProductId("563b52ff63438212bc902de2");
        productRef.setQty(1.0);
                System.out.println("productRef"+productRef.toJson());
        User user =new User();
        LineItem lineItem = new LineItem();
        
        //This code is temp code of UserAddress for POC
        Basket basket= new Basket(lineItem, user);
        basket.setCreatedDate(new Date());
        basket.setStatus("New");
       // This is real mandatory code to move in controller
        BasketServiceParams params = new BasketServiceParams();
        //params.initEnv("env.properties");
        
//        MongoClient mongo = new MongoClient("weicommerce-backend-dev.elasticbeanstalk.com", 27017);
//        MongoDatabase db = mongo.getDatabase("ecommerce");
//        
//        params.setEnv(db);
        params.getProperties().put(BasketVocabulary.COLLECTION.name(), "Organization");
        params.getProperties().put(BasketVocabulary.COMMAND.name(), BasketVocabulary.INSERT.name());

        DefaultPersist persist = null;//new DefaultPersist();
        //persist.initEnv("env.properties");

//        CrudNoSqlServiceInput json = new CrudNoSqlServiceInput();
        //
        BasketServiceInput inputTemp = new BasketServiceInput();
        inputTemp.setData(basket);
        inputTemp.setInfo(info);
        
        System.out.println (inputTemp.toJson());

//        CrudNoSqlService_0_0_1 service;        
        Service<BasketServiceInput, BasketServiceOutput, BasketServiceParams, DefaultPersist> service = new BasketServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        //service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new BasketServiceOutput());

        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
