/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.basket.defaults;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.ecom.commons.models.BasketProductRef;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public final class BasketProductServiceInput extends Input<BasketProductRef> {

//    @JsonUnwrapped
//    private String data;
    private String template;

    /**
     *
     */
    public BasketProductServiceInput() {
        this.init();
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, BasketProductServiceInput.class);
    }

    @Override
    public BasketProductServiceInput fromJson(String json) {
        return new Gson().fromJson(json, BasketProductServiceInput.class);
    }

    @Override
    public final void init() {
        this.setTemplate("");
    }

    /**
     *
     * @return gets template
     */
    public String getTemplate() {
        return template;
    }

    /**
     *
     * @param template set template
     */
    public void setTemplate(String template) {
        this.template = template;
    }
}
