/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.basket.util;

import com.rokittech.container.ecom.commons.core.Money;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.container.ecom.commons.models.Basket;
import com.rokittech.container.ecom.commons.models.LineItem;
import com.rokittech.container.ecom.commons.models.LineItemType;
import com.rokittech.container.ecom.commons.models.Product;
import com.rokittech.container.ecom.commons.models.User;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * Shopping Basket Helper
 *
 * @author Deependra Kumar
 */
public class BasketHelper {

    public BasketHelper() {
    }

    // Calculate basket for any change in tax
    public Basket calculateBasket(Basket basket) {

        Basket canculatedBasket = new Basket();
        canculatedBasket = copyBasket(basket);
        // Calculate LineItem pricing
        Double totalPriceValue = 0.0;
        Double itemTotalPrice = 0.0;
        Locale locale = null;
        double shipTotal = 0.0;
        for (Iterator iterator = canculatedBasket.getLineItems().iterator(); iterator.hasNext();) {
            LineItem lineItem = (LineItem) iterator.next();
            if(lineItem.getShippingMethod() != null)
                shipTotal += lineItem.getShippingMethod().getShippingCalcValue();
            Money grossPrice = lineItem.getItemGrossPrice();
            locale = grossPrice.getLocale();
            itemTotalPrice = grossPrice.getAmount() * lineItem.getQuantity();
            totalPriceValue += grossPrice.getAmount() * lineItem.getQuantity();
            lineItem.setGrossPrice(new Money(grossPrice.getLocale(), itemTotalPrice));

        }
        // Calculate basket pricing
        Money totalPrice = new Money(new Locale("en", "IN"), totalPriceValue + shipTotal);
        canculatedBasket.setSubTotal(totalPrice);
        canculatedBasket.setTax(new Money(new Locale("en", "IN"), 0.0));
        canculatedBasket.setTotal(totalPrice);

        return canculatedBasket;
    }

    // Calculate basket for any change in tax
    public Basket removeBasketLineItem(Basket basket, String productId) {

        Basket newBasket = new Basket();
        LineItem removedLineItem = null;
        newBasket = copyBasket(basket);
        // Calculate LineItem pricing
        Double totalPriceValue = 0.0;
        Double itemTotalPrice = 0.0;
        Locale locale = null;
        List<LineItem> lineItems = newBasket.getLineItems();
        for (Iterator iterator = newBasket.getLineItems().iterator(); iterator.hasNext();) {
            LineItem lineItem = (LineItem) iterator.next();
            if (lineItem.getProductId().equalsIgnoreCase(productId)) {
                removedLineItem = lineItem;
                break;
            }
        }
        if (removedLineItem != null) {
            lineItems.remove(removedLineItem);
            newBasket.setLineItems(lineItems);
        }

        // Calculate basket pricing
        return newBasket;
    }

    public Basket updateBasketLineItemQty(Basket basket, String productId, double qty) {

        Basket newBasket = new Basket();
        LineItem modifiedLineItem = null;
        newBasket = copyBasket(basket);
        // Calculate LineItem pricing
        Double totalPriceValue = 0.0;
        Double itemTotalPrice = 0.0;
        Locale locale = null;
        List<LineItem> lineItems = newBasket.getLineItems();
        for (Iterator iterator = newBasket.getLineItems().iterator(); iterator.hasNext();) {
            LineItem lineItem = (LineItem) iterator.next();
            if (lineItem.getProductId().equalsIgnoreCase(productId)) {
                modifiedLineItem = lineItem;
                lineItems.remove(modifiedLineItem);
                if (qty > 0.0) {

                    modifiedLineItem.setQuantity(qty);
                    lineItems.add(modifiedLineItem);
                }
                break;
            }
        }
        return newBasket;
    }

    private Basket copyBasket(Basket basket) {
        Basket cBasket = new Basket();
        cBasket.setId(basket.getId());
        cBasket.setLineItems(basket.getLineItems());
        cBasket.setUser(basket.getUser());
        if(basket.getBillingAddress()!=null){
            cBasket.setBillingAddress(basket.getBillingAddress());
        }
        
        cBasket.setPaymentType(basket.getPaymentType());
        cBasket.setPayment(basket.getPayment());
        cBasket.setStatus(basket.getStatus());
        cBasket.setTax(basket.getTax());
        cBasket.setSubTotal(basket.getSubTotal());
        cBasket.setTotal(basket.getTotal());
        cBasket.setCreatedDate(basket.getCreatedDate());
        return cBasket;
    }

    public LineItem createProductLineItem(Product product, double qty) {

        LineItem lineItem = new LineItem();
        lineItem.setProductSKU(product.getProductId());
        lineItem.setProductId(product.getId());
        lineItem.setQuantity(qty);
        lineItem.setProductName(product.getName());
        lineItem.setType(LineItemType.Product);
        lineItem.setTax(new Money(new Locale("en", "IN"), 0.00));
        // temp price assignment
        lineItem.setItemGrossPrice(product.getPricingList().iterator().next().getMoney());

        return lineItem;
    }

//    public Basket updateBasketBillingAddress(Basket basket, Address address) {
//
//        basket.setBillingAddress(address);
//                break;
//        Iterator<Address> addresses = basket.getUser().getBillingAddress().iterator();
//        while (addresses.hasNext()) {
//            Address address = addresses.next();
//            if (address.getAddressId().equalsIgnoreCase(addressId)) {
//                basket.setBillingAddress(address);
//                break;
//            }
//        }
//
//        return basket;
//    }
    public Basket updateBasketShippingAddress(Basket basket, Address shippingAddress) {
        List<LineItem> lineItems=basket.getLineItems();
        
        for (int idx=0; idx<lineItems.size();idx++) {
            lineItems.get(idx).setShippingAddress(shippingAddress);
        }
        
        basket.setLineItems(lineItems);
        
        return basket;
    }

    public static void main(String[] args) {
        Basket basket = new Basket();
        basket.setId("TestId");
        User user = new User();
        user.setId("TestUserId");
        user.setFirstName("Test Name");
        basket.setUser(user);

        System.out.println("Modified Basket id=" + basket.getId());
        System.out.println("Modified Basket user comp=" + basket.getUser().getCompany());

    }
}
