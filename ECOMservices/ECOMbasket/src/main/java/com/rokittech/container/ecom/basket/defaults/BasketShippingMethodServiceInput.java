/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.basket.defaults;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.ecom.commons.models.ShippingMethodRef;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public final class BasketShippingMethodServiceInput extends Input<ShippingMethodRef> {

    /**
     *
     */
    public BasketShippingMethodServiceInput() {
        this.init();
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, BasketShippingMethodServiceInput.class);
    }

    @Override
    public BasketShippingMethodServiceInput fromJson(String json) {
        return new Gson().fromJson(json, BasketShippingMethodServiceInput.class);
    }

    @Override
    public final void init() {
    }
}
