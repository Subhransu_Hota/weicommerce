/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.search;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Organization;
import com.rokittech.container.ecom.commons.models.SearchTerm;
import com.rokittech.container.ecom.search.defaults.SearchServiceInput;
import com.rokittech.container.ecom.search.defaults.SearchServiceOutput;
import com.rokittech.container.ecom.search.defaults.SearchServiceParams;
import com.rokittech.container.ecom.search.defaults.SearchVocabulary;
import com.rokittech.container.ecom.search.factory.SearchServiceFactory;
import com.rokittech.containers.api.core.Service;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);

        //This code is temp code of SrearcTerm for POC
        SearchTerm searchTerm=new SearchTerm();
        searchTerm.setKeyword("Camera");
       // This is real mandatory code to move in controller
        SearchServiceParams params = new SearchServiceParams();
        //params.initEnv("env.properties");
        
//        MongoClient mongo = new MongoClient("weicommerce-backend-dev.elasticbeanstalk.com", 27017);
//        MongoDatabase db = mongo.getDatabase("ecommerce");
//        
//        params.setEnv(db);
        params.getProperties().put(SearchVocabulary.COLLECTION.name(), "Product");
        params.getProperties().put(SearchVocabulary.COMMAND.name(), SearchVocabulary.PRODUCTSEARCH.name());

        DefaultPersist persist = null;//new DefaultPersist();
        //persist.initEnv("env.properties");

//        CrudNoSqlServiceInput json = new CrudNoSqlServiceInput();
        //
        SearchServiceInput inputTemp = new SearchServiceInput();
        inputTemp.setData(searchTerm);
        inputTemp.setInfo(info);
        
        System.out.println (inputTemp.toJson());

//        CrudNoSqlService_0_0_1 service;        
        Service<SearchServiceInput, SearchServiceOutput, SearchServiceParams, DefaultPersist> service = new SearchServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        //service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new SearchServiceOutput());

        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
