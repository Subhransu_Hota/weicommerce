/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.search.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Organization;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import com.rokittech.container.ecom.search.defaults.SearchServiceParams;
import com.rokittech.container.ecom.search.factory.SearchServiceFactory;
import com.rokittech.container.ecom.search.service_0_0_1.SearchService_0_0_1;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.Date;
import java.util.List;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class TestUtil {
    private final SearchService_0_0_1 service;
    private static final Logger logger = LoggerFactory.getLogger(TestUtil.class);

    private static final SearchServiceParams params = new SearchServiceParams();
    private MongoClient client;
    private MongoServer server;

    private TestUtil() {
        service = (SearchService_0_0_1) new SearchServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
        ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        //params.initEnv("env.properties");
        logger.info("Mongo Db connection created succesfully");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }

    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public Organization createOrganization() {
        Organization organization = new Organization();
        organization.setOrganizationId("rokitt");
        organization.setOrganizationName("Rokit Tech");
        organization.setDescription("Rokitt");
        organization.setCreatedDate(new Date());

        return organization;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);

        return info;
    }

    public boolean isOrganizationEqual(String input, String output) {
        logger.info("Input :" + input + "\n Output : " + output);

        Organization inputOrganization = new Organization().fromJson(input);
        Organization outputOrganization = new Organization().fromJson(output);

        boolean b = inputOrganization.getOrganizationId().equalsIgnoreCase(outputOrganization.getOrganizationId())
                && inputOrganization.getOrganizationName().equalsIgnoreCase(outputOrganization.getOrganizationName())
                && inputOrganization.getDescription().equalsIgnoreCase(outputOrganization.getDescription());

        return b;
    }

    public SearchServiceParams getParams() {
        return params;
    }

    public Organization createNewOrganizationInDatabase() {

        Organization organization = new Organization();
        organization.setOrganizationId("rokitt");
        organization.setOrganizationName("Rokit Tech");
        organization.setDescription("Rokit Technology");
        organization.setCreatedDate(new Date());

        DBObject object = (DBObject) JSON.parse(organization.toJson());
        object.put("is_deleted", false);

        client.getDatabase("ecommerce_test").getCollection("Organization", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId) object.get("_id");
        object.put("_id", objId.toString());

        return new Organization().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObject(String result, String input) {
        Organization inputOrganization = new Organization().fromJson(input);

        Type listOfTestObject = new TypeToken<List<Organization>>() {
        }.getType();
        List<Organization> list = new Gson().fromJson(result, listOfTestObject);

        boolean flag = false;
        for (Organization catalog : list) {
            if (catalog.getId().equalsIgnoreCase(inputOrganization.getId())) {
                flag = true;
                break;
            }
        }

        return flag;
    }

    public void cleanup() {
        logger.info("Cleanup test data");
        client.getDatabase("ecommerce_test").getCollection("Organization").drop();
        client.getDatabase("ecommerce_test").getCollection("Product").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    public SearchService_0_0_1 getService() {
        return service;
    }

    
    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }
}
