/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.attribute;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.attribute.defaults.AttributeServiceInput;
import com.rokittech.container.ecom.attribute.defaults.AttributeServiceOutput;
import com.rokittech.container.ecom.attribute.defaults.AttributeServiceParams;
import com.rokittech.container.ecom.attribute.defaults.AttributeVocabulary;
import com.rokittech.container.ecom.attribute.factory.AttributeServiceFactory;
import com.rokittech.container.ecom.commons.models.Attribute;
import com.rokittech.container.ecom.commons.models.AttributeType;
import com.rokittech.containers.api.core.Service;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);

        //This code is temp code of UserAttribute for POC
        
        // Temp User Id 
        String userId ="560b7d980e8543233407bc2a";
        //This code is temp code of UserAttribute for POC
        Attribute attribute = new Attribute();
        attribute.setName("attribute_name_1");
        attribute.setAttributeType(AttributeType.DEFAULT);
        attribute.setAttributeGroupId("Group_1");
        
       // This is real mandatory code to move in controller
        AttributeServiceParams params = new AttributeServiceParams();
        //params.initEnv("env.properties");
        
        //MongoClient mongo = new MongoClient("weicommerce-backend-dev.elasticbeanstalk.com", 27017);
        MongoClient mongo = new MongoClient("localhost", 27017);
        MongoDatabase db = mongo.getDatabase("ecommerce");
        
        params.setEnv(db);
        params.getProperties().put(AttributeVocabulary.COLLECTION.name(), "Attribute");
        params.getProperties().put(AttributeVocabulary.COMMAND.name(), AttributeVocabulary.INSERT.name());

        DefaultPersist persist = null;//new DefaultPersist();
        //persist.initEnv("env.properties");

//        CrudNoSqlServiceInput json = new CrudNoSqlServiceInput();
        //
        AttributeServiceInput inputTemp = new AttributeServiceInput();
        inputTemp.setData(attribute);
        inputTemp.setInfo(info);
        
        System.out.println (inputTemp.toJson());

//        CrudNoSqlService_0_0_1 service;        
        Service<AttributeServiceInput, AttributeServiceOutput, AttributeServiceParams, DefaultPersist> service = new AttributeServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        //service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new AttributeServiceOutput());

        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
