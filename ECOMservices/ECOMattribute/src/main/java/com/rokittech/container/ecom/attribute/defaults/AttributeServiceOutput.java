/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.attribute.defaults;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.google.gson.Gson;
import com.rokittech.container.base.defaults.Output;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class AttributeServiceOutput extends Output{
    
    @JsonUnwrapped
    private String data;

    public AttributeServiceOutput() {
        
    }

    /**
     * 
     * @return get Data
     */
    public String getData() {
        return data;
    }

    /**
     * 
     * @param data set Data
     */
    public void setData(String data) {
        this.data = data;
    }
    
    /**
     * 
     * @param data constructor
     */
    public AttributeServiceOutput(String data){
        this.data = data;
    }
    
    @Override
    public String toJson() {
        return new Gson().toJson(this,AttributeServiceOutput.class);
    }

    @Override
    public AttributeServiceOutput fromJson(String json) {
        return new Gson().fromJson(json,AttributeServiceOutput.class);
    }

    @Override
    public void init() {
        
    }
}
