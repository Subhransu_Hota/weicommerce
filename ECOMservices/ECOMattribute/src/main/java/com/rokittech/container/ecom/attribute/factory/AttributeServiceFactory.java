package com.rokittech.container.ecom.attribute.factory;


import com.google.inject.Provider;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.attribute.defaults.AttributeServiceInput;
import com.rokittech.container.ecom.attribute.defaults.AttributeServiceOutput;
import com.rokittech.container.ecom.attribute.defaults.AttributeServiceParams;
import com.rokittech.container.ecom.attribute.defaults.AttributeVocabulary;
import com.rokittech.container.ecom.attribute.service_0_0_1.AttributeService_0_0_1;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.containers.api.core.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class AttributeServiceFactory implements Provider<Service<AttributeServiceInput, AttributeServiceOutput, AttributeServiceParams, DefaultPersist>> {

    private static final Logger logger = LoggerFactory.getLogger(AttributeServiceFactory.class);
    private Service service;

    /**
     * This is the place where you are managing different versions
     *
     * @param version
     */
//    @Inject
    public AttributeServiceFactory(int version) {
        this(null, version);

    }

    public AttributeServiceFactory(String appId, int version) {
        logger.info("Will create the instance of service based on appID: " + appId + " Version: " + version);
        if (appId != null) {
            AttributeVocabulary app = AttributeVocabulary.valueOf(appId.toUpperCase());
            switch (app) {
                case WEI_ECOMMERCE:
                    weiEcommerce(version);
                    break;
                case TEST_ECOMMERCE:
                    testEcommerce(version);
                    break;
                default:
                    weiEcommerce( version);
                    break;
            }
        } else {
            weiEcommerce(version);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Service get() {
        return service;
    }

    private void weiEcommerce(int version) {

        switch (version) {
            case 1:
                this.service = new AttributeService_0_0_1();

                break;
            case 2:

                break;
            default:

                break;

        }
    }

    private void testEcommerce(int version) {
        switch (version) {
            case 1:
                this.service = new AttributeService_0_0_1(DBType.TEST);
            	
                break;
            case 2:

                break;
            default:

                break;

        }
    }

}
