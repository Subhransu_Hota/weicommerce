/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.attribute.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.attribute.defaults.AttributeServiceInput;
import com.rokittech.container.ecom.attribute.defaults.AttributeServiceOutput;
import com.rokittech.container.ecom.attribute.defaults.AttributeServiceParams;
import com.rokittech.container.ecom.attribute.defaults.AttributeVocabulary;
import com.rokittech.container.ecom.attribute.utils.TestUtil;
import com.rokittech.container.ecom.commons.models.Attribute;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class AttributeService_0_0_1UpdateAttributeTest {

    private static final Logger logger = LoggerFactory.getLogger(AttributeService_0_0_1UpdateAttributeTest.class);

    public AttributeService_0_0_1UpdateAttributeTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        logger.info("Setup test resouces");

        AttributeServiceInput input = new AttributeServiceInput();
        Attribute address = TestUtil.getInstance().createNewAttributeInDatabase(
        "test_product_" + new Random ().nextInt(Integer.MAX_VALUE));

        //Update description
        address.setName("updated Attribute Name");

        input.setData(address);
        input.setInfo(TestUtil.getInstance().createInfo());

        AttributeServiceOutput output = new AttributeServiceOutput();
        // This is real mandatory code to move in controller
        AttributeServiceParams params = TestUtil.getInstance().getParams();

        params.getProperties().put(AttributeVocabulary.COLLECTION.name(), "Attribute");
        params.getProperties().put(AttributeVocabulary.COMMAND.name(), AttributeVocabulary.UPDATE.name());

        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }

    @After
    public void tearDown() {
        logger.info("Cleanup all test resouces");
    }

    /**
     * Test of getInputAsJson method, of class AttributeService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        logger.info("Started Execution of getInputAsJson ");

        String result = TestUtil.getInstance().getService().getInputAsJson();
        AttributeServiceInput input = new AttributeServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);

        logger.info("Finished Execution of getInputAsJson ");
    }

    /**
     * Test of serve method, of class AttributeService_0_0_1.
     */
    @Test
    public void testServe() {
        logger.info("Started Execution of serve ");

        AttributeServiceOutput result = TestUtil.getInstance().getService().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isAttributeEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                result.getData()));

        logger.info("Finished Execution of serve ");
    }

    /**
     * Test of getOutputAsJson method, of class AttributeService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        logger.info("Started Execution of getOutputAsJson ");

        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        AttributeServiceOutput output = new AttributeServiceOutput().fromJson(result);

        assertTrue("input and output are not same", TestUtil.getInstance().isAttributeEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                output.getData()));

        logger.info("Finished Execution of getOutputAsJson ");
    }

    /**
     * Test of toJson method, of class AttributeService_0_0_1.
     */
    @Test
    public void testToJson() {
        logger.info("Started Execution of toJson ");

        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);

        logger.info("Finished Execution of toJson ");
    }
}
