package com.rokittech.container.ecom.customer.factory;

import javax.inject.Inject;

import com.google.inject.Provider;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Input;
import com.rokittech.container.base.defaults.Output;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceParams;
import com.rokittech.container.ecom.customer.defaults.CustomerVocabulary;
import com.rokittech.container.ecom.customer.service_0_0_1.CustomerService_0_0_1;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.containers.api.core.Service;
/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */

public class CustomerServiceFactory implements Provider<Service<Input, Output, 
        CustomerServiceParams, DefaultPersist>> {

    private Service service;

    /**
     * This is the place where you are managing different versions
     *
     * @param version
     */
//    @Inject
    public CustomerServiceFactory(int version) {
        this(null, version);

    }

    public CustomerServiceFactory(String appId, int version) {
        
        if (appId != null) {
            CustomerVocabulary app = CustomerVocabulary.valueOf(appId.toUpperCase());
            switch (app) {
                case WEI_ECOMMERCE:
                    weiEcommerce(version);
                    break;
                case TEST_ECOMMERCE:
                    testEcommerce(version);
                    break;
                default:
                    weiEcommerce( version);
                    break;
            }
        } else {
            weiEcommerce(version);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Service get() {
        return service;
    }

    private void weiEcommerce(int version) {

        switch (version) {
            case 1:
                this.service = new CustomerService_0_0_1();
            	
                break;
            case 2:

                break;
            default:

                break;

        }
    }

    private void testEcommerce(int version) {
        switch (version) {
            case 1:
                this.service = new CustomerService_0_0_1(DBType.TEST);
                break;
            case 2:

                break;
            default:

                break;
        }
    }

}
