/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.customer.service_0_0_1;

import com.google.gson.Gson;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.core.AbstractService;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceInput;

import com.rokittech.container.ecom.customer.defaults.CustomerServiceOutput;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceParams;
import com.rokittech.container.ecom.customer.defaults.CustomerVocabulary;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterManager;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.container.ecom.dbadapter.factory.DBManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class CustomerService_0_0_1 extends AbstractService <CustomerServiceInput, CustomerServiceOutput, CustomerServiceParams, DefaultPersist> {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(CustomerService_0_0_1.class);
    private CustomerServiceInput   input;
    private CustomerServiceOutput  output;
    private CustomerServiceParams  params;
    
    private DefaultPersist          persist;

    private String inputAsJson;
    private String outputAsJson;
    
    
    private final DBAdapterManager dbclientManager;
        
    public CustomerService_0_0_1() {
        dbclientManager = new DBManagerFactory().get();
    }

    public CustomerService_0_0_1(DBType dbType) {
        dbclientManager = new DBManagerFactory(dbType).get();
    }
    
    @Override
    public String serviceId() {
        return this.getClass().getName();
    }

    @Override
    public String getInputAsJson() {
        return input.toJson();
    }


    @Override
    public String getOutputAsJson() {
        return output.toJson();
    }

    @Override
    public void setInputAsJson(String input) {
        this.input = this.input.fromJson(input);
        this.inputAsJson = input;
    }

    @Override
    public void setOutputAsJson(String output) {
        this.output = this.output.fromJson(output);
        this.outputAsJson = output;
    }


    @Override
    public CustomerServiceOutput serve() {//TODO thorows ServiceException
        slf4jLogger.info("Inside serve() ");
        DBHandlerInput serviceinput = new DBHandlerInput();
        serviceinput.setData(this.getInput().getData().toJson());
        
        DBHandlerOutput serviceoutput = new DBHandlerOutput();
        // This is real mandatory code to move in controller
        DBHandlertParams serviceparams = new DBHandlertParams();
        serviceparams.setCommand(getParams().getProperties().getProperty(CustomerVocabulary.COMMAND.name()));
        serviceparams.setEntity(params.getProperties().getProperty(CustomerVocabulary.COLLECTION.name()));
        
        dbclientManager.setInput(serviceinput);
        dbclientManager.setOutput(serviceoutput);
        dbclientManager.setParams(serviceparams);
        dbclientManager.setPersist(new DefaultPersist());
        
        DBHandlerOutput serviceOutput = (DBHandlerOutput) dbclientManager.serve();
      
        output.setData(serviceOutput.getData());
        output.setInfo(input.getInfo());

        slf4jLogger.info("Created output object " + output.getClass());
        
        return output;
    }

    @Override
    public String toJson() {
        return new Gson().toJson(this, CustomerService_0_0_1.class);
    }

    @Override
    public CustomerServiceInput getInput() {
        return input;
    }

    @Override
    public void setInput(CustomerServiceInput input) {
        this.input = input;
    }

    @Override
    public CustomerServiceOutput getOutput() {
        return output;
    }

    @Override
    public void setOutput(CustomerServiceOutput output) {
        this.output = output;
    }

    @Override
    public CustomerServiceParams getParams() {
        return params;
    }

    @Override
    public void setParams(CustomerServiceParams params) {
        this.params = params;
    }

    public DBAdapterManager getDbclientManager() {
        return dbclientManager;
    }
    
    
    
}