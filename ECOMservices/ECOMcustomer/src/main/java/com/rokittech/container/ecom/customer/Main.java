/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.customer;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.core.PasswordUtil;
import com.rokittech.container.ecom.commons.models.Address;
import com.rokittech.container.ecom.commons.models.Customer;
import com.rokittech.container.ecom.commons.models.Phone;
import com.rokittech.container.ecom.commons.models.UserCredentials;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceInput;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceOutput;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceParams;
import com.rokittech.container.ecom.customer.defaults.CustomerVocabulary;
import com.rokittech.container.ecom.customer.factory.CustomerServiceFactory;
import com.rokittech.containers.api.core.Service;
import java.util.Date;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);

        //This code is temp code of UserAddress for POC
        Address address = new Address();
        address.setStreet1("test");
        address.setStreet2("test2");
        address.setCity("Bangalore");
        address.setState("Karnataka");
        address.setCountry("India");
        address.setZipCode("560037");

        //This code is temp code of PhoneInfo for POC
        Phone phone = new Phone();
        phone.setCountryCode("91");
        phone.setAreaCode("80");
        phone.setPhoneNo("11223344");

//        //This User code is temp code for POC
//         User user = new User();
//        user.setFirstName("Deependra");
//        user.setLastName("Kumar");
//        user.setCompany("Rokitt");
//        user.setCreatedDate(new Date());
//        user.setPhoneNo("9845555760");
        
        Customer user = new Customer();
        user.setFirstName("Deependra");
        user.setLastName("Kumar");
        user.setCompany("Rokitt");
        user.setCreatedDate(new Date());
        user.setPhoneNo("9845555760");

        //This code is temp code of UserCredentials for POC
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("deependra@gmail.com");
        userCredentials.setHintQuestion("What is your pet name");
        userCredentials.setHintAnswer("Test");
        userCredentials.setUserName(userCredentials.getEmail());
        userCredentials.setResetPassword(true);
        userCredentials.setPassword(PasswordUtil.generateSecurePassword("rokit2015"));

        //This User code is temp code for POC
       
        

        String userJson = user.toJson();

        // This is real mandatory code to move in controller
        CustomerServiceParams params = new CustomerServiceParams();
        //params.initEnv("env.properties");
        params.getProperties().put(CustomerVocabulary.COLLECTION.name(), "User");
        params.getProperties().put(CustomerVocabulary.COMMAND.name(), CustomerVocabulary.INSERT.name());

        DefaultPersist persist = new DefaultPersist();
        //persist.initEnv("env.properties");

//        CrudNoSqlServiceInput json = new CrudNoSqlServiceInput();
        //
        CustomerServiceInput inputTemp = new CustomerServiceInput();
        inputTemp.setData(user);
        inputTemp.setInfo(info);

//        CrudNoSqlService_0_0_1 service;        
        Service<CustomerServiceInput, CustomerServiceOutput, CustomerServiceParams, DefaultPersist> service = new CustomerServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new CustomerServiceOutput());

        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
