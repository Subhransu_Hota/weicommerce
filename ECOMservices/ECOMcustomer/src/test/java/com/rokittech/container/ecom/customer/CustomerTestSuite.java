package com.rokittech.container.ecom.customer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.rokittech.container.ecom.customer.service_0_0_1.CustomerService_0_0_1DeleteCustomerTest;
import com.rokittech.container.ecom.customer.service_0_0_1.CustomerService_0_0_1FindByIdCustomerTest;
import com.rokittech.container.ecom.customer.service_0_0_1.CustomerService_0_0_1InsertCustomerTest;
import com.rokittech.container.ecom.customer.service_0_0_1.CustomerService_0_0_1UpdateCustomerTest;
import com.rokittech.container.ecom.customer.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    CustomerService_0_0_1InsertCustomerTest.class,
    CustomerService_0_0_1UpdateCustomerTest.class,
    CustomerService_0_0_1DeleteCustomerTest.class,
    CustomerService_0_0_1FindByIdCustomerTest.class,
})
public class CustomerTestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Initiated Mongo database");

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Dropping tables... ");
        TestUtil.getInstance().cleanup ();
        
    }
    
}
