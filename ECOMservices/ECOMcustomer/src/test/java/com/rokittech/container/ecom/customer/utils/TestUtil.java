/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.customer.utils;

import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.models.Customer;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceParams;
import com.rokittech.container.ecom.customer.factory.CustomerServiceFactory;
import com.rokittech.container.ecom.customer.service_0_0_1.CustomerService_0_0_1;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.net.InetSocketAddress;
import java.util.Date;
import org.bson.types.ObjectId;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class TestUtil {
    private final CustomerService_0_0_1 service;
    
    private static final CustomerServiceParams params = new CustomerServiceParams();
    private MongoClient client;
    private MongoServer server;

    private TestUtil() {
        service = (CustomerService_0_0_1) new CustomerServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
        ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        //params.initEnv("env.properties");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }

    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public Customer createCategory(String catalogId) {
        Customer customer = new Customer();
        customer.setFirstName("Deependra");
        customer.setLastName("Kumar");
        customer.setCompany("Rokitt");
        customer.setCreatedDate(new Date());
        customer.setPhoneNo("9845555760");

        return customer;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);

        return info;
    }

    public boolean isCustomerEqual(String input, String output) {
        System.out.println("input: " + input);
        System.out.println("output: " + output);

        Customer inputCustomer = new Customer().fromJson(input);
        Customer outputCustomer = new Customer().fromJson(output);

        boolean b = inputCustomer.getFirstName().equalsIgnoreCase(outputCustomer.getFirstName())
                && inputCustomer.getLastName().equalsIgnoreCase(outputCustomer.getLastName())
                && inputCustomer.getCompany().equalsIgnoreCase(outputCustomer.getCompany())
                && inputCustomer.getPhoneNo().equalsIgnoreCase(outputCustomer.getPhoneNo());

        return b;
    }

    public CustomerServiceParams getParams() {
        return params;
    }

    public Customer createNewCustomerInDatabase(String catalogId) {

        Customer customer = new Customer();
        customer.setFirstName("Deependra");
        customer.setLastName("Kumar");
        customer.setCompany("Rokitt");
        customer.setCreatedDate(new Date());
        customer.setPhoneNo("9845555760");
        
        DBObject object = (DBObject) JSON.parse(customer.toJson());
        object.put("is_deleted", false);

        client.getDatabase("ecommerce_test").getCollection("Customer", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId) object.get("_id");
        object.put("_id", objId.toString());

        return new Customer().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObject(String result, String input) {
        Customer inputCustomer = new Customer ().fromJson(input);
        Customer outputCustomer = new Customer ().fromJson(result);
        
        return inputCustomer.getId().equalsIgnoreCase(outputCustomer.getId());
    }

    public void cleanup() {
        client.getDatabase("ecommerce_test").getCollection("Customer").drop();
        client.getDatabase("ecommerce_test").getCollection("Product").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    public CustomerService_0_0_1 getService() {
        return service;
    }

    
    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }
}
