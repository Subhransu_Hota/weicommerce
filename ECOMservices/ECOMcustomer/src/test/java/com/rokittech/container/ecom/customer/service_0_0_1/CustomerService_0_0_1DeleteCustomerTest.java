/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.customer.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.customer.utils.TestUtil;
import com.rokittech.container.ecom.commons.models.Customer;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceInput;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceOutput;
import com.rokittech.container.ecom.customer.defaults.CustomerServiceParams;
import com.rokittech.container.ecom.customer.defaults.CustomerVocabulary;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class CustomerService_0_0_1DeleteCustomerTest {
    public CustomerService_0_0_1DeleteCustomerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        CustomerServiceInput input = new CustomerServiceInput();
        Customer customer = TestUtil.getInstance ().createNewCustomerInDatabase (
                "test_customer_" + new Random ().nextInt(Integer.MAX_VALUE));
        
        input.setData(customer);
        input.setInfo(TestUtil.getInstance().createInfo());
        
        CustomerServiceOutput output = new CustomerServiceOutput();
        // This is real mandatory code to move in controller
        CustomerServiceParams params = TestUtil.getInstance().getParams ();
       
        params.getProperties().put(CustomerVocabulary.COLLECTION.name(), "Customer");
        params.getProperties().put(CustomerVocabulary.COMMAND.name(), CustomerVocabulary.DELETE.name());
        
        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInputAsJson method, of class CustomerService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        System.out.println("getInputAsJson");
        String result = TestUtil.getInstance().getService().getInputAsJson();
        CustomerServiceInput input = new CustomerServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
    }

    /**
     * Test of serve method, of class CustomerService_0_0_1.
     */
    @Test
    public void testServe() {
        System.out.println("serve");
        CustomerServiceOutput result = TestUtil.getInstance().getService().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isCustomerEqual (
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                result.getData()));
    }

    /**
     * Test of getOutputAsJson method, of class CustomerService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        System.out.println("getOutputAsJson");
        
        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        CustomerServiceOutput output = new CustomerServiceOutput().fromJson(result);
        
        assertTrue("input and output are not same", TestUtil.getInstance().isCustomerEqual (
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                output.getData()));
    }
    
    /**
     * Test of toJson method, of class CustomerService_0_0_1.
     */
    @Test
    public void testToJson() {
        System.out.println("toJson");
        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
    }
}
