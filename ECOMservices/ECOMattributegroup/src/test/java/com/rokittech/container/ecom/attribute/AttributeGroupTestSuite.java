package com.rokittech.container.ecom.attribute;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.rokittech.container.ecom.attribute.service_0_0_1.AttributeGroupService_0_0_1DeleteAttributeGroupTest;
import com.rokittech.container.ecom.attribute.service_0_0_1.AttributeGroupService_0_0_1FindByIdAttributeGroupTest;
import com.rokittech.container.ecom.attribute.service_0_0_1.AttributeGroupService_0_0_1InsertAttributeGroupTest;
import com.rokittech.container.ecom.attribute.service_0_0_1.AttributeGroupService_0_0_1UpdateAttributeGroupTest;
import com.rokittech.container.ecom.attribute.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    AttributeGroupService_0_0_1InsertAttributeGroupTest.class,
    AttributeGroupService_0_0_1UpdateAttributeGroupTest.class,
    AttributeGroupService_0_0_1DeleteAttributeGroupTest.class,
    AttributeGroupService_0_0_1FindByIdAttributeGroupTest.class,})
public class AttributeGroupTestSuite {

    private static final Logger logger = LoggerFactory.getLogger(AttributeGroupTestSuite.class);

    @BeforeClass
    public static void setUpClass() throws Exception {

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        logger.info("Initiated Mongo database");

        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        logger.info("Cleanup Mongo database");

        TestUtil.getInstance().cleanup();

    }

}
