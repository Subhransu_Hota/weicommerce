/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.attribute.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupServiceInput;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupServiceOutput;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupServiceParams;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupVocabulary;
import com.rokittech.container.ecom.attribute.utils.TestUtil;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class AttributeGroupService_0_0_1InsertAttributeGroupTest {

    private static final Logger logger = LoggerFactory.getLogger(AttributeGroupService_0_0_1InsertAttributeGroupTest.class);

    public AttributeGroupService_0_0_1InsertAttributeGroupTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        logger.info("Setup test resouces");

        
        AttributeGroupServiceInput input = new AttributeGroupServiceInput();
        input.setData(TestUtil.getInstance().createAttribute(
                "test_address_" + new Random ().nextInt(Integer.MAX_VALUE)));
        input.setInfo(TestUtil.getInstance().createInfo());

        AttributeGroupServiceOutput output = new AttributeGroupServiceOutput();
        // This is real mandatory code to move in controller
        AttributeGroupServiceParams params = TestUtil.getInstance().getParams();

        params.getProperties().put(AttributeGroupVocabulary.COLLECTION.name(), "Attribute");
        params.getProperties().put(AttributeGroupVocabulary.COMMAND.name(), AttributeGroupVocabulary.INSERT.name());

        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }

    @After
    public void tearDown() {
        logger.info("Cleanup all test resouces");
    }

    /**
     * Test of getInputAsJson method, of class AttributeService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        logger.info("Started Execution of getInputAsJson ");

        String result = TestUtil.getInstance().getService().getInputAsJson();
        AttributeGroupServiceInput input = new AttributeGroupServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);

        logger.info("Finished Execution of getInputAsJson ");
    }

    /**
     * Test of serve method, of class AttributeService_0_0_1.
     */
    @Test
    public void testServe() {
        logger.info("Started Execution of serve ");

        AttributeGroupServiceOutput result = TestUtil.getInstance().getService().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isAttributeEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                result.getData()));

        logger.info("Finished Execution of serve ");
    }

    /**
     * Test of getOutputAsJson method, of class AttributeService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        logger.info("Started Execution of getOutputAsJson ");

        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        AttributeGroupServiceOutput output = new AttributeGroupServiceOutput().fromJson(result);

        assertTrue("input and output are not same", TestUtil.getInstance().isAttributeEqual(
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                output.getData()));

        logger.info("Finished Execution of getOutputAsJson ");
    }

    /**
     * Test of toJson method, of class AttributeService_0_0_1.
     */
    @Test
    public void testToJson() {
        logger.info("Started Execution of toJson ");

        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);

        logger.info("Finished Execution of toJson ");
    }
}
