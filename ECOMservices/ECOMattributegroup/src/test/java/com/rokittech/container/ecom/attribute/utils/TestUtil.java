/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.attribute.utils;

import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupServiceParams;
import com.rokittech.container.ecom.attributegroup.factory.AttributeGroupServiceFactory;
import com.rokittech.container.ecom.attributegroup.service_0_0_1.AttributeGroupService_0_0_1;
import com.rokittech.container.ecom.commons.models.AttributeGroup;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.net.InetSocketAddress;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 28th Sep 2015
 */
public class TestUtil {

    private static final Logger logger = LoggerFactory.getLogger(TestUtil.class);

    private static final AttributeGroupServiceParams params = new AttributeGroupServiceParams();
    private final AttributeGroupService_0_0_1 service;
    private MongoClient client;
    private MongoServer server;

    private TestUtil() {
        service = (AttributeGroupService_0_0_1) new AttributeGroupServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAttribute = server.bind();
        client = new MongoClient(new ServerAddress(serverAttribute));

        //Initiate Mongodb
        ((MongoManager) service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        logger.info("Mongo Db connection created succesfully");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }

    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public AttributeGroup createAttribute(String userId) {
        AttributeGroup attribute = new AttributeGroup();
        attribute.setName("attributegroup_name_1");
        return attribute;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);

        return info;
    }

    public boolean isAttributeEqual(String input, String output) {
        logger.info("Input :" + input + "\n Output : " + output);

        AttributeGroup inputAttributeGroup = new AttributeGroup().fromJson(input);
        AttributeGroup outputAttributeGroup = new AttributeGroup().fromJson(output);

        return inputAttributeGroup.getName().equalsIgnoreCase(outputAttributeGroup.getName());
    }

    public AttributeGroupServiceParams getParams() {
        return params;
    }

    public AttributeGroup createNewAttributeGroupInDatabase(String userId) {

        AttributeGroup attribute = new AttributeGroup();

        attribute.setName("attributegroup_name_1");
       
        DBObject object = (DBObject) JSON.parse(attribute.toJson());

        client.getDatabase("ecommerce_test").getCollection("AttributeGroup", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId) object.get("_id");
        object.put("_id", objId.toString());

        return new AttributeGroup().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObject(String result, String input) {
        AttributeGroup inputAttributeGroup = new AttributeGroup().fromJson(input);
        AttributeGroup outputAttributeGroup = new AttributeGroup().fromJson(result);

        return inputAttributeGroup.getId().equalsIgnoreCase(outputAttributeGroup.getId());
    }

    public void cleanup() {
        logger.info("Cleanup test data");
        client.getDatabase("ecommerce_test").getCollection("AttributeGroup").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }

    public AttributeGroupService_0_0_1 getService() {
        return service;
    }
}