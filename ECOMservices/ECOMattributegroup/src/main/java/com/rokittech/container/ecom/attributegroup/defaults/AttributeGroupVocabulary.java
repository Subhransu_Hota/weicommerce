/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.attributegroup.defaults;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public enum AttributeGroupVocabulary {
    INSERT,
    RETRIEVE,
    RETRIEVEBYID,
    LIST,
    UPDATE,
    DELETE,
    COLLECTION,
    LIMIT, 
    COMMAND,
    ECOMMERCE,
    WEI_ECOMMERCE,
    TEST_ECOMMERCE,
    MONGODB,
    HAZELCAST,
    USERS, 
    QUERY,
    TEMPLATE;
}
