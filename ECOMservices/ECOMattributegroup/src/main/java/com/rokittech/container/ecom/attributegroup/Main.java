/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.attributegroup;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupServiceInput;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupServiceOutput;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupServiceParams;
import com.rokittech.container.ecom.attributegroup.defaults.AttributeGroupVocabulary;
import com.rokittech.container.ecom.attributegroup.factory.AttributeGroupServiceFactory;
import com.rokittech.container.ecom.commons.models.AttributeGroup;
import com.rokittech.containers.api.core.Service;
import java.util.Arrays;

/**
 *
 * @author Deependra Kumar
 * @CreatedDate 18th Sep 2015
 */
public class Main {

    public static void main(String[] args) {
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("Deependra");
        info.setVersion(1);

        //This code is temp code of UserAttribute for POC
        
        // Temp User Id 
        String userId ="560b7d980e8543233407bc2a";
        //This code is temp code of UserAttribute for POC
        AttributeGroup attributeGroup = new AttributeGroup();
        attributeGroup.setName("Mobile");
        attributeGroup.setAttributes(Arrays.asList("Screen Size", "Processor", "Resolution", "Camera", "Front-Camera", "Back-Camera"));
        
       // This is real mandatory code to move in controller
        AttributeGroupServiceParams params = new AttributeGroupServiceParams();
        //params.initEnv("env.properties");
        
        //MongoClient mongo = new MongoClient("weicommerce-backend-dev.elasticbeanstalk.com", 27017);
//        MongoClient mongo = new MongoClient("localhost", 27017);
//        MongoDatabase db = mongo.getDatabase("ecommerce");
//        
//        params.setEnv(db);
        params.getProperties().put(AttributeGroupVocabulary.COLLECTION.name(), "Attribute");
        params.getProperties().put(AttributeGroupVocabulary.COMMAND.name(), AttributeGroupVocabulary.INSERT.name());

        DefaultPersist persist = null;//new DefaultPersist();
        //persist.initEnv("env.properties");

//        CrudNoSqlServiceInput json = new CrudNoSqlServiceInput();
        //
        AttributeGroupServiceInput inputTemp = new AttributeGroupServiceInput();
        inputTemp.setData(attributeGroup);
        inputTemp.setInfo(info);
        
        System.out.println (inputTemp.toJson());

//        CrudNoSqlService_0_0_1 service;        
        Service<AttributeGroupServiceInput, AttributeGroupServiceOutput, AttributeGroupServiceParams, DefaultPersist> service = new AttributeGroupServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        //service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new AttributeGroupServiceOutput());

        service.serve();

        System.out.println(service.getOutput().getData());
    }

}
