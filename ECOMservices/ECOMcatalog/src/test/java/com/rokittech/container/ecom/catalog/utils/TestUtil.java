/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.catalog.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceParams;
import com.rokittech.container.ecom.catalog.factory.CatalogServiceFactory;
import com.rokittech.container.ecom.catalog.service_0_0_1.CatalogService_0_0_1;
import com.rokittech.container.ecom.commons.models.Catalog;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.List;
import org.bson.types.ObjectId;

/**
 *
 * @author sangamesh
 */
public class TestUtil {
    private static final CatalogServiceParams params = new CatalogServiceParams();
    private final CatalogService_0_0_1 service;
    
    private MongoClient client;
    private MongoServer server;
    
    private TestUtil() {
        service = (CatalogService_0_0_1) new CatalogServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
        ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
    }

    public CatalogService_0_0_1 getService() {
        return service;
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }
    
    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public Catalog createCategory(String catalogId) {
        Catalog catalog = new Catalog ();
        catalog.setCatalogId(catalogId);
        catalog.setDescription("descr");
        catalog.setName("name");
        catalog.setParentCatalogId("0");
        
        return catalog;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        return info;
    }

    public boolean isCatalogEqual(String input, String output) {
        System.out.println ("input: " + input);
        System.out.println("output: " + output);
        
        Catalog inputCatalog = new Catalog ().fromJson(input);
        Catalog outputCatalog = new Catalog ().fromJson(output);
        
        boolean b =  inputCatalog.getCatalogId().equalsIgnoreCase(outputCatalog.getCatalogId()) &&
                inputCatalog.getDescription().equalsIgnoreCase(outputCatalog.getDescription()) &&
                inputCatalog.getName().equalsIgnoreCase(outputCatalog.getName()) &&
                inputCatalog.getParentCatalogId().equalsIgnoreCase(outputCatalog.getParentCatalogId());
        
        return b;
    }

    public CatalogServiceParams getParams() {
        return params;
    }

    public Catalog createNewCategoryInDatabase (String catalogId) {
        Catalog catalog = new Catalog ();
        catalog.setCatalogId(catalogId);
        catalog.setDescription("descr");
        catalog.setName("name");
        catalog.setParentCatalogId("0");
        DBObject object = (DBObject) JSON.parse(catalog.toJson());
        object.put("is_deleted", false);
        
        client.getDatabase("ecommerce_test").getCollection("Catalog", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId)object.get("_id");
        object.put("_id", objId.toString());
        
        return new Catalog ().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObjectInList(String result, String input) {
        Catalog inputCatalog = new Catalog ().fromJson(input);
        
        Type listOfTestObject = new TypeToken<List<Catalog>>(){}.getType();
        List<Catalog> list = new Gson().fromJson(result, listOfTestObject);
        
        boolean flag = false;
        for (Catalog catalog : list) {
            if (catalog.getId().equalsIgnoreCase(inputCatalog.getId())){
                flag = true;
                break;
            }
        }
        
        return flag;
    }
    
    public boolean hasInputObject(String result, String input) {
        Catalog inputCatalog = new Catalog ().fromJson(input);
        Catalog outputCatalog = new Catalog ().fromJson(result);
        
        return inputCatalog.getId().equalsIgnoreCase(outputCatalog.getId());
    }

    public void cleanup() {
        client.getDatabase("ecommerce_test").getCollection("Catalog").drop();
        client.getDatabase("ecommerce_test").getCollection("Product").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }
    
    
    private static class TestDataCreationHolder {

        private static final TestUtil INSTANCE = new TestUtil();
    }
}
