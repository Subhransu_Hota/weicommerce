/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.catalog;

import com.rokittech.container.ecom.catalog.service_0_0_1.CatalogService_0_0_1DeleteCategoryTest;
import com.rokittech.container.ecom.catalog.service_0_0_1.CatalogService_0_0_1FindByIdCategoryTest;
import com.rokittech.container.ecom.catalog.service_0_0_1.CatalogService_0_0_1InsertCategoryTest;
import com.rokittech.container.ecom.catalog.service_0_0_1.CatalogService_0_0_1ListCategoryTest;
import com.rokittech.container.ecom.catalog.service_0_0_1.CatalogService_0_0_1UpdateCategoryTest;
import com.rokittech.container.ecom.catalog.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author sangamesh
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    CatalogService_0_0_1InsertCategoryTest.class,
    CatalogService_0_0_1UpdateCategoryTest.class,
    CatalogService_0_0_1DeleteCategoryTest.class,
    CatalogService_0_0_1FindByIdCategoryTest.class,
    CatalogService_0_0_1ListCategoryTest.class
})

public class CatalogTestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Initiated Mongo database");

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Dropping tables... ");
        TestUtil.getInstance().cleanup ();
        
    }
}
