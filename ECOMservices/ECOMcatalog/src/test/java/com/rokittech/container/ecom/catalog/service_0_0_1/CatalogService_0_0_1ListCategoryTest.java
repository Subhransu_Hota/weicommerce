/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.catalog.service_0_0_1;

import com.google.gson.Gson;
import com.google.gson.internal.StringMap;
import com.google.gson.reflect.TypeToken;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.catalog.utils.TestUtil;
import com.rokittech.container.ecom.commons.models.Catalog;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceInput;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceOutput;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceParams;
import com.rokittech.container.ecom.catalog.defaults.CatalogVocabulary;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sangamesh
 */
public class CatalogService_0_0_1ListCategoryTest {
    public CatalogService_0_0_1ListCategoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        TestUtil.getInstance ().createNewCategoryInDatabase (
                "test_category_" + new Random ().nextInt(Integer.MAX_VALUE));
        
        CatalogServiceInput input = new CatalogServiceInput();
        input.setData(new Catalog ());
        input.setInfo(TestUtil.getInstance().createInfo());
        
        CatalogServiceOutput output = new CatalogServiceOutput();
        // This is real mandatory code to move in controller
        CatalogServiceParams params = TestUtil.getInstance().getParams ();
       
        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), "Catalog");
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.LIST.name());
        
        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInputAsJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        System.out.println("getInputAsJson");
        String result = TestUtil.getInstance().getService().getInputAsJson();
        CatalogServiceInput input = new CatalogServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
    }

    /**
     * Test of serve method, of class CatalogService_0_0_1.
     */
    @Test
    public void testServe() {
        System.out.println("serve");
        CatalogServiceOutput result = TestUtil.getInstance().getService().serve();
        System.out.println (result.getData().getClass().getName());
        assertNotEquals("Result does not have object ", 
                TestUtil.getInstance().hasInputObjectInList (result.getData(), TestUtil.getInstance().getService().getInput().getData().toJson()));
    }

    /**
     * Test of getOutputAsJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        System.out.println("getOutputAsJson");
        
        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        CatalogServiceOutput output = new CatalogServiceOutput().fromJson(result);
        
        assertNotEquals("Result does not have object ", 
                TestUtil.getInstance().hasInputObjectInList(output.getData(), TestUtil.getInstance().getService().getInput().getData().toJson()));
    }
    
    /**
     * Test of toJson method, of class CatalogService_0_0_1.
     */
    @Test
    public void testToJson() {
        System.out.println("toJson");
        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
    }
}
