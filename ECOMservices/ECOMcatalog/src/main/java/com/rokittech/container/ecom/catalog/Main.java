/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.catalog;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceInput;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceOutput;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceParams;
import com.rokittech.container.ecom.catalog.defaults.CatalogVocabulary;
import com.rokittech.container.ecom.catalog.factory.CatalogServiceFactory;
import com.rokittech.container.ecom.commons.models.Catalog;
import com.rokittech.containers.api.core.Service;



/**
 *
 * @author alexmylnikov
 */
public class Main {

    public static void main(String[] args) {
        
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        Catalog catalog = new Catalog ();
        catalog.setCatalogId("catalog_1");
        catalog.setDescription("descr");
        catalog.setName("name");
        catalog.setParentCatalogId("0");
        
        // This is real mandatory code to move in controller
        CatalogServiceParams params = new CatalogServiceParams();
        
//        MongoClient mongo = new MongoClient("weicommerce-backend-dev.elasticbeanstalk.com", 27017);
//        MongoDatabase db = mongo.getDatabase("ecommerce");
//        
//        //params.initEnv("env.properties");
//        params.setEnv(db);
        params.getProperties().put(CatalogVocabulary.COLLECTION.name(), "Catalog");
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.INSERT.name());

        DefaultPersist persist = new DefaultPersist();
        CatalogServiceInput inputTemp = new CatalogServiceInput();
        inputTemp.setData(catalog);
        inputTemp.setInfo(info);
      
        System.out.println ("***" + inputTemp.toJson());
        Service<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> service = new CatalogServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new CatalogServiceOutput());

        System.out.println(service.serve().getData());
         
        params.getProperties().put(CatalogVocabulary.COMMAND.name(), CatalogVocabulary.RETRIEVE.name());
        Catalog catalog1 = new Catalog ();
        catalog.setCatalogId("catalog_1");
        
        inputTemp.setData(catalog1);
               
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        
        System.out.println(service.getOutput().getData());
    }

}
