/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.catalog.service_0_0_1;

import com.google.gson.Gson;
import com.rokittech.container.base.defaults.DefaultPersist;

import com.rokittech.container.ecom.catalog.defaults.CatalogServiceInput;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceOutput;
import com.rokittech.container.ecom.catalog.defaults.CatalogServiceParams;
import com.rokittech.container.ecom.catalog.defaults.CatalogVocabulary;
import com.rokittech.container.ecom.commons.core.AbstractService;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterManager;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams.DBType;
import com.rokittech.container.ecom.dbadapter.factory.DBManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author deependr
 */
public class CatalogService_0_0_1 extends AbstractService<CatalogServiceInput, CatalogServiceOutput, CatalogServiceParams, DefaultPersist> {
    private static final Logger slf4jLogger = LoggerFactory.getLogger(CatalogService_0_0_1.class);
    private CatalogServiceInput input;
    private CatalogServiceOutput output;
    private CatalogServiceParams params;
    private final DBAdapterManager dbclientManager;
        
    public CatalogService_0_0_1() {
        dbclientManager = new DBManagerFactory().get();
    }
    
    public CatalogService_0_0_1(DBType dbType) {
        dbclientManager = new DBManagerFactory(dbType).get();
    }

    public DBAdapterManager getDbclientManager() {
        return dbclientManager;
    }
   
    @Override
    public String serviceId() {
        return this.getClass().getName();
    }

    @Override
    public String getInputAsJson() {
        slf4jLogger.info("returning Input Object in the form of JSON");
        
        return input.toJson();
    }

    @Override
    public String getOutputAsJson() {
        slf4jLogger.info("returning Output Object in the form of JSON");
        return output.toJson();
    }

    @Override
    public void setInputAsJson(String input) {
        slf4jLogger.info("Recreating Input object from json");
        this.input = this.input.fromJson(input);
    }

    @Override
    public void setOutputAsJson(String output) {
        slf4jLogger.info("Recreating Ouput object from json");
        this.output = this.output.fromJson(output);
    }

    @Override
    public CatalogServiceOutput serve() {
        slf4jLogger.info("Inside serve() ");
        DBHandlerInput serviceinput = new DBHandlerInput();
        serviceinput.setData(this.getInput().getData().toJson());
        
        DBHandlerOutput serviceoutput = new DBHandlerOutput();
        // This is real mandatory code to move in controller
        DBHandlertParams serviceparams = new DBHandlertParams();
        serviceparams.setCommand(getParams().getProperties().getProperty(CatalogVocabulary.COMMAND.name()));
        serviceparams.setEntity(params.getProperties().getProperty(CatalogVocabulary.COLLECTION.name()));
        
        dbclientManager.setInput(serviceinput);
        dbclientManager.setOutput(serviceoutput);
        dbclientManager.setParams(serviceparams);
        dbclientManager.setPersist(new DefaultPersist());
        
        DBHandlerOutput serviceOutput = (DBHandlerOutput) dbclientManager.serve();
      
        output.setData(serviceOutput.getData());
        output.setInfo(input.getInfo());

        slf4jLogger.info("Created output object " + output.getClass());
        
        return output;
    }

    @Override
    public String toJson() {
        slf4jLogger.info("generating json for service object");
        return new Gson().toJson(this, CatalogService_0_0_1.class);
    }

    @Override
    public CatalogServiceInput getInput() {
        return input;
    }

    @Override
    public void setInput(CatalogServiceInput input) {
        this.input = input;
    }

    @Override
    public CatalogServiceOutput getOutput() {
        return output;
    }

    @Override
    public void setOutput(CatalogServiceOutput output) {
        this.output = output;
    }

    @Override
    public CatalogServiceParams getParams() {
        return params;
    }

    @Override
    public void setParams(CatalogServiceParams params) {
        this.params = params;
    }
}
