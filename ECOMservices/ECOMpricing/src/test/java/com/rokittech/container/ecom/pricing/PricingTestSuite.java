/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.pricing;

import com.rokittech.container.ecom.pricing.service_0_0_1.PricingService_0_0_1DeletePricingTest;
import com.rokittech.container.ecom.pricing.service_0_0_1.PricingService_0_0_1FindByIdPricingTest;
import com.rokittech.container.ecom.pricing.service_0_0_1.PricingService_0_0_1InsertPricingTest;
import com.rokittech.container.ecom.pricing.service_0_0_1.PricingService_0_0_1UpdatePricingTest;
import com.rokittech.container.ecom.pricing.utils.TestUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author sangamesh
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    PricingService_0_0_1InsertPricingTest.class,
    PricingService_0_0_1UpdatePricingTest.class,
    PricingService_0_0_1DeletePricingTest.class,
    PricingService_0_0_1FindByIdPricingTest.class
})

public class PricingTestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Initiated Mongo database");

    }

    @AfterClass
    public static void tearDownClass() throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        //Initiate Mongodb
        TestUtil.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Dropping tables... ");
        TestUtil.getInstance().cleanup ();
        
    }
}
