/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.pricing.utils;

import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceParams;
import com.rokittech.container.ecom.commons.core.Money;
import com.rokittech.container.ecom.commons.models.Pricing;
import com.rokittech.container.ecom.dbadapter.mongo.manager.MongoManager;
import com.rokittech.container.ecom.pricing.factory.PricingServiceFactory;
import com.rokittech.container.ecom.pricing.service_0_0_1.PricingService_0_0_1;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import java.net.InetSocketAddress;
import java.util.Date;
import java.util.Locale;
import org.bson.types.ObjectId;

/**
 *
 * @author sangamesh
 */
public class TestUtil {
    private static final PricingServiceParams params = new PricingServiceParams();
    private final PricingService_0_0_1 service;
    private MongoClient client;
    private MongoServer server;
    
    private TestUtil() {
        service = (PricingService_0_0_1) new PricingServiceFactory("test_ecommerce", 1).get();
        server = new MongoServer(new MemoryBackend());

        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();
        client = new MongoClient(new ServerAddress(serverAddress));

        //Initiate Mongodb
        ((MongoManager)service.getDbclientManager()).setDb(client.getDatabase("ecommerce_test"));
        //params.initEnv("env.properties");
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

    public MongoServer getServer() {
        return server;
    }

    public void setServer(MongoServer server) {
        this.server = server;
    }
    
    public static TestUtil getInstance() {
        return TestDataCreationHolder.INSTANCE;
    }

    public Pricing createPricing(String pricingId) {
        Pricing pricing = new Pricing();
        pricing.setMoney(new Money(new Locale("en", "IN"), 10.00));
        pricing.setPricingFromDate(new Date ().toString());
        pricing.setPricingToDate(new Date ().toString());
        pricing.setPricingId(pricingId);
        pricing.setPricingPurposeType(Pricing.PricingPurposeType.PURCHASE);
        pricing.setPricingType(Pricing.PricingType.LIST_PRICE);
        
        return pricing;
    }

    public Info createInfo() {
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        return info;
    }

    public boolean isPricingEqual(String input, String output) {
        System.out.println ("input: " + input);
        System.out.println("output: " + output);
        
        Pricing inputPricing = new Pricing ().fromJson(input);
        Pricing outputPricing = new Pricing ().fromJson(output);
        
        return inputPricing.getPricingId().equalsIgnoreCase(outputPricing.getPricingId());
    }

    public PricingServiceParams getParams() {
        return params;
    }

    public Pricing createNewPricingInDatabase (String pricingId) {
        Pricing pricing = new Pricing();
        pricing.setMoney(new Money(new Locale("en", "IN"), 10.00));
        pricing.setPricingFromDate(new Date ().toString());
        pricing.setPricingToDate(new Date ().toString());
        pricing.setPricingId(pricingId);
        pricing.setPricingPurposeType(Pricing.PricingPurposeType.PURCHASE);
        pricing.setPricingType(Pricing.PricingType.LIST_PRICE);
        
        DBObject object = (DBObject) JSON.parse(pricing.toJson());
        object.put("is_deleted", false);
        
        client.getDatabase("ecommerce_test").getCollection("Pricing", DBObject.class).insertOne(object);
        ObjectId objId = (ObjectId)object.get("_id");
        object.put("_id", objId.toString());
        
        return new Pricing ().fromJson(JSON.serialize(object));
    }

    public boolean hasInputObject(String result, String input) {
        Pricing inputPricing = new Pricing ().fromJson(input);
        Pricing outputPricing = new Pricing ().fromJson(result);
        
        return inputPricing.getId().equalsIgnoreCase(outputPricing.getId());
    }

    public void cleanup() {
        client.getDatabase("ecommerce_test").getCollection("Pricing").drop();
        client.getDatabase("ecommerce_test").drop();

        client.close();
        server.shutdown();
    }

    public PricingService_0_0_1 getService() {
        return service;
    }
    
    
    private static class TestDataCreationHolder {
        private static final TestUtil INSTANCE = new TestUtil();
    }
}
