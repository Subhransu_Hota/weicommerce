/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.pricing.service_0_0_1;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.core.Money;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceInput;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceOutput;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceParams;
import com.rokittech.container.ecom.pricing.defaults.PricingVocabulary;
import com.rokittech.container.ecom.pricing.utils.TestUtil;
import com.rokittech.container.ecom.commons.models.Pricing;
import java.util.Locale;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sangamesh
 */
public class PricingService_0_0_1UpdatePricingTest {
    
    public PricingService_0_0_1UpdatePricingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        PricingServiceInput input = new PricingServiceInput();
        Pricing pricing = TestUtil.getInstance ().createNewPricingInDatabase (
                "test_pricing_" + new Random ().nextInt(Integer.MAX_VALUE));
        
        //Update description
        pricing.setMoney(new Money(new Locale("en", "IN"), 10.00));
        
        input.setData(pricing);
        input.setInfo(TestUtil.getInstance().createInfo());
        
        PricingServiceOutput output = new PricingServiceOutput();
        // This is real mandatory code to move in controller
        PricingServiceParams params = TestUtil.getInstance().getParams ();
       
        params.getProperties().put(PricingVocabulary.COLLECTION.name(), "Pricing");
        params.getProperties().put(PricingVocabulary.COMMAND.name(), PricingVocabulary.UPDATE.name());
        
        TestUtil.getInstance().getService().setInput(input);
        TestUtil.getInstance().getService().setOutput(output);
        TestUtil.getInstance().getService().setParams(params);
        TestUtil.getInstance().getService().setPersist(new DefaultPersist());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInputAsJson method, of class PricingService_0_0_1.
     */
    @Test
    public void testGetInputAsJson() {
        System.out.println("getInputAsJson");
        String result = TestUtil.getInstance().getService().getInputAsJson();
        PricingServiceInput input = new PricingServiceInput().fromJson(result);
        assertNotNull("Test Failed: Input As Json is null", input);
    }

    /**
     * Test of serve method, of class PricingService_0_0_1.
     */
    @Test
    public void testServe() {
        System.out.println("serve");
        PricingServiceOutput result = TestUtil.getInstance().getService().serve();
        assertTrue("input and output are not same", TestUtil.getInstance().isPricingEqual (
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                result.getData()));
    }

    /**
     * Test of getOutputAsJson method, of class PricingService_0_0_1.
     */
    @Test
    public void testGetOutputAsJson() {
        System.out.println("getOutputAsJson");
        
        TestUtil.getInstance().getService().serve();
        String result = TestUtil.getInstance().getService().getOutputAsJson();
        PricingServiceOutput output = new PricingServiceOutput().fromJson(result);
        
        assertTrue("input and output are not same", TestUtil.getInstance().isPricingEqual (
                TestUtil.getInstance().getService().getInput().getData().toJson(),
                output.getData()));
    }
    
    /**
     * Test of toJson method, of class PricingService_0_0_1.
     */
    @Test
    public void testToJson() {
        System.out.println("toJson");
        String json = TestUtil.getInstance().getService().serve().toJson();
        System.out.println(json);
        assertNotNull("Test Failed: Output As Json is null", json);
    }
}
