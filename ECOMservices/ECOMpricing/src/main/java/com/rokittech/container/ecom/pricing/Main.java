/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.pricing;

import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.base.defaults.Info;
import com.rokittech.container.ecom.commons.core.Money;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceInput;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceOutput;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceParams;
import com.rokittech.container.ecom.pricing.defaults.PricingVocabulary;
import com.rokittech.container.ecom.pricing.factory.PricingServiceFactory;
import com.rokittech.container.ecom.commons.models.Pricing;
import com.rokittech.containers.api.core.Service;
import java.util.Date;
import java.util.Locale;



/**
 *
 * @author alexmylnikov
 */
public class Main {

    public static void main(String[] args) {
        
        // Running version 1
        // Set all service properties: Input, Output, Params.
        //This code is temp code for POC
        Info info = new Info();
        info.setAppId("wei");
        info.setUserId("test");
        info.setVersion(1);
        
        Pricing pricing = new Pricing();
        pricing.setMoney(new Money(new Locale("en", "IN"), 10.00));
        pricing.setPricingFromDate(new Date ().toString());
        pricing.setPricingToDate(new Date ().toString());
        pricing.setPricingId("pricing_id_1");
        pricing.setPricingPurposeType(Pricing.PricingPurposeType.PURCHASE);
        pricing.setPricingType(Pricing.PricingType.LIST_PRICE);
        
        // This is real mandatory code to move in controller
        PricingServiceParams params = new PricingServiceParams();
        params.getProperties().put(PricingVocabulary.COLLECTION.name(), "Pricing");
        params.getProperties().put(PricingVocabulary.COMMAND.name(), PricingVocabulary.INSERT.name());

        DefaultPersist persist = new DefaultPersist();
        PricingServiceInput inputTemp = new PricingServiceInput();
        inputTemp.setData(pricing);
        inputTemp.setInfo(info);
      
        Service<PricingServiceInput, PricingServiceOutput, PricingServiceParams, DefaultPersist> service = new PricingServiceFactory("wei_ecommerce", 1).get();

        service.setParams(params);
        service.setPersist(persist);
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        service.setOutput(new PricingServiceOutput());

        System.out.println(service.serve().getData());
         
        params.getProperties().put(PricingVocabulary.COMMAND.name(), PricingVocabulary.RETRIEVE.name());
        Pricing pricing1 = new Pricing ();
        pricing1.setPricingId("pricing_1");
        
        inputTemp.setData(pricing1);
               
        service.setInput(inputTemp.fromJson(inputTemp.toJson()));
        
        System.out.println(service.getOutput().getData());
    }

}
