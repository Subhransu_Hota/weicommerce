/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rokittech.container.ecom.pricing.service_0_0_1;

import com.google.gson.Gson;
import com.rokittech.container.base.defaults.DefaultPersist;
import com.rokittech.container.ecom.commons.core.AbstractService;
import com.rokittech.container.ecom.dbadapter.defaults.DBAdapterManager;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerInput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlerOutput;
import com.rokittech.container.ecom.dbadapter.defaults.DBHandlertParams;
import com.rokittech.container.ecom.dbadapter.factory.DBManagerFactory;

import com.rokittech.container.ecom.pricing.defaults.PricingServiceInput;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceOutput;
import com.rokittech.container.ecom.pricing.defaults.PricingServiceParams;
import com.rokittech.container.ecom.pricing.defaults.PricingVocabulary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author deependr
 */
public class PricingService_0_0_1 extends AbstractService<PricingServiceInput, PricingServiceOutput, PricingServiceParams, DefaultPersist> {

    private static final Logger slf4jLogger = LoggerFactory.getLogger(PricingService_0_0_1.class);
    private PricingServiceInput input;
    private PricingServiceOutput output;
    private PricingServiceParams params;

    private final DBAdapterManager dbclientManager;
        
    public PricingService_0_0_1() {
        dbclientManager = new DBManagerFactory().get();
    }

    public PricingService_0_0_1(DBHandlertParams.DBType dbType) {
        dbclientManager = new DBManagerFactory(dbType).get();
    }
    
    @Override
    public String serviceId() {
        return this.getClass().getName();
    }

    @Override
    public String getInputAsJson() {
        slf4jLogger.info("returning Input Object in the form of JSON");
        
        return input.toJson();
    }

    @Override
    public String getOutputAsJson() {
        slf4jLogger.info("returning Output Object in the form of JSON");
        return output.toJson();
    }

    @Override
    public void setInputAsJson(String input) {
        slf4jLogger.info("Recreating Input object from json");
        this.input = this.input.fromJson(input);
    }

    @Override
    public void setOutputAsJson(String output) {
        slf4jLogger.info("Recreating Ouput object from json");
        this.output = this.output.fromJson(output);
    }

    @Override
    public PricingServiceOutput serve() {
        slf4jLogger.info("Inside serve() ");
        DBHandlerInput serviceinput = new DBHandlerInput();
        serviceinput.setData(this.getInput().getData().toJson());
        
        DBHandlerOutput serviceoutput = new DBHandlerOutput();
        // This is real mandatory code to move in controller
        DBHandlertParams serviceparams = new DBHandlertParams();
        serviceparams.setCommand(getParams().getProperties().getProperty(PricingVocabulary.COMMAND.name()));
        serviceparams.setEntity(params.getProperties().getProperty(PricingVocabulary.COLLECTION.name()));
        
        dbclientManager.setInput(serviceinput);
        dbclientManager.setOutput(serviceoutput);
        dbclientManager.setParams(serviceparams);
        dbclientManager.setPersist(new DefaultPersist());
        
        DBHandlerOutput serviceOutput = (DBHandlerOutput) dbclientManager.serve();
      
        output.setData(serviceOutput.getData());
        output.setInfo(input.getInfo());

        slf4jLogger.info("Created output object " + output.getClass());
        
        return output;
    }

    @Override
    public String toJson() {
        slf4jLogger.info("generating json for service object");
        return new Gson().toJson(this, PricingService_0_0_1.class);
    }

    @Override
    public PricingServiceInput getInput() {
        return input;
    }

    @Override
    public void setInput(PricingServiceInput input) {
        this.input = input;
    }

    @Override
    public PricingServiceOutput getOutput() {
        return output;
    }

    @Override
    public void setOutput(PricingServiceOutput output) {
        this.output = output;
    }

    @Override
    public PricingServiceParams getParams() {
        return params;
    }

    public DBAdapterManager getDbclientManager() {
        return dbclientManager;
    }

    
    @Override
    public void setParams(PricingServiceParams params) {
        this.params = params;
    }
}
