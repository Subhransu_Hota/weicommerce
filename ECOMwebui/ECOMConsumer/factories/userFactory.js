/**
 * Created by manojkum on 18/12/15.
 */
ecomApp.factory('userFactory', function ($http) {
    var customer = {};
    dataObj.data={};
    dataObj.data.userCredentials = {};
    customer.get = function (id) {
        return $http.get(baseUrl+"/customer/find/"+id);
    };
    customer.update = function (formData) {
        console.log(formData)
        dataObj.data.userCredentials = {};
        dataObj.data._id = formData._id;
        dataObj.data.userCredentials.userName = formData.userCredentials.userName;
        dataObj.data.userCredentials.email = formData.userCredentials.email;
        dataObj.data.firstName = formData.firstName;
        dataObj.data.lastName = formData.lastName;
        dataObj.data.phoneNo = formData.phoneNo;

        return $http.put(baseUrl+"/customer/update", dataObj);
    };
    customer.delete = function (id) {
        return $http.delete(baseUrl+"/customer/delete/"+id);
    };
    customer.changePassword = function (formData) {
        console.log(formData)
        dataObj.data.userCredentials = {};
        dataObj.data._id = formData._id;
        dataObj.data.userCredentials.userName = formData.userCredentials.userName;
        dataObj.data.userCredentials.email = formData.userCredentials.email;
        dataObj.data.userCredentials.password = formData.password;
        return $http.put(baseUrl+"/customer/update", dataObj)

    }
    customer.addAddress = function (formData) {
        dataObj.data.billingAddress =[];
        dataObj.data.shippingAddress = [];
        dataObj.data._id = formData._id;
        dataObj.data.billingAddress[0] = formData.billingAddress[0];
        /*{
            "addressName":"mera ghar",
            "city":"Bangalore",
            "createdDate":"Nov 30, 2015 4:45:33 PM",
            "addressType":"Billing_And_Shipping"
        };*/
        dataObj.data.shippingAddress[0] = formData.shippingAddress[0];
            /*{
                "addressName":"mera ghar1",
                "city":"Bangalore",
                "createdDate":"Nov 30, 2015 4:45:33 PM",
                "addressType":"Billing_And_Shipping"
            };*/
        return $http.put(baseUrl+"/customer/update", dataObj)
    };
    customer.getOrders = function (id){
        dataObj.data = {};
        dataObj.data.customerId = id;
        return $http.post(baseUrl+"/order/list", dataObj);
    }
    return customer;
})