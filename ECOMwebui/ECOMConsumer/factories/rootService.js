/**
 * Created by manojkum on 16/12/15.
 */
ecomApp.factory('rootService', function ($rootScope, $cookieStore) {
    var sharedService = {};
    sharedService.username = '';
    sharedService.getLoggedInUser = function () {
        var loggedIn = $rootScope.globals.currentUser;
        this.username = loggedIn.username;
        console.log(loggedIn)
        this.broadCastUser();
    };
    sharedService.broadCastUser = function () {
        $rootScope.$broadcast('handleBroadCastUser');
    };
    return sharedService;
});