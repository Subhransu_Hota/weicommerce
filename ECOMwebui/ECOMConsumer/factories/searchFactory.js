/**
 * Created by manojkum on 18/12/15.
 */
ecomApp.factory('search', function ($http, $cacheFactory) {
    var esUrl = "http://localhost:9080/rokitt/ecommerce/" ,
        search = {};
    search.get = function (payload, successCallback) {
        dataObj.data = payload
        var key = 'search_' + payload.keyword;
        $http.post(baseUrl + '/search', dataObj).then(function (resp) {
            //$cacheFactory(key).put('result', resp.data);
            //console.log(resp.data)
            successCallback(resp.data);
        });
        /*  if($cacheFactory.get(key) == undefined || $cacheFactory.get(key) == ''){
              $http.post(baseUrl+'/search', dataObj).then(function(resp){
                  $cacheFactory(key).put('result', resp.data);
                  //console.log(resp.data)
                  successCallback(resp.data);
              });
          }else{
              successCallback($cacheFactory.get(key).get('result'));
          }*/
    };
    search.getSuggester = function (params,successCallback) {
        var dataJson = {
            "info": {
                "userId": "",
                "appId": "",
                "version": 1,
                "serviceId": "",
                "persistId": "",
                "recId": ""
            },
            "data": {
                "text": params,
                "categoryCount": 5,
                "productCount": 5
            }
        };
        $http.post(esUrl+'suggester', dataJson).then(function (resp) {
            //$cacheFactory(key).put('result', resp.data);
            //console.log(resp.data)
            successCallback(resp.data);
        });


    };
    search.getProduct = function (params) {

    };
    
    return search;
});