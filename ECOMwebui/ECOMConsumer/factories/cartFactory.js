/**
 * Created by manojkum on 01/12/15.
 */
ecomApp.factory('cartFactory', function ($http) {
    var cartFactory = {},
        dataObj = {
            "template":"",
            "info":{"version":1,"userId":"test","appId":"wei"},
            "data":{"keyword":""
            }
        };
    cartFactory.createCart = function (userId,productId,callback) {
        dataObj.data = {
            userId:userId,
            productId:productId,
            qty:1
        };
        $http.post(baseUrl+'/cart/insert',dataObj)
            .success(function (data) {
                callback(data);
            })
            .error(function (err) {
                callback(err);
            });
    }
    cartFactory.addToCart = function (productId, basketId, userId, callback){
        dataObj.data = {
            basketId:basketId,
            userId:userId,
            productId:productId,
            qty:1
        };
        $http.put(baseUrl+'/cart/addproduct',dataObj )
            .success(function (data) {
                callback(data);
            })
            .error(function (err) {
                callback(err);
            });
    };
    cartFactory.getCartItem = function (id,callback) {
        $http.get(baseUrl+'/cart/find/'+id)
            .success(function (data) {
                //console.log(data);
                callback(data);
                //return data;
            })
            .error(function (err) {
                callback(err);
            });
        /*this.cartItem = localStorage.getItem('cartItem');
        console.log(this.cartItem)
        return this.cartItem;*/
    };
    cartFactory.updateCart = function (productId,basketId,userId,qty,callback){
        dataObj.data = {
            basketId:basketId,
            userId:userId,
            productId:productId,
            qty:qty
        };
        $http.post(baseUrl+'/cart/updateproduct',dataObj)
            .success(function (data) {
                callback(data);
            })
            .error(function (err) {
                callback(err);
            });
    };
    cartFactory.deleteProductFromCart = function (productId,basketId,qty,callback){
        dataObj.data = {
            basketId:basketId,
            userId:"564daf9706354d172e803b0f",
            productId:productId,
            qty:qty
        };
        $http.put(baseUrl+'/cart/removeproduct',dataObj)
            .success(function (data) {
                callback(data);
            })
            .error(function (err) {
                callback(err);
            });
    };
    
    cartFactory.updateAddress = function (basketId,addressType,address,callback) {
        dataObj.data = {
            basketId:basketId,
            addressType:addressType,
            address:address
        };
        $http.put(baseUrl+'/cart/updateaddres',dataObj)
            .success(function (resp) {
                callback(resp);
            })
            .error(function (err) {
                callback(err);
            });
    }
    return cartFactory;
} );