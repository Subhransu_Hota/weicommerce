/**
 * Created by manojkum on 17/12/15.
 */
ecomApp.factory('routeHistory', ['$rootScope', '$state', function ($rootScope, $state) {

        var lastHref = "/",
            lastStateName = "",
            lastParams = {};

        $rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
            lastStateName = fromState.name;
            lastParams = fromParams;
            lastHref = $state.href(lastStateName, lastParams)
        })

        return {
            getLastHref: function (){ return lastHref ; }
            //goToLastState: function (){ return $state.go(lastStateName, lastParams); },
        }

    }]);