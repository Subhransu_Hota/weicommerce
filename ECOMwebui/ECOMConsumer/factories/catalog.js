/* global ecomApp */

ecomApp.factory('catalogFactory', function ($http){
	var catalogService = {},
        dataObj = {
               "template":"",
               "info":{"version":1,"userId":"test","appId":"wei"},
               "data":{} 
               
        };
    catalogService.getAll = function (callback) {
        $http.post(baseUrl+'/catalog/list',{cache : true},dataObj )
              .success(callback);
        //console.log(baseUrl + '/catalog/list');
    };
    catalogService.searchCategory = function (categoryId,callback) {
        dataObj.data.keyword = categoryId;
        $http.post(baseUrl+'/search',dataObj)
                .success(callback);
    };
    return catalogService;
});

