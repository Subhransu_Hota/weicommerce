//Define an angular module for our App
var ecomApp = angular.module('ecomApp', ['ngRoute','ngTouch', 'ngSanitize', 'ngAnimate', 'ngAria', 'ui.bootstrap','ngCookies']);
var baseUrl = baseUrl,
    dataObj = {
        "template":"",
        "info":{"version":1,"userId":"test","appId":"wei"},
        "data":{
        }
    };
//Define Routing for app
ecomApp.config(config);
ecomApp.run(run);
function config ($routeProvider, $locationProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html'
        })
        .when('/product/:productId', {
            templateUrl: 'views/singleProduct.html',
            controller: 'singleProductCtrl'
        })
        .when('/cart', {
            templateUrl: 'views/cart.html',
        })
        .when('/payment', {
            templateUrl: 'views/paymentForm.html'
        })

        .when('/address', {
            templateUrl: 'views/addressUpdate.html'
        })
        .when('/order/:orderId', {
            templateUrl: 'views/order.html',
            controller: 'orderCtrl'
        })
        .when('/signup', {
            templateUrl: "views/signUp.html"
        })
        .when('/login', {
            templateUrl: "views/login.html"
        })
        .when('/profile',{
            templateUrl: "views/userProfile.html",
            controller: "userProfile"
        })
        .when('/editProfile',{
            templateUrl: "views/editProfile.html",
            controller: "userProfile"
        })
        .when('/changePassword',{
            templateUrl: "views/changePassword.html",
            controller: "userProfile"
        })
        .when('/addAddress',{
            templateUrl: "/views/addAddress.html"
        })
        .when('/orders',{
            templateUrl: "/views/ordersDetail.html"
        })
        .when('/searchResult', {
            templateUrl: "/views/searchResult.html"
        })
        .otherwise({
            redirectTo: '/'
        });
    $locationProvider.html5Mode(true);
};
toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "0",
    "hideDuration": "0",
    "timeOut": "1000",
    "extendedTimeOut": "000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];
function run($rootScope, $location, $cookieStore, $http, $routeparams) {
    // keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
    }

    $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        var authenticatedPage = $.inArray($location.path(), ['/address','/changePassword','/profile']) !== -1;
       // console.log(authenticatedPage)
        var loggedIn = $rootScope.globals.currentUser;
        //console.log(loggedIn)
        if (authenticatedPage && !loggedIn) {
            $location.path('/login');
        }
    });
};
