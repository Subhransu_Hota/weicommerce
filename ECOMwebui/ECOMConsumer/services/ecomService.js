/**
 * Created by manojkum on 03/12/15.
 */
ecomApp.service('cartService', function () {
    this.getCartItem = function () {
        this.cartItem = localStorage.getItem('cartItem');
        //console.log(this.cartItem)
        return this.cartItem;
    };
    this.sum = function(items, prop){
        return items.reduce( function(a, b){
            return a + b[prop];
        }, 0);
    };
    this.removeProduct = function (productId) {
        var cartItem = this.getCartItem();
        if (cartItem !== null){
            cartItem = JSON.parse(cartItem);
            angular.forEach(cartItem.lineItems, function (value,key) {
                //console.log(value.productId)
                if (value.productId === productId)
                    cartItem.lineItems.splice(key,1);
            })
           // console.log(cartItem)
            //removing item
        }


    };
   /* this.appendToStorage = function(name, data){
        var old = localStorage.getItem(name);
        if(old === null) old = "";
        localStorage.setItem(name, old + data);
    };*/




});
