/**
 * Created by manojkum on 18/12/15.
 */
ecomApp.service('ConfigService', function (){
    var _environments = {
        local: {
            host: ['l', 'localhost'],
            config: {
                api_endpoint: '/api/v1',
                node_url: 'http://localhost:9998'
            }
        },
        test: {
            host: ['test.domain.com', 'beta.domain.com'],
            config: {
                api_endpoint: '/api/v1',
                node_url: 'http://domain.com:9998'
            }
        },
        prod: {
            host: 'domain.com',
            config: {
                api_endpoint: '/api/v1',
                node_url: 'http://domain.com:9998'
            }
        }
    }, _environment;

    this.getEnvironment = function (){
        var host = window.location.host;
        if (_environment) {
            return _environment;
        }
        for (var environment in _environments){
            if (typeof(_environments[environment].host) && typeof(_environments[environment].host) == 'object') {
                if (_environments[environment].host.indexOf(host) >= 0) {
                    _environment = environment;
                    return _environment;
                }
            } else {
                if (typeof(_environments[environment].host) && _environments[environment].host == host) {
                    _environment = environment;
                    return _environment;
                }
            }
        }
        return null;
    };
    this.get = function (property) {
        return _environments[this.getEnvironment()].config[property];
    };
});