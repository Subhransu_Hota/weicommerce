/**
 * Created by manojkum on 01/12/15.
 */
ecomApp.controller('singleProductCtrl', function ($scope, $rootScope,$cookieStore, $route, $routeParams, $location, productsFactory, cartService, cartFactory) {
    console.log($routeParams.productId);
    $rootScope.globals = $cookieStore.get('globals') || {};
    var user = $rootScope.globals.currentUser;
        console.log(user)
    var basketItem = cartService.getCartItem();
    if (basketItem !== null) {
        basketItem = JSON.parse(basketItem);
        cartFactory.getCartItem(basketItem._id, function (data) {
            $rootScope.cartItem = JSON.parse(data.data);
            //console.log($scope.cartItem)
            $rootScope.cartItemCount = cartService.sum($rootScope.cartItem.lineItems, 'quantity');
            if ($rootScope.cartItem.lineItems.length != 0 && $rootScope.cartItem.lineItems[0].productId == $routeParams.productId) {
                $scope.pointerEvent = "none";
                $scope.cursor = "default";
                $scope.backgroundColor = "grey";
                $scope.addCartBtn = {
                    'pointer-events': $scope.pointerEvent,
                    'background-color': $scope.backgroundColor,
                    'cursor': $scope.cursor
                }
            }
        });
    } else {
        $rootScope.cartItemCount = 0;
    }


    /* if (basketItem.lineItems[0] != undefined && basketItem.lineItems[0].productId == $routeParams.productId){
     $scope.pointerEvent= "none";
     $scope.cursor = "default";
     $scope.backgroundColor= "grey";
     $scope.addCartBtn = {
     'pointer-events' :$scope.pointerEvent,
     'background-color':$scope.backgroundColor,
     'cursor':$scope.cursor
     }
     }*/

    productsFactory.getOne($routeParams.productId)
        .success(function (resp) {
            $scope.product = JSON.parse(resp.data);
            //console.log($scope.product)


        })
        .error(function (err) {
            $location.path('/');
        });
    $scope.addToCart = function (productId) {
        var items = JSON.parse(cartService.getCartItem());
        console.log(items)

        if (items === null) {
            console.log("creating cart")
            cartFactory.createCart(user.id,productId, function (resp) {
                toastr.success('Item added successfully', {timeOut: 0})
                console.log("creating cart")
                console.log(resp)
                $rootScope.cartItem = JSON.parse(resp.data);
                $rootScope.cartItemCount = cartService.sum($rootScope.cartItem.lineItems, 'quantity');
                console.log("created cart")
                console.log($rootScope.cartItem);
                localStorage.setItem('cartItem', JSON.stringify($rootScope.cartItem));
                if ($rootScope.cartItem != null && $rootScope.cartItem.lineItems[0].productId == productId) {
                    $scope.pointerEvent = "none";
                    $scope.cursor = "default";
                    $scope.backgroundColor = "grey";
                    $scope.addCartBtn = {
                        'pointer-events': $scope.pointerEvent,
                        'background-color': $scope.backgroundColor,
                        'cursor': $scope.cursor
                    }
                }

            });

        } else {
            var basketId = items._id,
                userId = items.user._id;
            cartFactory.addToCart(productId, basketId, userId, function (resp) {
                console.log(resp);
                toastr.success('Item added successfully', {timeOut: 0})
                $rootScope.cartItem = JSON.parse(resp.data);
                $rootScope.cartItemCount = cartService.sum($rootScope.cartItem.lineItems, 'quantity');
                console.log($rootScope.cartItem);
                localStorage.setItem('cartItem', JSON.stringify($rootScope.cartItem));
                for (var i = 0; i < $rootScope.cartItemCount; i++) {
                    if ($rootScope.cartItem != null && $rootScope.cartItem.lineItems[i].productId == productId) {
                        $scope.pointerEvent = "none";
                        $scope.cursor = "default";
                        $scope.backgroundColor = "grey";
                        $scope.addCartBtn = {
                            'pointer-events': $scope.pointerEvent,
                            'background-color': $scope.backgroundColor,
                            'cursor': $scope.cursor
                        }
                    }
                }

            });
        }


        //}
        //productsFactory.getCartItems()
        /*   productsFactory.addToCart(id, function (resp) {
         $scope.cartItem = [];
         $scope.cartItem = JSON.parse(resp.data);
         console.log($scope.cartItem);
         localStorage.setItem('cartItem',JSON.stringify($scope.cartItem));
         console.log(localStorage.getItem(cartItem))
         });*/

    };

    $scope.getCartItems = function () {

    }
});