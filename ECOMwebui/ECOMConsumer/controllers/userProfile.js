/**
 * Created by manojkum on 21/12/15.
 */
ecomApp.controller("userProfile", function ($rootScope, $scope, $cookieStore, userFactory, $timeout, $location) {
    $rootScope.user ={};
    $scope.isAddressSame = false;
    //console.log($scope.isAddressSame)
    var self = $scope;
    $rootScope.globals = $cookieStore.get('globals') || {};
    var user = $rootScope.globals.currentUser;
    userFactory.getOrders(user.id)
        .then(function (resp){
            var orders = JSON.parse(resp.data.data);
            $scope.orders = orders;
            console.log(orders)
        });


    if (user == undefined){

        $rootScope.user = null;


    } else {
        var promise = userFactory.get(user.id);
        promise.then(function (resp) {
            console.log(resp.data);
            //var resp = JSON.parse(resp.data);
            $rootScope.user = JSON.parse(resp.data.data);
            //console.log($scope.user)
        }, function (err) {

        });
    }


    self.changePassword = function (db) {
        console.log(db)
        var promise = userFactory.changePassword(db);
        promise.then(function (resp) {
            //console.log(resp);
            $scope.success = true;
            toastr.success("user updated successfully")
            $timeout(function () {
                $location.path('/profile');
            },2000);

        }), function (err) {
            console.log(err)
        }
    };
    self.updateProfile = function (db) {
        var promise = userFactory.update(db);
        promise.then(function (resp) {
            //console.log(resp.status)
            $scope.success = true;
            toastr.success("user updated successfully")
            $timeout(function () {
                $location.path('/profile');
            },2000);

        }), function (err) {
            console.log(err)
        }
    }
    self.addAddress = function (db) {
        //console.log($scope.isAddressSame)
        if ($scope.isAddressSame == true){
            db.billingAddress = [];
            db.billingAddress.push(db.shippingAddress[0]);
            //console.log(db.billingAddress)
        }
        console.log(db);
        var promise = userFactory.addAddress(db);
        promise.then(function (resp) {
            console.log(resp.success);
            $scope.success = true;
            toastr.success("address updated successfully")
            $timeout(function () {
                $location.path('/profile');
            },2000);

        })

    }
    self.getOrders = function () {
        userFactory.getOrders()
            .then(function (resp){
                var orders = JSON.parse(resp.data);
                $scope.orders = orders;
                console.log($scope.orders)
            });
    }


});