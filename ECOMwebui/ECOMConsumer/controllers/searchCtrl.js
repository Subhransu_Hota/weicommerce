/**
 * Created by manojkum on 30/11/15.
 */
ecomApp.controller('searchCtrl', function ($scope, $rootScope, search) {
    $rootScope.searchItems = [];
    $scope.searchText = '';
    //var payload = {'keyword': $scope.searchText};

    //Sort Array
    $rootScope.searchItems.sort();
    //Define Suggestions List
    $rootScope.suggestions = [];
    //Define Selected Suggestion Item
    $rootScope.selectedIndex = -1;

    //search function on ng-change
    $scope.search = function () {
        $scope.suggestions = [];
        var myMaxSuggestionListLength = 0;
        search.getSuggester($scope.searchText, function (resp) {
            console.log(resp.data.data.suggestion.product)
                //var data = JSON.parse(data);
            angular.forEach(resp.data.data.suggestion.product, function (value) {
                $rootScope.searchItems.push(value);

                console.log($rootScope.searchItems.length)
            });
            for (var i = 0; i < $rootScope.searchItems.length; i++) {
                var searchItems = $rootScope.searchItems[i].productName;
                console.log(searchItems)
                var searchText = $scope.searchText;
                //$scope.doSearch()
                if (searchItems.indexOf(searchText) !== -1) {
                    $scope.suggestions.push(searchItems);
                    console.log($rootScope.suggestions)
                    myMaxSuggestionListLength += 1;
                    if (myMaxSuggestionListLength == 8) {
                        break;
                    }

                }
            }

        });


    }

    //Keep Track Of Search Text Value During The Selection From The Suggestions List
    $rootScope.$watch('selectedIndex', function (val) {
        if (val !== -1) {
            $scope.searchText = $rootScope.suggestions[$rootScope.selectedIndex];
            $scope.doSearch();

        }
    });


    //Text Field Events
    //Function To Call on ng-keydown
    $rootScope.checkKeyDown = function (event) {
            if (event.keyCode === 40) { //down key, increment selectedIndex
                event.preventDefault();
                if ($rootScope.selectedIndex + 1 !== $rootScope.suggestions.length) {
                    $rootScope.selectedIndex++;
                }
            } else if (event.keyCode === 38) { //up key, decrement selectedIndex
                event.preventDefault();
                if ($rootScope.selectedIndex - 1 !== -1) {
                    $rootScope.selectedIndex--;
                }
            } else if (event.keyCode === 13) { //enter key, empty suggestions array
                event.preventDefault();
                $rootScope.suggestions = [];
            }
        }
        //Function To Call on ng-keyup
    $rootScope.checkKeyUp = function (event) {
            if (event.keyCode !== 8 || event.keyCode !== 46) { //delete or backspace
                if ($scope.searchText == "") {
                    $rootScope.suggestions = [];
                }
            }
        }
        //======================================

    //List Item Events
    //Function To Call on ng-click
    $rootScope.AssignValueAndHide = function (index) {
            $scope.searchText = $rootScope.suggestions[index];
            $rootScope.suggestions = [];
            $scope.doSearch();

        }
        //======================================
    $scope.doSearch = function () {
        var payload = {
            'keyword': $scope.searchText
        };
        search.get(payload, function (resp) {
            $rootScope.products = JSON.parse(resp.data);
        });
    };

});