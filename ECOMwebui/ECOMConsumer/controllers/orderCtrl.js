ecomApp.controller('orderCtrl', function($scope,$routeParams,orderFactory,$rootScope){
	var fileName =  'order'+$routeParams.orderId+'.html';
	$scope.init = function(){
		var orderId = $routeParams.orderId;
		console.log(orderId);
		orderFactory.getOrderDetails(orderId,function(result){
	        console.log(result);
	    	$scope.order = JSON.parse(result.data);
			$rootScope.cartItemCount = 0;
	    });
		downloadInnerHtml(fileName, 'text/html');

	}
	localStorage.clear();

	$scope.downloadInnerHtml = function(filename, mimeType) {
		var elHtml = document.body.innerHTML;
		var link = document.createElement('a');
		mimeType = mimeType || 'text/plain';

		link.setAttribute('download', filename);
		link.setAttribute('href', 'data:' + mimeType + ';charset=utf-8,' + encodeURIComponent(elHtml));
		link.click();
	}

	 // You can use the .txt extension if you want
});