/**
 * Created by manojkum on 14/12/15.
 */
ecomApp.controller('signUpCtrl', function ($scope, $location, credentialsFactory) {
    $scope.user = {};
    $scope.completeSignUp = function () {
        var user = $scope.user;
        console.log(user)
        var promise = credentialsFactory.doSignUp(user);
        promise.then(function (resp) {
            if (resp.data){
                var data = JSON.parse(resp.data.data)
                credentialsFactory.setCredentials(data._id,user.username, user.password);
                dataObj.data = {"keyword":""}
                $location.path('/');
            }
        }, function (err) {
            console.log(err);
        })
    };



    $scope.showModal = true;
    $scope.toggleModal = function(){
        $scope.showModal = !$scope.showModal;
    };
    $('ul.nav-pills li a').click(function (e) {
        e.preventDefault();
        $('ul.nav-pills li.active').removeClass('active')
        $(this).parent('li').addClass('active')
    })
})