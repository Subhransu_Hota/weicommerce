/**
 * Created by manojkum on 03/12/15.
 */
ecomApp.controller("paymentCtrl", function ($scope, $http, cartService, $timeout)
    {
        $scope.payment = {"data":{}};

        var basketItem = cartService.getCartItem();
        basketItem = JSON.parse(basketItem);

        $scope.payment.data.firstname = "Sangamesh";
        $scope.payment.data.amount = basketItem.total.amount;
        $scope.payment.data.email = "acxyz@gmail.com";
        $scope.payment.data.phone = "9845098450";
        $scope.payment.data.productInfo = "starting Payment";
        $scope.ajaxSubmitResult = null;

        $scope.onPlaceOrder = function ()
        {
            $scope.payment.data.paymentType = "PAYUMONEY";
            var info = {
                version: "1",
                userId: "test",
                appId: "wei"
            };
            $scope.payment.info = info;
            
            $scope.payment.basketId = basketItem._id;
            
            $http.post(baseUrl+"/payment/process", $scope.payment)
                .success(function (data, status, headers, config)
                {
                    $scope.ajaxSubmitResult = angular.fromJson(data.data);


                    console.log($scope.ajaxSubmitResult);
                    $timeout(function() {
                        $scope.submit();
                        console.log('update with timeout fired')
                    }, 0);
                    

                })
                .error(function (data, status, headers, config)
                {
                    $scope[resultVarName] = "SUBMIT ERROR";
                });
        };

        // $scope.$watch("ajaxSubmitResult",
        //     function handleOnResponse( newValue, oldValue ) {
        //         if(newValue !== null && newValue.hasOwnProperty("formData")){
        //             $scope.submit();
        //         }
        //         console.log( "vm.fooCount:", newValue );
        //     }
        // );

        $scope.submit = function (){

            var payuForm = document.forms.payuForm;
            payuForm.action = $scope.ajaxSubmitResult.host;
            payuForm.submit();
        };
    });