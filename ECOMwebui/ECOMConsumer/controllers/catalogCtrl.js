/**
 * Created by manojkum on 28/11/15.
 */
ecomApp.controller('catalogCtrl', function ($scope, $rootScope, $timeout, rootService, catalogFactory, cartFactory, cartService, $location, credentialsFactory) {
    var mainItems = [];
    $scope.$on('handleBroadCastUser', function () {
        $scope.username = rootService.username;
    })
    $scope.logout = function () {
        $timeout(function () {
            credentialsFactory.clearCredentials();
            $rootScope.user = null;
            var path = $location.path();
            toastr.success("logout successfully !!");
            $location.path(path);
        },1000)

    };

    catalogFactory.getAll(function (response) {
        $scope.menuItems = [];
        $scope.subMenuItems = [];
        $scope.subsubMenuItems = [];

        $scope.catalogItems = JSON.parse(response.data);
        //console.log($scope.catalogItems)
        angular.forEach($scope.catalogItems, function (value, key) {
            if (value.parentCatalogId === '0') {
                $scope.menuItems.push(value);
                //console.log($scope.menuItems)
            }
            else {
                $scope.subMenuItems.push(value);
                $scope.subsubMenuItems.push(value);


                //console.log($scope.subMenuItems)
            }
        });
        //console.log($scope.subsubMenuItems.length)
        angular.forEach($scope.subMenuItems, function (value, key) {

        })
        //console.log($scope.menuItems);

        //$scope.getList()

        angular.forEach($scope.menuItems, function (value1, key1) {
            var sumMainItems = [];
            angular.forEach($scope.subMenuItems, function (value2, key2) {

                //$scope.menuItems[key2].subMenuItems = [];
                if (value2.parentCatalogId === value1._id) {
                    sumMainItems.push(value2);
                    //console.log(sumMainItems)
                    $scope.menuItems[key1].subMenuItems = sumMainItems;

                    //console.log($scope.menuItems[key2].subMenuItems);
                }
                //else {
                //    $scope.menuItems[key1].subMenuItems.push('null');
                //}

            });


        });
        //console.log($scope.menuItems);
    });

    /* if (productsFactory.getCartItem()){
     $scope.cartItem = JSON.parse(productsFactory.getCartItem()) ;
     console.log($scope.cartItem.lineItems[0].quantity)
     }else {
     $scope.cartItem = null;
     }*/


    var basketItem = cartService.getCartItem();
    if (basketItem !== null) {
        basketItem = JSON.parse(basketItem);
        cartFactory.getCartItem(basketItem._id, function (data) {
            $rootScope.cartItem = JSON.parse(data.data);
            //console.log($scope.cartItem)
            $rootScope.cartItemCount = cartService.sum($rootScope.cartItem.lineItems, 'quantity');
        });
    } else {
        $rootScope.cartItemCount = 0;
    }


    $scope.$on('$routeChangeSuccess', function () {
        var path = $location.path();
        //console.log(path);
        //$scope.catalogVisible = false;
        if (path == '/address') {
            $scope.catalogVisible = false;
        } else {
            $scope.catalogVisible = true;
        }
    });

    $scope.categorySearch = function (id) {
        catalogFactory.searchCategory(id, function (data) {
            $rootScope.productData = JSON.parse(data.data);
        })
    }


});
