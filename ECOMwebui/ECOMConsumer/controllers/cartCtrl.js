/**
 * Created by manojkum on 01/12/15.
 */


ecomApp.controller('cartCtrl', function ($scope, $rootScope, cartFactory, cartService, $location, $timeout) {

    var basketItem = cartService.getCartItem();
    //console.log()
    if (basketItem !== null) {
        basketItem = JSON.parse(basketItem);
        cartFactory.getCartItem(basketItem._id, function (data) {
            $scope.cartItem = JSON.parse(data.data);
            console.log($scope.cartItem)
            $rootScope.cartItemCount = cartService.sum($scope.cartItem.lineItems, 'quantity');
        });
    } else {
        console.log("no basket found");
        $scope.cartItemCount = 0;
    }
    $scope.removeProduct = function (id) {
        $('.close1').addClass('animated fadeIn')
        console.log(id)
        cartFactory.deleteProductFromCart(id, basketItem._id, 1, function (resp) {
            console.log(resp)
            toastr.success('item from cart removed successfully');
            cartFactory.getCartItem(basketItem._id, function (data) {

                $scope.cartItem = JSON.parse(data.data);
                localStorage.setItem('cartItem', JSON.stringify($scope.cartItem))
                console.log($scope.cartItem)
                $rootScope.cartItemCount = cartService.sum($scope.cartItem.lineItems, 'quantity');
            });
        });


    }
    $scope.updateQty = function (id, qty) {
        console.log(basketItem.user._id.toString());
        cartFactory.updateCart(id, basketItem._id, basketItem.user._id.toString(), qty, function (resp) {
            console.log(resp)
            toastr.success('item updated successfully');
            cartFactory.getCartItem(basketItem._id, function (data) {

                $scope.cartItem = JSON.parse(data.data);
                localStorage.setItem('cartItem', JSON.stringify($scope.cartItem))
                console.log($scope.cartItem)
                $rootScope.cartItemCount = cartService.sum($scope.cartItem.lineItems, 'quantity');
            });
        })
    };
    $scope.updateAddress = function () {
        var address = $scope.address;
        address.addressType = 'Billing';
        console.log(address)
        cartFactory.updateAddress(basketItem._id, address.addressType, address, function (data) {
            $scope.cartItem = JSON.parse(data.data);
            localStorage.setItem('cartItem', JSON.stringify($scope.cartItem));
            $timeout(function () {
                $location.path('payment');
            }, 2000);

        });
    }


});