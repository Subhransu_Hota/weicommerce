'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglifyjs');
var concat = require('gulp-concat');
var notify = require('gulp-notify');
var del = require('del');
var paths = {
    scripts: [
        './**/*.js',
        '!./configFile.js',
        '!./node_modules/**',
        '!./gulpfile.js',
        '!./dist/js/all.js',
        './app.js',
        '!./bower_components/**',
        '!./server.js'
    ],
    images: './images/**/*',
    css: './styles/scss/*.scss'
};
gulp.task('clean', function() {
    return del(['dist/css', 'dist/js']);
});
gulp.task('styles', function () {
    gulp.src('./styles/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('scripts', function () {
    gulp.src(paths.scripts)
        .pipe(concat('all.js'))
        /*.pipe(uglify())*/
        .pipe(gulp.dest('./dist/js/'))
        .pipe(notify({ message: 'Finished concating JavaScript'}));
});


//watch tasks

gulp.task('watch', function () {
    gulp.watch(paths.css, ['styles']);
    gulp.watch(paths.scripts, ['scripts']);
});

//default task

gulp.task('default', ['clean', 'watch', 'scripts', 'styles']);