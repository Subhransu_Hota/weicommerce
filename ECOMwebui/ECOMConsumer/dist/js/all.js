//Define an angular module for our App
var ecomApp = angular.module('ecomApp', ['ngRoute','ngTouch', 'ngSanitize', 'ngAnimate', 'ngAria', 'ui.bootstrap','ngCookies']);
var baseUrl = baseUrl,
    dataObj = {
        "template":"",
        "info":{"version":1,"userId":"test","appId":"wei"},
        "data":{
        }
    };
//Define Routing for app
ecomApp.config(config);
ecomApp.run(run);
function config ($routeProvider, $locationProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html'
        })
        .when('/product/:productId', {
            templateUrl: 'views/singleProduct.html',
            controller: 'singleProductCtrl'
        })
        .when('/cart', {
            templateUrl: 'views/cart.html',
        })
        .when('/payment', {
            templateUrl: 'views/paymentForm.html'
        })

        .when('/address', {
            templateUrl: 'views/addressUpdate.html'
        })
        .when('/order/:orderId', {
            templateUrl: 'views/order.html',
            controller: 'orderCtrl'
        })
        .when('/signup', {
            templateUrl: "views/signUp.html"
        })
        .when('/login', {
            templateUrl: "views/login.html"
        })
        .when('/profile',{
            templateUrl: "views/userProfile.html",
            controller: "userProfile"
        })
        .when('/editProfile',{
            templateUrl: "views/editProfile.html",
            controller: "userProfile"
        })
        .when('/changePassword',{
            templateUrl: "views/changePassword.html",
            controller: "userProfile"
        })
        .when('/addAddress',{
            templateUrl: "/views/addAddress.html"
        })
        .when('/orders',{
            templateUrl: "/views/ordersDetail.html"
        })
        .when('/searchResult', {
            templateUrl: "/views/searchResult.html"
        })
        .otherwise({
            redirectTo: '/'
        });
    $locationProvider.html5Mode(true);
};
toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "0",
    "hideDuration": "0",
    "timeOut": "1000",
    "extendedTimeOut": "000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];
function run($rootScope, $location, $cookieStore, $http, $routeparams) {
    // keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
    }

    $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        var authenticatedPage = $.inArray($location.path(), ['/address','/changePassword','/profile']) !== -1;
       // console.log(authenticatedPage)
        var loggedIn = $rootScope.globals.currentUser;
        //console.log(loggedIn)
        if (authenticatedPage && !loggedIn) {
            $location.path('/login');
        }
    });
};

/**
 * Created by manojkum on 19/12/15.
 */
ecomApp.service('configService', function (){
    var _environments = {
        local: {
            host: ['l', 'localhost'],
            config: {
                api_endpoint: '/api/v1',
                node_url: 'http://localhost:3000'
            }
        },
        test: {
            host: ['test.domain.com', 'beta.domain.com'],
            config: {
                api_endpoint: '/api/v1',
                node_url: 'http://localhost:3000'
            }
        },
        prod: {
            host: 'weicommerce.com',
            config: {
                api_endpoint: '/api/v1',
                node_url: 'http://localhost:3000'
            }
        }
    }, _environment;

    this.getEnvironment = function (){
        var host = window.location.host;
        if (_environment) {
            return _environment;
        }
        for (var environment in _environments){
            if (typeof(_environments[environment].host) && typeof(_environments[environment].host) == 'object') {
                if (_environments[environment].host.indexOf(host) >= 0) {
                    _environment = environment;
                    return _environment;
                }
            } else {
                if (typeof(_environments[environment].host) && _environments[environment].host == host) {
                    _environment = environment;
                    return _environment;
                }
            }
        }
        return null;
    };
    this.get = function (property) {
        return _environments[this.getEnvironment()].config[property];
    };
});
/**
 * Created by manojkum on 28/11/15.
 */
ecomApp.directive('catalogItem', function () {
    return{
        restrict: 'E',
        templateUrl: './directives/catalog.html',
        controller: function($scope){
            //console.log($scope)
        }
    }
})
/**
 * Created by manojkum on 28/11/15.
 */

ecomApp.directive('itemContainer', function () {
    return{
        restrict: 'E',
        templateUrl: './directives/itemContainer.html',
    }
})
ecomApp.directive('slider', function ($timeout) {
    return {
        restrict: 'AE',
        replace: true,
        scope: {
            images: '='
        },
        link: function (scope, elem, attrs) {
            scope.currentIndex = 0; // Initially the index is at the first image

            scope.next = function () {
                scope.currentIndex < scope.images.length - 1 ? scope.currentIndex++ : scope.currentIndex = 0;
            };

            scope.prev = function () {
                scope.currentIndex > 0 ? scope.currentIndex-- : scope.currentIndex = scope.images.length - 1;
            };
            scope.$watch('currentIndex', function () {
                scope.images.forEach(function (image) {
                    image.visible = false; // make every image invisible
                });

                scope.images[scope.currentIndex].visible = true; // make the current image visible
            });
            var timer;
            var sliderFunc = function () {
                timer = $timeout(function () {
                    scope.next();
                    timer = $timeout(sliderFunc, 5000);
                }, 5000);
            };

            sliderFunc();

            scope.$on('$destroy', function () {
                $timeout.cancel(timer); // when the scope is getting destroyed, cancel the timer
            });
        },
        templateUrl: 'views/slider.html'

    };
});
/**
 * Created by manojkum on 01/12/15.
 */


ecomApp.controller('cartCtrl', function ($scope, $rootScope, cartFactory, cartService, $location, $timeout) {

    var basketItem = cartService.getCartItem();
    //console.log()
    if (basketItem !== null) {
        basketItem = JSON.parse(basketItem);
        cartFactory.getCartItem(basketItem._id, function (data) {
            $scope.cartItem = JSON.parse(data.data);
            console.log($scope.cartItem)
            $rootScope.cartItemCount = cartService.sum($scope.cartItem.lineItems, 'quantity');
        });
    } else {
        console.log("no basket found");
        $scope.cartItemCount = 0;
    }
    $scope.removeProduct = function (id) {
        $('.close1').addClass('animated fadeIn')
        console.log(id)
        cartFactory.deleteProductFromCart(id, basketItem._id, 1, function (resp) {
            console.log(resp)
            toastr.success('item from cart removed successfully');
            cartFactory.getCartItem(basketItem._id, function (data) {

                $scope.cartItem = JSON.parse(data.data);
                localStorage.setItem('cartItem', JSON.stringify($scope.cartItem))
                console.log($scope.cartItem)
                $rootScope.cartItemCount = cartService.sum($scope.cartItem.lineItems, 'quantity');
            });
        });


    }
    $scope.updateQty = function (id, qty) {
        console.log(basketItem.user._id.toString());
        cartFactory.updateCart(id, basketItem._id, basketItem.user._id.toString(), qty, function (resp) {
            console.log(resp)
            toastr.success('item updated successfully');
            cartFactory.getCartItem(basketItem._id, function (data) {

                $scope.cartItem = JSON.parse(data.data);
                localStorage.setItem('cartItem', JSON.stringify($scope.cartItem))
                console.log($scope.cartItem)
                $rootScope.cartItemCount = cartService.sum($scope.cartItem.lineItems, 'quantity');
            });
        })
    };
    $scope.updateAddress = function () {
        var address = $scope.address;
        address.addressType = 'Billing';
        console.log(address)
        cartFactory.updateAddress(basketItem._id, address.addressType, address, function (data) {
            $scope.cartItem = JSON.parse(data.data);
            localStorage.setItem('cartItem', JSON.stringify($scope.cartItem));
            $timeout(function () {
                $location.path('payment');
            }, 2000);

        });
    }


});
/**
 * Created by manojkum on 28/11/15.
 */
ecomApp.controller('catalogCtrl', function ($scope, $rootScope, $timeout, rootService, catalogFactory, cartFactory, cartService, $location, credentialsFactory) {
    var mainItems = [];
    $scope.$on('handleBroadCastUser', function () {
        $scope.username = rootService.username;
    })
    $scope.logout = function () {
        $timeout(function () {
            credentialsFactory.clearCredentials();
            $rootScope.user = null;
            var path = $location.path();
            toastr.success("logout successfully !!");
            $location.path(path);
        },1000)

    };

    catalogFactory.getAll(function (response) {
        $scope.menuItems = [];
        $scope.subMenuItems = [];
        $scope.subsubMenuItems = [];

        $scope.catalogItems = JSON.parse(response.data);
        //console.log($scope.catalogItems)
        angular.forEach($scope.catalogItems, function (value, key) {
            if (value.parentCatalogId === '0') {
                $scope.menuItems.push(value);
                //console.log($scope.menuItems)
            }
            else {
                $scope.subMenuItems.push(value);
                $scope.subsubMenuItems.push(value);


                //console.log($scope.subMenuItems)
            }
        });
        //console.log($scope.subsubMenuItems.length)
        angular.forEach($scope.subMenuItems, function (value, key) {

        })
        //console.log($scope.menuItems);

        //$scope.getList()

        angular.forEach($scope.menuItems, function (value1, key1) {
            var sumMainItems = [];
            angular.forEach($scope.subMenuItems, function (value2, key2) {

                //$scope.menuItems[key2].subMenuItems = [];
                if (value2.parentCatalogId === value1._id) {
                    sumMainItems.push(value2);
                    //console.log(sumMainItems)
                    $scope.menuItems[key1].subMenuItems = sumMainItems;

                    //console.log($scope.menuItems[key2].subMenuItems);
                }
                //else {
                //    $scope.menuItems[key1].subMenuItems.push('null');
                //}

            });


        });
        //console.log($scope.menuItems);
    });

    /* if (productsFactory.getCartItem()){
     $scope.cartItem = JSON.parse(productsFactory.getCartItem()) ;
     console.log($scope.cartItem.lineItems[0].quantity)
     }else {
     $scope.cartItem = null;
     }*/


    var basketItem = cartService.getCartItem();
    if (basketItem !== null) {
        basketItem = JSON.parse(basketItem);
        cartFactory.getCartItem(basketItem._id, function (data) {
            $rootScope.cartItem = JSON.parse(data.data);
            //console.log($scope.cartItem)
            $rootScope.cartItemCount = cartService.sum($rootScope.cartItem.lineItems, 'quantity');
        });
    } else {
        $rootScope.cartItemCount = 0;
    }


    $scope.$on('$routeChangeSuccess', function () {
        var path = $location.path();
        //console.log(path);
        //$scope.catalogVisible = false;
        if (path == '/address') {
            $scope.catalogVisible = false;
        } else {
            $scope.catalogVisible = true;
        }
    });

    $scope.categorySearch = function (id) {
        catalogFactory.searchCategory(id, function (data) {
            $rootScope.productData = JSON.parse(data.data);
        })
    }


});



/**
 * Created by manojkum on 16/12/15.
 */
ecomApp.controller('loginCtrl', function ($scope, $routeParams, $location, rootService, credentialsFactory) {
        // reset login status
    //credentialsFactory.clearCredentials();

    $scope.user = {};
    $scope.login = function () {
        var user = $scope.user;
        var promise = credentialsFactory.doLogin(user.username, user.password);
        promise.then( function (resp) {
            console.log(resp.data)
            if (resp.data){
                console.log(resp.data)
                var data = JSON.parse(resp.data.data)
                credentialsFactory.setCredentials(data._id, user.username, user.password);
                //rootService.getLoggedInUser();
                dataObj.data = {"keyword":""}
                    history.back();
                    //$scope.apply();
            }
        }), function (err) {
            console.log("error:")
        }

    };

    $scope.showModal = true;
    $scope.toggleModal = function(){
        $scope.showModal = !$scope.showModal;
    };
    $('ul.nav-pills li a').click(function (e) {
        e.preventDefault();
        $('ul.nav-pills li.active').removeClass('active')
        $(this).parent('li').addClass('active')
    })
});
ecomApp.controller('orderCtrl', function($scope,$routeParams,orderFactory,$rootScope){
	var fileName =  'order'+$routeParams.orderId+'.html';
	$scope.init = function(){
		var orderId = $routeParams.orderId;
		console.log(orderId);
		orderFactory.getOrderDetails(orderId,function(result){
	        console.log(result);
	    	$scope.order = JSON.parse(result.data);
			$rootScope.cartItemCount = 0;
	    });
		downloadInnerHtml(fileName, 'text/html');

	}
	localStorage.clear();

	$scope.downloadInnerHtml = function(filename, mimeType) {
		var elHtml = document.body.innerHTML;
		var link = document.createElement('a');
		mimeType = mimeType || 'text/plain';

		link.setAttribute('download', filename);
		link.setAttribute('href', 'data:' + mimeType + ';charset=utf-8,' + encodeURIComponent(elHtml));
		link.click();
	}

	 // You can use the .txt extension if you want
});
/**
 * Created by manojkum on 03/12/15.
 */
ecomApp.controller("paymentCtrl", function ($scope, $http, cartService, $timeout)
    {
        $scope.payment = {"data":{}};

        var basketItem = cartService.getCartItem();
        basketItem = JSON.parse(basketItem);

        $scope.payment.data.firstname = "Sangamesh";
        $scope.payment.data.amount = basketItem.total.amount;
        $scope.payment.data.email = "acxyz@gmail.com";
        $scope.payment.data.phone = "9845098450";
        $scope.payment.data.productInfo = "starting Payment";
        $scope.ajaxSubmitResult = null;

        $scope.onPlaceOrder = function ()
        {
            $scope.payment.data.paymentType = "PAYUMONEY";
            var info = {
                version: "1",
                userId: "test",
                appId: "wei"
            };
            $scope.payment.info = info;
            
            $scope.payment.basketId = basketItem._id;
            
            $http.post(baseUrl+"/payment/process", $scope.payment)
                .success(function (data, status, headers, config)
                {
                    $scope.ajaxSubmitResult = angular.fromJson(data.data);


                    console.log($scope.ajaxSubmitResult);
                    $timeout(function() {
                        $scope.submit();
                        console.log('update with timeout fired')
                    }, 0);
                    

                })
                .error(function (data, status, headers, config)
                {
                    $scope[resultVarName] = "SUBMIT ERROR";
                });
        };

        // $scope.$watch("ajaxSubmitResult",
        //     function handleOnResponse( newValue, oldValue ) {
        //         if(newValue !== null && newValue.hasOwnProperty("formData")){
        //             $scope.submit();
        //         }
        //         console.log( "vm.fooCount:", newValue );
        //     }
        // );

        $scope.submit = function (){

            var payuForm = document.forms.payuForm;
            payuForm.action = $scope.ajaxSubmitResult.host;
            payuForm.submit();
        };
    });
ecomApp.controller('productsCtrl', function ($rootScope, productsFactory) {
    productsFactory.getAll(function (products) {
        $rootScope.products = JSON.parse(products.data);
        //console.log("productCtrl");
        //console.log($scope.products);
    });
});
/**
 * Created by manojkum on 30/11/15.
 */
ecomApp.controller('searchCtrl', function ($scope, $rootScope, search) {
    $rootScope.searchItems = [];
    $scope.searchText = '';
    //var payload = {'keyword': $scope.searchText};

    //Sort Array
    $rootScope.searchItems.sort();
    //Define Suggestions List
    $rootScope.suggestions = [];
    //Define Selected Suggestion Item
    $rootScope.selectedIndex = -1;

    //search function on ng-change
    $scope.search = function () {
        $scope.suggestions = [];
        var myMaxSuggestionListLength = 0;
        search.getSuggester($scope.searchText, function (resp) {
            console.log(resp.data.data.suggestion.product)
                //var data = JSON.parse(data);
            angular.forEach(resp.data.data.suggestion.product, function (value) {
                $rootScope.searchItems.push(value);

                console.log($rootScope.searchItems.length)
            });
            for (var i = 0; i < $rootScope.searchItems.length; i++) {
                var searchItems = $rootScope.searchItems[i].productName;
                console.log(searchItems)
                var searchText = $scope.searchText;
                //$scope.doSearch()
                if (searchItems.indexOf(searchText) !== -1) {
                    $scope.suggestions.push(searchItems);
                    console.log($rootScope.suggestions)
                    myMaxSuggestionListLength += 1;
                    if (myMaxSuggestionListLength == 8) {
                        break;
                    }

                }
            }

        });


    }

    //Keep Track Of Search Text Value During The Selection From The Suggestions List
    $rootScope.$watch('selectedIndex', function (val) {
        if (val !== -1) {
            $scope.searchText = $rootScope.suggestions[$rootScope.selectedIndex];
            $scope.doSearch();

        }
    });


    //Text Field Events
    //Function To Call on ng-keydown
    $rootScope.checkKeyDown = function (event) {
            if (event.keyCode === 40) { //down key, increment selectedIndex
                event.preventDefault();
                if ($rootScope.selectedIndex + 1 !== $rootScope.suggestions.length) {
                    $rootScope.selectedIndex++;
                }
            } else if (event.keyCode === 38) { //up key, decrement selectedIndex
                event.preventDefault();
                if ($rootScope.selectedIndex - 1 !== -1) {
                    $rootScope.selectedIndex--;
                }
            } else if (event.keyCode === 13) { //enter key, empty suggestions array
                event.preventDefault();
                $rootScope.suggestions = [];
            }
        }
        //Function To Call on ng-keyup
    $rootScope.checkKeyUp = function (event) {
            if (event.keyCode !== 8 || event.keyCode !== 46) { //delete or backspace
                if ($scope.searchText == "") {
                    $rootScope.suggestions = [];
                }
            }
        }
        //======================================

    //List Item Events
    //Function To Call on ng-click
    $rootScope.AssignValueAndHide = function (index) {
            $scope.searchText = $rootScope.suggestions[index];
            $rootScope.suggestions = [];
            $scope.doSearch();

        }
        //======================================
    $scope.doSearch = function () {
        var payload = {
            'keyword': $scope.searchText
        };
        search.get(payload, function (resp) {
            $rootScope.products = JSON.parse(resp.data);
        });
    };

});
/**
 * Created by manojkum on 14/12/15.
 */
ecomApp.controller('signUpCtrl', function ($scope, $location, credentialsFactory) {
    $scope.user = {};
    $scope.completeSignUp = function () {
        var user = $scope.user;
        console.log(user)
        var promise = credentialsFactory.doSignUp(user);
        promise.then(function (resp) {
            if (resp.data){
                var data = JSON.parse(resp.data.data)
                credentialsFactory.setCredentials(data._id,user.username, user.password);
                dataObj.data = {"keyword":""}
                $location.path('/');
            }
        }, function (err) {
            console.log(err);
        })
    };



    $scope.showModal = true;
    $scope.toggleModal = function(){
        $scope.showModal = !$scope.showModal;
    };
    $('ul.nav-pills li a').click(function (e) {
        e.preventDefault();
        $('ul.nav-pills li.active').removeClass('active')
        $(this).parent('li').addClass('active')
    })
})
/**
 * Created by manojkum on 01/12/15.
 */
ecomApp.controller('singleProductCtrl', function ($scope, $rootScope,$cookieStore, $route, $routeParams, $location, productsFactory, cartService, cartFactory) {
    console.log($routeParams.productId);
    $rootScope.globals = $cookieStore.get('globals') || {};
    var user = $rootScope.globals.currentUser;
        console.log(user)
    var basketItem = cartService.getCartItem();
    if (basketItem !== null) {
        basketItem = JSON.parse(basketItem);
        cartFactory.getCartItem(basketItem._id, function (data) {
            $rootScope.cartItem = JSON.parse(data.data);
            //console.log($scope.cartItem)
            $rootScope.cartItemCount = cartService.sum($rootScope.cartItem.lineItems, 'quantity');
            if ($rootScope.cartItem.lineItems.length != 0 && $rootScope.cartItem.lineItems[0].productId == $routeParams.productId) {
                $scope.pointerEvent = "none";
                $scope.cursor = "default";
                $scope.backgroundColor = "grey";
                $scope.addCartBtn = {
                    'pointer-events': $scope.pointerEvent,
                    'background-color': $scope.backgroundColor,
                    'cursor': $scope.cursor
                }
            }
        });
    } else {
        $rootScope.cartItemCount = 0;
    }


    /* if (basketItem.lineItems[0] != undefined && basketItem.lineItems[0].productId == $routeParams.productId){
     $scope.pointerEvent= "none";
     $scope.cursor = "default";
     $scope.backgroundColor= "grey";
     $scope.addCartBtn = {
     'pointer-events' :$scope.pointerEvent,
     'background-color':$scope.backgroundColor,
     'cursor':$scope.cursor
     }
     }*/

    productsFactory.getOne($routeParams.productId)
        .success(function (resp) {
            $scope.product = JSON.parse(resp.data);
            //console.log($scope.product)


        })
        .error(function (err) {
            $location.path('/');
        });
    $scope.addToCart = function (productId) {
        var items = JSON.parse(cartService.getCartItem());
        console.log(items)

        if (items === null) {
            console.log("creating cart")
            cartFactory.createCart(user.id,productId, function (resp) {
                toastr.success('Item added successfully', {timeOut: 0})
                console.log("creating cart")
                console.log(resp)
                $rootScope.cartItem = JSON.parse(resp.data);
                $rootScope.cartItemCount = cartService.sum($rootScope.cartItem.lineItems, 'quantity');
                console.log("created cart")
                console.log($rootScope.cartItem);
                localStorage.setItem('cartItem', JSON.stringify($rootScope.cartItem));
                if ($rootScope.cartItem != null && $rootScope.cartItem.lineItems[0].productId == productId) {
                    $scope.pointerEvent = "none";
                    $scope.cursor = "default";
                    $scope.backgroundColor = "grey";
                    $scope.addCartBtn = {
                        'pointer-events': $scope.pointerEvent,
                        'background-color': $scope.backgroundColor,
                        'cursor': $scope.cursor
                    }
                }

            });

        } else {
            var basketId = items._id,
                userId = items.user._id;
            cartFactory.addToCart(productId, basketId, userId, function (resp) {
                console.log(resp);
                toastr.success('Item added successfully', {timeOut: 0})
                $rootScope.cartItem = JSON.parse(resp.data);
                $rootScope.cartItemCount = cartService.sum($rootScope.cartItem.lineItems, 'quantity');
                console.log($rootScope.cartItem);
                localStorage.setItem('cartItem', JSON.stringify($rootScope.cartItem));
                for (var i = 0; i < $rootScope.cartItemCount; i++) {
                    if ($rootScope.cartItem != null && $rootScope.cartItem.lineItems[i].productId == productId) {
                        $scope.pointerEvent = "none";
                        $scope.cursor = "default";
                        $scope.backgroundColor = "grey";
                        $scope.addCartBtn = {
                            'pointer-events': $scope.pointerEvent,
                            'background-color': $scope.backgroundColor,
                            'cursor': $scope.cursor
                        }
                    }
                }

            });
        }


        //}
        //productsFactory.getCartItems()
        /*   productsFactory.addToCart(id, function (resp) {
         $scope.cartItem = [];
         $scope.cartItem = JSON.parse(resp.data);
         console.log($scope.cartItem);
         localStorage.setItem('cartItem',JSON.stringify($scope.cartItem));
         console.log(localStorage.getItem(cartItem))
         });*/

    };

    $scope.getCartItems = function () {

    }
});
ecomApp.controller('sliderCtrl', function ($scope) {
    $scope.myInterval = 5000;
    $scope.noWrapSlides = false;
    $scope.slides = [
    {
      image: './images/R10000.jpeg',
        title: 'Cotton Silk saree'
    },
    {
      image: './images/R1000_1.jpeg',
        title: 'Chocolate Leather Long Lace-Up Boots'
    },
    {
      image: './images/R10004_1.jpeg',
        title: 'Pink Belly Shoes'
    },
    {
      image: './images/R10009_1.jpeg',
    title: 'Safety Shoes'
    }
  ];
    
});

/**
 * Created by manojkum on 21/12/15.
 */
ecomApp.controller("userProfile", function ($rootScope, $scope, $cookieStore, userFactory, $timeout, $location) {
    $rootScope.user ={};
    $scope.isAddressSame = false;
    //console.log($scope.isAddressSame)
    var self = $scope;
    $rootScope.globals = $cookieStore.get('globals') || {};
    var user = $rootScope.globals.currentUser;
    userFactory.getOrders(user.id)
        .then(function (resp){
            var orders = JSON.parse(resp.data.data);
            $scope.orders = orders;
            console.log(orders)
        });


    if (user == undefined){

        $rootScope.user = null;


    } else {
        var promise = userFactory.get(user.id);
        promise.then(function (resp) {
            console.log(resp.data);
            //var resp = JSON.parse(resp.data);
            $rootScope.user = JSON.parse(resp.data.data);
            //console.log($scope.user)
        }, function (err) {

        });
    }


    self.changePassword = function (db) {
        console.log(db)
        var promise = userFactory.changePassword(db);
        promise.then(function (resp) {
            //console.log(resp);
            $scope.success = true;
            toastr.success("user updated successfully")
            $timeout(function () {
                $location.path('/profile');
            },2000);

        }), function (err) {
            console.log(err)
        }
    };
    self.updateProfile = function (db) {
        var promise = userFactory.update(db);
        promise.then(function (resp) {
            //console.log(resp.status)
            $scope.success = true;
            toastr.success("user updated successfully")
            $timeout(function () {
                $location.path('/profile');
            },2000);

        }), function (err) {
            console.log(err)
        }
    }
    self.addAddress = function (db) {
        //console.log($scope.isAddressSame)
        if ($scope.isAddressSame == true){
            db.billingAddress = [];
            db.billingAddress.push(db.shippingAddress[0]);
            //console.log(db.billingAddress)
        }
        console.log(db);
        var promise = userFactory.addAddress(db);
        promise.then(function (resp) {
            console.log(resp.success);
            $scope.success = true;
            toastr.success("address updated successfully")
            $timeout(function () {
                $location.path('/profile');
            },2000);

        })

    }
    self.getOrders = function () {
        userFactory.getOrders()
            .then(function (resp){
                var orders = JSON.parse(resp.data);
                $scope.orders = orders;
                console.log($scope.orders)
            });
    }


});
/**
 * Created by manojkum on 01/12/15.
 */
ecomApp.factory('cartFactory', function ($http) {
    var cartFactory = {},
        dataObj = {
            "template":"",
            "info":{"version":1,"userId":"test","appId":"wei"},
            "data":{"keyword":""
            }
        };
    cartFactory.createCart = function (userId,productId,callback) {
        dataObj.data = {
            userId:userId,
            productId:productId,
            qty:1
        };
        $http.post(baseUrl+'/cart/insert',dataObj)
            .success(function (data) {
                callback(data);
            })
            .error(function (err) {
                callback(err);
            });
    }
    cartFactory.addToCart = function (productId, basketId, userId, callback){
        dataObj.data = {
            basketId:basketId,
            userId:userId,
            productId:productId,
            qty:1
        };
        $http.put(baseUrl+'/cart/addproduct',dataObj )
            .success(function (data) {
                callback(data);
            })
            .error(function (err) {
                callback(err);
            });
    };
    cartFactory.getCartItem = function (id,callback) {
        $http.get(baseUrl+'/cart/find/'+id)
            .success(function (data) {
                //console.log(data);
                callback(data);
                //return data;
            })
            .error(function (err) {
                callback(err);
            });
        /*this.cartItem = localStorage.getItem('cartItem');
        console.log(this.cartItem)
        return this.cartItem;*/
    };
    cartFactory.updateCart = function (productId,basketId,userId,qty,callback){
        dataObj.data = {
            basketId:basketId,
            userId:userId,
            productId:productId,
            qty:qty
        };
        $http.post(baseUrl+'/cart/updateproduct',dataObj)
            .success(function (data) {
                callback(data);
            })
            .error(function (err) {
                callback(err);
            });
    };
    cartFactory.deleteProductFromCart = function (productId,basketId,qty,callback){
        dataObj.data = {
            basketId:basketId,
            userId:"564daf9706354d172e803b0f",
            productId:productId,
            qty:qty
        };
        $http.put(baseUrl+'/cart/removeproduct',dataObj)
            .success(function (data) {
                callback(data);
            })
            .error(function (err) {
                callback(err);
            });
    };
    
    cartFactory.updateAddress = function (basketId,addressType,address,callback) {
        dataObj.data = {
            basketId:basketId,
            addressType:addressType,
            address:address
        };
        $http.put(baseUrl+'/cart/updateaddres',dataObj)
            .success(function (resp) {
                callback(resp);
            })
            .error(function (err) {
                callback(err);
            });
    }
    return cartFactory;
} );
/* global ecomApp */

ecomApp.factory('catalogFactory', function ($http){
	var catalogService = {},
        dataObj = {
               "template":"",
               "info":{"version":1,"userId":"test","appId":"wei"},
               "data":{} 
               
        };
    catalogService.getAll = function (callback) {
        $http.post(baseUrl+'/catalog/list',{cache : true},dataObj )
              .success(callback);
        //console.log(baseUrl + '/catalog/list');
    };
    catalogService.searchCategory = function (categoryId,callback) {
        dataObj.data.keyword = categoryId;
        $http.post(baseUrl+'/search',dataObj)
                .success(callback);
    };
    return catalogService;
});


/**
 * Created by manojkum on 14/12/15.
 */
ecomApp.factory('credentialsFactory', function ($http, $cookieStore, $rootScope, $timeout) {
    var credObj = {};

    credObj.doSignUp = function (db) {
        dataObj.data.firstName = db.fName;
        dataObj.data.lastName = db.lName;
        dataObj.data.company = db.company;
        dataObj.data.company = db.phone;
        dataObj.data.userCredentials = {userName:db.username, password:db.password, email:db.email, resetPassword: false};
        dataObj.data.shippingAddress = [];
        dataObj.data.billingAddress = [] ;
        //dataObj.data.createdDate = new Date();
        return $http.post(baseUrl+'/customer/insert',dataObj);
    };
    credObj.doLogin = function (username, password) {
        dataObj.data = { userName: username, password: password };
        return $http.post(baseUrl+'/credentials/login', dataObj);

           /* .success(function (response) {
                callback(response);
            })
            .error(function (err) {
                callback(err);
            })*/


    };
    credObj.setCredentials = function (id, username, password) {
        var authdata = Base64.encode(username + ':' + password);

        $rootScope.globals = {
            currentUser: {
                id:id,
                username: username,
                authdata: authdata
            }
        };

        $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
        $cookieStore.put('globals', $rootScope.globals);
    };
    credObj.clearCredentials = function () {
        $rootScope.globals = {};
        $cookieStore.remove('globals');
        $http.defaults.headers.common.Authorization = 'basic';
    };
    // Base64 encoding service used
    var Base64 = {

        keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                    this.keyStr.charAt(enc1) +
                    this.keyStr.charAt(enc2) +
                    this.keyStr.charAt(enc3) +
                    this.keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = this.keyStr.indexOf(input.charAt(i++));
                enc2 = this.keyStr.indexOf(input.charAt(i++));
                enc3 = this.keyStr.indexOf(input.charAt(i++));
                enc4 = this.keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }
    };

    return credObj;
});
/* global ecomApp */

ecomApp.factory('orderFactory', function ($http){
	var orderService = {},
        dataObj = {
               "template":"",
               "info":{"version":1,"userId":"test","appId":"wei"},
               "data":{} 
               
        };
    orderService.getOrderDetails = function (orderId,callback) {
        $http.get(baseUrl+'/order/find/'+orderId,{cache : true})
              .success(callback);
              
        console.log(baseUrl + '/order/find/');
    };
    return orderService;
});

/**
 * Created by manojkum on 03/12/15.
 */


ecomApp.factory('productsFactory', function ($http) {
    var products = {};
    dataObj.data.keyword = "";
    products.getAll = function (callback) {
        $http.post(baseUrl+'/search',dataObj )
              .success(function (data) {
                  callback(data);
              });
        //console.log(baseUrl + '/search');
    };
    products.getOne = function (id) {
        return $http.get(baseUrl+'/product/find/'+id)
    };

    return products;

});

/**
 * Created by manojkum on 16/12/15.
 */
ecomApp.factory('rootService', function ($rootScope, $cookieStore) {
    var sharedService = {};
    sharedService.username = '';
    sharedService.getLoggedInUser = function () {
        var loggedIn = $rootScope.globals.currentUser;
        this.username = loggedIn.username;
        console.log(loggedIn)
        this.broadCastUser();
    };
    sharedService.broadCastUser = function () {
        $rootScope.$broadcast('handleBroadCastUser');
    };
    return sharedService;
});
/**
 * Created by manojkum on 17/12/15.
 */
ecomApp.factory('routeHistory', ['$rootScope', '$state', function ($rootScope, $state) {

        var lastHref = "/",
            lastStateName = "",
            lastParams = {};

        $rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
            lastStateName = fromState.name;
            lastParams = fromParams;
            lastHref = $state.href(lastStateName, lastParams)
        })

        return {
            getLastHref: function (){ return lastHref ; }
            //goToLastState: function (){ return $state.go(lastStateName, lastParams); },
        }

    }]);
/**
 * Created by manojkum on 18/12/15.
 */
ecomApp.factory('search', function ($http, $cacheFactory) {
    var esUrl = "http://localhost:9080/rokitt/ecommerce/" ,
        search = {};
    search.get = function (payload, successCallback) {
        dataObj.data = payload
        var key = 'search_' + payload.keyword;
        $http.post(baseUrl + '/search', dataObj).then(function (resp) {
            //$cacheFactory(key).put('result', resp.data);
            //console.log(resp.data)
            successCallback(resp.data);
        });
        /*  if($cacheFactory.get(key) == undefined || $cacheFactory.get(key) == ''){
              $http.post(baseUrl+'/search', dataObj).then(function(resp){
                  $cacheFactory(key).put('result', resp.data);
                  //console.log(resp.data)
                  successCallback(resp.data);
              });
          }else{
              successCallback($cacheFactory.get(key).get('result'));
          }*/
    };
    search.getSuggester = function (params,successCallback) {
        var dataJson = {
            "info": {
                "userId": "",
                "appId": "",
                "version": 1,
                "serviceId": "",
                "persistId": "",
                "recId": ""
            },
            "data": {
                "text": params,
                "categoryCount": 5,
                "productCount": 5
            }
        };
        $http.post(esUrl+'suggester', dataJson).then(function (resp) {
            //$cacheFactory(key).put('result', resp.data);
            //console.log(resp.data)
            successCallback(resp.data);
        });


    };
    search.getProduct = function (params) {

    };
    
    return search;
});
/**
 * Created by manojkum on 18/12/15.
 */
ecomApp.factory('userFactory', function ($http) {
    var customer = {};
    dataObj.data={};
    dataObj.data.userCredentials = {};
    customer.get = function (id) {
        return $http.get(baseUrl+"/customer/find/"+id);
    };
    customer.update = function (formData) {
        console.log(formData)
        dataObj.data.userCredentials = {};
        dataObj.data._id = formData._id;
        dataObj.data.userCredentials.userName = formData.userCredentials.userName;
        dataObj.data.userCredentials.email = formData.userCredentials.email;
        dataObj.data.firstName = formData.firstName;
        dataObj.data.lastName = formData.lastName;
        dataObj.data.phoneNo = formData.phoneNo;

        return $http.put(baseUrl+"/customer/update", dataObj);
    };
    customer.delete = function (id) {
        return $http.delete(baseUrl+"/customer/delete/"+id);
    };
    customer.changePassword = function (formData) {
        console.log(formData)
        dataObj.data.userCredentials = {};
        dataObj.data._id = formData._id;
        dataObj.data.userCredentials.userName = formData.userCredentials.userName;
        dataObj.data.userCredentials.email = formData.userCredentials.email;
        dataObj.data.userCredentials.password = formData.password;
        return $http.put(baseUrl+"/customer/update", dataObj)

    }
    customer.addAddress = function (formData) {
        dataObj.data.billingAddress =[];
        dataObj.data.shippingAddress = [];
        dataObj.data._id = formData._id;
        dataObj.data.billingAddress[0] = formData.billingAddress[0];
        /*{
            "addressName":"mera ghar",
            "city":"Bangalore",
            "createdDate":"Nov 30, 2015 4:45:33 PM",
            "addressType":"Billing_And_Shipping"
        };*/
        dataObj.data.shippingAddress[0] = formData.shippingAddress[0];
            /*{
                "addressName":"mera ghar1",
                "city":"Bangalore",
                "createdDate":"Nov 30, 2015 4:45:33 PM",
                "addressType":"Billing_And_Shipping"
            };*/
        return $http.put(baseUrl+"/customer/update", dataObj)
    };
    customer.getOrders = function (id){
        dataObj.data = {};
        dataObj.data.customerId = id;
        return $http.post(baseUrl+"/order/list", dataObj);
    }
    return customer;
})
/**
 * Created by manojkum on 18/12/15.
 */
ecomApp.service('ConfigService', function (){
    var _environments = {
        local: {
            host: ['l', 'localhost'],
            config: {
                api_endpoint: '/api/v1',
                node_url: 'http://localhost:9998'
            }
        },
        test: {
            host: ['test.domain.com', 'beta.domain.com'],
            config: {
                api_endpoint: '/api/v1',
                node_url: 'http://domain.com:9998'
            }
        },
        prod: {
            host: 'domain.com',
            config: {
                api_endpoint: '/api/v1',
                node_url: 'http://domain.com:9998'
            }
        }
    }, _environment;

    this.getEnvironment = function (){
        var host = window.location.host;
        if (_environment) {
            return _environment;
        }
        for (var environment in _environments){
            if (typeof(_environments[environment].host) && typeof(_environments[environment].host) == 'object') {
                if (_environments[environment].host.indexOf(host) >= 0) {
                    _environment = environment;
                    return _environment;
                }
            } else {
                if (typeof(_environments[environment].host) && _environments[environment].host == host) {
                    _environment = environment;
                    return _environment;
                }
            }
        }
        return null;
    };
    this.get = function (property) {
        return _environments[this.getEnvironment()].config[property];
    };
});
/**
 * Created by manojkum on 03/12/15.
 */
ecomApp.service('cartService', function () {
    this.getCartItem = function () {
        this.cartItem = localStorage.getItem('cartItem');
        //console.log(this.cartItem)
        return this.cartItem;
    };
    this.sum = function(items, prop){
        return items.reduce( function(a, b){
            return a + b[prop];
        }, 0);
    };
    this.removeProduct = function (productId) {
        var cartItem = this.getCartItem();
        if (cartItem !== null){
            cartItem = JSON.parse(cartItem);
            angular.forEach(cartItem.lineItems, function (value,key) {
                //console.log(value.productId)
                if (value.productId === productId)
                    cartItem.lineItems.splice(key,1);
            })
           // console.log(cartItem)
            //removing item
        }


    };
   /* this.appendToStorage = function(name, data){
        var old = localStorage.getItem(name);
        if(old === null) old = "";
        localStorage.setItem(name, old + data);
    };*/




});
