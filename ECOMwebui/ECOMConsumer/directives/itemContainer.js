/**
 * Created by manojkum on 28/11/15.
 */

ecomApp.directive('itemContainer', function () {
    return{
        restrict: 'E',
        templateUrl: './directives/itemContainer.html',
    }
})