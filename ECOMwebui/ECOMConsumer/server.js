var express = require('express');
var app = express();
app.use(express.static(__dirname + '/'));
console.log((__dirname + '/'))
app.use(function(req, res) {
  res.sendfile(__dirname + '/index.html');
});
var server = app.listen(process.env.PORT || 3000,'0.0.0.0', function () {
  //var host = server.address().address;
  var port = server.address().port;

  console.log('App is listening at port : %s', port);
});