var ecomAppServices = angular.module('EcomServices',[]);

ecomAppServices.service("OrganizationService",['$http', function($http){
    
    this.getOrganizationList= function(){
        var serviceURL = config.apiUrl+"/organization/list"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
		//console.log("serviceURL :" + serviceURL);
		return  $http.post(serviceURL , dataObj);
    }
    
    this.addOrganization= function(dataObj){
        var serviceURL = config.apiUrl+"/organization/insert"; 
		var reqparam = {"template":"","info":config.apiInfo,"data":dataObj};
		//console.log("serviceURL :" + serviceURL);
		return  $http.post(serviceURL , reqparam);
    }
    this.updateOrganization= function(dataObj){
        var serviceURL = config.apiUrl+"/organization/update";
        var reqparam = {"template":"","info":config.apiInfo,"data":dataObj};
		//console.log("serviceURL :" + serviceURL);
		return  $http.put(serviceURL , reqparam);
    }
    this.deleteOrganization= function(dborgid){
        var serviceURL = config.apiUrl+"/organization/delete/"+dborgid;
		//console.log("serviceURL :" + serviceURL);
		return  $http.delete(serviceURL);
    }
    this.findOrganization= function(dborgid){
        var serviceURL = config.apiUrl+"/organization/find/"+dborgid;
        console.log("findOrganization :" + serviceURL);
        return  $http.get(serviceURL);
    }
    this.getExportOrganizationList= function(){
        var serviceURL = config.apiUrl+"/export/organization/list"; 
        //var serviceURL = "http://localhost:4567/export/organization/list"; 
        
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
		return  $http.get(serviceURL , dataObj);
    }
    this.fileUpload= function(fileObj){
        var serviceURL = config.apiUrl+"/organization/upload/";
        var dataObj = {"template":"","info":config.apiInfo,"data":{'file':fileObj}};
		return  $http.post(serviceURL , dataObj);
    }
}]);

ecomAppServices.service("ProductService",['$http', function($http) {
    
    this.getProductList= function(){
        var serviceURL = config.apiUrl+"/product/list"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
        //console.log("serviceURL :" + serviceURL);
        return  $http.post(serviceURL , dataObj);
    }
    
    this.addProduct= function(dataObj){
        var serviceURL = config.apiUrl+"/product/insert"; 
        var reqparam = {"template":"","info":config.apiInfo,"data":dataObj};
        //console.log("serviceURL :" + serviceURL);
        return  $http.post(serviceURL , reqparam);
    }
    this.findProduct= function(dborgid){
        var serviceURL = config.apiUrl+"/product/find/"+dborgid;
        //console.log("serviceURL :" + serviceURL);
        return  $http.get(serviceURL);
    }
    this.updateProduct= function(dataObj){
        var serviceURL = config.apiUrl+"/product/update";
        var reqparam = {"template":"","info":config.apiInfo,"data":dataObj};
        console.log("updateProduct :" + JSON.stringify(dataObj));
        return  $http.put(serviceURL , reqparam);
    }
    this.deleteProduct= function(dborgid){
        var serviceURL = config.apiUrl+"/product/delete/"+dborgid;
        //console.log("serviceURL :" + serviceURL);
        return  $http.delete(serviceURL);
    }
}]);

ecomAppServices.service("PricingService",['$http', function($http) {
    
    this.getPricingList= function(){
        var serviceURL = config.apiUrl+"/pricing/list"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
        return  $http.post(serviceURL , dataObj);
    }
    
    this.addPricing= function(dataObj){
        var serviceURL = config.apiUrl+"/pricing/insert"; 
        var reqparam = {"template":"","info":config.apiInfo,"data":dataObj};
        return  $http.post(serviceURL , reqparam);
    }
    this.findPricing= function(pricingid){
        var serviceURL = config.apiUrl+"/pricing/find/"+pricingid; 
        
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
		return  $http.get(serviceURL , dataObj);
        
    }
    this.updatePricing= function(dataObj){
        var serviceURL = config.apiUrl+"/pricing/update"; 
        var reqparam = {"template":"","info":config.apiInfo,"data":dataObj};
        return  $http.put(serviceURL , reqparam);
    }
    this.deletePricing= function(dbpricingid){
        var serviceURL = config.apiUrl+"/pricing/delete/"+dbpricingid;
		return  $http.delete(serviceURL);
    }
}]);

ecomAppServices.service("UserService",['$http', function($http) {
    
    this.getUserList= function(){
        var serviceURL = config.apiUrl+"/user/list"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
		//console.log("serviceURL :" + serviceURL);
		return  $http.post(serviceURL , dataObj);
    }
    this.findUser= function(userid){
        var serviceURL = config.apiUrl+"/user/find/"+userid; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
		//console.log("serviceURL :" + serviceURL);
		return  $http.get(serviceURL , dataObj);
        
    }
    this.addUser= function(dataObj){
        var serviceURL = config.apiUrl+"/user/insert"; 
		var reqparam = {"template":"","info":config.apiInfo,"data":dataObj};
		//console.log("serviceURL :" + serviceURL);
		return  $http.post(serviceURL , reqparam);
    }
    this.updateUser= function(dataObj){
        var serviceURL = config.apiUrl+"/user/update";
        var reqparam = {"template":"","info":config.apiInfo,"data":dataObj};
		//console.log("serviceURL :" + serviceURL);
		return  $http.put(serviceURL , reqparam);
    }
    this.deleteUser= function(dborgid){
        var serviceURL = config.apiUrl+"/user/delete/"+dborgid;
		//console.log("serviceURL :" + serviceURL);
		return  $http.delete(serviceURL);
    }
}]);

ecomAppServices.service("ConsumerService",['$http', function($http) {
    
    this.getUserList= function(){
        var serviceURL = config.apiUrl+"/customer/list"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
		//console.log("serviceURL :" + serviceURL);
		return  $http.post(serviceURL , dataObj);
    }
    this.findUser= function(userid){
        var serviceURL = config.apiUrl+"/customer/find/"+userid; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
		//console.log("serviceURL :" + serviceURL);
		return  $http.get(serviceURL , dataObj);
        
    }
    this.addUser= function(dataObj){
        var serviceURL = config.apiUrl+"/customer/insert"; 
		var reqparam = {"template":"","info":config.apiInfo,"data":dataObj};
		//console.log("serviceURL :" + serviceURL);
		return  $http.post(serviceURL , reqparam);
    }
    this.updateUser= function(dataObj){
        var serviceURL = config.apiUrl+"/customer/update";
        var reqparam = {"template":"","info":config.apiInfo,"data":dataObj};
		//console.log("serviceURL :" + serviceURL);
		return  $http.put(serviceURL , reqparam);
    }
    this.deleteUser= function(dborgid){
        var serviceURL = config.apiUrl+"/customer/delete/"+dborgid;
		//console.log("serviceURL :" + serviceURL);
		return  $http.delete(serviceURL);
    }
}]);

ecomAppServices.service("AuthService",['$http', function($http) {
    
    this.login= function(data){
        var serviceURL = config.apiUrl+"/credentials/login"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":data};
		//console.log("serviceURL :" + serviceURL);
		return  $http.post(serviceURL , dataObj);
    }
    
}]);

ecomAppServices.service("CatalogService",['$http', function($http) {
    
    this.getAllCatalogList= function(){
        var serviceURL = config.apiUrl+"/catalog/list"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
        //console.log("serviceURL :" + serviceURL);
        return  $http.post(serviceURL , dataObj);
    }

    this.getCatalogList= function(data){
        var serviceURL = config.apiUrl+"/catalog/list"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":data};
        return  $http.post(serviceURL , dataObj);
    }
    this.insertCatalog=function(data){
        var serviceURL = config.apiUrl+"/catalog/insert"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":data};
        return  $http.post(serviceURL , dataObj);
    }
    this.updateCatalog=function(data){
        var serviceURL = config.apiUrl+"/catalog/update"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":data};
        console.log("dataObj of Catalog update : " + JSON.stringify(dataObj));
        return  $http.put(serviceURL , dataObj);
    }
    this.deleteCatalog=function(catID){
        var serviceURL = config.apiUrl+"/catalog/delete/"+catID; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}};
        return  $http.delete(serviceURL , dataObj);
    }
    
}]);

ecomAppServices.service("AttributeGroupService",['$http', function($http) {
    
    this.getAttributeGroupList= function(){
        var serviceURL = config.apiUrl+"/attributegroup/list"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
        return  $http.post(serviceURL , dataObj);
    }  
    this.addAttributeGroup= function(data){
        var serviceURL = config.apiUrl+"/attributegroup/insert"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":data}
        return  $http.post(serviceURL , dataObj);
    } 
    this.findAttributeGroup= function(agID){
        var serviceURL = config.apiUrl+"/attributegroup/find/"+ agID; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
        return  $http.get(serviceURL , dataObj);
    }  
    this.updateAttributeGroup= function(data){
        var serviceURL = config.apiUrl+"/attributegroup/update"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":data}
        return  $http.put(serviceURL , dataObj);
    } 
    this.deleteAttributeGroup= function(id){
        var serviceURL = config.apiUrl+"/attributegroup/delete/"+id;
		return  $http.delete(serviceURL);
    }
}]);

ecomAppServices.service("OrderService",['$http', function($http) {
    
    this.getOrderList= function(){
        var serviceURL = config.apiUrl+"/order/list"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
        return  $http.post(serviceURL , dataObj);
    }  
    
    this.findOrder= function(orderID){
        var serviceURL = config.apiUrl+"/order/find/"+ orderID; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
        return  $http.get(serviceURL , dataObj);
    } 
    this.deleteOrder= function(id){
        var serviceURL = config.apiUrl+"/order/delete/"+id;
		return  $http.delete(serviceURL);
    }
    this.getExportOrderList= function(){
        var serviceURL = config.apiUrl+"/export/order/list"; 
        //var serviceURL = "http://localhost:4567/export/order/list"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{}}
		return  $http.get(serviceURL , dataObj);
    }
}]);

ecomAppServices.service("MDTService",['$http', function($http) {
    this.createDump= function(){
        var serviceURL = config.apiUrl+"mongodb/dump"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{"filePath":config.dumpPath}};
		return  $http.post(serviceURL , dataObj);
    }
    this.restoreDump= function(){
        var serviceURL = config.apiUrl+"mongodb/restore"; 
        var dataObj = {"template":"","info":config.apiInfo,"data":{"filePath":config.dumpPath}};
		return  $http.post(serviceURL , dataObj);
    }
}]);








