var express = require('express');
var app = express();
app.use(express.static(__dirname + '/'));
app.use(express.static(__dirname + '/BOUser'));
app.use(express.static(__dirname + '/Seller'));



app.use(function(req, res){
    var url = req.url;
    var index = url.indexOf('/seller');
    console.log(url + " : " + index);
    if (index!=-1) {
        res.sendFile(__dirname + '/Seller/index.html');
    } else {
        res.sendFile(__dirname + '/BOUser/index.html');
    }
}); 

var server = app.listen(process.env.PORT || 4000, function () {
  var port = server.address().port;
  console.log('App is listening at port : %s', port);
});