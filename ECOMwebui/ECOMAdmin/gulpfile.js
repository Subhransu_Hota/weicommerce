'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

var paths = {
    scripts: [
        './BOUser/**/*.js',
        '!./node_modules/**',
        '!./gulpfile.js',
        '!./BOUser/pkgs/js/all.js',
        '!./BOUser/apps.js',
        '!./bower_components/**',
        '!./server.js'
    ],
    images: './BOUser/images/*',
    css: './BOUser/styles/scss/*.scss'
};
var path = {
    scripts: [
        './Seller/**/*.js',
        '!./node_modules/**',
        '!./gulpfile.js',
        '!./Seller/pkg/js/all.js',
        '!./Seller/app.js',
        '!./bower_components/**',
        '!./server.js'
    ],
    images: './Seller/image/*',
    css: './Seller/style/scss/*.scss'
};
gulp.task('style', function () {
    gulp.src('./Seller/style/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./Seller/pkg/css/'));
});
gulp.task('script', function () {
    gulp.src(path.scripts)
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./Seller/pkg/js/'));
});
//watch task for seller
gulp.task('watch', function () {
    gulp.watch(path.css, ['style']);
    gulp.watch(path.scripts, ['script']);
});

gulp.task('styles', function () {
    gulp.src('./BOUser/styles/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./BOUser/pkgs/css/'));
});

gulp.task('scripts', function () {
    gulp.src(paths.scripts)
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./BOUser/pkgs/js/'));
});
//watch task for BOUser
gulp.task('watchs', function () {
    gulp.watch(paths.css, ['styles']);
    gulp.watch(paths.scripts, ['scripts']);
});

//default task
gulp.task('default', ['watchs','watch', 'scripts','script','style','styles']);