//Define an angular module for our App
var ecomApp = angular.module('ecomApp', ['ngRoute','toaster','ngAnimate','ui.bootstrap','EcomServices','GlobalFun']);

//Define Routing for app
ecomApp.config(['$routeProvider', '$locationProvider',function($routeProvider,$locationProvider) {
    $routeProvider
		.when('/', {
                templateUrl: './views/login.html',
                controller: 'AuthCtrl'
        })
        .when('/home', {
			templateUrl: './views/home.html',
            controller: 'homeCtrl'
		})
        .when('/pricing', {
                templateUrl: './views/pricing.html',
                controller: 'pricingCtrl'
            })
        .when('/pricing/new', {
                templateUrl: './views/pricingnew.html',
                controller: 'pricingCtrl'
            })
        .when('/pricing/details/:id', {
                templateUrl: './views/pricingupdate.html',
                controller: 'pricingCtrl'
            })
        .when('/organization', {
                templateUrl: './views/organization.html',
                controller: 'organizationCtrl'
            })
         .when('/organization/new', {
                templateUrl: './views/organizationnew.html',
                controller: 'organizationCtrl'
            })
         .when('/orgimportexport', {
                templateUrl: './views/organizationimportexport.html',
                controller: 'organizationImportExportCtrl'
            })
        .when('/orgexport', {
                templateUrl: './views/organizationexport.html',
                controller: 'organizationImportExportCtrl'
            })
        .when('/orgimport', {
                templateUrl: './views/organizationimport.html',
                controller: 'organizationImportExportCtrl'
            })
        
        .when('/organization/details/:id', {
            templateUrl: './views/organizationupdate.html',
            controller: 'organizationCtrl'
        })
        .when('/consumer', {
                templateUrl: './views/consumer.html',
                controller: 'consumerCtrl'
            })
        .when('/consumer/new', {
                templateUrl: './views/consumernew.html',
                controller: 'consumerCtrl'
            })
        .when('/consumer/details/:id', {
                templateUrl: './views/consumerupdate.html',
                controller: 'updateConsumerCtrl'
            })
        .when('/user', {
                templateUrl: './views/user.html',
                controller: 'userCtrl'
            })
        .when('/user/new', {
                templateUrl: './views/usernew.html',
                controller: 'userCtrl'
            })
        .when('/user/details/:id', {
                templateUrl: './views/userupdate.html',
                controller: 'updateUserCtrl'
            })
        .when('/catalog', {
                templateUrl: './views/catalog.html',
                controller: 'catalogCtrl'
            })
        .when('/attributegroup', {
                templateUrl: './views/attributegroup.html',
                controller: 'attributeGroupCtrl'
            })
        .when('/attributegroup/new', {
                templateUrl: './views/attributegroupnew.html',
                controller: 'attributeGroupCtrl'
            })
        .when('/attributegroup/details/:id', {
                templateUrl: './views/attributegroupupdate.html',
                controller: 'updateAGCtrl'
            })
        .when('/order', {
                templateUrl: './views/order.html',
                controller: 'orderCtrl'
            })
         .when('/order/details/:id', {
                templateUrl: './views/orderdetail.html',
                controller: 'orderCtrl'
            })
        .when('/orderimportexport', {
                templateUrl: './views/orderimportexport.html',
                controller: 'orderImportExportCtrl'
            })
        .when('/orderexport', {
                templateUrl: './views/orderexport.html',
                controller: 'orderImportExportCtrl'
            })
        .when('/orderimport', {
                templateUrl: './views/orderimport.html',
                controller: 'orderImportExportCtrl'
            })
        .when('/mdtimportexport', {
                templateUrl: './views/mdtimportexport.html',
                controller: 'mdtImportExportCtrl'
            })
        .when('/mdtexport', {
                templateUrl: './views/mdtexport.html',
                controller: 'mdtImportExportCtrl'
            })
        .when('/mdtimport', {
                templateUrl: './views/mdtimport.html',
                controller: 'mdtImportExportCtrl'
            })
        .when('/product', {
                templateUrl: './views/product/home.html',
                controller: 'productCtrl'
            })
        .when('/product/new', {
                templateUrl: './views/product/product.html',
                controller: 'productCtrl'
            })
        .when('/product/details/:id', {
                templateUrl: './views/product/product_details.html',
                controller: 'updateProductCtrl'
            })
        .when('/404', {
            templateUrl: 'views/error.html',
            controller: 'errorCtrl'
        });
        $locationProvider.html5Mode(true);
}]).run(function($rootScope, $location){
            $rootScope.$on( "$routeChangeStart", function(event, next, current)
            {   
                var loginuser = sessionStorage.userName;
                $rootScope.loginUserName =  loginuser;
                console.log("Loggedin User : " + loginuser);
                $rootScope.isHeaderVisible = true;
                var isUserDefined = angular.isUndefined(loginuser);
                
                if(loginuser!=="null" && (!isUserDefined)) {
                        var path=$location.path();
                        
                        if(path === "/" || angular.isUndefined(path)){
                            $location.path("/home");
                        }
                }else{
                        
                        $location.path("/");
                        $rootScope.isHeaderVisible = false;
                }

            });
});

