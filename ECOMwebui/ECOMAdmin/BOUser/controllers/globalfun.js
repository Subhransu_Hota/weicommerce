var globalFun = angular.module('GlobalFun',[]);
globalFun.service("Validation",[function() {
    
    this.isNameValid= function(name)
    {
        // true  ---> if String is not empty & alfa numeric.
        name = name.trim();
        if(name.length>0) {
                var patt1 = /([^a-z0-9])/gi;   // check for alfa numeric characters
                var result = name.match(patt1);
                if(result==null) 
                {
                    return true;
                }else{ return false;}
        } else { return false;}
    }
    this.isEmailValid=function(email){
        
        var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
        if (filter.test(email)) {
            return true;
        }   
        else{
            return false;
        }
        
    }
    
    this.isPhoneValid= function(phonenum)
    {
        //return true  -- > if phone num is of 10 digit
        if(angular.isNumber(Number(phonenum))){
            if(phonenum.length==10){ 
                return true;
            }else{ return false;}
            
        }else{
            return false;
        }
    }
}]);

globalFun.service("CommonFun",[function() { 

    this.getCurrentDateTime= function()
    {
        //date format  ---> Sep 30, 2015 11:43:44 AM
        var d = new Date();
		var monthsArr = ["Jan","Feb","Mar", "Apr","May","Jun","Jul","Sep","Oct","Nov","Dec"];
		var monthIndex  = d.getMonth()-1;
		var hours = d.getHours()%12 || 12;
		var mins  = d.getMinutes();
        var secs = d.getSeconds();
		var specifier = d.getHours()<12 ? "AM" :"PM";
		var formattedDate = monthsArr[monthIndex]+" " + paddingZero(d.getDate()) + ", "+d.getFullYear() + " "
                            + paddingZero(hours) +":" + paddingZero(mins) +":" + 
                            paddingZero(secs)+ " " + specifier;
        
        return  formattedDate;
    }
    function paddingZero(txt)
    {
            txt = txt.toString();

            if (txt.length === 1)
            {
                return "0" + txt;
            }
            else
            {
                return txt;
            }
    }

}]);
