ecomApp.controller('organizationCtrl', function ($scope, $rootScope, $routeParams, $location, OrganizationService, toaster) {

    $scope.organizationList = {};
    $scope.orgID = "";
    $scope.orgName = "";
    $scope.orgDescription = "";
    $scope.dbOrgID = "";
    $scope.masterSelect = false;
    $scope.orgIDErrorMessage = "";
    $scope.isUpdate = false;
    $scope.orgModalTitle = "";
    $scope.isSingleDelete = true;
    $scope.deletedItem = {};
    $scope.orgDeleteMessage = "";
    $scope.isNoItemSelected = "true";
    $scope.isOrgIDAvailable = true;

    $scope.init = function () {
        var promise = OrganizationService.findOrganization($routeParams.id);
        promise.success(function (response) {
            var org = JSON.parse(response.data);
            $scope.dbOrgID = org._id;
            $scope.orgID = org.organizationId;
            $scope.orgName = org.organizationName;
            $scope.orgDescription = org.description;
        });
        promise.error(function (err) {
            toaster.pop('error', "Organization", "Service failed");
        });
    }
    $scope.getOrganizationList = function () {
        var promise = OrganizationService.getOrganizationList();
        promise.success(function (response) {
            $scope.organizationList = JSON.parse(response.data);
            console.log("Count of Organization : " + $scope.organizationList.length);
        });
        promise.error(function (err) {
            toaster.pop('error', "Organization", "Service failed");
        });
    }


    $scope.addOrganizationData = function () {
        $location.path("/organization/new");
    }
    $scope.addOrganization = function () {
        $scope.orgID = $scope.orgID.trim();
        $scope.isOrgIDAvailable = true;

        if (isIDValid($scope.orgID)) {
            $scope.isOrgIDAvailable = true;
            var dataObj = {
                "organizationId": $scope.orgID,
                "organizationName": $scope.orgName,
                "description": $scope.orgDescription
            };
            var promise = OrganizationService.addOrganization(dataObj);
            promise.success(function (response) {
                var msg = $scope.orgName + " organization has added successfully ";
                toaster.pop('success', "Organization", msg);
                resetOrganizationField();
            });
            promise.error(function (err) {
                toaster.pop('error', "Organization", "Service failed");
            });
        } else {
            $scope.orgID = "";
            $scope.isOrgIDAvailable = false;
            $scope.orgIDErrorMessage = "Organization ID must be alfa-numeric.";
        }
    }
    $scope.updateOrganization = function ($this) {
        $scope.orgID = $scope.orgID.trim();
        $scope.isOrgIDAvailable = true;

        if (isIDValid($scope.orgID)) {
            $scope.isOrgIDAvailable = true;

            var dataObj = {
                "_id": $scope.dbOrgID,
                "organizationId": $scope.orgID,
                "organizationName": $scope.orgName,
                "description": $scope.orgDescription
            };

            var promise = OrganizationService.updateOrganization(dataObj);
            promise.success(function (response) {
                var msg = $scope.orgName + " organization has updated successfully ";
                toaster.pop('success', "Organization", msg);
                //resetOrganizationField();
            });
            promise.error(function (err) {
                toaster.pop('error', "Organization", "Service failed");
                resetOrganizationField();
            });

        } else {
            $scope.orgID = "";
            $scope.isOrgIDAvailable = false;
            $scope.orgIDErrorMessage = "Organization ID must be alfa-numeric.";
        }

    }
    $scope.updateOrganizationData = function ($this) {
        $location.path("/organization/details/" + $this._id);
    }

    $scope.uncheckMasterSelection = function ($this) {
        var noneSelected = true;
        angular.forEach($scope.organizationList, function (item) {
            if (item.Selected) {
                noneSelected = false;
            } else {
                $scope.masterSelect = false;
            }
        });
        $scope.isNoItemSelected = noneSelected;
    }

    $scope.selectAllOrganization = function ($this) {

        angular.forEach($scope.organizationList, function (item) {
            item.Selected = $scope.masterSelect;
        });
        $scope.isNoItemSelected = !($scope.masterSelect);
    }
    $scope.prepareSingleDelete = function ($this) {
        $scope.isSingleDelete = true;

        $scope.deletedItem = $this;
        showDeleteConfirmationAlert("Are you sure want to delete this Organization record?");
    }
    $scope.prepareMultiDelete = function () {
        $scope.isSingleDelete = false;

        showDeleteConfirmationAlert("Are you sure want to delete all selected Organization record?");
    }
    var showDeleteConfirmationAlert = function (msg) {
        $scope.orgDeleteMessage = msg;
        $("#orgDeleteAlert").modal('show');

    }
    $scope.delete=function()
    {
        
        if($scope.isSingleDelete){
            $scope.deleteSingle();
        }else{
            
            $scope.deleteMulti();
        }
    }
    $scope.deleteSingle = function () {
        var dbOrgID = $scope.deletedItem._id;
        var tempOrgName = $scope.deletedItem.organizationName;

        var promise = OrganizationService.deleteOrganization(dbOrgID);
        promise.success(function (response) {
            var msg = tempOrgName + " organization has deleted  successfully."
            toaster.pop('success',"Organization",msg);
            resetOrganizationField();
            $scope.getOrganizationList();

        });
        promise.error(function (err) {
            toaster.pop('error', "Organization", "Service failed");
        });
    }
    $scope.deleteMulti = function () {
        //iteration of each item in organization list
        angular.forEach($scope.organizationList, function (item) {
            if (item.Selected) {
                console.log("deleted item id : " + item._id);
                $scope.deletedItem = item;
                $scope.deleteSingle();
            }
        });

        resetOrganizationField();
    }
    $scope.goBack = function () {
        $location.path("/organization");
    }
    var isIDValid = function (tempID) {
        // true  ---> if String is not empty.
        //true   ---> if String is alfa numeric.
        tempID = tempID.trim();
        if (tempID.length > 0) {
            var patt1 = /([^a-z0-9])/gi; // check for alfa numeric characters
            var result = tempID.match(patt1);
            if (result == null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    var resetOrganizationField = function () {
        $scope.dbOrgID = "";
        $scope.orgID = "";
        $scope.orgName = "";
        $scope.orgDescription = "";
        $scope.masterSelect = false;
    }

});