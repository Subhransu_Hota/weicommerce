ecomApp.controller('headerCtrl', function ($scope,$rootScope,$location) {

    $scope.menuOrganization =   {"menuItem":[["Organization","organization"],["Admin","user"], ["Consumers","consumer"],["Import & Export","orgimportexport"]]};
    $scope.menuOrders =         {"menuItem":[["Orders","order"],["Import & Export","orderimportexport" ]]};
    
    $scope.menuMassDataTasks =  {"menuItem":[["Import & Export","mdtimportexport"],["Search Indexes",""],["Batch Processes",""]]};

    $scope.menuMasterCatalogs = {"menuItem":[["Products","product"],["Catalogs","catalog"],["Product Attribute Groups","attributegroup"],["Price Lists","pricing"],["Image Management",""],["Import & Export",""]]};
    /*
    $scope.menuChannels =       {"menuItem":[["Channels",""],["Channel Browser",""]], "active":false};
    $scope.menuMasterContent =  {"menuItem":[["Master Pages",""],["Master Page Variants",""],["Master Page Templates",""],
                                ["Master Component Templates",""],["Master Components",""],["Master Includes",""],["Master View Contexts",""],
                                ["Content Sharing",""], ["Content Upload",""],["Import & Export",""],["Batch Processes",""]], "active":false};
    
    $scope.menuLocalization =   {"menuItem":[["Localization Management",""],["Import & Export",""]], "active":false};
    $scope.menuServices =       {"menuItem":[], "active":false};
    $scope.menuPreferences =    {"menuItem":[["Profanity Word Definition",""],["Product History",""],["Product Locking",""],["Product Deletion",""],
                                ["Product Editing",""],["Master Page Editing Channel",""],["Content Object Locking",""]], "active":false};
   */
    
    $scope.popoverTemplate  = "myPopoverTemplate.html";
    
    function setMenuItem(){
        var defaultMenuSelection = $('button.tabMenuBtn');
        var index = 0;
        if(sessionStorage.activeMenu ==="Organization"){
            index=0;
            $scope.menuList = $scope.menuOrganization.menuItem;
            
        }else if(sessionStorage.activeMenu ==="Orders"){
            index=1;
            $scope.menuList = $scope.menuOrders.menuItem;
        }else if(sessionStorage.activeMenu ==="Mass Data Tasks"){
            index=2;
            $scope.menuList = $scope.menuMassDataTasks.menuItem;
        }else if(sessionStorage.activeMenu ==="Catalogs"){
            index=3;
            $scope.menuList = $scope.menuMasterCatalogs.menuItem;
        }else{
                index=0;
                $scope.menuList = $scope.menuOrganization.menuItem;
        }
       
        var selectedButton = $(defaultMenuSelection[index]);
        selectedButton.addClass('selectedMenuBtn');
    }
    $('button.tabMenuBtn').click(function(){
            var $this = $(event.target);
            if($this.hasClass('tabMenuBtn')){
                sessionStorage.activeMenu = $this[0].innerText;
                setMenuItem();
                $this.siblings().removeClass('selectedMenuBtn');
                $this.addClass('selectedMenuBtn');
                
            }
            
    });
    $scope.goOrganization = function(){
        $scope.menuList=$scope.menuOrganization.menuItem;
        $location.path("/home");
    }
    $scope.goOrders = function(){
        $scope.menuList=$scope.menuOrders.menuItem;
        $location.path("/home");
    }
    $scope.goMassDataTasks = function(){
        $scope.menuList=$scope.menuMassDataTasks.menuItem;
        $location.path("/home");
    }
    $scope.goMasterCatalogs = function(){
        $scope.menuList=$scope.menuMasterCatalogs.menuItem;
        $location.path("/home");
    }
    
    $scope.logout = function(){
        sessionStorage.userName  =  null;
        $scope.loginUserName = null;
        $location.path("/");
        sessionStorage.activeMenu = "";
    }
    setMenuItem();
    
});