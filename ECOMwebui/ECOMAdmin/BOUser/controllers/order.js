ecomApp.controller('orderCtrl', function ($scope, $rootScope, $routeParams, $location, OrderService, toaster,Validation,CommonFun) {

    $scope.orderList = [];
    $scope.filteredList = [];
    $scope.masterSelect = false;
    $scope.isSingleDelete = true;
    $scope.isNoItemSelected = "true";
    $scope.filterCategory = [];
    $scope.currentDate = CommonFun.getCurrentDateTime();
    
    $scope.getOrderList = function () {
        var promise = OrderService.getOrderList();
        promise.success(function (response) {
            $scope.orderList = JSON.parse(response.data);
             $scope.filterStatus();
            console.log("Count of  Order : " + $scope.orderList.length);
        });
        promise.error(function (err) {
            toaster.pop('error', " Order", "Service failed");
        });
    }
    $scope.showOrderDetail = function($this){
          $location.path("/order/details/"+$this._id);  
    }
    $scope.filterStatus = function(){
        if($scope.filterCategory.length==0){
            resetCategory();
        }
        $scope.filteredList = [];
        angular.forEach($scope.orderList, function (item) {
            if($scope.filterCategory.indexOf(item.status)!=-1){
                $scope.filteredList.push(item);
            }
        });
    }
    $scope.getOrderDetail = function(){
        
            var promise = OrderService.findOrder($routeParams.id);
            promise.success(function (response) {
                $scope.currentOrder = JSON.parse(response.data);
                console.log("Order detail : " + JSON.stringify($scope.currentOrder));
            });
            promise.error(function (err) {
                    toaster.pop('error',"Order","Service failed");
            });
    }
    
    $scope.goBack = function () {
        $location.path("/order");
    }
    
    $scope.selectAllOrderStatus = function ($this) {
        $scope.filterCategory = [];
        var allCheckbox = $(":checkbox:not(:first)");
        
        angular.forEach(allCheckbox, function (item) {
            item.checked = $scope.masterSelect;
            if($scope.masterSelect){
                $scope.filterCategory.push(item.value);
            }
        });
        $scope.filterStatus();
        
    }
    $scope.selectSingleOrderStatus = function ($this) {
        $scope.filterCategory = [];
        var allCheckbox = $(":checkbox:not(:first)"); //select all checkbox except first
        var isAllItemChecked = true;
        angular.forEach(allCheckbox, function (item) {
            if(!item.checked){
                isAllItemChecked = false;
            }else{
                $scope.filterCategory.push(item.value);
            }
            
        });
        $scope.masterSelect = isAllItemChecked;
        
        
        $scope.filterStatus();
    }
    $scope.uncheckMasterSelection = function ($this) {
        var noneSelected = true;
        angular.forEach($scope.orderList, function (item) {
            if (item.Selected) {
                noneSelected = false;
            } else {
                $scope.masterSelect = false;
            }
        });
        $scope.isNoItemSelected = noneSelected;
    }
    var resetCategory = function(){
        $scope.filterCategory = ["NEW","In Progress","Canceled","Cancelled and Exported","Not Deliverable","Delivered","Returned","Pending"];
    }

});