ecomApp.controller('catalogCtrl', function ($scope, $rootScope,$location,CatalogService,Validation,CommonFun,toaster) {
    
    $scope.catalogList = [];
    $scope.categoryList = [];
    $scope.subCategoryList = [];
    
    $scope.selectedCatalogDBID = "";
    $scope.selectedCategoryDBID = "";
    
    $scope.init=function(){
            $scope.getCatalogList();
    }
    $scope.getCatalogList = function(){
        
            var data = {"parentCatalogId":"0"};
            var promise = CatalogService.getCatalogList(data);
			promise.success(function (response) {
                $scope.catalogList = JSON.parse(response.data);
                var numOfCatalog = $scope.catalogList.length;
                if(numOfCatalog>0)
                    {
                        $scope.selectedCatalogDBID = $scope.catalogList[0]._id;
                        $scope.getCatalogCategory($scope.selectedCatalogDBID);
                    }
			});
			promise.error(function (err) {
				toaster.pop('error',"Catalog","Service Failed");
			});
    }
    
    $scope.getCatalogCategory = function(catalogId){
            $scope.selectedCatalogDBID = catalogId;
            $scope.subCategoryList = [];
            var data = {"parentCatalogId":catalogId};
            var promise = CatalogService.getCatalogList(data);
			promise.success(function (response) {
                $scope.categoryList = JSON.parse(response.data);
                console.log("Count of categoryList : " + $scope.categoryList.length);
                var numOfCatagory = $scope.categoryList.length;
                if(numOfCatagory>0)
                    {
                        $scope.selectedCategoryDBID = $scope.categoryList[0]._id;
                        $scope.getCatalogSubCategory($scope.selectedCategoryDBID);
                    }
			});
			promise.error(function (err) {
				toaster.pop('error',"Catalog","Service Failed");
			});
    }
    $scope.getCatalogSubCategory = function(CategoryId){
            $scope.selectedCategoryDBID = CategoryId;
            var data = {"parentCatalogId":CategoryId};
            var promise = CatalogService.getCatalogList(data);
			promise.success(function (response) {
                $scope.subCategoryList = JSON.parse(response.data);
                console.log("Count of subCategoryList : " + $scope.subCategoryList.length);
			});
			promise.error(function (err) {
				toaster.pop('error',"Catalog","Service Failed");
			});
    }
    $scope.prepareCatalogEditData = function($this){
        $scope.catlogModalTitle = "Catalog";
        updateEditModal($this);
    }
    $scope.prepareCategoryEditData = function($this){
        $scope.catlogModalTitle = "Category";
        updateEditModal($this);
    }
    $scope.prepareSubCategoryEditData = function($this){
        $scope.catlogModalTitle = "SubCategory";
        updateEditModal($this);
        
    }
    var updateEditModal=function($this){
        $scope.isUpdate = true;
        $scope.selectedCatItemIndex = $this.$index;
        $scope.catlogID   =  $this.item.catalogId;
        $scope.catlogName =  $this.item.name;
        $scope.catlogDescription = $this.item.description;
        $scope.catalogUpdateDBId = $this.item._id;
        $("#catalogModal").modal('show');
    }
    $scope.editCatalog = function(){
            prepareCatalogDataObject();
            var promise = CatalogService.updateCatalog($scope.catalogData);
			promise.success(function (response) {
                var newCatalog = JSON.parse(response.data);
                handleEditSuccess(newCatalog);
			});
			promise.error(function (err) {
				toaster.pop('error',"Catalog","Service Failed");
			});
    }
    var handleEditSuccess=function(newItem){
        var catalogType =  $scope.catlogModalTitle;
        var index = $scope.selectedCatItemIndex;
        var itemName = newItem.name;
                if(catalogType==="Catalog"){
                    $scope.catalogList.splice(index,1,newItem);
                    $scope.catlogAlertMessage = itemName + " catalog has edited successfully";
                }else if(catalogType==="Category"){
                    $scope.categoryList.splice(index,1,newItem);
                    $scope.catlogAlertMessage = itemName + " category has edited successfully";
                }else if(catalogType==="SubCategory"){
                    $scope.subCategoryList.splice(index,1,newItem);
                    $scope.catlogAlertMessage = itemName + " subcategory has edited successfully";
                }
                $("#catalogModal").modal('hide');
                toaster.pop('success',"Catalog",$scope.catlogAlertMessage);
                resetCatalogData();
    }
    $scope.prepareCatalogData = function(){
        resetCatalogData();
        $scope.catlogModalTitle = "Catalog";
        $scope.isUpdate = false;
    }
    $scope.prepareCategoryData = function(){
        resetCatalogData();
        $scope.catlogModalTitle = "Category";
        $scope.isUpdate = false;
    }
    $scope.prepareSubCategoryData = function(){
        resetCatalogData();
        $scope.catlogModalTitle = "SubCategory";
        $scope.isUpdate = false;
    }
    var prepareCatalogDataObject = function(){
        var catalogType = $scope.catlogModalTitle;
        var parentCatalogID = "";
        
        if(catalogType === "Catalog"){
            parentCatalogID = 0;
        }else if(catalogType === "Category"){
            parentCatalogID = $scope.selectedCatalogDBID;
        }else if(catalogType === "SubCategory"){
            parentCatalogID = $scope.selectedCategoryDBID;
        }
        if($scope.isUpdate){
                            $scope.catalogData = {"_id":$scope.catalogUpdateDBId,  
                                "catalogId":$scope.catlogID, 
                                "name":$scope.catlogName, 
                                "description":$scope.catlogDescription,         
                                "parentCatalogId":parentCatalogID};
        }else{
            
            $scope.catalogData = {  "catalogId":$scope.catlogID, 
                                "name":$scope.catlogName, 
                                "description":$scope.catlogDescription,         
                                "parentCatalogId":parentCatalogID};
        
        }
        
    }
    $scope.createCatalog = function(){
            prepareCatalogDataObject();
            var promise = CatalogService.insertCatalog($scope.catalogData);
			promise.success(function (response) {
                var newCatalog = JSON.parse(response.data);
                handleCreateSuccess(newCatalog);
			});
			promise.error(function (err) {
				toaster.pop('error',"Catalog","Service Failed");
			});
    }
    

      var handleCreateSuccess = function(newItem){
                var catalogType =  $scope.catlogModalTitle;
                var itemName =  newItem.name;
                if(catalogType==="Catalog"){
                    $scope.catalogList.push(newItem);
                    $scope.catlogAlertMessage = itemName + " catalog has created successfully";
                }else if(catalogType==="Category"){
                    $scope.categoryList.push(newItem);
                    $scope.catlogAlertMessage = itemName + " category has created successfully";
                }else if(catalogType==="SubCategory"){
                    $scope.subCategoryList.push(newItem);
                    $scope.catlogAlertMessage = itemName +" subcategory has created successfully";
                }
                $("#catalogModal").modal('hide');
                toaster.pop('success',"Catalog",$scope.catlogAlertMessage);
                resetCatalogData();
    }
    $scope.prepareSubCategoryEdit=function($this){
        $scope.isCatalog = false;
        $scope.isCategory = false;
        $scope.isSubCategory = false;
        $("#catalogModal").modal('show');
    }
    $scope.prepareCatalogDelete = function($this){
        $scope.deletedItem = { "item":$this.item, "catalogType" : "catalog", "itemIndex" : $this.$index};
        $scope.catalogDeleteMessage = "Are you sure want to delete "+$this.item.name+  " ?";
        //$scope.isSingleDelete = true;
        $("#catalogDeleteAlert").modal('show');
    }
    $scope.prepareCategoryDelete = function($this){
        $scope.deletedItem = { "item":$this.item, "catalogType" : "category", "itemIndex" : $this.$index};
        $scope.catalogDeleteMessage = "Are you sure want to delete "+$this.item.name+  " ?";
        //$scope.isSingleDelete = true;
        $("#catalogDeleteAlert").modal('show');
    }
    $scope.prepareSubCategoryDelete = function($this){
        $scope.deletedItem = { "item":$this.item, "catalogType" : "subcategory", "itemIndex" : $this.$index};
        $scope.catalogDeleteMessage = "Are you sure want to delete "+$this.item.name+  " ?";
        //$scope.isSingleDelete = true;
        $("#catalogDeleteAlert").modal('show');
    }
    $scope.delete=function(){
        $scope.deleteCatalog();
    }
    $scope.deleteCatalog=function(){
        var catID = $scope.deletedItem.item._id;
        var promise = CatalogService.deleteCatalog(catID);
			promise.success(function (response) {
                handleDeleteSuccess();
			});
			promise.error(function (err) {
				toaster.pop('error',"Catalog","Service Failed");
			});
        
    }
  var handleDeleteSuccess = function(){
                var catalogType = $scope.deletedItem.catalogType;
                var index       = $scope.deletedItem.itemIndex;
                var name        = $scope.deletedItem.item.name ;
                if(catalogType==="catalog"){
                    $scope.catalogList.splice(index,1);
                    $scope.catlogAlertMessage = name+ " catalog has deleted successfully";
                }else if(catalogType==="category"){
                    $scope.categoryList.splice(index,1);
                    $scope.catlogAlertMessage = name + " category has deleted successfully";
                }else if(catalogType==="subcategory"){
                    $scope.subCategoryList.splice(index,1);
                    $scope.catlogAlertMessage = name + " subcategory has deleted successfully";
                }
               toaster.pop('success',"Catalog",$scope.catlogAlertMessage);
    }
    var resetCatalogData =  function(){
        $scope.catlogID = "";
        $scope.catlogName = "";
        $scope.catlogDescription = "";
    }
    
    $('div.catalogBg div').click(function(){
            var $this = $(event.target);
            if($this.hasClass('tabClass')){
                $this.parents('.active').siblings().find('div.tabClass').removeClass('tabActive');
                $this.addClass('tabActive');
            }
    });
    
    
});


   