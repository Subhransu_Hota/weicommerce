ecomApp.controller('organizationImportExportCtrl', function ($scope, $rootScope, $routeParams, $location, OrganizationService, toaster,$timeout) {

    $scope.downloadMessage = "";
    $scope.importFileNames  = [];/*[
                                    "Organization1.csv", 
                                    "Organization2.csv",
                                    "Organization3.csv"];*/
    $scope.downloadableFileName  = []; 

    
    $scope.init =  function(){
        var promise = OrganizationService.getExportOrganizationList();
        promise.success(function (response) {
            $scope.downloadableFileName  = response;
            $scope.importFileNames = response;
        });
        promise.error(function (err) {
            toaster.pop('error', "Organization", "Service failed");
        });
    }
    
    $scope.fileNameChanged =  function($this){
        //$scope.selectedFile = $("#txtInputFile");
        //$("#txtInputFile").val($this.value);
        $("#txtInputFile").val($this.files[0].name);
    }
    
    $scope.uploadFile = function(){
        event.preventDefault();
        var data = new FormData( this );
        jQuery.ajax({
            url: 'http://weicommerce-backend-dev.elasticbeanstalk.com/organization/upload',
            data: data,
            cache: false,
            datatype : "application/json",
            contentType: "multipart/form-data",
            processData: false,
            type: 'POST',
            success: function(data){
                $scope.importFileNames = JSON.parse(data);
                $scope.$apply();
                toaster.pop('success',"Organization","file has uploaded successfully");
            }
        });
    }
    $scope.goOrgImport = function(){
        $location.path("/orgimport");
    }
    $scope.goOrgExport = function(){
        $location.path("/orgexport");
    }
    $scope.goBack = function(){
        $location.path("/orgimportexport");
    }

    $scope.download = function($this){
        $scope.downloadMessage = "";
        
        var downloadUrl = config.downloadExportPath + $this.item;
        var downloadwindow = window.open(downloadUrl);
        //downloadwindow.close();
    }
    
});
