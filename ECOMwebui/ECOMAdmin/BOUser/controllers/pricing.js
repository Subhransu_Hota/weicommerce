ecomApp.controller('pricingCtrl', function ($scope, $rootScope, $routeParams, $location, PricingService, toaster, Validation) {

    $scope.pricingList = {};
    $scope.newPricing =  {  
                           "pricingId":"",
                           "pricingPurposeType":"PURCHASE",
                           "pricingType":"LIST_PRICE",
                           "money":{  
                              "locale":"",
                              "amount":0,
                              "currency":{  
                                 "currencyCode":""
                              }
                           },
                           "pricingFromDate":"",
                           "pricingToDate":""
                        };
    $scope.masterSelect = false;
    $scope.pricingIDErrorMessage = "";
    $scope.isSingleDelete = true;
    $scope.deletedItem = {};
    $scope.pricingDeleteMessage = "";
    $scope.isNoItemSelected = "true";
    $scope.isPricingIDAvailable = true;
    $scope.status = { opened: false, openedFrom:false  };//Calendar variable
    $scope.init = function () {
        var promise = PricingService.findPricing($routeParams.id);
        promise.success(function (response) {
            $scope.newPricing = JSON.parse(response.data);
        });
        promise.error(function (err) {
            toaster.pop('error', "Pricing", "Service failed");
        });
    }
    $scope.getPricingList = function () {
        var promise = PricingService.getPricingList();
        promise.success(function (response) {
            $scope.pricingList = JSON.parse(response.data);
            //console.log(" Pricing : " + JSON.stringify($scope.pricingList));
        });
        promise.error(function (err) {
            toaster.pop('error', "Pricing", "Service failed");
        });
    }
    
    $scope.addPricing = function () {
        $scope.newPricing.money.locale = window.navigator.language;
        
        $scope.newPricing.pricingId = $scope.newPricing.pricingId.trim();
        $scope.isPricingIDAvailable = true;

        if (Validation.isNameValid($scope.newPricing.pricingId)) {
            $scope.isPricingIDAvailable = true;
            
            var promise = PricingService.addPricing($scope.newPricing);
            promise.success(function (response) {
                var msg = $scope.newPricing.pricingId + "  has created successfully ";
                toaster.pop('success', "Pricing", msg);
                resetPricingField();
            });
            promise.error(function (err) {
                toaster.pop('error', "Pricing", "Service failed");
            });
        } else {
            $scope.newPricing.pricingId = "";
            $scope.isPricingIDAvailable = false;
            $scope.pricingIDErrorMessage = "Pricing ID must be alfa-numeric.";
        }
    }
    $scope.updatePricing = function ($this) {
        $scope.newPricing.pricingId = $scope.newPricing.pricingId.trim();
        $scope.isPricingIDAvailable = true;

        if (Validation.isNameValid($scope.newPricing.pricingId)) {
            $scope.isPricingIDAvailable = true;

            var promise = PricingService.updatePricing($scope.newPricing);
            promise.success(function (response) {
                var msg = $scope.newPricing.pricingId + " pricing has updated successfully ";
                toaster.pop('success', "Pricing", msg);
            });
            promise.error(function (err) {
                toaster.pop('error', "Pricing", "Service failed");
                resetPricingField();
            });

        } else {
            $scope.newPricing.pricingId = "";
            $scope.isPricingIDAvailable = false;
            $scope.pricingIDErrorMessage = "Pricing ID must be alfa-numeric.";
        }

    }
    $scope.updatePricingData = function ($this) {
        $location.path("/pricing/details/" + $this._id);
    }

    $scope.uncheckMasterSelection = function ($this) {
        var noneSelected = true;
        angular.forEach($scope.pricingList, function (item) {
            if (item.Selected) {
                noneSelected = false;
            } else {
                $scope.masterSelect = false;
            }
        });
        $scope.isNoItemSelected = noneSelected;
    }

    $scope.selectAllPricing = function ($this) {

        angular.forEach($scope.pricingList, function (item) {
            item.Selected = $scope.masterSelect;
        });
        $scope.isNoItemSelected = !($scope.masterSelect);
    }
    $scope.prepareSingleDelete = function ($this) {
        $scope.isSingleDelete = true;

        $scope.deletedItem = $this;
        showDeleteConfirmationAlert("Are you sure want to delete this Pricing record?");
    }
    $scope.prepareMultiDelete = function () {
        $scope.isSingleDelete = false;

        showDeleteConfirmationAlert("Are you sure want to delete all selected Pricing record?");
    }
    var showDeleteConfirmationAlert = function (msg) {
        $scope.pricingDeleteMessage = msg;
        $("#pricingDeleteAlert").modal('show');

    }
    $scope.delete=function()    {
        
        if($scope.isSingleDelete){
            $scope.deleteSingle();
        }else{
            
            $scope.deleteMulti();
        }
    }
    $scope.deleteSingle = function () {
        var dbPricingID = $scope.deletedItem._id;
        var tempPricingName = $scope.deletedItem.pricingId;

        var promise = PricingService.deletePricing(dbPricingID);
        promise.success(function (response) {
            var msg = tempPricingName + " pricing has deleted  successfully."
            toaster.pop('success',"Pricing",msg);
            resetPricingField();
            $scope.getPricingList();

        });
        promise.error(function (err) {
            toaster.pop('error', "Pricing", "Service failed");
        });
    }
    $scope.deleteMulti = function () {
        //iteration of each item in pricing list
        angular.forEach($scope.pricingList, function (item) {
            if (item.Selected) {
                console.log("deleted item id : " + item._id);
                $scope.deletedItem = item;
                $scope.deleteSingle();
            }
        });

        resetPricingField();
    }
    $scope.goBack = function () {
        $location.path("/pricing");
    }
    
    var resetPricingField = function () {
        $scope.masterSelect = false;
        $scope.newPricing =  {  
                           "pricingId":"",
                           "pricingPurposeType":"PURCHASE",
                           "pricingType":"LIST_PRICE",
                           "money":{  
                              "locale":"",
                              "amount":0,
                              "currency":{  
                                 "currencyCode":""
                              }
                           },
                           "pricingFromDate":"",
                           "pricingToDate":""
                        };
        
    }
   
    //Calendar function
    $scope.openFromDate = function($event) 
    {
        $scope.status.openedFrom = true;
        
    };
    $scope.open = function($event) 
    {
        $scope.status.opened = true;
        $scope.toggleMin();
    };
    $scope.toggleMin = function() 
    {
        var day = $scope.newPricing.pricingFromDate.getDate();
        var month = $scope.newPricing.pricingFromDate.getMonth();
        var year = $scope.newPricing.pricingFromDate.getFullYear();
        $scope.minDate = new Date(year, month, day);
        
    };
    
    

});