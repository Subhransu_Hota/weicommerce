ecomApp.controller('mdtImportExportCtrl', function ($scope, $rootScope, $routeParams, $location, MDTService, toaster,$timeout) {

    $scope.createDump = function(){
        
        var promise = MDTService.createDump();
        promise.success(function (response) {
             var msg = "Dump  has created  successfully at :  " +config.dumpPath;
             toaster.pop('success',"Mass Data Task",msg);
            console.log(response.data);
        });
        promise.error(function (err) {
            toaster.pop('error', "Mass Data Task", "Service failed");
        });
        
    }
    $scope.restoreDump = function(){
        var promise = MDTService.restoreDump();
        promise.success(function (response) {
             var msg = "Dump has restored  successfully";
             toaster.pop('success',"Mass Data Task",msg);
            console.log(msg);
        });
        promise.error(function (err) {
            toaster.pop('error', "Mass Data Task", "Service failed");
        });
    }
    
});
