ecomApp.controller('AuthCtrl', function ($scope, $rootScope, $location,AuthService,toaster) {
    
    $scope.login=function()
    {
        var dataObj = { "userName" : $scope.userName , "password" : $scope.password};   
        if(!(angular.isUndefined($scope.userName)))
        {
            
            var promise = AuthService.login(dataObj);
            resetUSerData();

            promise.success(function (response) {
                    var loginResponse = JSON.parse(response.data);
                    sessionStorage.userName = loginResponse.firstName + " " +loginResponse.lastName ;
                    console.log("loginResponse : " + JSON.stringify(loginResponse));
                    resetUSerData();
                    if(config.userType == 'seller'){
                        var newwindow = window.open("http://localhost:4000/seller","_self");
                    } else{
                        $location.path("/home");
                    }
                });

            promise.error(function (err) {
                    var failedLoginResponse =  JSON.parse(err.data);
                    toaster.pop('error',"Login failed",failedLoginResponse.msg);
                });
        }
        else{
                toaster.pop('error',"Login failed","Please enter valid user name");   
        }
    }
    
    var resetUSerData = function(){
        $scope.userName = "";
        $scope.password = "";
    }
});