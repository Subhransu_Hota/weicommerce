ecomApp.directive( 'buttonHref', function ( $location ) {
      return function ( scope, element, attrs ) {
        var path;
        
        attrs.$observe( 'buttonHref', function (val) {
          path = val;
        });
        
        element.bind( 'click', function () {
          scope.$apply( function () {
            $location.path( path );
          });
        });
      };
  });
ecomApp.directive('deletealert', function(){
    
    return {
                restrict:'E',
                replace:true,
                scope : {
			         title   : '@',
                     deleteMessage : '@'
                     //isSingleDelete : '='
		         },
                templateUrl:"views/deletealert.html",
                link: function(scope,elem, attrs){
    
                    //scope.deleteMulti  = function(){  console.log("delete multi");scope.$parent.deleteMulti();  }
                    scope.delete = function(){  scope.$parent.delete(); }
                }
    };
});
ecomApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
ecomApp.directive('header', function(){
    
    return {
                restrict:'E',
                controller:'headerCtrl',
                templateUrl:"views/header.html"
    };
});


ecomApp.controller('attributeGroupCtrl', function ($scope, $rootScope, $routeParams, $location, AttributeGroupService, toaster,Validation) {

    $scope.agList = {};
    $scope.agName = "";
    $scope.dbAGID = "";
    $scope.masterSelect = false;
    $scope.isSingleDelete = true;
    
    $scope.deletedItem = {};
    $scope.agDeleteMessage = "";
    $scope.isNoItemSelected = "true";
    $scope.isAGNameValid =  true;
    

    
    $scope.getAGList = function () {
        var promise = AttributeGroupService.getAttributeGroupList();
        promise.success(function (response) {
            $scope.agList = JSON.parse(response.data);
            console.log("Count of  AttributeGroup : " + $scope.agList.length);
        });
        promise.error(function (err) {
            toaster.pop('error', " Attribute Group", "Service failed");
        });
    }
    $scope.uncheckMasterSelection = function ($this) {
        var noneSelected = true;
        angular.forEach($scope.agList, function (item) {
            if (item.Selected) {
                noneSelected = false;
            } else {
                $scope.masterSelect = false;
            }
        });
        $scope.isNoItemSelected = noneSelected;
    }

    $scope.selectAllAG = function ($this) {

        angular.forEach($scope.agList, function (item) {
            item.Selected = $scope.masterSelect;
        });
        $scope.isNoItemSelected = !($scope.masterSelect);
    }
    $scope.addAGData = function () {
        $location.path("/attributegroup/new");
    }
    $scope.updateAGData = function (item) {
        $location.path("/attributegroup/details/" + item._id);
    }

    
    
    
    
    
    $scope.addAttributeGroup = function () {
        if ( Validation.isNameValid($scope.agName )) 
        {
            var dataObj = {  "name": $scope.agName };
            var promise = AttributeGroupService.addAttributeGroup(dataObj);
            promise.success(function (response) {
                var msg = $scope.agName + " attribute group has added successfully";
                toaster.pop('success', "Attribute Group", msg);
                resetAttributeGroupField();
            });
            promise.error(function (err) {
                toaster.pop('error', "Attribute Group", "Service failed");
            });
        } else {
            $scope.isAGNameValid =  false;
            $scope.agNameErrorMessage = "Attribute Group name must be alfa-numeric.";
            
        }
    }
    
    $scope.goBack = function () {
        $location.path("/attributegroup");
    }
    var resetAttributeGroupField = function () {
        $scope.dbAGID = "";
        $scope.agName = "";
        $scope.masterSelect = false;
        $scope.isAGNameValid =  true;
    }
    
    $scope.prepareSingleDelete = function ($this) {
        $scope.isSingleDelete = true;
        $scope.deletedItem = $this;
        showDeleteConfirmationAlert("Are you sure want to delete this Attribute Group record?");
    }
    $scope.prepareMultiDelete = function () {
        $scope.isSingleDelete = false;
        showDeleteConfirmationAlert("Are you sure want to delete all selected Attribute Group record?");
    }
    $scope.delete=function()
    {   
        if($scope.isSingleDelete){
            $scope.deleteSingle();
        }else{
            
            $scope.deleteMulti();
        }
    }
    var showDeleteConfirmationAlert = function (msg) {
        $scope.agDeleteMessage = msg;
        $("#agDeleteAlert").modal('show');
    }
    
    $scope.deleteSingle = function () {
        var dbID = $scope.deletedItem._id;
        var tempName = $scope.deletedItem.name;

        var promise = AttributeGroupService.deleteAttributeGroup(dbID);
        promise.success(function (response) {
            var msg = tempName + " attribute group has deleted  successfully."
            toaster.pop('success',"Attribute Group",msg);
            resetAttributeGroupField();
            $scope.getAGList();

        });
        promise.error(function (err) {
            toaster.pop('error', "Attribute Group", "Service failed");
        });
    }
    $scope.deleteMulti = function () {
        //iteration of each item in Attribute Group list
        angular.forEach($scope.agList, function (item) {
            if (item.Selected) {
                $scope.deletedItem = item;
                $scope.deleteSingle();
            }
        });
       resetAttributeGroupField();
    }
    
   

});
ecomApp.controller('updateAGCtrl', function ($scope, $rootScope,$routeParams,$location, AttributeGroupService,Validation,CommonFun,toaster) {   
    $scope.isAGNameValid    =   true;
    $scope.isNewAddress     =   "";
    //$scope.newAttributes    =   [];
    $scope.currentAG        =   {};
    
    $scope.getAttributeGroup=function(){
            var promise = AttributeGroupService.findAttributeGroup($routeParams.id);
            promise.success(function (response) {
                var agdetail  = JSON.parse(response.data);
                var attributes = [];
                if(agdetail.attributes){
                    attributes = agdetail.attributes;
                }
                $scope.currentAG =  {   "_id" : agdetail._id, 
                                        "name":agdetail.name,
                                        "attributes":attributes
                                   };
                
            });
            promise.error(function (err) {
                    toaster.pop('error',"Product Attribute Group","Service failed");
            });
    }
    
    $scope.updateAG=function()
    {       
        
        if($scope.isUpdateAllowed())
        {
            
            if( (!(angular.isUndefined($scope.newAttributeName)))&& ($scope.newAttributeName.length > 0)) 
            {      
                   if($scope.isNewAttribute ==="new"){
                       $scope.currentAG.attributes.push($scope.newAttributeName);
                   }else if($scope.isNewAttribute ==="edit"){
                       var index = $scope.updatedAttributeIndex;
                       $scope.currentAG.attributes.splice(index,1,$scope.newAttributeName);
                   }
             }
            $scope.newAttributeName = "";
            var promise = AttributeGroupService.updateAttributeGroup($scope.currentAG);
            promise.success(function (response) {
                $scope.agAlertMessage = $scope.currentAG.name + " record has updated successfully ";
                toaster.pop('success',"Product Attribute Group",$scope.agAlertMessage);
                 //resetData();

            });
            promise.error(function (err) {
                   toaster.pop('error',"Product Attribute Group","Service failed");
            });
        }
    }
    $scope.prepareDeleteAttribute=function($this){
        $scope.deletedIndex = $this.$index;
        $scope.newAttributeName = "";
        $scope.isInputChange = true;
        $scope.agDeleteMessage = "Are you sure want to delete attribute?";
        $("#agDeleteAlert").modal('show');
    }
    
    $scope.delete=function(){
        $scope.currentAG.attributes.splice($scope.deletedIndex,1);
        $scope.newAttributeName = "";
    }
    $scope.fieldChanged =  function(){
        $scope.isInputChange = true;
    }
    $scope.isUpdateAllowed=function(){
        $scope.agAlertMessage = "";
        $scope.isAGNameValid =    Validation.isNameValid($scope.currentAG.name);
        
        if(!$scope.isInputChange ){
            $scope.agAlertMessage = "No change in data";
        }else if(!$scope.isAGNameValid){
            $scope.agNameErrorMessage = "Attribute Group name should be alfa numeric";
        }
        
        if($scope.agAlertMessage===""){
            return true;
        }else{
             toaster.pop('error',"Product Attribute Group",$scope.agAlertMessage);
             return false;
        }
    }
    
    $scope.updateAttribute=function($this){
        $scope.isNewAttribute     =   "edit";
        $scope.showAttributeSection = true;
        $scope.updatedAttributeIndex = $this.$index;
        
        $scope.newAttributeName = $this.attr;
        
    }
    
    $scope.addNewAttribute=function(){
        $scope.showAttributeSection = true;
        $scope.isNewAttribute     =   "new";
        $scope.newAttributeName = "";
    }
    
    $scope.goBack = function(){
        $location.path("/attributegroup");
    }
    
    var resetData=function(){
        $scope.isNewAttribute ="";
        $scope.updatedAttributeIndex = "";
        $scope.isInputChange = false;
    }
//    $scope.onClose = function(){
//        console.log("closing consumer alert");
//    }

});
ecomApp.controller('AuthCtrl', function ($scope, $rootScope, $location,AuthService,toaster) {
    
    $scope.login=function()
    {
        var dataObj = { "userName" : $scope.userName , "password" : $scope.password};   
        if(!(angular.isUndefined($scope.userName)))
        {
            
            var promise = AuthService.login(dataObj);
            resetUSerData();

            promise.success(function (response) {
                    var loginResponse = JSON.parse(response.data);
                    sessionStorage.userName = loginResponse.firstName + " " +loginResponse.lastName ;
                    console.log("loginResponse : " + JSON.stringify(loginResponse));
                    resetUSerData();
                    if(config.userType == 'seller'){
                        var newwindow = window.open("http://localhost:4000/seller","_self");
                    } else{
                        $location.path("/home");
                    }
                });

            promise.error(function (err) {
                    var failedLoginResponse =  JSON.parse(err.data);
                    toaster.pop('error',"Login failed",failedLoginResponse.msg);
                });
        }
        else{
                toaster.pop('error',"Login failed","Please enter valid user name");   
        }
    }
    
    var resetUSerData = function(){
        $scope.userName = "";
        $scope.password = "";
    }
});
ecomApp.controller('catalogCtrl', function ($scope, $rootScope,$location,CatalogService,Validation,CommonFun,toaster) {
    
    $scope.catalogList = [];
    $scope.categoryList = [];
    $scope.subCategoryList = [];
    
    $scope.selectedCatalogDBID = "";
    $scope.selectedCategoryDBID = "";
    
    $scope.init=function(){
            $scope.getCatalogList();
    }
    $scope.getCatalogList = function(){
        
            var data = {"parentCatalogId":"0"};
            var promise = CatalogService.getCatalogList(data);
			promise.success(function (response) {
                $scope.catalogList = JSON.parse(response.data);
                var numOfCatalog = $scope.catalogList.length;
                if(numOfCatalog>0)
                    {
                        $scope.selectedCatalogDBID = $scope.catalogList[0]._id;
                        $scope.getCatalogCategory($scope.selectedCatalogDBID);
                    }
			});
			promise.error(function (err) {
				toaster.pop('error',"Catalog","Service Failed");
			});
    }
    
    $scope.getCatalogCategory = function(catalogId){
            $scope.selectedCatalogDBID = catalogId;
            $scope.subCategoryList = [];
            var data = {"parentCatalogId":catalogId};
            var promise = CatalogService.getCatalogList(data);
			promise.success(function (response) {
                $scope.categoryList = JSON.parse(response.data);
                console.log("Count of categoryList : " + $scope.categoryList.length);
                var numOfCatagory = $scope.categoryList.length;
                if(numOfCatagory>0)
                    {
                        $scope.selectedCategoryDBID = $scope.categoryList[0]._id;
                        $scope.getCatalogSubCategory($scope.selectedCategoryDBID);
                    }
			});
			promise.error(function (err) {
				toaster.pop('error',"Catalog","Service Failed");
			});
    }
    $scope.getCatalogSubCategory = function(CategoryId){
            $scope.selectedCategoryDBID = CategoryId;
            var data = {"parentCatalogId":CategoryId};
            var promise = CatalogService.getCatalogList(data);
			promise.success(function (response) {
                $scope.subCategoryList = JSON.parse(response.data);
                console.log("Count of subCategoryList : " + $scope.subCategoryList.length);
			});
			promise.error(function (err) {
				toaster.pop('error',"Catalog","Service Failed");
			});
    }
    $scope.prepareCatalogEditData = function($this){
        $scope.catlogModalTitle = "Catalog";
        updateEditModal($this);
    }
    $scope.prepareCategoryEditData = function($this){
        $scope.catlogModalTitle = "Category";
        updateEditModal($this);
    }
    $scope.prepareSubCategoryEditData = function($this){
        $scope.catlogModalTitle = "SubCategory";
        updateEditModal($this);
        
    }
    var updateEditModal=function($this){
        $scope.isUpdate = true;
        $scope.selectedCatItemIndex = $this.$index;
        $scope.catlogID   =  $this.item.catalogId;
        $scope.catlogName =  $this.item.name;
        $scope.catlogDescription = $this.item.description;
        $scope.catalogUpdateDBId = $this.item._id;
        $("#catalogModal").modal('show');
    }
    $scope.editCatalog = function(){
            prepareCatalogDataObject();
            var promise = CatalogService.updateCatalog($scope.catalogData);
			promise.success(function (response) {
                var newCatalog = JSON.parse(response.data);
                handleEditSuccess(newCatalog);
			});
			promise.error(function (err) {
				toaster.pop('error',"Catalog","Service Failed");
			});
    }
    var handleEditSuccess=function(newItem){
        var catalogType =  $scope.catlogModalTitle;
        var index = $scope.selectedCatItemIndex;
        var itemName = newItem.name;
                if(catalogType==="Catalog"){
                    $scope.catalogList.splice(index,1,newItem);
                    $scope.catlogAlertMessage = itemName + " catalog has edited successfully";
                }else if(catalogType==="Category"){
                    $scope.categoryList.splice(index,1,newItem);
                    $scope.catlogAlertMessage = itemName + " category has edited successfully";
                }else if(catalogType==="SubCategory"){
                    $scope.subCategoryList.splice(index,1,newItem);
                    $scope.catlogAlertMessage = itemName + " subcategory has edited successfully";
                }
                $("#catalogModal").modal('hide');
                toaster.pop('success',"Catalog",$scope.catlogAlertMessage);
                resetCatalogData();
    }
    $scope.prepareCatalogData = function(){
        resetCatalogData();
        $scope.catlogModalTitle = "Catalog";
        $scope.isUpdate = false;
    }
    $scope.prepareCategoryData = function(){
        resetCatalogData();
        $scope.catlogModalTitle = "Category";
        $scope.isUpdate = false;
    }
    $scope.prepareSubCategoryData = function(){
        resetCatalogData();
        $scope.catlogModalTitle = "SubCategory";
        $scope.isUpdate = false;
    }
    var prepareCatalogDataObject = function(){
        var catalogType = $scope.catlogModalTitle;
        var parentCatalogID = "";
        
        if(catalogType === "Catalog"){
            parentCatalogID = 0;
        }else if(catalogType === "Category"){
            parentCatalogID = $scope.selectedCatalogDBID;
        }else if(catalogType === "SubCategory"){
            parentCatalogID = $scope.selectedCategoryDBID;
        }
        if($scope.isUpdate){
                            $scope.catalogData = {"_id":$scope.catalogUpdateDBId,  
                                "catalogId":$scope.catlogID, 
                                "name":$scope.catlogName, 
                                "description":$scope.catlogDescription,         
                                "parentCatalogId":parentCatalogID};
        }else{
            
            $scope.catalogData = {  "catalogId":$scope.catlogID, 
                                "name":$scope.catlogName, 
                                "description":$scope.catlogDescription,         
                                "parentCatalogId":parentCatalogID};
        
        }
        
    }
    $scope.createCatalog = function(){
            prepareCatalogDataObject();
            var promise = CatalogService.insertCatalog($scope.catalogData);
			promise.success(function (response) {
                var newCatalog = JSON.parse(response.data);
                handleCreateSuccess(newCatalog);
			});
			promise.error(function (err) {
				toaster.pop('error',"Catalog","Service Failed");
			});
    }
    

      var handleCreateSuccess = function(newItem){
                var catalogType =  $scope.catlogModalTitle;
                var itemName =  newItem.name;
                if(catalogType==="Catalog"){
                    $scope.catalogList.push(newItem);
                    $scope.catlogAlertMessage = itemName + " catalog has created successfully";
                }else if(catalogType==="Category"){
                    $scope.categoryList.push(newItem);
                    $scope.catlogAlertMessage = itemName + " category has created successfully";
                }else if(catalogType==="SubCategory"){
                    $scope.subCategoryList.push(newItem);
                    $scope.catlogAlertMessage = itemName +" subcategory has created successfully";
                }
                $("#catalogModal").modal('hide');
                toaster.pop('success',"Catalog",$scope.catlogAlertMessage);
                resetCatalogData();
    }
    $scope.prepareSubCategoryEdit=function($this){
        $scope.isCatalog = false;
        $scope.isCategory = false;
        $scope.isSubCategory = false;
        $("#catalogModal").modal('show');
    }
    $scope.prepareCatalogDelete = function($this){
        $scope.deletedItem = { "item":$this.item, "catalogType" : "catalog", "itemIndex" : $this.$index};
        $scope.catalogDeleteMessage = "Are you sure want to delete "+$this.item.name+  " ?";
        //$scope.isSingleDelete = true;
        $("#catalogDeleteAlert").modal('show');
    }
    $scope.prepareCategoryDelete = function($this){
        $scope.deletedItem = { "item":$this.item, "catalogType" : "category", "itemIndex" : $this.$index};
        $scope.catalogDeleteMessage = "Are you sure want to delete "+$this.item.name+  " ?";
        //$scope.isSingleDelete = true;
        $("#catalogDeleteAlert").modal('show');
    }
    $scope.prepareSubCategoryDelete = function($this){
        $scope.deletedItem = { "item":$this.item, "catalogType" : "subcategory", "itemIndex" : $this.$index};
        $scope.catalogDeleteMessage = "Are you sure want to delete "+$this.item.name+  " ?";
        //$scope.isSingleDelete = true;
        $("#catalogDeleteAlert").modal('show');
    }
    $scope.delete=function(){
        $scope.deleteCatalog();
    }
    $scope.deleteCatalog=function(){
        var catID = $scope.deletedItem.item._id;
        var promise = CatalogService.deleteCatalog(catID);
			promise.success(function (response) {
                handleDeleteSuccess();
			});
			promise.error(function (err) {
				toaster.pop('error',"Catalog","Service Failed");
			});
        
    }
  var handleDeleteSuccess = function(){
                var catalogType = $scope.deletedItem.catalogType;
                var index       = $scope.deletedItem.itemIndex;
                var name        = $scope.deletedItem.item.name ;
                if(catalogType==="catalog"){
                    $scope.catalogList.splice(index,1);
                    $scope.catlogAlertMessage = name+ " catalog has deleted successfully";
                }else if(catalogType==="category"){
                    $scope.categoryList.splice(index,1);
                    $scope.catlogAlertMessage = name + " category has deleted successfully";
                }else if(catalogType==="subcategory"){
                    $scope.subCategoryList.splice(index,1);
                    $scope.catlogAlertMessage = name + " subcategory has deleted successfully";
                }
               toaster.pop('success',"Catalog",$scope.catlogAlertMessage);
    }
    var resetCatalogData =  function(){
        $scope.catlogID = "";
        $scope.catlogName = "";
        $scope.catlogDescription = "";
    }
    
    $('div.catalogBg div').click(function(){
            var $this = $(event.target);
            if($this.hasClass('tabClass')){
                $this.parents('.active').siblings().find('div.tabClass').removeClass('tabActive');
                $this.addClass('tabActive');
            }
    });
    
    
});


   
ecomApp.controller('consumerCtrl', function ($scope, $rootScope,$location, ConsumerService,Validation,CommonFun,toaster) {
    
    $scope.consumerList = {};
    $scope.masterSelect=false;
    $scope.isNoItemSelected = true;
    $scope.currentItem = {
                           dbID : "",
                           firstName:"",
                           lastName:"",
                           company:"",
                           phone:"",
                           createdDate:""
                         };
    $scope.isUpdate = false;
    $scope.isFirstNameValid =  true;
    $scope.isPhoneValid =  true;
    $scope.firstNameErrorMessage = "First name should be alfa-numeric.";
    $scope.phoneErrorMessage = "Phone num must be 10 digit number.";
    
    $scope.getConsumerList = function(){
            var promise = ConsumerService.getUserList();
			promise.success(function (response) {
                $scope.consumerList = JSON.parse(response.data);
                console.log("Count of Consumer : " + $scope.consumerList.length);
			});
			promise.error(function (err) {
				toaster.pop('error',"Consumer","Service failed");
			});
    }
    
    $scope.selectAllConsumer=function($this){
        angular.forEach($scope.consumerList, function (item) {
            item.Selected = $scope.masterSelect;
        });
        $scope.isNoItemSelected = !$scope.masterSelect;
    }
    $scope.uncheckMasterSelection=function($this){
        var noneSelected =  true;
        angular.forEach($scope.consumerList, function (item) {
            if(item.Selected){noneSelected =  false;}
            else{  $scope.masterSelect = false; }
        });
        $scope.isNoItemSelected = noneSelected;
    }
    
    $scope.addConsumerData =  function(){
       $location.path('/consumer/new');
    }
    
    $scope.addConsumer=function()
    {       
        consumerDataValidation(); 
        if($scope.isFirstNameValid && $scope.isPhoneValid)
        {
            
            var dataObj =  {"firstName":$scope.currentItem.firstName,
                            "lastName":$scope.currentItem.lastName,
                            "company":$scope.currentItem.company,
                            "userType":"Customer",
                            "roles":[],
                            "phoneNo":$scope.currentItem.phone,
                            "createdDate":CommonFun.getCurrentDateTime()};

            var promise = ConsumerService.addUser(dataObj);
            promise.success(function (response) {
                $("#consumerModal").modal('hide');
                var msg = $scope.currentItem.firstName + "  record has created successfully ";
                toaster.pop('success',"Consumer",msg);

                resetConsumerField();
                $scope.getConsumerList();

            });
            promise.error(function (err) {
                    toaster.pop('error',"Consumer","Service failed");
            });
        }
    }
    var consumerDataValidation =  function()
    {
        $scope.isFirstNameValid = Validation.isNameValid($scope.currentItem.firstName);
        $scope.isPhoneValid  =  Validation.isPhoneValid($scope.currentItem.phone);
    }
    $scope.updateConsumer=function()
    {       
        consumerDataValidation(); 
        if($scope.isFirstNameValid && $scope.isPhoneValid)
        {   
            var dataObj =  {"_id" : $scope.currentItem.dbID,
                            "firstName":$scope.currentItem.firstName,
                            "lastName":$scope.currentItem.lastName,
                            "company":$scope.currentItem.company,
                            "userType":"Customer",
                            "roles":[],
                            "phoneNo":$scope.currentItem.phone,
                            "createdDate":$scope.currentItem.createdDate};

            var promise = ConsumerService.updateUser(dataObj);
            promise.success(function (response) {
                var msg = $scope.currentItem.firstName + " record has updated successfully ";
                $("#consumerModal").modal('hide');
                toaster.pop('success',"Consumer",msg);
                resetConsumerField();
                $scope.getConsumerList();

            });
            promise.error(function (err) {
                toaster.pop('error',"Consumer","Service failed");
            });
        }
    }
    $scope.updateConsumerData =  function($this){
        $location.path("/consumer/details/"+$this._id);
    }
    $scope.prepareSingleDelete = function($this){
        $scope.isSingleDelete = true;
        $scope.deletedItem = $this;
        showDeleteConfirmationAlert("Are you sure want to delete this Consumer record?");
    }
    $scope.prepareMultiDelete = function(){
        $scope.isSingleDelete = false;
        showDeleteConfirmationAlert("Are you sure want to delete all selected Consumer record?");
    }
    var showDeleteConfirmationAlert = function(msg){
        $scope.consumerDeleteMessage = msg;
        $("#consumerDeleteAlert").modal('show');
    }
    $scope.delete=function()
    {   
        if($scope.isSingleDelete){
            $scope.deleteSingle();
        }else{
            
            $scope.deleteMulti();
        }
    }
    $scope.deleteSingle=function()
    {
            var dbOrgID =   $scope.deletedItem._id;
            var tempFirstName = $scope.deletedItem.firstName;
        
            var promise = ConsumerService.deleteUser(dbOrgID);
			promise.success(function (response) {
               
                var msg = tempFirstName + " consumer has deleted  successfully.";
                toaster.pop('success',"Consumer",msg);

                resetConsumerField();
                $scope.getConsumerList();
			});
			promise.error(function (err) {
				toaster.pop('error',"Consumer","Service failed");
			});
    }
    $scope.deleteMulti=function(){
        //iteration of each item in organization list
        angular.forEach($scope.consumerList, function (item) {
            if(item.Selected ){
                console.log("deleted consumer id : " + item._id);
                $scope.deletedItem = item;
                $scope.deleteSingle();
            }
        });
        
    }
    $scope.goBack = function () {
        $location.path("/consumer");
    }
    
    var resetConsumerField = function(){
        $scope.currentItem = {
                               dbID : "",
                               firstName:"",
                               lastName:"",
                               company:"",
                               phone:"",
                               createdDate:""
                           };
        
    }
    
});
ecomApp.controller('updateConsumerCtrl', function ($scope, $rootScope,$routeParams,$location, ConsumerService,Validation,CommonFun,toaster) { 
    
    $scope.isFirstNameValid = true;
    $scope.isNewAddress     =   "";
    $scope.isPhoneValid     = true;
    $scope.newAddress = {
                            consumerAddress:"",
                            consumerCityName:"",
                            consumerState:"",
                            zipCode:""
                            
    };
    
    $scope.init=function(){
            
            var promise = ConsumerService.findUser($routeParams.id);
            promise.success(function (response) {
                var consumer = JSON.parse(response.data);
                var address = [];
                if(consumer.shippingAddress){
                    address = consumer.shippingAddress;
                }
                $scope.currentConsumer =  
                                    {"dbID" : consumer._id, 
                                     "firstName":consumer.firstName, 
                                    "lastName":consumer.lastName,
                                    "company":consumer.company,
                                    "userType":consumer.userType, 
                                    "phoneNo":consumer.phoneNo,
                                    "createdDate":consumer.createdDate,
                                     "roles":[],
                                    "userCredentials" : consumer.userCredentials, 
                                    "shippingAddress":address,
                                    "billingAddress":address
                                   }; 
                if($scope.currentConsumer.userCredentials){
                    $scope.currentConsumer.userCredentials.repassword = $scope.currentConsumer.userCredentials.password;
                }
                
            });
            promise.error(function (err) {
                    toaster.pop('error',"Consumer","Service failed");
            });
    }
    
    $scope.updateConsumer=function()
    {       
        //$scope.isFirstNameValid = Validation.isNameValid($scope.currentConsumer.firstName);
        //$scope.isPhoneValid     =    Validation.isPhoneValid($scope.currentConsumer.phoneNo);
       
        if($scope.isUpdateAllowed())
        {
            if(($scope.newAddress.consumerAddress.length > 0) ||       
                ($scope.newAddress.consumerCityName.length > 0) || 
                ($scope.newAddress.zipCode.length>0))
            {
                   var address = { "addressName" : $scope.newAddress.consumerAddress, 
                                  "city" : $scope.newAddress.consumerCityName,
                                  "state": $scope.newAddress.consumerState,
                                  "zipCode":$scope.newAddress.zipCode,
                                  "createdDate" : CommonFun.getCurrentDateTime(), 
                                  "addressType" : "Billing_And_Shipping" };
                   
                   if($scope.isNewAddress ==="new"){
                       $scope.currentConsumer.shippingAddress.push(address);
                   }else if($scope.isNewAddress ==="edit"){
                       
                       var index = $scope.updatedAddressIndex;
                       $scope.currentConsumer.shippingAddress.splice(index,1,address);
                   }
            }
            
            var dataObj =  {"_id" : $scope.currentConsumer.dbID, "firstName":$scope.currentConsumer.firstName, 
                            "lastName":$scope.currentConsumer.lastName,"company":$scope.currentConsumer.company,
                            "userType":"Customer", 
                            "phoneNo":$scope.currentConsumer.phoneNo,
                            "roles":[],
                            "createdDate":CommonFun.getCurrentDateTime(),
                            "userCredentials" : $scope.currentConsumer.userCredentials, 
                            "shippingAddress":$scope.currentConsumer.shippingAddress,
                            "billingAddress":$scope.currentConsumer.billingAddress
                           };

            var promise = ConsumerService.updateUser(dataObj);
            promise.success(function (response) {
                console.log($scope.currentConsumer.firstName + " record has updated successfully " );
                $scope.consumerAlertMessage = $scope.currentConsumer.firstName + " record has updated successfully ";
                 toaster.pop('success',"Consumer",$scope.consumerAlertMessage);
                 //resetData();

            });
            promise.error(function (err) {
                   toaster.pop('error',"Consumer","Service failed");
            });
        }
    }
    $scope.prepareDeleteAddress=function($this){
        $scope.deletedIndex = $this.$index;
        $scope.isSingleDelete = true;
        $scope.consumerDeleteMessage = "Are you sure want to delete address?";
        $("#consumerDeleteAlert").modal('show');
        
    }
    $scope.deleteSingle=function(){
        
        $scope.currentConsumer.shippingAddress.splice($scope.deletedIndex,1);
    }
    $scope.fieldChanged =  function(){
        $scope.isInputChange = true;
    }
    $scope.isUpdateAllowed=function(){
        $scope.consumerAlertMessage = "";
        $scope.isFirstNameValid =    Validation.isNameValid($scope.currentConsumer.firstName);
        $scope.isPhoneValid     =    Validation.isPhoneValid($scope.currentConsumer.phoneNo);
        
        if(!$scope.isInputChange ){
            $scope.consumerAlertMessage = "No change in data";
        }else if(!$scope.isFirstNameValid){
            
            $scope.consumerAlertMessage = "First name should be alfa numeric";
        }
        else if(!$scope.isPhoneValid){
            
            $scope.consumerAlertMessage = "Phone number should be of 10 digits";
        }
        else if(!angular.isUndefined($scope.currentConsumer.userCredentials))
        {
            if(!angular.isUndefined($scope.currentConsumer.userCredentials.password)){
               
                if(!($scope.currentConsumer.userCredentials.password === $scope.currentConsumer.userCredentials.repassword))
                 {   
                    $scope.currentConsumer.userCredentials.password = "";
                    $scope.currentConsumer.userCredentials.repassword="";
                    $scope.consumerAlertMessage = "Password does not match. Please re-enter the password.";
                 }
            }
        }
        
        if($scope.consumerAlertMessage===""){
            return true;
        }else{
             toaster.pop('error',"Consumer",$scope.consumerAlertMessage);
             return false;
        }
    }
    
    $scope.updateAddress=function($this){
        $scope.isNewAddress     =   "edit";
        $scope.showAddressSection = true;
        $scope.updatedAddressIndex = $this.$index;
        $scope.newAddress.consumerAddress = $this.addr.addressName;
        $scope.newAddress.consumerCityName = $this.addr.city;
        $scope.newAddress.consumerState = $this.addr.state;
        $scope.newAddress.zipCode = $this.addr.zipCode;
    }
    
    $scope.addNewAddress=function(){
        $scope.showAddressSection = true;
        $scope.isNewAddress     =   "new";
        $scope.newAddress.consumerAddress = "";
        $scope.newAddress.consumerCityName = "";
        $scope.newAddress.consumerState = "";
        $scope.newAddress.zipCode = "";
        
    }
    
    $scope.goBack = function(){
        $location.path("/consumer");
    }
    
    var resetData=function(){
        
            $scope.newAddress = {
                                consumerAddress:"",
                                consumerCityName:"",
                                consumerState:"",
                                zipCode:""

            };
        $scope.isNewAddress ="";
        $scope.updatedAddressIndex = "";
        $scope.isInputChange = false;
    }
    $scope.onClose = function(){
        console.log("closing consumer alert");
    }

});
ecomApp.controller('errorCtrl', function ($scope,$rootScope) {
	



});
var globalFun = angular.module('GlobalFun',[]);
globalFun.service("Validation",[function() {
    
    this.isNameValid= function(name)
    {
        // true  ---> if String is not empty & alfa numeric.
        name = name.trim();
        if(name.length>0) {
                var patt1 = /([^a-z0-9])/gi;   // check for alfa numeric characters
                var result = name.match(patt1);
                if(result==null) 
                {
                    return true;
                }else{ return false;}
        } else { return false;}
    }
    this.isEmailValid=function(email){
        
        var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
        if (filter.test(email)) {
            return true;
        }   
        else{
            return false;
        }
        
    }
    
    this.isPhoneValid= function(phonenum)
    {
        //return true  -- > if phone num is of 10 digit
        if(angular.isNumber(Number(phonenum))){
            if(phonenum.length==10){ 
                return true;
            }else{ return false;}
            
        }else{
            return false;
        }
    }
}]);

globalFun.service("CommonFun",[function() { 

    this.getCurrentDateTime= function()
    {
        //date format  ---> Sep 30, 2015 11:43:44 AM
        var d = new Date();
		var monthsArr = ["Jan","Feb","Mar", "Apr","May","Jun","Jul","Sep","Oct","Nov","Dec"];
		var monthIndex  = d.getMonth()-1;
		var hours = d.getHours()%12 || 12;
		var mins  = d.getMinutes();
        var secs = d.getSeconds();
		var specifier = d.getHours()<12 ? "AM" :"PM";
		var formattedDate = monthsArr[monthIndex]+" " + paddingZero(d.getDate()) + ", "+d.getFullYear() + " "
                            + paddingZero(hours) +":" + paddingZero(mins) +":" + 
                            paddingZero(secs)+ " " + specifier;
        
        return  formattedDate;
    }
    function paddingZero(txt)
    {
            txt = txt.toString();

            if (txt.length === 1)
            {
                return "0" + txt;
            }
            else
            {
                return txt;
            }
    }

}]);

ecomApp.controller('headerCtrl', function ($scope,$rootScope,$location) {

    $scope.menuOrganization =   {"menuItem":[["Organization","organization"],["Admin","user"], ["Consumers","consumer"],["Import & Export","orgimportexport"]]};
    $scope.menuOrders =         {"menuItem":[["Orders","order"],["Import & Export","orderimportexport" ]]};
    
    $scope.menuMassDataTasks =  {"menuItem":[["Import & Export","mdtimportexport"],["Search Indexes",""],["Batch Processes",""]]};

    $scope.menuMasterCatalogs = {"menuItem":[["Products","product"],["Catalogs","catalog"],["Product Attribute Groups","attributegroup"],["Price Lists","pricing"],["Image Management",""],["Import & Export",""]]};
    /*
    $scope.menuChannels =       {"menuItem":[["Channels",""],["Channel Browser",""]], "active":false};
    $scope.menuMasterContent =  {"menuItem":[["Master Pages",""],["Master Page Variants",""],["Master Page Templates",""],
                                ["Master Component Templates",""],["Master Components",""],["Master Includes",""],["Master View Contexts",""],
                                ["Content Sharing",""], ["Content Upload",""],["Import & Export",""],["Batch Processes",""]], "active":false};
    
    $scope.menuLocalization =   {"menuItem":[["Localization Management",""],["Import & Export",""]], "active":false};
    $scope.menuServices =       {"menuItem":[], "active":false};
    $scope.menuPreferences =    {"menuItem":[["Profanity Word Definition",""],["Product History",""],["Product Locking",""],["Product Deletion",""],
                                ["Product Editing",""],["Master Page Editing Channel",""],["Content Object Locking",""]], "active":false};
   */
    
    $scope.popoverTemplate  = "myPopoverTemplate.html";
    
    function setMenuItem(){
        var defaultMenuSelection = $('button.tabMenuBtn');
        var index = 0;
        if(sessionStorage.activeMenu ==="Organization"){
            index=0;
            $scope.menuList = $scope.menuOrganization.menuItem;
            
        }else if(sessionStorage.activeMenu ==="Orders"){
            index=1;
            $scope.menuList = $scope.menuOrders.menuItem;
        }else if(sessionStorage.activeMenu ==="Mass Data Tasks"){
            index=2;
            $scope.menuList = $scope.menuMassDataTasks.menuItem;
        }else if(sessionStorage.activeMenu ==="Catalogs"){
            index=3;
            $scope.menuList = $scope.menuMasterCatalogs.menuItem;
        }else{
                index=0;
                $scope.menuList = $scope.menuOrders.menuItem;
        }
       
        var selectedButton = $(defaultMenuSelection[index]);
        selectedButton.addClass('selectedMenuBtn');
    }
    $('button.tabMenuBtn').click(function(){
            var $this = $(event.target);
            if($this.hasClass('tabMenuBtn')){
                sessionStorage.activeMenu = $this[0].innerText;
                setMenuItem();
                $this.siblings().removeClass('selectedMenuBtn');
                $this.addClass('selectedMenuBtn');
                
            }
            
    });
    
    
    $scope.logout = function(){
        sessionStorage.userName  =  null;
        $scope.loginUserName = null;
        $location.path("/");
        sessionStorage.activeMenu = "";
    }
    setMenuItem();
    
});
ecomApp.controller('homeCtrl', function ($scope, $rootScope) {
    

});
ecomApp.controller('mdtImportExportCtrl', function ($scope, $rootScope, $routeParams, $location, MDTService, toaster,$timeout) {

    $scope.createDump = function(){
        
        var promise = MDTService.createDump();
        promise.success(function (response) {
             var msg = "Dump  has created  successfully at :  " +config.dumpPath;
             toaster.pop('success',"Mass Data Task",msg);
            console.log(response.data);
        });
        promise.error(function (err) {
            toaster.pop('error', "Mass Data Task", "Service failed");
        });
        
    }
    $scope.restoreDump = function(){
        var promise = MDTService.restoreDump();
        promise.success(function (response) {
             var msg = "Dump has restored  successfully";
             toaster.pop('success',"Mass Data Task",msg);
            console.log(msg);
        });
        promise.error(function (err) {
            toaster.pop('error', "Mass Data Task", "Service failed");
        });
    }
    
});

ecomApp.controller('orderCtrl', function ($scope, $rootScope, $routeParams, $location, OrderService, toaster,Validation,CommonFun) {

    $scope.orderList = [];
    $scope.filteredList = [];
    $scope.masterSelect = false;
    $scope.isSingleDelete = true;
    $scope.isNoItemSelected = "true";
    $scope.filterCategory = [];
    $scope.currentDate = CommonFun.getCurrentDateTime();
    
    $scope.getOrderList = function () {
        var promise = OrderService.getOrderList();
        promise.success(function (response) {
            $scope.orderList = JSON.parse(response.data);
             $scope.filterStatus();
            console.log("Count of  Order : " + $scope.orderList.length);
        });
        promise.error(function (err) {
            toaster.pop('error', " Order", "Service failed");
        });
    }
    $scope.showOrderDetail = function($this){
          $location.path("/order/details/"+$this._id);  
    }
    $scope.filterStatus = function(){
        if($scope.filterCategory.length==0){
            resetCategory();
        }
        $scope.filteredList = [];
        angular.forEach($scope.orderList, function (item) {
            if($scope.filterCategory.indexOf(item.status)!=-1){
                $scope.filteredList.push(item);
            }
        });
    }
    $scope.getOrderDetail = function(){
        
            var promise = OrderService.findOrder($routeParams.id);
            promise.success(function (response) {
                $scope.currentOrder = JSON.parse(response.data);
                console.log("Order detail : " + JSON.stringify($scope.currentOrder));
            });
            promise.error(function (err) {
                    toaster.pop('error',"Order","Service failed");
            });
    }
    
    $scope.goBack = function () {
        $location.path("/order");
    }
    
    $scope.selectAllOrderStatus = function ($this) {
        $scope.filterCategory = [];
        var allCheckbox = $(":checkbox:not(:first)");
        
        angular.forEach(allCheckbox, function (item) {
            item.checked = $scope.masterSelect;
            if($scope.masterSelect){
                $scope.filterCategory.push(item.value);
            }
        });
        $scope.filterStatus();
        
    }
    $scope.selectSingleOrderStatus = function ($this) {
        $scope.filterCategory = [];
        var allCheckbox = $(":checkbox:not(:first)"); //select all checkbox except first
        var isAllItemChecked = true;
        angular.forEach(allCheckbox, function (item) {
            if(!item.checked){
                isAllItemChecked = false;
            }else{
                $scope.filterCategory.push(item.value);
            }
            
        });
        $scope.masterSelect = isAllItemChecked;
        
        
        $scope.filterStatus();
    }
    $scope.uncheckMasterSelection = function ($this) {
        var noneSelected = true;
        angular.forEach($scope.orderList, function (item) {
            if (item.Selected) {
                noneSelected = false;
            } else {
                $scope.masterSelect = false;
            }
        });
        $scope.isNoItemSelected = noneSelected;
    }
    var resetCategory = function(){
        $scope.filterCategory = ["NEW","In Progress","Canceled","Cancelled and Exported","Not Deliverable","Delivered","Returned","Pending"];
    }

});
ecomApp.controller('orderImportExportCtrl', function ($scope, $rootScope, $routeParams, $location, OrderService, toaster,$timeout) {

    $scope.downloadMessage = "";
    $scope.importFileNames  = [
                                    "Order1.csv", 
                                    "Order2.csv",
                                    "Order3.csv"];
    $scope.downloadableFileName  = [];
    
    $scope.init =  function(){
        var promise = OrderService.getExportOrderList();
        promise.success(function (response) {
            $scope.downloadableFileName  = response;
        });
        promise.error(function (err) {
            toaster.pop('error', "Order", "Service failed");
        });
    }
    
    $scope.fileNameChanged =  function($this){
        
        $("#txtInputFile").val($this.value);
        
    }
    $scope.uploadFile = function(fileList){
        
        $scope.importFileNames = [];
        angular.forEach(fileList, function (file) {
            if(file.type == "text/csv"){
                var fname = file.name;
                if(fname.indexOf("Order")!=-1){
                    $scope.importFileNames.push(fname);
                }
                
            }
        });
    }
    $scope.goOrderImport = function(){
        $location.path("/orderimport");
    }
    $scope.goOrderExport = function(){
        $location.path("/orderexport");
    }
    $scope.goBack = function(){
        $location.path("/orderimportexport");
    }
    
    $scope.download = function($this){
        $scope.downloadMessage = "";
        
        var downloadUrl = config.downloadExportPath + $this.item;
        var downloadwindow = window.open(downloadUrl);
        //downloadwindow.close();
    }
    
});

ecomApp.controller('organizationCtrl', function ($scope, $rootScope, $routeParams, $location, OrganizationService, toaster) {

    $scope.organizationList = {};
    $scope.orgID = "";
    $scope.orgName = "";
    $scope.orgDescription = "";
    $scope.dbOrgID = "";
    $scope.masterSelect = false;
    $scope.orgIDErrorMessage = "";
    $scope.isUpdate = false;
    $scope.orgModalTitle = "";
    $scope.isSingleDelete = true;
    $scope.deletedItem = {};
    $scope.orgDeleteMessage = "";
    $scope.isNoItemSelected = "true";
    $scope.isOrgIDAvailable = true;

    $scope.init = function () {
        var promise = OrganizationService.findOrganization($routeParams.id);
        promise.success(function (response) {
            var org = JSON.parse(response.data);
            $scope.dbOrgID = org._id;
            $scope.orgID = org.organizationId;
            $scope.orgName = org.organizationName;
            $scope.orgDescription = org.description;
        });
        promise.error(function (err) {
            toaster.pop('error', "Organization", "Service failed");
        });
    }
    $scope.getOrganizationList = function () {
        var promise = OrganizationService.getOrganizationList();
        promise.success(function (response) {
            $scope.organizationList = JSON.parse(response.data);
            console.log("Count of Organization : " + $scope.organizationList.length);
        });
        promise.error(function (err) {
            toaster.pop('error', "Organization", "Service failed");
        });
    }


    $scope.addOrganizationData = function () {
        $location.path("/organization/new");
    }
    $scope.addOrganization = function () {
        $scope.orgID = $scope.orgID.trim();
        $scope.isOrgIDAvailable = true;

        if (isIDValid($scope.orgID)) {
            $scope.isOrgIDAvailable = true;
            var dataObj = {
                "organizationId": $scope.orgID,
                "organizationName": $scope.orgName,
                "description": $scope.orgDescription
            };
            var promise = OrganizationService.addOrganization(dataObj);
            promise.success(function (response) {
                var msg = $scope.orgName + " organization has added successfully ";
                toaster.pop('success', "Organization", msg);
                resetOrganizationField();
            });
            promise.error(function (err) {
                toaster.pop('error', "Organization", "Service failed");
            });
        } else {
            $scope.orgID = "";
            $scope.isOrgIDAvailable = false;
            $scope.orgIDErrorMessage = "Organization ID must be alfa-numeric.";
        }
    }
    $scope.updateOrganization = function ($this) {
        $scope.orgID = $scope.orgID.trim();
        $scope.isOrgIDAvailable = true;

        if (isIDValid($scope.orgID)) {
            $scope.isOrgIDAvailable = true;

            var dataObj = {
                "_id": $scope.dbOrgID,
                "organizationId": $scope.orgID,
                "organizationName": $scope.orgName,
                "description": $scope.orgDescription
            };

            var promise = OrganizationService.updateOrganization(dataObj);
            promise.success(function (response) {
                var msg = $scope.orgName + " organization has updated successfully ";
                toaster.pop('success', "Organization", msg);
                //resetOrganizationField();
            });
            promise.error(function (err) {
                toaster.pop('error', "Organization", "Service failed");
                resetOrganizationField();
            });

        } else {
            $scope.orgID = "";
            $scope.isOrgIDAvailable = false;
            $scope.orgIDErrorMessage = "Organization ID must be alfa-numeric.";
        }

    }
    $scope.updateOrganizationData = function ($this) {
        $location.path("/organization/details/" + $this._id);
    }

    $scope.uncheckMasterSelection = function ($this) {
        var noneSelected = true;
        angular.forEach($scope.organizationList, function (item) {
            if (item.Selected) {
                noneSelected = false;
            } else {
                $scope.masterSelect = false;
            }
        });
        $scope.isNoItemSelected = noneSelected;
    }

    $scope.selectAllOrganization = function ($this) {

        angular.forEach($scope.organizationList, function (item) {
            item.Selected = $scope.masterSelect;
        });
        $scope.isNoItemSelected = !($scope.masterSelect);
    }
    $scope.prepareSingleDelete = function ($this) {
        $scope.isSingleDelete = true;

        $scope.deletedItem = $this;
        showDeleteConfirmationAlert("Are you sure want to delete this Organization record?");
    }
    $scope.prepareMultiDelete = function () {
        $scope.isSingleDelete = false;

        showDeleteConfirmationAlert("Are you sure want to delete all selected Organization record?");
    }
    var showDeleteConfirmationAlert = function (msg) {
        $scope.orgDeleteMessage = msg;
        $("#orgDeleteAlert").modal('show');

    }
    $scope.delete=function()
    {
        
        if($scope.isSingleDelete){
            $scope.deleteSingle();
        }else{
            
            $scope.deleteMulti();
        }
    }
    $scope.deleteSingle = function () {
        var dbOrgID = $scope.deletedItem._id;
        var tempOrgName = $scope.deletedItem.organizationName;

        var promise = OrganizationService.deleteOrganization(dbOrgID);
        promise.success(function (response) {
            var msg = tempOrgName + " organization has deleted  successfully."
            toaster.pop('success',"Organization",msg);
            resetOrganizationField();
            $scope.getOrganizationList();

        });
        promise.error(function (err) {
            toaster.pop('error', "Organization", "Service failed");
        });
    }
    $scope.deleteMulti = function () {
        //iteration of each item in organization list
        angular.forEach($scope.organizationList, function (item) {
            if (item.Selected) {
                console.log("deleted item id : " + item._id);
                $scope.deletedItem = item;
                $scope.deleteSingle();
            }
        });

        resetOrganizationField();
    }
    $scope.goBack = function () {
        $location.path("/organization");
    }
    var isIDValid = function (tempID) {
        // true  ---> if String is not empty.
        //true   ---> if String is alfa numeric.
        tempID = tempID.trim();
        if (tempID.length > 0) {
            var patt1 = /([^a-z0-9])/gi; // check for alfa numeric characters
            var result = tempID.match(patt1);
            if (result == null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    var resetOrganizationField = function () {
        $scope.dbOrgID = "";
        $scope.orgID = "";
        $scope.orgName = "";
        $scope.orgDescription = "";
        $scope.masterSelect = false;
    }

});
ecomApp.controller('organizationImportExportCtrl', function ($scope, $rootScope, $routeParams, $location, OrganizationService, toaster,$timeout) {

    $scope.downloadMessage = "";
    $scope.importFileNames  = [
                                    "Organization1.csv", 
                                    "Organization2.csv",
                                    "Organization3.csv"];
    $scope.downloadableFileName  = []; 

    
    $scope.init =  function(){
        var promise = OrganizationService.getExportOrganizationList();
        promise.success(function (response) {
            $scope.downloadableFileName  = response;
        });
        promise.error(function (err) {
            toaster.pop('error', "Organization", "Service failed");
        });
    }
    
    $scope.fileNameChanged =  function($this){
        //$scope.selectedFile = $("#txtInputFile");
        $("#txtInputFile").val($this.value);
    }
    
    $scope.uploadFile = function($this){
        
        if($scope.selectedFile.type == "text/csv"){
            var promise = OrganizationService.fileUpload($scope.selectedFile);
            promise.success(function (response) {
                $scope.selectedFile=null;
                var msg = $scope.selectedFile.name + " has uploaded successfully";
                toaster.pop('success', "Organization", msg);
            });
            promise.error(function (err) {
                toaster.pop('error', "Organization", "Service failed");
            });
        }else{
                toaster.pop('error', "Organization", "Select valid file");
        }
        
        
        
        /*$scope.importFileNames = [];
        angular.forEach(fileList, function (file) {
            if(file.type == "text/csv"){
                var fname = file.name;
                if(fname.indexOf("Organization")!=-1){
                    $scope.importFileNames.push(fname);
                }
                
            }
        });*/
    }
    $scope.goOrgImport = function(){
        $location.path("/orgimport");
    }
    $scope.goOrgExport = function(){
        $location.path("/orgexport");
    }
    $scope.goBack = function(){
        $location.path("/orgimportexport");
    }
    
    $scope.download = function($this){
        $scope.downloadMessage = "";
        
        var downloadUrl = config.downloadExportPath + $this.item;
        var downloadwindow = window.open(downloadUrl);
        //downloadwindow.close();
    }
    
});

ecomApp.controller('pricingCtrl', function ($scope, $rootScope, $routeParams, $location, PricingService, toaster, Validation) {

    $scope.pricingList = {};
    $scope.newPricing =  {  
                           "pricingId":"",
                           "pricingPurposeType":"PURCHASE",
                           "pricingType":"LIST_PRICE",
                           "money":{  
                              "locale":"",
                              "amount":0,
                              "currency":{  
                                 "currencyCode":""
                              }
                           },
                           "pricingFromDate":"",
                           "pricingToDate":""
                        };
    $scope.masterSelect = false;
    $scope.pricingIDErrorMessage = "";
    $scope.isSingleDelete = true;
    $scope.deletedItem = {};
    $scope.pricingDeleteMessage = "";
    $scope.isNoItemSelected = "true";
    $scope.isPricingIDAvailable = true;
    $scope.status = { opened: false, openedFrom:false  };//Calendar variable
    $scope.init = function () {
        var promise = PricingService.findPricing($routeParams.id);
        promise.success(function (response) {
            $scope.newPricing = JSON.parse(response.data);
        });
        promise.error(function (err) {
            toaster.pop('error', "Pricing", "Service failed");
        });
    }
    $scope.getPricingList = function () {
        var promise = PricingService.getPricingList();
        promise.success(function (response) {
            $scope.pricingList = JSON.parse(response.data);
            //console.log(" Pricing : " + JSON.stringify($scope.pricingList));
        });
        promise.error(function (err) {
            toaster.pop('error', "Pricing", "Service failed");
        });
    }
    
    $scope.addPricing = function () {
        $scope.newPricing.money.locale = window.navigator.language;
        
        $scope.newPricing.pricingId = $scope.newPricing.pricingId.trim();
        $scope.isPricingIDAvailable = true;

        if (Validation.isNameValid($scope.newPricing.pricingId)) {
            $scope.isPricingIDAvailable = true;
            
            var promise = PricingService.addPricing($scope.newPricing);
            promise.success(function (response) {
                var msg = $scope.newPricing.pricingId + "  has created successfully ";
                toaster.pop('success', "Pricing", msg);
                resetPricingField();
            });
            promise.error(function (err) {
                toaster.pop('error', "Pricing", "Service failed");
            });
        } else {
            $scope.newPricing.pricingId = "";
            $scope.isPricingIDAvailable = false;
            $scope.pricingIDErrorMessage = "Pricing ID must be alfa-numeric.";
        }
    }
    $scope.updatePricing = function ($this) {
        $scope.newPricing.pricingId = $scope.newPricing.pricingId.trim();
        $scope.isPricingIDAvailable = true;

        if (Validation.isNameValid($scope.newPricing.pricingId)) {
            $scope.isPricingIDAvailable = true;

            var promise = PricingService.updatePricing($scope.newPricing);
            promise.success(function (response) {
                var msg = $scope.newPricing.pricingId + " pricing has updated successfully ";
                toaster.pop('success', "Pricing", msg);
            });
            promise.error(function (err) {
                toaster.pop('error', "Pricing", "Service failed");
                resetPricingField();
            });

        } else {
            $scope.newPricing.pricingId = "";
            $scope.isPricingIDAvailable = false;
            $scope.pricingIDErrorMessage = "Pricing ID must be alfa-numeric.";
        }

    }
    $scope.updatePricingData = function ($this) {
        $location.path("/pricing/details/" + $this._id);
    }

    $scope.uncheckMasterSelection = function ($this) {
        var noneSelected = true;
        angular.forEach($scope.pricingList, function (item) {
            if (item.Selected) {
                noneSelected = false;
            } else {
                $scope.masterSelect = false;
            }
        });
        $scope.isNoItemSelected = noneSelected;
    }

    $scope.selectAllPricing = function ($this) {

        angular.forEach($scope.pricingList, function (item) {
            item.Selected = $scope.masterSelect;
        });
        $scope.isNoItemSelected = !($scope.masterSelect);
    }
    $scope.prepareSingleDelete = function ($this) {
        $scope.isSingleDelete = true;

        $scope.deletedItem = $this;
        showDeleteConfirmationAlert("Are you sure want to delete this Pricing record?");
    }
    $scope.prepareMultiDelete = function () {
        $scope.isSingleDelete = false;

        showDeleteConfirmationAlert("Are you sure want to delete all selected Pricing record?");
    }
    var showDeleteConfirmationAlert = function (msg) {
        $scope.pricingDeleteMessage = msg;
        $("#pricingDeleteAlert").modal('show');

    }
    $scope.delete=function()    {
        
        if($scope.isSingleDelete){
            $scope.deleteSingle();
        }else{
            
            $scope.deleteMulti();
        }
    }
    $scope.deleteSingle = function () {
        var dbPricingID = $scope.deletedItem._id;
        var tempPricingName = $scope.deletedItem.pricingId;

        var promise = PricingService.deletePricing(dbPricingID);
        promise.success(function (response) {
            var msg = tempPricingName + " pricing has deleted  successfully."
            toaster.pop('success',"Pricing",msg);
            resetPricingField();
            $scope.getPricingList();

        });
        promise.error(function (err) {
            toaster.pop('error', "Pricing", "Service failed");
        });
    }
    $scope.deleteMulti = function () {
        //iteration of each item in pricing list
        angular.forEach($scope.pricingList, function (item) {
            if (item.Selected) {
                console.log("deleted item id : " + item._id);
                $scope.deletedItem = item;
                $scope.deleteSingle();
            }
        });

        resetPricingField();
    }
    $scope.goBack = function () {
        $location.path("/pricing");
    }
    
    var resetPricingField = function () {
        $scope.masterSelect = false;
        $scope.newPricing =  {  
                           "pricingId":"",
                           "pricingPurposeType":"PURCHASE",
                           "pricingType":"LIST_PRICE",
                           "money":{  
                              "locale":"",
                              "amount":0,
                              "currency":{  
                                 "currencyCode":""
                              }
                           },
                           "pricingFromDate":"",
                           "pricingToDate":""
                        };
        
    }
   
    //Calendar function
    $scope.openFromDate = function($event) 
    {
        $scope.status.openedFrom = true;
        
    };
    $scope.open = function($event) 
    {
        $scope.status.opened = true;
        $scope.toggleMin();
    };
    $scope.toggleMin = function() 
    {
        var day = $scope.newPricing.pricingFromDate.getDate();
        var month = $scope.newPricing.pricingFromDate.getMonth();
        var year = $scope.newPricing.pricingFromDate.getFullYear();
        $scope.minDate = new Date(year, month, day);
        
    };
    
    

});
ecomApp.controller('productCtrl', function ($scope, $rootScope, $window, $location,ProductService,toaster) {
    
    
    $scope.isNoItemSelected = true;
    $scope.getProductList=function()
    {
            var promise = ProductService.getProductList();
            promise.success(function (response) {
                $scope.productList = JSON.parse(response.data);
                console.log("Count of Products : " + $scope.productList.length);
            });
            promise.error(function (err) {
                toaster.pop('error',"Product","Service Failed");
            });
    }
    
    $scope.initProductData =  function(){
        $scope.productList = {};
        $scope.resetProductField();
        $scope.productIDErrorMessage="";
        $scope.isUpdate   =  false;
        $scope.isSingleDelete = true;
        $scope.deletedItem = {};
        $scope.productDeleteMessage = "";
        $scope.productModalTitle = "Add Product";
    }

    $scope.addProduct=function()
    {
            $scope.productModalTitle = "Add Product";
            $scope.isUpdate = false;
            $scope.productID =  $scope.productID.trim();      
            $scope.isProductIDAvailable =  true;
            
            if(isIDValid($scope.productID)){
                    $scope.isProductIDAvailable =  true;
                    var dataObj = {"productId":$scope.productID,"name":$scope.productName,"shortDescr":$scope.shortDescription,
                          "longDescr":$scope.longDescription, "manufactureName":$scope.manufactureName,"startDate":$scope.startDate,
                          "endDate":$scope.endDate,"visibleFlag":$scope.visibleFlag,"stockCount":$scope.stockCount};
                    dataObj.pricingList = [];
                    //cost pricing
                    $scope.costPricing = {"pricingId":"","pricingPurposeType":"PURCHASE",
                    "pricingType":"COST_PRICE","money":{"locale":"en_es","amount":"0",
                    "currency":{"currencyCode":"INR"}},"pricingFromDate":new Date(),
                    "pricingToDate":new Date()};
                    //list pricing
                    $scope.listPricing = {"pricingId":"","pricingPurposeType":"PURCHASE",
                    "pricingType":"LIST_PRICE","money":{"locale":"en_es","amount":"0",
                    "currency":{"currencyCode":"INR"}},"pricingFromDate":new Date(),
                    "pricingToDate":new Date()};
                    dataObj.pricingList.push($scope.costPricing);
                    dataObj.pricingList.push($scope.listPricing);

                    //attributes
                    dataObj.productAttributes = $scope.attributes;
                    var promise = ProductService.addProduct(dataObj);
                    promise.success(function (response) {
                        
                        $scope.productAlertMessage = $scope.productName + " product has added successfully ";
                        toaster.pop('success',"User",$scope.productAlertMessage);
                        //$("#productAlert").modal('show');
                        //TODO onclose modal function in alert
                        $window.open('product', '_self');
                    });
                    promise.error(function (err) {
                        toaster.pop('error',"Product","Service Failed");
                    });
            }else{
                    $scope.productID = "";
                    $scope.isproductIDAvailable  =  false;
                    $scope.productIDErrorMessage = "Product ID must be alfa-numeric.";
            }
    }

    $scope.updateProductData =  function($this){
        $location.path('/product/details/'+$this._id);
    }
    
    $scope.uncheckMasterSelection=function($this){
        
        var noneSelected =  true;
        angular.forEach($scope.productList, function (item) {
            if(item.Selected){noneSelected =  false;}
            else{  $scope.masterSelect = false; }
        });
        $scope.isNoItemSelected = noneSelected;
       
    }
    
    $scope.selectAllProduct=function($this){
       
        angular.forEach($scope.productList, function (item) {
            item.Selected = $scope.masterSelect;
        });
        $scope.isNoItemSelected = !($scope.masterSelect);
        
    }
    $scope.prepareSingleDelete = function($this){
        $scope.isSingleDelete = true;
        $scope.deletedItem = $this;
        showDeleteConfirmationAlert("Are you sure want to delete this Product record?");
    }
    $scope.prepareMultiDelete = function(){
        $scope.isSingleDelete = false;
        showDeleteConfirmationAlert("Are you sure want to delete all selected Product record?");
    }
    var showDeleteConfirmationAlert = function(msg){
        $scope.productDeleteMessage = msg;
        $("#productDeleteAlert").modal('show');
        
    }
    $scope.delete=function()
    {
        
        if($scope.isSingleDelete){
            $scope.deleteSingle();
        }else{
            
            $scope.deleteMulti();
        }
    }
    $scope.deleteSingle=function()
    {       
            var dbProductID =   $scope.deletedItem._id;
            var tempProductName = $scope.deletedItem.name;
        
            var promise = ProductService.deleteProduct(dbProductID);
            promise.success(function (response) {
                console.log(tempProductName + " product has deleted  successfully." );
                $scope.resetProductField();
                $scope.getProductList();
                
            });
            promise.error(function (err) {
                toaster.pop('error',"Product","Service Failed");
            });
    }
    $scope.deleteMulti = function(){
        $scope.isSingleDelete = false;
        //iteration of each item in product list
        angular.forEach($scope.productList, function (item) {
            if(item.Selected ){
                $scope.deletedItem = item;
                $scope.deleteSingle();
            }
        });
        $scope.resetProductField();
    }
    var isIDValid = function(tempID){   
        // true  ---> if String is not empty.
        //true   ---> if String is alfa numeric.
        tempID = tempID.trim();
        if(tempID.length>0) {
                var patt1 = /([^a-z0-9])/gi;   // check for alfa numeric characters
                var result = tempID.match(patt1);
                if(result==null) 
                {
                    return true;
                }else{ return false;}
        } else { return false;}
    }
    
    $scope.resetProductField = function (){
        $scope.dbProductID = "";
        $scope.productID = "";
        $scope.productName = "";
        $scope.shortDescription="";
        $scope.longDescription="";
        $scope.manufactureName = "";
        $scope.startDate = new Date();
        $scope.endDate = new Date();
        $scope.visibleFlag="";
        $scope.stockCount="";
        $scope.masterSelect=false;
        $scope.attributes = [];
    }

    $scope.createNewAttribute = function(){
        var attribute = {"name" : "", "value" : "", "attributeType" : "default"};
        $scope.attributes.push(attribute);
    }

    $scope.removeAttribute = function(index){
        
        $scope.attributes.splice(index,1);
    }
});

ecomApp.controller('updateProductCtrl', function ($scope, $rootScope, $routeParams, $window,
                 $uibModal, ProductService, CatalogService, AttributeGroupService,toaster) {
    
    var costPriceFound = false;
    var listPriceFound = false;

    $scope.init = function(){
        $scope.dbProductID = $routeParams.id;

        var promise = ProductService.findProduct($scope.dbProductID);
        promise.success(function (response) {
            console.log("Fetched product details: " + response.data);
            $scope.product = JSON.parse(response.data);
            $scope.product.startDate = new Date($scope.product.startDate);
            $scope.product.endDate = new Date($scope.product.endDate);
            $scope.resetPricing();
            $scope.initCategories();
        });
        promise.error(function (err) {
            toaster.pop('error',"Product","Service Failed");
        });
        
        
        // $scope.pricingPurposeTypes = ["PURCHASE","RECURRING_CHARGE","USAGE_CHARGE","COMPONENT_PRICE"];
        // $scope.pricingTypes = ["LIST_PRICE","DEFAULT_PRICE","COST_PRICE","MINIMUM_PRICE","MAXIMUM_PRICE",
            // "PROMO_PRICE","COMPETITIVE_PRICE","WHOLESALE_PRICE","SPECIAL_PROMO_PRICE","BOX_PRICE","MINIMUM_ORDER_PRICE"];
    }

    $scope.updateProduct = function(){
        var dataObj = $scope.product;
        var promise = ProductService.updateProduct(dataObj);
        promise.success(function (response) {
            $scope.product = JSON.parse(response.data);
            $scope.product.startDate = new Date($scope.product.startDate);
            $scope.product.endDate = new Date($scope.product.endDate);
            $scope.resetPricing();
            //console.log($scope.product.name+ " product has updated  successfully.");
            var msg = $scope.product.name+ " product has updated  successfully.";
            toaster.pop('success',"Product",msg);
            //$window.open('product','_self');   
        });
        promise.error(function (err) {
            toaster.pop('error',"Product","Service Failed");
        });
    }

    $scope.createNewAttribute = function(){
        if(!($scope.product.hasOwnProperty("productAttributes"))){
            $scope.product.productAttributes = [];
        }
        var attribute = {"name" : "", "value" : "", "attributeType" : "default"};
        $scope.product.productAttributes.push(attribute);
    }

    $scope.removeAttribute = function(index){
        console.log(index);
        $scope.product.productAttributes.splice(index,1);
    }

    $scope.resetPricing = function(){
        
        if(!($scope.product.hasOwnProperty("pricingList"))){
            $scope.product.pricingList = [];
        } else {
            
            var i;
            for(i in $scope.product.pricingList){
                if($scope.product.pricingList[i].pricingType == "COST_PRICE"){
                    $scope.costPricing = $scope.product.pricingList[i];
                    $scope.costPricing.pricingFromDate = new Date($scope.costPricing.pricingFromDate);
                    $scope.costPricing.pricingToDate = new Date($scope.costPricing.pricingToDate);
                    costPriceFound = true;
                }
                if($scope.product.pricingList[i].pricingType == "LIST_PRICE"){
                    $scope.listPricing = $scope.product.pricingList[i]
                    $scope.listPricing.pricingFromDate = new Date($scope.listPricing.pricingFromDate);
                    $scope.listPricing.pricingToDate = new Date($scope.listPricing.pricingToDate);
                    listPriceFound = true;
                }
            }
        } 
        if(!costPriceFound){
            $scope.costPricing = {"pricingId":"","pricingPurposeType":"PURCHASE",
            "pricingType":"COST_PRICE","money":{"locale":"en_es","amount":"0",
            "currency":{"currencyCode":"INR"}},"pricingFromDate":new Date(),
            "pricingToDate":new Date()};
        }
        if(!listPriceFound){
            $scope.listPricing = {"pricingId":"","pricingPurposeType":"PURCHASE",
            "pricingType":"LIST_PRICE","money":{"locale":"en_es","amount":"0",
            "currency":{"currencyCode":"INR"}},"pricingFromDate":new Date(),
            "pricingToDate":new Date()};
        }
        console.log($scope.costPricing);  
        console.log($scope.listPricing);     
    }

    $scope.addPricing = function(){
            if(!costPriceFound){
                $scope.product.pricingList.push($scope.costPricing);
            }
            if(!listPriceFound){
                $scope.product.pricingList.push($scope.listPricing);
            }
            $scope.updateProduct();
            $scope.resetPricing();
    }

    $scope.initCategories = function(){
        $scope.productCategories = {};
        $scope.availableCategories = [];
        var promise = CatalogService.getAllCatalogList();
        promise.success(function (response) {
            console.log("Received catalog list successfully.");
            $scope.availableCategories = JSON.parse(response.data);
            if($scope.product.hasOwnProperty("catalogList")){
                for(var i = $scope.availableCategories.length - 1; i >= 0; i--) {
                    var catalogDBId = $scope.availableCategories[i]._id;
                    if($scope.product.catalogList.indexOf(catalogDBId) != -1) {
                       $scope.productCategories[catalogDBId] = $scope.availableCategories[i];
                       $scope.availableCategories.splice(i, 1);
                    }
                }
            }
        });
        promise.error(function (err) {
            toaster.pop('error',"Product","Service Failed");
        });
    }

    $scope.addCategory = function(catalogDBId,name){
        if(!($scope.product.hasOwnProperty("catalogList"))){
            $scope.product.catalogList = [];
        }
        $scope.product.catalogList.push(catalogDBId);
        for(var i = $scope.availableCategories.length - 1; i >= 0; i--) {
            if(catalogDBId == $scope.availableCategories[i]._id) {
               $scope.productCategories[catalogDBId] = $scope.availableCategories[i];
               $scope.availableCategories.splice(i, 1);
               break;
            }
        }
        $scope.updateProduct();
    }

    $scope.removeCategory = function(catalogDBId){
        for(var i = $scope.product.catalogList.length - 1; i >= 0; i--) {
            if($scope.product.catalogList[i] == catalogDBId) {
               $scope.product.catalogList.splice(i, 1);
               $scope.availableCategories.push($scope.productCategories[catalogDBId]);
               delete $scope.productCategories[catalogDBId];
               break;
            }
        }
        $scope.updateProduct();
    }

    $scope.openAssignGroupModal = function(){
        $scope.unassignedAttributes = [];

        var promise = AttributeGroupService.getAttributeGroupList();
        promise.success(function (response) {
            console.log("Received attribute group list successfully");
            $scope.attributeGroups = JSON.parse(response.data);
            if($scope.attributeGroups.length>0){
                  var modalInstance = $uibModal.open({
                  animation: true,
                  templateUrl: './views/product/attribute_group.html',
                  controller: 'ModalInstanceCtrl',
                  size: 'sm',
                  resolve: {
                    attributeGroups: function () {
                      return $scope.attributeGroups;
                    }
                  }
                });

                modalInstance.result.then(function (unassignedAttributes) {
                  var x;
                  for(x in unassignedAttributes){
                        var attribute = {"name" : unassignedAttributes[x], "value" : "", "attributeType" : "default"};
                        $scope.unassignedAttributes.push(attribute);
                  }
                  console.log($scope.unassignedAttributes);
                }, function () {
                  console.log('Modal dismissed at: ' + new Date());
                });
            }else{   toaster.pop('success',"Product","No assigned attributes");     }
            

        });
        promise.error(function (err) {
            toaster.pop('error',"Product","Service Failed");
        });

    }

    $scope.updateAttributes = function(){
        if(!($scope.product.hasOwnProperty("productAttributes"))){
            $scope.product.productAttributes = [];
        }
        if($scope.unassignedAttributes && ($scope.unassignedAttributes.length>0)){
            Array.prototype.push.apply($scope.product.productAttributes, $scope.unassignedAttributes);
           
        }
         $scope.updateProduct();
    }
});

ecomApp.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, attributeGroups, AttributeGroupService,toaster) {
  
  $scope.attributeGroups = attributeGroups;
  $scope.unassignedAttributes = [];

  $scope.selectAttributeGroup = function(unassignedAttributes){
    
    $scope.unassignedAttributes = JSON.parse(unassignedAttributes);
  }

  $scope.ok = function () {
    $uibModalInstance.close($scope.unassignedAttributes);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
ecomApp.controller('userCtrl', function ($scope, $rootScope,$location,UserService,Validation,CommonFun,toaster) {
    
    $scope.userList = {};
    $scope.masterSelect=false;
    $scope.isNoItemSelected = true;
    $scope.currentItem = {
                           dbID : "",
                           firstName:"",
                           lastName:"",
                           company:"",
                           phone:"",
                           createdDate:"",
                           userid:"",
                           password:"",
                           email:"",
                           userCredentials:{}
                         };
    $scope.isUpdate = false;
    $scope.isFirstNameValid = true;
    $scope.isPhoneValid     = true;
    $scope.firstNameErrorMessage = "First name should be alfa-numeric.";
    $scope.phoneErrorMessage = "Phone num must be 10 digit number.";
    $scope.getUserList = function(){
            var promise = UserService.getUserList();
			promise.success(function (response) {
                $scope.userList = JSON.parse(response.data);
                console.log("Count of User : " + $scope.userList.length);
			});
			promise.error(function (err) {
				toaster.pop('error',"User","Service Failed");
			});
    }
    $scope.isUserDataAllowed=function(){
        $scope.userAlertMessage = "";
        $scope.isFirstNameValid =    Validation.isNameValid($scope.currentItem.firstName);
        $scope.isPhoneValid     =    Validation.isPhoneValid($scope.currentItem.phone);

        if(!$scope.isFirstNameValid){
            
            $scope.userAlertMessage = "First name should be alfa numeric";
        }
        else if(!$scope.isPhoneValid){
            
            $scope.userAlertMessage = "Phone number should be of 10 digits";
        }
        else if(!(Validation.isEmailValid($scope.currentItem.email))){
            
            $scope.userAlertMessage = "Please enter valid email.";
        }
        
        if($scope.userAlertMessage===""){
            return true;
        }else{
            toaster.pop('error',"User",$scope.userAlertMessage);
            return false;
        }
    }
    $scope.selectAllUser=function($this){
        angular.forEach($scope.userList, function (item) {
            item.Selected = $scope.masterSelect;
        });
        $scope.isNoItemSelected = !$scope.masterSelect;
    }
    $scope.uncheckMasterSelection=function($this){
        var noneSelected =  true;
        angular.forEach($scope.userList, function (item) {
            if(item.Selected){noneSelected =  false;}
            else{  $scope.masterSelect = false; }
        });
        $scope.isNoItemSelected = noneSelected;
        
    }
    $scope.addUserData =  function(){
        $location.path('/user/new');
    }
    
    $scope.addUser=function()
    {    
        if( $scope.isUserDataAllowed())
        {
            
            var dataObj =  {"userCredentials" : { "userName" : $scope.currentItem.userid, 
                                                 "password" : $scope.currentItem.password, 
                                                 "email" : $scope.currentItem.email,
                                                 "resetPassword" : false
                                                },
                            "roles" : ["565c2df67e399fc549a91683", "565c2e677e399fc549a91685"],
                            "firstName":$scope.currentItem.firstName,
                            "lastName":$scope.currentItem.lastName,
                            "company":$scope.currentItem.company,
                            "userType":"BackOfficeUser",
                            "phoneNo":$scope.currentItem.phone,
                            "createdDate":CommonFun.getCurrentDateTime()};

            var promise = UserService.addUser(dataObj);
            promise.success(function (response) {
                $scope.userAlertMessage = $scope.currentItem.firstName + " record has created successfully ";
                $("#userModal").modal('hide');
                toaster.pop('success',"User",$scope.userAlertMessage);
                resetUserField();
                $scope.getUserList();

            });
            promise.error(function (err) {
                    toaster.pop('error',"User","Service Failed");
            });
        }
    }
    
    
    $scope.updateUserData =  function($this){
        $location.path("/user/details/"+$this._id);
    }
    $scope.prepareSingleDelete = function($this){
        $scope.isSingleDelete = true;
        $scope.deletedItem = $this;
        $this.selected = true;
        showDeleteConfirmationAlert("Are you sure want to delete this User record?");
    }
    $scope.prepareMultiDelete = function(){
        $scope.isSingleDelete = false;
        showDeleteConfirmationAlert("Are you sure want to delete all selected User record?");
    }
    var showDeleteConfirmationAlert = function(msg){
        $scope.userDeleteMessage = msg;
        $("#userDeleteAlert").modal('show');
    }
    $scope.delete=function()
    {
        
        if($scope.isSingleDelete){
            $scope.deleteSingle();
        }else{
            
            $scope.deleteMulti();
        }
    }
    $scope.deleteSingle=function()
    {       
            var dbOrgID =   $scope.deletedItem._id;
            var tempFirstName = $scope.deletedItem.firstName;
        
            var promise = UserService.deleteUser(dbOrgID);
			promise.success(function (response) {
                var msg = tempFirstName + " user has deleted  successfully.";
                toaster.pop('success',"User",msg);
                resetUserField();
                $scope.getUserList();
                
			});
			promise.error(function (err) {
				toaster.pop('error',"User","Service Failed");
			});
    }
    $scope.deleteMulti=function(){
        //iteration of each item in organization list
        angular.forEach($scope.userList, function (item) {
            if(item.Selected ){
                console.log("deleted user id : " + item._id);
                $scope.deletedItem = item;
                $scope.deleteSingle();
            }
        });
    }
    
     $scope.goBack = function () {
        $location.path("/user");
    }
    
    var resetUserField = function(){
        $scope.currentItem = {
                               dbID : "",
                               firstName:"",
                               lastName:"",
                               company:"",
                               phone:"",
                               createdDate:"",
                               userid:"",
                               password:"",
                               email:"",
                               userCredentials:{}
                           };
        
    }
    
    
    
});
ecomApp.controller('updateUserCtrl', function ($scope, $rootScope,$routeParams,$location, UserService,Validation,CommonFun,toaster) { 
    
    $scope.isFirstNameValid = true;
    $scope.isNewAddress     =   "";
    $scope.isPhoneValid     = true;
    $scope.newAddress = {
                            userAddress:"",
                            userCityName:"",
                            userState:"",
                            zipCode:""
    };
    
    $scope.init=function(){
            $scope.currentUser1 = {};
            var promise = UserService.findUser($routeParams.id);
            promise.success(function (response) {
                var user = JSON.parse(response.data);
                var address = [];
                if(user.shippingAddress){
                    address = user.shippingAddress;
                }
                $scope.currentUser =  
                                    {"dbID" : user._id, 
                                     "firstName":user.firstName, 
                                    "lastName":user.lastName,"company":user.company,
                                    "userType":user.userType, 
                                    "phoneNo":user.phoneNo,
                                    "createdDate":user.createdDate,
                                    "userCredentials" : user.userCredentials, 
                                    "shippingAddress":address,
                                    "billingAddress":address
                                   };
                $scope.currentUser.userCredentials.repassword = $scope.currentUser.userCredentials.password
            });
            promise.error(function (err) {
                    console.log("Service failed.");
            });
    }
    
    $scope.updateUser=function()
    {       
        
        if($scope.isUpdateAllowed())
        {
            if(($scope.newAddress.userAddress.length > 0) ||       
                ($scope.newAddress.userCityName.length > 0))
            {
                   var address = { "addressName" : $scope.newAddress.userAddress, 
                                  "city" : $scope.newAddress.userCityName,
                                  "state": $scope.newAddress.userState,
                                  "zipCode":$scope.newAddress.zipCode,
                                  "createdDate" : CommonFun.getCurrentDateTime(), 
                                  "addressType" : "Billing_And_Shipping" };
                   
                   if($scope.isNewAddress ==="new"){
                       $scope.currentUser.shippingAddress.push(address);
                   }else if($scope.isNewAddress ==="edit"){
                       var index = $scope.updatedAddressIndex;
                       $scope.currentUser.shippingAddress.splice(index,1,address);
                   }
            }
            
            var dataObj =  {"_id" : $scope.currentUser.dbID, "firstName":$scope.currentUser.firstName, 
                            "lastName":$scope.currentUser.lastName,"company":$scope.currentUser.company,
                            "userType":$scope.currentUser.userType, 
                            "phoneNo":$scope.currentUser.phoneNo,
                            "createdDate":CommonFun.getCurrentDateTime(),
                            "userCredentials" : $scope.currentUser.userCredentials, 
                            "shippingAddress":$scope.currentUser.shippingAddress,
                            "billingAddress":$scope.currentUser.billingAddress
                           };

            var promise = UserService.updateUser(dataObj);
            promise.success(function (response) {
                console.log($scope.currentUser.firstName + " record has updated successfully " );
                var msg = $scope.currentUser.firstName + " record has updated successfully ";
                toaster.pop('success',"User",msg);
                //resetData();

            });
            promise.error(function (err) {
                    toaster.pop('error',"User","Service Failed");
            });
        }
    }
    $scope.isUpdateAllowed=function(){
        $scope.userAlertMessage = "";
        $scope.isFirstNameValid =    Validation.isNameValid($scope.currentUser.firstName);
        $scope.isPhoneValid     =    Validation.isPhoneValid($scope.currentUser.phoneNo);
        
        if(!$scope.isInputChange ){
            $scope.userAlertMessage = "No change in data";
        }else if(!$scope.isFirstNameValid){
            
            $scope.userAlertMessage = "First name should be alfa numeric";
        }
        else if(!$scope.isPhoneValid){
            
            $scope.userAlertMessage = "Phone number should be of 10 digits";
        }
        else if(!angular.isUndefined($scope.currentUser.userCredentials))
        {
            if(!angular.isUndefined($scope.currentUser.userCredentials.password)){
               
                if(!($scope.currentUser.userCredentials.password === $scope.currentUser.userCredentials.repassword))
                 {   
                    $scope.currentUser.userCredentials.password = "";
                    $scope.currentUser.userCredentials.repassword="";
                    $scope.userAlertMessage = "Password does not match. Please re-enter the password.";
                 }
            }
        }
        
        if($scope.userAlertMessage===""){
            return true;
        }else{
            toaster.pop('error',"User", $scope.userAlertMessage);
            return false;
        }
    }
    $scope.prepareDeleteAddress=function($this){
        $scope.deletedIndex = $this.$index;
        $scope.isSingleDelete = true;
        $scope.userDeleteMessage = "Are you sure want to delete address?";
        $("#userDeleteAlert").modal('show');
        
    }
    $scope.deleteSingle=function(){
        
        $scope.currentUser.shippingAddress.splice($scope.deletedIndex,1);
    }
    $scope.fieldChanged =  function(){
        $scope.isInputChange = true;
    }
    
    $scope.updateAddress=function($this){
        $scope.isNewAddress     =   "edit";
        $scope.showAddressSection = true;
        $scope.updatedAddressIndex = $this.$index;
        $scope.newAddress.userAddress = $this.addr.addressName;
        $scope.newAddress.userCityName = $this.addr.city;
        $scope.newAddress.userState = $this.addr.state;
        $scope.newAddress.zipCode = $this.addr.zipCode;
    }
    
    $scope.addNewAddress=function(){
        $scope.showAddressSection = true;
        $scope.isNewAddress     =   "new";
        $scope.newAddress.userAddress = "";
        $scope.newAddress.userCityName = "";
        $scope.newAddress.userState = "";
        $scope.newAddress.zipCode = "";
        
    }
    
    $scope.goBack = function(){
        $location.path("/user");
    }
    
    var resetData=function(){
        
            $scope.newAddress = {
                                userAddress:"",
                                userCityName:"",
                                userState:"",
                                zipCode:""

            };
        $scope.isNewAddress ="";
        $scope.updatedAddressIndex = "";
        $scope.isInputChange = false;
    }
    $scope.onClose = function(){
        $scope.goBack();
    }

});