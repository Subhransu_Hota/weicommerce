ecomApp.controller('attributeGroupCtrl', function ($scope, $rootScope, $routeParams, $location, AttributeGroupService, toaster,Validation) {

    $scope.agList = {};
    $scope.agName = "";
    $scope.dbAGID = "";
    $scope.masterSelect = false;
    $scope.isSingleDelete = true;
    
    $scope.deletedItem = {};
    $scope.agDeleteMessage = "";
    $scope.isNoItemSelected = "true";
    $scope.isAGNameValid =  true;
    

    
    $scope.getAGList = function () {
        var promise = AttributeGroupService.getAttributeGroupList();
        promise.success(function (response) {
            $scope.agList = JSON.parse(response.data);
            console.log("Count of  AttributeGroup : " + $scope.agList.length);
        });
        promise.error(function (err) {
            toaster.pop('error', " Attribute Group", "Service failed");
        });
    }
    $scope.uncheckMasterSelection = function ($this) {
        var noneSelected = true;
        angular.forEach($scope.agList, function (item) {
            if (item.Selected) {
                noneSelected = false;
            } else {
                $scope.masterSelect = false;
            }
        });
        $scope.isNoItemSelected = noneSelected;
    }

    $scope.selectAllAG = function ($this) {

        angular.forEach($scope.agList, function (item) {
            item.Selected = $scope.masterSelect;
        });
        $scope.isNoItemSelected = !($scope.masterSelect);
    }
    $scope.addAGData = function () {
        $location.path("/attributegroup/new");
    }
    $scope.updateAGData = function (item) {
        $location.path("/attributegroup/details/" + item._id);
    }

    
    
    
    
    
    $scope.addAttributeGroup = function () {
        if ( Validation.isNameValid($scope.agName )) 
        {
            var dataObj = {  "name": $scope.agName };
            var promise = AttributeGroupService.addAttributeGroup(dataObj);
            promise.success(function (response) {
                var msg = $scope.agName + " attribute group has added successfully";
                toaster.pop('success', "Attribute Group", msg);
                resetAttributeGroupField();
            });
            promise.error(function (err) {
                toaster.pop('error', "Attribute Group", "Service failed");
            });
        } else {
            $scope.isAGNameValid =  false;
            $scope.agNameErrorMessage = "Attribute Group name must be alfa-numeric.";
            
        }
    }
    
    $scope.goBack = function () {
        $location.path("/attributegroup");
    }
    var resetAttributeGroupField = function () {
        $scope.dbAGID = "";
        $scope.agName = "";
        $scope.masterSelect = false;
        $scope.isAGNameValid =  true;
    }
    
    $scope.prepareSingleDelete = function ($this) {
        $scope.isSingleDelete = true;
        $scope.deletedItem = $this;
        showDeleteConfirmationAlert("Are you sure want to delete this Attribute Group record?");
    }
    $scope.prepareMultiDelete = function () {
        $scope.isSingleDelete = false;
        showDeleteConfirmationAlert("Are you sure want to delete all selected Attribute Group record?");
    }
    $scope.delete=function()
    {   
        if($scope.isSingleDelete){
            $scope.deleteSingle();
        }else{
            
            $scope.deleteMulti();
        }
    }
    var showDeleteConfirmationAlert = function (msg) {
        $scope.agDeleteMessage = msg;
        $("#agDeleteAlert").modal('show');
    }
    
    $scope.deleteSingle = function () {
        var dbID = $scope.deletedItem._id;
        var tempName = $scope.deletedItem.name;

        var promise = AttributeGroupService.deleteAttributeGroup(dbID);
        promise.success(function (response) {
            var msg = tempName + " attribute group has deleted  successfully."
            toaster.pop('success',"Attribute Group",msg);
            resetAttributeGroupField();
            $scope.getAGList();

        });
        promise.error(function (err) {
            toaster.pop('error', "Attribute Group", "Service failed");
        });
    }
    $scope.deleteMulti = function () {
        //iteration of each item in Attribute Group list
        angular.forEach($scope.agList, function (item) {
            if (item.Selected) {
                $scope.deletedItem = item;
                $scope.deleteSingle();
            }
        });
       resetAttributeGroupField();
    }
    
   

});