ecomApp.controller('orderImportExportCtrl', function ($scope, $rootScope, $routeParams, $location, OrderService, toaster,$timeout) {

    $scope.downloadMessage = "";
    $scope.importFileNames  = [
                                    "Order1.csv", 
                                    "Order2.csv",
                                    "Order3.csv"];
    $scope.downloadableFileName  = [];
    
    $scope.init =  function(){
        var promise = OrderService.getExportOrderList();
        promise.success(function (response) {
            $scope.downloadableFileName  = response;
        });
        promise.error(function (err) {
            toaster.pop('error', "Order", "Service failed");
        });
    }
    
    $scope.fileNameChanged =  function($this){
        
        $("#txtInputFile").val($this.value);
        
    }
    $scope.uploadFile = function(fileList){
        
        $scope.importFileNames = [];
        angular.forEach(fileList, function (file) {
            if(file.type == "text/csv"){
                var fname = file.name;
                if(fname.indexOf("Order")!=-1){
                    $scope.importFileNames.push(fname);
                }
                
            }
        });
    }
    $scope.goOrderImport = function(){
        $location.path("/orderimport");
    }
    $scope.goOrderExport = function(){
        $location.path("/orderexport");
    }
    $scope.goBack = function(){
        $location.path("/orderimportexport");
    }
    
    $scope.download = function($this){
        $scope.downloadMessage = "";
        
        var downloadUrl = config.downloadExportPath + $this.item;
        var downloadwindow = window.open(downloadUrl);
        //downloadwindow.close();
    }
    
});
