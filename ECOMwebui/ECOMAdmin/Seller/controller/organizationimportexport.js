ecomApp.controller('organizationImportExportCtrl', function ($scope, $rootScope, $routeParams, $location, OrganizationService, toaster,$timeout) {

    $scope.downloadMessage = "";
    $scope.importFileNames  = [
                                    "Organization1.csv", 
                                    "Organization2.csv",
                                    "Organization3.csv"];
    $scope.downloadableFileName  = []; 

    
    $scope.init =  function(){
        var promise = OrganizationService.getExportOrganizationList();
        promise.success(function (response) {
            $scope.downloadableFileName  = response;
        });
        promise.error(function (err) {
            toaster.pop('error', "Organization", "Service failed");
        });
    }
    
    $scope.fileNameChanged =  function($this){
        //$scope.selectedFile = $("#txtInputFile");
        $("#txtInputFile").val($this.value);
    }
    
    $scope.uploadFile = function($this){
        
        if($scope.selectedFile.type == "text/csv"){
            var promise = OrganizationService.fileUpload($scope.selectedFile);
            promise.success(function (response) {
                $scope.selectedFile=null;
                var msg = $scope.selectedFile.name + " has uploaded successfully";
                toaster.pop('success', "Organization", msg);
            });
            promise.error(function (err) {
                toaster.pop('error', "Organization", "Service failed");
            });
        }else{
                toaster.pop('error', "Organization", "Select valid file");
        }
        
        
        
        /*$scope.importFileNames = [];
        angular.forEach(fileList, function (file) {
            if(file.type == "text/csv"){
                var fname = file.name;
                if(fname.indexOf("Organization")!=-1){
                    $scope.importFileNames.push(fname);
                }
                
            }
        });*/
    }
    $scope.goOrgImport = function(){
        $location.path("/orgimport");
    }
    $scope.goOrgExport = function(){
        $location.path("/orgexport");
    }
    $scope.goBack = function(){
        $location.path("/orgimportexport");
    }
    
    $scope.download = function($this){
        $scope.downloadMessage = "";
        
        var downloadUrl = config.downloadExportPath + $this.item;
        var downloadwindow = window.open(downloadUrl);
        //downloadwindow.close();
    }
    
});
