ecomApp.controller('userCtrl', function ($scope, $rootScope,$location,UserService,Validation,CommonFun,toaster) {
    
    $scope.userList = {};
    $scope.masterSelect=false;
    $scope.isNoItemSelected = true;
    $scope.currentItem = {
                           dbID : "",
                           firstName:"",
                           lastName:"",
                           company:"",
                           phone:"",
                           createdDate:"",
                           userid:"",
                           password:"",
                           email:"",
                           userCredentials:{}
                         };
    $scope.isUpdate = false;
    $scope.isFirstNameValid = true;
    $scope.isPhoneValid     = true;
    $scope.firstNameErrorMessage = "First name should be alfa-numeric.";
    $scope.phoneErrorMessage = "Phone num must be 10 digit number.";
    $scope.getUserList = function(){
            var promise = UserService.getUserList();
			promise.success(function (response) {
                $scope.userList = JSON.parse(response.data);
                console.log("Count of User : " + $scope.userList.length);
			});
			promise.error(function (err) {
				toaster.pop('error',"User","Service Failed");
			});
    }
    $scope.isUserDataAllowed=function(){
        $scope.userAlertMessage = "";
        $scope.isFirstNameValid =    Validation.isNameValid($scope.currentItem.firstName);
        $scope.isPhoneValid     =    Validation.isPhoneValid($scope.currentItem.phone);

        if(!$scope.isFirstNameValid){
            
            $scope.userAlertMessage = "First name should be alfa numeric";
        }
        else if(!$scope.isPhoneValid){
            
            $scope.userAlertMessage = "Phone number should be of 10 digits";
        }
        else if(!(Validation.isEmailValid($scope.currentItem.email))){
            
            $scope.userAlertMessage = "Please enter valid email.";
        }
        
        if($scope.userAlertMessage===""){
            return true;
        }else{
            toaster.pop('error',"User",$scope.userAlertMessage);
            return false;
        }
    }
    $scope.selectAllUser=function($this){
        angular.forEach($scope.userList, function (item) {
            item.Selected = $scope.masterSelect;
        });
        $scope.isNoItemSelected = !$scope.masterSelect;
    }
    $scope.uncheckMasterSelection=function($this){
        var noneSelected =  true;
        angular.forEach($scope.userList, function (item) {
            if(item.Selected){noneSelected =  false;}
            else{  $scope.masterSelect = false; }
        });
        $scope.isNoItemSelected = noneSelected;
        
    }
    $scope.addUserData =  function(){
        $location.path('/user/new');
    }
    
    $scope.addUser=function()
    {    
        if( $scope.isUserDataAllowed())
        {
            
            var dataObj =  {"userCredentials" : { "userName" : $scope.currentItem.userid, 
                                                 "password" : $scope.currentItem.password, 
                                                 "email" : $scope.currentItem.email,
                                                 "resetPassword" : false
                                                },
                            "roles" : ["565c2df67e399fc549a91683", "565c2e677e399fc549a91685"],
                            "firstName":$scope.currentItem.firstName,
                            "lastName":$scope.currentItem.lastName,
                            "company":$scope.currentItem.company,
                            "userType":"BackOfficeUser",
                            "phoneNo":$scope.currentItem.phone,
                            "createdDate":CommonFun.getCurrentDateTime()};

            var promise = UserService.addUser(dataObj);
            promise.success(function (response) {
                $scope.userAlertMessage = $scope.currentItem.firstName + " record has created successfully ";
                $("#userModal").modal('hide');
                toaster.pop('success',"User",$scope.userAlertMessage);
                resetUserField();
                $scope.getUserList();

            });
            promise.error(function (err) {
                    toaster.pop('error',"User","Service Failed");
            });
        }
    }
    
    
    $scope.updateUserData =  function($this){
        $location.path("/user/details/"+$this._id);
    }
    $scope.prepareSingleDelete = function($this){
        $scope.isSingleDelete = true;
        $scope.deletedItem = $this;
        $this.selected = true;
        showDeleteConfirmationAlert("Are you sure want to delete this User record?");
    }
    $scope.prepareMultiDelete = function(){
        $scope.isSingleDelete = false;
        showDeleteConfirmationAlert("Are you sure want to delete all selected User record?");
    }
    var showDeleteConfirmationAlert = function(msg){
        $scope.userDeleteMessage = msg;
        $("#userDeleteAlert").modal('show');
    }
    $scope.delete=function()
    {
        
        if($scope.isSingleDelete){
            $scope.deleteSingle();
        }else{
            
            $scope.deleteMulti();
        }
    }
    $scope.deleteSingle=function()
    {       
            var dbOrgID =   $scope.deletedItem._id;
            var tempFirstName = $scope.deletedItem.firstName;
        
            var promise = UserService.deleteUser(dbOrgID);
			promise.success(function (response) {
                var msg = tempFirstName + " user has deleted  successfully.";
                toaster.pop('success',"User",msg);
                resetUserField();
                $scope.getUserList();
                
			});
			promise.error(function (err) {
				toaster.pop('error',"User","Service Failed");
			});
    }
    $scope.deleteMulti=function(){
        //iteration of each item in organization list
        angular.forEach($scope.userList, function (item) {
            if(item.Selected ){
                console.log("deleted user id : " + item._id);
                $scope.deletedItem = item;
                $scope.deleteSingle();
            }
        });
    }
    
     $scope.goBack = function () {
        $location.path("/user");
    }
    
    var resetUserField = function(){
        $scope.currentItem = {
                               dbID : "",
                               firstName:"",
                               lastName:"",
                               company:"",
                               phone:"",
                               createdDate:"",
                               userid:"",
                               password:"",
                               email:"",
                               userCredentials:{}
                           };
        
    }
    
    
    
});