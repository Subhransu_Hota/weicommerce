ecomApp.controller('updateAGCtrl', function ($scope, $rootScope,$routeParams,$location, AttributeGroupService,Validation,CommonFun,toaster) {   
    $scope.isAGNameValid    =   true;
    $scope.isNewAddress     =   "";
    //$scope.newAttributes    =   [];
    $scope.currentAG        =   {};
    
    $scope.getAttributeGroup=function(){
            var promise = AttributeGroupService.findAttributeGroup($routeParams.id);
            promise.success(function (response) {
                var agdetail  = JSON.parse(response.data);
                var attributes = [];
                if(agdetail.attributes){
                    attributes = agdetail.attributes;
                }
                $scope.currentAG =  {   "_id" : agdetail._id, 
                                        "name":agdetail.name,
                                        "attributes":attributes
                                   };
                
            });
            promise.error(function (err) {
                    toaster.pop('error',"Product Attribute Group","Service failed");
            });
    }
    
    $scope.updateAG=function()
    {       
        
        if($scope.isUpdateAllowed())
        {
            
            if( (!(angular.isUndefined($scope.newAttributeName)))&& ($scope.newAttributeName.length > 0)) 
            {      
                   if($scope.isNewAttribute ==="new"){
                       $scope.currentAG.attributes.push($scope.newAttributeName);
                   }else if($scope.isNewAttribute ==="edit"){
                       var index = $scope.updatedAttributeIndex;
                       $scope.currentAG.attributes.splice(index,1,$scope.newAttributeName);
                   }
             }
            $scope.newAttributeName = "";
            var promise = AttributeGroupService.updateAttributeGroup($scope.currentAG);
            promise.success(function (response) {
                $scope.agAlertMessage = $scope.currentAG.name + " record has updated successfully ";
                toaster.pop('success',"Product Attribute Group",$scope.agAlertMessage);
                 //resetData();

            });
            promise.error(function (err) {
                   toaster.pop('error',"Product Attribute Group","Service failed");
            });
        }
    }
    $scope.prepareDeleteAttribute=function($this){
        $scope.deletedIndex = $this.$index;
        $scope.newAttributeName = "";
        $scope.isInputChange = true;
        $scope.agDeleteMessage = "Are you sure want to delete attribute?";
        $("#agDeleteAlert").modal('show');
    }
    
    $scope.delete=function(){
        $scope.currentAG.attributes.splice($scope.deletedIndex,1);
        $scope.newAttributeName = "";
    }
    $scope.fieldChanged =  function(){
        $scope.isInputChange = true;
    }
    $scope.isUpdateAllowed=function(){
        $scope.agAlertMessage = "";
        $scope.isAGNameValid =    Validation.isNameValid($scope.currentAG.name);
        
        if(!$scope.isInputChange ){
            $scope.agAlertMessage = "No change in data";
        }else if(!$scope.isAGNameValid){
            $scope.agNameErrorMessage = "Attribute Group name should be alfa numeric";
        }
        
        if($scope.agAlertMessage===""){
            return true;
        }else{
             toaster.pop('error',"Product Attribute Group",$scope.agAlertMessage);
             return false;
        }
    }
    
    $scope.updateAttribute=function($this){
        $scope.isNewAttribute     =   "edit";
        $scope.showAttributeSection = true;
        $scope.updatedAttributeIndex = $this.$index;
        
        $scope.newAttributeName = $this.attr;
        
    }
    
    $scope.addNewAttribute=function(){
        $scope.showAttributeSection = true;
        $scope.isNewAttribute     =   "new";
        $scope.newAttributeName = "";
    }
    
    $scope.goBack = function(){
        $location.path("/attributegroup");
    }
    
    var resetData=function(){
        $scope.isNewAttribute ="";
        $scope.updatedAttributeIndex = "";
        $scope.isInputChange = false;
    }
//    $scope.onClose = function(){
//        console.log("closing consumer alert");
//    }

});