ecomApp.controller('productCtrl', function ($scope, $rootScope, $window, $location,ProductService,toaster) {
    
    
    $scope.isNoItemSelected = true;
    $scope.getProductList=function()
    {
            var promise = ProductService.getProductList();
            promise.success(function (response) {
                $scope.productList = JSON.parse(response.data);
                console.log("Count of Products : " + $scope.productList.length);
            });
            promise.error(function (err) {
                toaster.pop('error',"Product","Service Failed");
            });
    }
    
    $scope.initProductData =  function(){
        $scope.productList = {};
        $scope.resetProductField();
        $scope.productIDErrorMessage="";
        $scope.isUpdate   =  false;
        $scope.isSingleDelete = true;
        $scope.deletedItem = {};
        $scope.productDeleteMessage = "";
        $scope.productModalTitle = "Add Product";
    }

    $scope.addProduct=function()
    {
            $scope.productModalTitle = "Add Product";
            $scope.isUpdate = false;
            $scope.productID =  $scope.productID.trim();      
            $scope.isProductIDAvailable =  true;
            
            if(isIDValid($scope.productID)){
                    $scope.isProductIDAvailable =  true;
                    var dataObj = {"productId":$scope.productID,"name":$scope.productName,"shortDescr":$scope.shortDescription,
                          "longDescr":$scope.longDescription, "manufactureName":$scope.manufactureName,"startDate":$scope.startDate,
                          "endDate":$scope.endDate,"visibleFlag":$scope.visibleFlag,"stockCount":$scope.stockCount};
                    dataObj.pricingList = [];
                    //cost pricing
                    $scope.costPricing = {"pricingId":"","pricingPurposeType":"PURCHASE",
                    "pricingType":"COST_PRICE","money":{"locale":"en_es","amount":"0",
                    "currency":{"currencyCode":"INR"}},"pricingFromDate":new Date(),
                    "pricingToDate":new Date()};
                    //list pricing
                    $scope.listPricing = {"pricingId":"","pricingPurposeType":"PURCHASE",
                    "pricingType":"LIST_PRICE","money":{"locale":"en_es","amount":"0",
                    "currency":{"currencyCode":"INR"}},"pricingFromDate":new Date(),
                    "pricingToDate":new Date()};
                    dataObj.pricingList.push($scope.costPricing);
                    dataObj.pricingList.push($scope.listPricing);

                    //attributes
                    dataObj.productAttributes = $scope.attributes;
                    var promise = ProductService.addProduct(dataObj);
                    promise.success(function (response) {
                        
                        $scope.productAlertMessage = $scope.productName + " product has added successfully ";
                        toaster.pop('success',"User",$scope.productAlertMessage);
                        //$("#productAlert").modal('show');
                        //TODO onclose modal function in alert
                        $window.open('product', '_self');
                    });
                    promise.error(function (err) {
                        toaster.pop('error',"Product","Service Failed");
                    });
            }else{
                    $scope.productID = "";
                    $scope.isproductIDAvailable  =  false;
                    $scope.productIDErrorMessage = "Product ID must be alfa-numeric.";
            }
    }

    $scope.updateProductData =  function($this){
        $location.path('/product/details/'+$this._id);
    }
    
    $scope.uncheckMasterSelection=function($this){
        
        var noneSelected =  true;
        angular.forEach($scope.productList, function (item) {
            if(item.Selected){noneSelected =  false;}
            else{  $scope.masterSelect = false; }
        });
        $scope.isNoItemSelected = noneSelected;
       
    }
    
    $scope.selectAllProduct=function($this){
       
        angular.forEach($scope.productList, function (item) {
            item.Selected = $scope.masterSelect;
        });
        $scope.isNoItemSelected = !($scope.masterSelect);
        
    }
    $scope.prepareSingleDelete = function($this){
        $scope.isSingleDelete = true;
        $scope.deletedItem = $this;
        showDeleteConfirmationAlert("Are you sure want to delete this Product record?");
    }
    $scope.prepareMultiDelete = function(){
        $scope.isSingleDelete = false;
        showDeleteConfirmationAlert("Are you sure want to delete all selected Product record?");
    }
    var showDeleteConfirmationAlert = function(msg){
        $scope.productDeleteMessage = msg;
        $("#productDeleteAlert").modal('show');
        
    }
    $scope.delete=function()
    {
        
        if($scope.isSingleDelete){
            $scope.deleteSingle();
        }else{
            
            $scope.deleteMulti();
        }
    }
    $scope.deleteSingle=function()
    {       
            var dbProductID =   $scope.deletedItem._id;
            var tempProductName = $scope.deletedItem.name;
        
            var promise = ProductService.deleteProduct(dbProductID);
            promise.success(function (response) {
                console.log(tempProductName + " product has deleted  successfully." );
                $scope.resetProductField();
                $scope.getProductList();
                
            });
            promise.error(function (err) {
                toaster.pop('error',"Product","Service Failed");
            });
    }
    $scope.deleteMulti = function(){
        $scope.isSingleDelete = false;
        //iteration of each item in product list
        angular.forEach($scope.productList, function (item) {
            if(item.Selected ){
                $scope.deletedItem = item;
                $scope.deleteSingle();
            }
        });
        $scope.resetProductField();
    }
    var isIDValid = function(tempID){   
        // true  ---> if String is not empty.
        //true   ---> if String is alfa numeric.
        tempID = tempID.trim();
        if(tempID.length>0) {
                var patt1 = /([^a-z0-9])/gi;   // check for alfa numeric characters
                var result = tempID.match(patt1);
                if(result==null) 
                {
                    return true;
                }else{ return false;}
        } else { return false;}
    }
    
    $scope.resetProductField = function (){
        $scope.dbProductID = "";
        $scope.productID = "";
        $scope.productName = "";
        $scope.shortDescription="";
        $scope.longDescription="";
        $scope.manufactureName = "";
        $scope.startDate = new Date();
        $scope.endDate = new Date();
        $scope.visibleFlag="";
        $scope.stockCount="";
        $scope.masterSelect=false;
        $scope.attributes = [];
    }

    $scope.createNewAttribute = function(){
        var attribute = {"name" : "", "value" : "", "attributeType" : "default"};
        $scope.attributes.push(attribute);
    }

    $scope.removeAttribute = function(index){
        
        $scope.attributes.splice(index,1);
    }
});

ecomApp.controller('updateProductCtrl', function ($scope, $rootScope, $routeParams, $window,
                 $uibModal, ProductService, CatalogService, AttributeGroupService,toaster) {
    
    var costPriceFound = false;
    var listPriceFound = false;

    $scope.init = function(){
        $scope.dbProductID = $routeParams.id;

        var promise = ProductService.findProduct($scope.dbProductID);
        promise.success(function (response) {
            console.log("Fetched product details: " + response.data);
            $scope.product = JSON.parse(response.data);
            $scope.product.startDate = new Date($scope.product.startDate);
            $scope.product.endDate = new Date($scope.product.endDate);
            $scope.resetPricing();
            $scope.initCategories();
        });
        promise.error(function (err) {
            toaster.pop('error',"Product","Service Failed");
        });
        
        
        // $scope.pricingPurposeTypes = ["PURCHASE","RECURRING_CHARGE","USAGE_CHARGE","COMPONENT_PRICE"];
        // $scope.pricingTypes = ["LIST_PRICE","DEFAULT_PRICE","COST_PRICE","MINIMUM_PRICE","MAXIMUM_PRICE",
            // "PROMO_PRICE","COMPETITIVE_PRICE","WHOLESALE_PRICE","SPECIAL_PROMO_PRICE","BOX_PRICE","MINIMUM_ORDER_PRICE"];
    }

    $scope.updateProduct = function(){
        var dataObj = $scope.product;
        var promise = ProductService.updateProduct(dataObj);
        promise.success(function (response) {
            $scope.product = JSON.parse(response.data);
            $scope.product.startDate = new Date($scope.product.startDate);
            $scope.product.endDate = new Date($scope.product.endDate);
            $scope.resetPricing();
            //console.log($scope.product.name+ " product has updated  successfully.");
            var msg = $scope.product.name+ " product has updated  successfully.";
            toaster.pop('success',"Product",msg);
            //$window.open('product','_self');   
        });
        promise.error(function (err) {
            toaster.pop('error',"Product","Service Failed");
        });
    }

    $scope.createNewAttribute = function(){
        if(!($scope.product.hasOwnProperty("productAttributes"))){
            $scope.product.productAttributes = [];
        }
        var attribute = {"name" : "", "value" : "", "attributeType" : "default"};
        $scope.product.productAttributes.push(attribute);
    }

    $scope.removeAttribute = function(index){
        console.log(index);
        $scope.product.productAttributes.splice(index,1);
    }

    $scope.resetPricing = function(){
        
        if(!($scope.product.hasOwnProperty("pricingList"))){
            $scope.product.pricingList = [];
        } else {
            
            var i;
            for(i in $scope.product.pricingList){
                if($scope.product.pricingList[i].pricingType == "COST_PRICE"){
                    $scope.costPricing = $scope.product.pricingList[i];
                    $scope.costPricing.pricingFromDate = new Date($scope.costPricing.pricingFromDate);
                    $scope.costPricing.pricingToDate = new Date($scope.costPricing.pricingToDate);
                    costPriceFound = true;
                }
                if($scope.product.pricingList[i].pricingType == "LIST_PRICE"){
                    $scope.listPricing = $scope.product.pricingList[i]
                    $scope.listPricing.pricingFromDate = new Date($scope.listPricing.pricingFromDate);
                    $scope.listPricing.pricingToDate = new Date($scope.listPricing.pricingToDate);
                    listPriceFound = true;
                }
            }
        } 
        if(!costPriceFound){
            $scope.costPricing = {"pricingId":"","pricingPurposeType":"PURCHASE",
            "pricingType":"COST_PRICE","money":{"locale":"en_es","amount":"0",
            "currency":{"currencyCode":"INR"}},"pricingFromDate":new Date(),
            "pricingToDate":new Date()};
        }
        if(!listPriceFound){
            $scope.listPricing = {"pricingId":"","pricingPurposeType":"PURCHASE",
            "pricingType":"LIST_PRICE","money":{"locale":"en_es","amount":"0",
            "currency":{"currencyCode":"INR"}},"pricingFromDate":new Date(),
            "pricingToDate":new Date()};
        }
        console.log($scope.costPricing);  
        console.log($scope.listPricing);     
    }

    $scope.addPricing = function(){
            if(!costPriceFound){
                $scope.product.pricingList.push($scope.costPricing);
            }
            if(!listPriceFound){
                $scope.product.pricingList.push($scope.listPricing);
            }
            $scope.updateProduct();
            $scope.resetPricing();
    }

    $scope.initCategories = function(){
        $scope.productCategories = {};
        $scope.availableCategories = [];
        var promise = CatalogService.getAllCatalogList();
        promise.success(function (response) {
            console.log("Received catalog list successfully.");
            $scope.availableCategories = JSON.parse(response.data);
            if($scope.product.hasOwnProperty("catalogList")){
                for(var i = $scope.availableCategories.length - 1; i >= 0; i--) {
                    var catalogDBId = $scope.availableCategories[i]._id;
                    if($scope.product.catalogList.indexOf(catalogDBId) != -1) {
                       $scope.productCategories[catalogDBId] = $scope.availableCategories[i];
                       $scope.availableCategories.splice(i, 1);
                    }
                }
            }
        });
        promise.error(function (err) {
            toaster.pop('error',"Product","Service Failed");
        });
    }

    $scope.addCategory = function(catalogDBId,name){
        if(!($scope.product.hasOwnProperty("catalogList"))){
            $scope.product.catalogList = [];
        }
        $scope.product.catalogList.push(catalogDBId);
        for(var i = $scope.availableCategories.length - 1; i >= 0; i--) {
            if(catalogDBId == $scope.availableCategories[i]._id) {
               $scope.productCategories[catalogDBId] = $scope.availableCategories[i];
               $scope.availableCategories.splice(i, 1);
               break;
            }
        }
        $scope.updateProduct();
    }

    $scope.removeCategory = function(catalogDBId){
        for(var i = $scope.product.catalogList.length - 1; i >= 0; i--) {
            if($scope.product.catalogList[i] == catalogDBId) {
               $scope.product.catalogList.splice(i, 1);
               $scope.availableCategories.push($scope.productCategories[catalogDBId]);
               delete $scope.productCategories[catalogDBId];
               break;
            }
        }
        $scope.updateProduct();
    }

    $scope.openAssignGroupModal = function(){
        $scope.unassignedAttributes = [];

        var promise = AttributeGroupService.getAttributeGroupList();
        promise.success(function (response) {
            console.log("Received attribute group list successfully");
            $scope.attributeGroups = JSON.parse(response.data);
            if($scope.attributeGroups.length>0){
                  var modalInstance = $uibModal.open({
                  animation: true,
                  templateUrl: './views/product/attribute_group.html',
                  controller: 'ModalInstanceCtrl',
                  size: 'sm',
                  resolve: {
                    attributeGroups: function () {
                      return $scope.attributeGroups;
                    }
                  }
                });

                modalInstance.result.then(function (unassignedAttributes) {
                  var x;
                  for(x in unassignedAttributes){
                        var attribute = {"name" : unassignedAttributes[x], "value" : "", "attributeType" : "default"};
                        $scope.unassignedAttributes.push(attribute);
                  }
                  console.log($scope.unassignedAttributes);
                }, function () {
                  console.log('Modal dismissed at: ' + new Date());
                });
            }else{   toaster.pop('success',"Product","No assigned attributes");     }
            

        });
        promise.error(function (err) {
            toaster.pop('error',"Product","Service Failed");
        });

    }

    $scope.updateAttributes = function(){
        if(!($scope.product.hasOwnProperty("productAttributes"))){
            $scope.product.productAttributes = [];
        }
        if($scope.unassignedAttributes && ($scope.unassignedAttributes.length>0)){
            Array.prototype.push.apply($scope.product.productAttributes, $scope.unassignedAttributes);
           
        }
         $scope.updateProduct();
    }
});

ecomApp.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, attributeGroups, AttributeGroupService,toaster) {
  
  $scope.attributeGroups = attributeGroups;
  $scope.unassignedAttributes = [];

  $scope.selectAttributeGroup = function(unassignedAttributes){
    
    $scope.unassignedAttributes = JSON.parse(unassignedAttributes);
  }

  $scope.ok = function () {
    $uibModalInstance.close($scope.unassignedAttributes);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});