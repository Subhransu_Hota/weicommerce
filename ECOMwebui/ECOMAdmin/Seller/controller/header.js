sellerApp.controller('headerCtrl', function ($scope, $rootScope, $location) {


    $scope.menuOrders = {
        "menuItem": [["Orders", ""], ["Import & Export", ""]]    
    };

    $scope.menuMasterCatalogs = {
        "menuItem": [["Products", ""], ["Catalogs", ""], ["Product Attribute Groups", ""], ["Price Lists", ""], ["Image Management", ""], ["Import & Export", ""]]
    };
    

    $scope.popoverTemplate = "myPopoverTemplate.html";
    
    function setMenuItem(){
        var defaultMenuSelection = $('button.tabMenuBtn');
        var index = 0;
        if(sessionStorage.activeMenu ==="Orders"){
            index=0;
            $scope.menuList = $scope.menuOrders.menuItem;
            
        }else if(sessionStorage.activeMenu ==="Catalogs"){
            index=1;
            $scope.menuList = $scope.menuMasterCatalogs.menuItem;
        }else{
                index=0;
                $scope.menuList = $scope.menuOrders.menuItem;
        }
       
        var selectedButton = $(defaultMenuSelection[index]);
        selectedButton.addClass('selectedMenuBtn');
    }
    
    $scope.logout = function () {
        sessionStorage.userName = null;
        $scope.loginUserName = null;
        var newwindow = window.open(config.devServer, "_self");
        sessionStorage.activeMenu = "";
    }
    $('button.tabMenuBtn').click(function(){
            var $this = $(event.target);
            if($this.hasClass('tabMenuBtn')){
                sessionStorage.activeMenu = $this[0].innerText;
                setMenuItem();
                $this.siblings().removeClass('selectedMenuBtn');
                $this.addClass('selectedMenuBtn');
                
            }
            
    });
    
    setMenuItem();

});