ecomApp.controller('updateConsumerCtrl', function ($scope, $rootScope,$routeParams,$location, ConsumerService,Validation,CommonFun,toaster) { 
    
    $scope.isFirstNameValid = true;
    $scope.isNewAddress     =   "";
    $scope.isPhoneValid     = true;
    $scope.newAddress = {
                            consumerAddress:"",
                            consumerCityName:"",
                            consumerState:"",
                            zipCode:""
                            
    };
    
    $scope.init=function(){
            
            var promise = ConsumerService.findUser($routeParams.id);
            promise.success(function (response) {
                var consumer = JSON.parse(response.data);
                var address = [];
                if(consumer.shippingAddress){
                    address = consumer.shippingAddress;
                }
                $scope.currentConsumer =  
                                    {"dbID" : consumer._id, 
                                     "firstName":consumer.firstName, 
                                    "lastName":consumer.lastName,
                                    "company":consumer.company,
                                    "userType":consumer.userType, 
                                    "phoneNo":consumer.phoneNo,
                                    "createdDate":consumer.createdDate,
                                     "roles":[],
                                    "userCredentials" : consumer.userCredentials, 
                                    "shippingAddress":address,
                                    "billingAddress":address
                                   }; 
                if($scope.currentConsumer.userCredentials){
                    $scope.currentConsumer.userCredentials.repassword = $scope.currentConsumer.userCredentials.password;
                }
                
            });
            promise.error(function (err) {
                    toaster.pop('error',"Consumer","Service failed");
            });
    }
    
    $scope.updateConsumer=function()
    {       
        //$scope.isFirstNameValid = Validation.isNameValid($scope.currentConsumer.firstName);
        //$scope.isPhoneValid     =    Validation.isPhoneValid($scope.currentConsumer.phoneNo);
       
        if($scope.isUpdateAllowed())
        {
            if(($scope.newAddress.consumerAddress.length > 0) ||       
                ($scope.newAddress.consumerCityName.length > 0) || 
                ($scope.newAddress.zipCode.length>0))
            {
                   var address = { "addressName" : $scope.newAddress.consumerAddress, 
                                  "city" : $scope.newAddress.consumerCityName,
                                  "state": $scope.newAddress.consumerState,
                                  "zipCode":$scope.newAddress.zipCode,
                                  "createdDate" : CommonFun.getCurrentDateTime(), 
                                  "addressType" : "Billing_And_Shipping" };
                   
                   if($scope.isNewAddress ==="new"){
                       $scope.currentConsumer.shippingAddress.push(address);
                   }else if($scope.isNewAddress ==="edit"){
                       
                       var index = $scope.updatedAddressIndex;
                       $scope.currentConsumer.shippingAddress.splice(index,1,address);
                   }
            }
            
            var dataObj =  {"_id" : $scope.currentConsumer.dbID, "firstName":$scope.currentConsumer.firstName, 
                            "lastName":$scope.currentConsumer.lastName,"company":$scope.currentConsumer.company,
                            "userType":"Customer", 
                            "phoneNo":$scope.currentConsumer.phoneNo,
                            "roles":[],
                            "createdDate":CommonFun.getCurrentDateTime(),
                            "userCredentials" : $scope.currentConsumer.userCredentials, 
                            "shippingAddress":$scope.currentConsumer.shippingAddress,
                            "billingAddress":$scope.currentConsumer.billingAddress
                           };

            var promise = ConsumerService.updateUser(dataObj);
            promise.success(function (response) {
                console.log($scope.currentConsumer.firstName + " record has updated successfully " );
                $scope.consumerAlertMessage = $scope.currentConsumer.firstName + " record has updated successfully ";
                 toaster.pop('success',"Consumer",$scope.consumerAlertMessage);
                 //resetData();

            });
            promise.error(function (err) {
                   toaster.pop('error',"Consumer","Service failed");
            });
        }
    }
    $scope.prepareDeleteAddress=function($this){
        $scope.deletedIndex = $this.$index;
        $scope.isSingleDelete = true;
        $scope.consumerDeleteMessage = "Are you sure want to delete address?";
        $("#consumerDeleteAlert").modal('show');
        
    }
    $scope.deleteSingle=function(){
        
        $scope.currentConsumer.shippingAddress.splice($scope.deletedIndex,1);
    }
    $scope.fieldChanged =  function(){
        $scope.isInputChange = true;
    }
    $scope.isUpdateAllowed=function(){
        $scope.consumerAlertMessage = "";
        $scope.isFirstNameValid =    Validation.isNameValid($scope.currentConsumer.firstName);
        $scope.isPhoneValid     =    Validation.isPhoneValid($scope.currentConsumer.phoneNo);
        
        if(!$scope.isInputChange ){
            $scope.consumerAlertMessage = "No change in data";
        }else if(!$scope.isFirstNameValid){
            
            $scope.consumerAlertMessage = "First name should be alfa numeric";
        }
        else if(!$scope.isPhoneValid){
            
            $scope.consumerAlertMessage = "Phone number should be of 10 digits";
        }
        else if(!angular.isUndefined($scope.currentConsumer.userCredentials))
        {
            if(!angular.isUndefined($scope.currentConsumer.userCredentials.password)){
               
                if(!($scope.currentConsumer.userCredentials.password === $scope.currentConsumer.userCredentials.repassword))
                 {   
                    $scope.currentConsumer.userCredentials.password = "";
                    $scope.currentConsumer.userCredentials.repassword="";
                    $scope.consumerAlertMessage = "Password does not match. Please re-enter the password.";
                 }
            }
        }
        
        if($scope.consumerAlertMessage===""){
            return true;
        }else{
             toaster.pop('error',"Consumer",$scope.consumerAlertMessage);
             return false;
        }
    }
    
    $scope.updateAddress=function($this){
        $scope.isNewAddress     =   "edit";
        $scope.showAddressSection = true;
        $scope.updatedAddressIndex = $this.$index;
        $scope.newAddress.consumerAddress = $this.addr.addressName;
        $scope.newAddress.consumerCityName = $this.addr.city;
        $scope.newAddress.consumerState = $this.addr.state;
        $scope.newAddress.zipCode = $this.addr.zipCode;
    }
    
    $scope.addNewAddress=function(){
        $scope.showAddressSection = true;
        $scope.isNewAddress     =   "new";
        $scope.newAddress.consumerAddress = "";
        $scope.newAddress.consumerCityName = "";
        $scope.newAddress.consumerState = "";
        $scope.newAddress.zipCode = "";
        
    }
    
    $scope.goBack = function(){
        $location.path("/consumer");
    }
    
    var resetData=function(){
        
            $scope.newAddress = {
                                consumerAddress:"",
                                consumerCityName:"",
                                consumerState:"",
                                zipCode:""

            };
        $scope.isNewAddress ="";
        $scope.updatedAddressIndex = "";
        $scope.isInputChange = false;
    }
    $scope.onClose = function(){
        console.log("closing consumer alert");
    }

});