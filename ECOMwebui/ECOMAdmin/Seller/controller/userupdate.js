ecomApp.controller('updateUserCtrl', function ($scope, $rootScope,$routeParams,$location, UserService,Validation,CommonFun,toaster) { 
    
    $scope.isFirstNameValid = true;
    $scope.isNewAddress     =   "";
    $scope.isPhoneValid     = true;
    $scope.newAddress = {
                            userAddress:"",
                            userCityName:"",
                            userState:"",
                            zipCode:""
    };
    
    $scope.init=function(){
            $scope.currentUser1 = {};
            var promise = UserService.findUser($routeParams.id);
            promise.success(function (response) {
                var user = JSON.parse(response.data);
                var address = [];
                if(user.shippingAddress){
                    address = user.shippingAddress;
                }
                $scope.currentUser =  
                                    {"dbID" : user._id, 
                                     "firstName":user.firstName, 
                                    "lastName":user.lastName,"company":user.company,
                                    "userType":user.userType, 
                                    "phoneNo":user.phoneNo,
                                    "createdDate":user.createdDate,
                                    "userCredentials" : user.userCredentials, 
                                    "shippingAddress":address,
                                    "billingAddress":address
                                   };
                $scope.currentUser.userCredentials.repassword = $scope.currentUser.userCredentials.password
            });
            promise.error(function (err) {
                    console.log("Service failed.");
            });
    }
    
    $scope.updateUser=function()
    {       
        
        if($scope.isUpdateAllowed())
        {
            if(($scope.newAddress.userAddress.length > 0) ||       
                ($scope.newAddress.userCityName.length > 0))
            {
                   var address = { "addressName" : $scope.newAddress.userAddress, 
                                  "city" : $scope.newAddress.userCityName,
                                  "state": $scope.newAddress.userState,
                                  "zipCode":$scope.newAddress.zipCode,
                                  "createdDate" : CommonFun.getCurrentDateTime(), 
                                  "addressType" : "Billing_And_Shipping" };
                   
                   if($scope.isNewAddress ==="new"){
                       $scope.currentUser.shippingAddress.push(address);
                   }else if($scope.isNewAddress ==="edit"){
                       var index = $scope.updatedAddressIndex;
                       $scope.currentUser.shippingAddress.splice(index,1,address);
                   }
            }
            
            var dataObj =  {"_id" : $scope.currentUser.dbID, "firstName":$scope.currentUser.firstName, 
                            "lastName":$scope.currentUser.lastName,"company":$scope.currentUser.company,
                            "userType":$scope.currentUser.userType, 
                            "phoneNo":$scope.currentUser.phoneNo,
                            "createdDate":CommonFun.getCurrentDateTime(),
                            "userCredentials" : $scope.currentUser.userCredentials, 
                            "shippingAddress":$scope.currentUser.shippingAddress,
                            "billingAddress":$scope.currentUser.billingAddress
                           };

            var promise = UserService.updateUser(dataObj);
            promise.success(function (response) {
                console.log($scope.currentUser.firstName + " record has updated successfully " );
                var msg = $scope.currentUser.firstName + " record has updated successfully ";
                toaster.pop('success',"User",msg);
                //resetData();

            });
            promise.error(function (err) {
                    toaster.pop('error',"User","Service Failed");
            });
        }
    }
    $scope.isUpdateAllowed=function(){
        $scope.userAlertMessage = "";
        $scope.isFirstNameValid =    Validation.isNameValid($scope.currentUser.firstName);
        $scope.isPhoneValid     =    Validation.isPhoneValid($scope.currentUser.phoneNo);
        
        if(!$scope.isInputChange ){
            $scope.userAlertMessage = "No change in data";
        }else if(!$scope.isFirstNameValid){
            
            $scope.userAlertMessage = "First name should be alfa numeric";
        }
        else if(!$scope.isPhoneValid){
            
            $scope.userAlertMessage = "Phone number should be of 10 digits";
        }
        else if(!angular.isUndefined($scope.currentUser.userCredentials))
        {
            if(!angular.isUndefined($scope.currentUser.userCredentials.password)){
               
                if(!($scope.currentUser.userCredentials.password === $scope.currentUser.userCredentials.repassword))
                 {   
                    $scope.currentUser.userCredentials.password = "";
                    $scope.currentUser.userCredentials.repassword="";
                    $scope.userAlertMessage = "Password does not match. Please re-enter the password.";
                 }
            }
        }
        
        if($scope.userAlertMessage===""){
            return true;
        }else{
            toaster.pop('error',"User", $scope.userAlertMessage);
            return false;
        }
    }
    $scope.prepareDeleteAddress=function($this){
        $scope.deletedIndex = $this.$index;
        $scope.isSingleDelete = true;
        $scope.userDeleteMessage = "Are you sure want to delete address?";
        $("#userDeleteAlert").modal('show');
        
    }
    $scope.deleteSingle=function(){
        
        $scope.currentUser.shippingAddress.splice($scope.deletedIndex,1);
    }
    $scope.fieldChanged =  function(){
        $scope.isInputChange = true;
    }
    
    $scope.updateAddress=function($this){
        $scope.isNewAddress     =   "edit";
        $scope.showAddressSection = true;
        $scope.updatedAddressIndex = $this.$index;
        $scope.newAddress.userAddress = $this.addr.addressName;
        $scope.newAddress.userCityName = $this.addr.city;
        $scope.newAddress.userState = $this.addr.state;
        $scope.newAddress.zipCode = $this.addr.zipCode;
    }
    
    $scope.addNewAddress=function(){
        $scope.showAddressSection = true;
        $scope.isNewAddress     =   "new";
        $scope.newAddress.userAddress = "";
        $scope.newAddress.userCityName = "";
        $scope.newAddress.userState = "";
        $scope.newAddress.zipCode = "";
        
    }
    
    $scope.goBack = function(){
        $location.path("/user");
    }
    
    var resetData=function(){
        
            $scope.newAddress = {
                                userAddress:"",
                                userCityName:"",
                                userState:"",
                                zipCode:""

            };
        $scope.isNewAddress ="";
        $scope.updatedAddressIndex = "";
        $scope.isInputChange = false;
    }
    $scope.onClose = function(){
        $scope.goBack();
    }

});