ecomApp.controller('consumerCtrl', function ($scope, $rootScope,$location, ConsumerService,Validation,CommonFun,toaster) {
    
    $scope.consumerList = {};
    $scope.masterSelect=false;
    $scope.isNoItemSelected = true;
    $scope.currentItem = {
                           dbID : "",
                           firstName:"",
                           lastName:"",
                           company:"",
                           phone:"",
                           createdDate:""
                         };
    $scope.isUpdate = false;
    $scope.isFirstNameValid =  true;
    $scope.isPhoneValid =  true;
    $scope.firstNameErrorMessage = "First name should be alfa-numeric.";
    $scope.phoneErrorMessage = "Phone num must be 10 digit number.";
    
    $scope.getConsumerList = function(){
            var promise = ConsumerService.getUserList();
			promise.success(function (response) {
                $scope.consumerList = JSON.parse(response.data);
                console.log("Count of Consumer : " + $scope.consumerList.length);
			});
			promise.error(function (err) {
				toaster.pop('error',"Consumer","Service failed");
			});
    }
    
    $scope.selectAllConsumer=function($this){
        angular.forEach($scope.consumerList, function (item) {
            item.Selected = $scope.masterSelect;
        });
        $scope.isNoItemSelected = !$scope.masterSelect;
    }
    $scope.uncheckMasterSelection=function($this){
        var noneSelected =  true;
        angular.forEach($scope.consumerList, function (item) {
            if(item.Selected){noneSelected =  false;}
            else{  $scope.masterSelect = false; }
        });
        $scope.isNoItemSelected = noneSelected;
    }
    
    $scope.addConsumerData =  function(){
       $location.path('/consumer/new');
    }
    
    $scope.addConsumer=function()
    {       
        consumerDataValidation(); 
        if($scope.isFirstNameValid && $scope.isPhoneValid)
        {
            
            var dataObj =  {"firstName":$scope.currentItem.firstName,
                            "lastName":$scope.currentItem.lastName,
                            "company":$scope.currentItem.company,
                            "userType":"Customer",
                            "roles":[],
                            "phoneNo":$scope.currentItem.phone,
                            "createdDate":CommonFun.getCurrentDateTime()};

            var promise = ConsumerService.addUser(dataObj);
            promise.success(function (response) {
                $("#consumerModal").modal('hide');
                var msg = $scope.currentItem.firstName + "  record has created successfully ";
                toaster.pop('success',"Consumer",msg);

                resetConsumerField();
                $scope.getConsumerList();

            });
            promise.error(function (err) {
                    toaster.pop('error',"Consumer","Service failed");
            });
        }
    }
    var consumerDataValidation =  function()
    {
        $scope.isFirstNameValid = Validation.isNameValid($scope.currentItem.firstName);
        $scope.isPhoneValid  =  Validation.isPhoneValid($scope.currentItem.phone);
    }
    $scope.updateConsumer=function()
    {       
        consumerDataValidation(); 
        if($scope.isFirstNameValid && $scope.isPhoneValid)
        {   
            var dataObj =  {"_id" : $scope.currentItem.dbID,
                            "firstName":$scope.currentItem.firstName,
                            "lastName":$scope.currentItem.lastName,
                            "company":$scope.currentItem.company,
                            "userType":"Customer",
                            "roles":[],
                            "phoneNo":$scope.currentItem.phone,
                            "createdDate":$scope.currentItem.createdDate};

            var promise = ConsumerService.updateUser(dataObj);
            promise.success(function (response) {
                var msg = $scope.currentItem.firstName + " record has updated successfully ";
                $("#consumerModal").modal('hide');
                toaster.pop('success',"Consumer",msg);
                resetConsumerField();
                $scope.getConsumerList();

            });
            promise.error(function (err) {
                toaster.pop('error',"Consumer","Service failed");
            });
        }
    }
    $scope.updateConsumerData =  function($this){
        $location.path("/consumer/details/"+$this._id);
    }
    $scope.prepareSingleDelete = function($this){
        $scope.isSingleDelete = true;
        $scope.deletedItem = $this;
        showDeleteConfirmationAlert("Are you sure want to delete this Consumer record?");
    }
    $scope.prepareMultiDelete = function(){
        $scope.isSingleDelete = false;
        showDeleteConfirmationAlert("Are you sure want to delete all selected Consumer record?");
    }
    var showDeleteConfirmationAlert = function(msg){
        $scope.consumerDeleteMessage = msg;
        $("#consumerDeleteAlert").modal('show');
    }
    $scope.delete=function()
    {   
        if($scope.isSingleDelete){
            $scope.deleteSingle();
        }else{
            
            $scope.deleteMulti();
        }
    }
    $scope.deleteSingle=function()
    {
            var dbOrgID =   $scope.deletedItem._id;
            var tempFirstName = $scope.deletedItem.firstName;
        
            var promise = ConsumerService.deleteUser(dbOrgID);
			promise.success(function (response) {
               
                var msg = tempFirstName + " consumer has deleted  successfully.";
                toaster.pop('success',"Consumer",msg);

                resetConsumerField();
                $scope.getConsumerList();
			});
			promise.error(function (err) {
				toaster.pop('error',"Consumer","Service failed");
			});
    }
    $scope.deleteMulti=function(){
        //iteration of each item in organization list
        angular.forEach($scope.consumerList, function (item) {
            if(item.Selected ){
                console.log("deleted consumer id : " + item._id);
                $scope.deletedItem = item;
                $scope.deleteSingle();
            }
        });
        
    }
    $scope.goBack = function () {
        $location.path("/consumer");
    }
    
    var resetConsumerField = function(){
        $scope.currentItem = {
                               dbID : "",
                               firstName:"",
                               lastName:"",
                               company:"",
                               phone:"",
                               createdDate:""
                           };
        
    }
    
});