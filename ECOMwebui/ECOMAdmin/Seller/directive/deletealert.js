sellerApp.directive('deletealert', function(){
    
    return {
                restrict:'E',
                replace:true,
                scope : {
			         title   : '@',
                     deleteMessage : '@'
                     //isSingleDelete : '='
		         },
                templateUrl:"view/deletealert.html",
                link: function(scope,elem, attrs){
    
                    //scope.deleteMulti  = function(){  console.log("delete multi");scope.$parent.deleteMulti();  }
                    scope.delete = function(){  scope.$parent.delete(); }
                }
    };
});